package test;

import model.DBConnectionConstants;
import persistence.ConnectionHandler;
import persistence.PersistenceException;

public class TestSupport {
	

	//Manual path start
	private static final String Password = "pa";
	//Manual path end

	
	private static java.sql.Connection connection;	
	
	private TestSupport(){}	
	
	public static void prepareSingletons(){
        model.dg38.reset$For$Test = true;
        model.dg39.reset$For$Test = true;
        model.dg36.reset$For$Test = true;
        model.dg34.reset$For$Test = true;
        model.dg35.reset$For$Test = true;
        model.dg32.reset$For$Test = true;
        model.dg31.reset$For$Test = true;
        model.meta.CommandCoordinator.reset$For$Test = true;
        model.PassedNotPassedGrades.reset$For$Test = true;
        model.tg40.reset$For$Test = true;
        model.tg37.reset$For$Test = true;
        model.Finished.reset$For$Test = true;
        model.dg29.reset$For$Test = true;
        model.tg33.reset$For$Test = true;
        model.tg30.reset$For$Test = true;
        model.dg28.reset$For$Test = true;
        model.dg25.reset$For$Test = true;
        model.dg26.reset$For$Test = true;
        model.dg24.reset$For$Test = true;
        model.dg21.reset$For$Test = true;
        model.dg22.reset$For$Test = true;
        model.NoStudentGroup.reset$For$Test = true;
        model.CreditPointsManager.reset$For$Test = true;
        model.Falsum.reset$For$Test = true;
        model.NoProgram_GL.reset$For$Test = true;
        model.RealGrades.reset$For$Test = true;
        model.NoProgram_PL.reset$For$Test = true;
        model.tg50.reset$For$Test = true;
        model.NotYetStudied.reset$For$Test = true;
        model.Passed.reset$For$Test = true;
        model.Verum.reset$For$Test = true;
        model.Active.reset$For$Test = true;
        model.NoProgram_SL.reset$For$Test = true;
        model.tg23.reset$For$Test = true;
        model.dg18.reset$For$Test = true;
        model.dg19.reset$For$Test = true;
        model.dg16.reset$For$Test = true;
        model.tg20.reset$For$Test = true;
        model.dg14.reset$For$Test = true;
        model.dg15.reset$For$Test = true;
        model.dg12.reset$For$Test = true;
        model.dg11.reset$For$Test = true;
        model.tg27.reset$For$Test = true;
        model.NoGrade.reset$For$Test = true;
        model.NotPassed.reset$For$Test = true;
        model.tg13.reset$For$Test = true;
        model.tg10.reset$For$Test = true;
        model.tg17.reset$For$Test = true;

	}

	public static void prepareDatabase() throws PersistenceException{
		initializeConnectionHandler();
		resetDatabase();
	}
	public static void resetDatabase() throws PersistenceException{
		ConnectionHandler.disconnect();
		disconnect();
		initializeConnectionHandler();
	}
	public static void clearDatabase() throws PersistenceException{
		resetDatabase();
	}
	private static void disconnect() {
		if (connection != null) {
			connection = null;
		}		
	}
	private static void initializeConnectionHandler() throws PersistenceException{
		ConnectionHandler connection = ConnectionHandler.getTheConnectionHandler();
		connection.connect(DBConnectionConstants.DataBaseName,
			DBConnectionConstants.SchemaName,
			DBConnectionConstants.UserName,
			Password.toCharArray(),
			true);
		ConnectionHandler.initializeMapsForMappedFields();		
	}
}
