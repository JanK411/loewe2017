package test;

import common.Fraction;
import model.AdminService;
import model.CreditPoints;
import model.meta.StringFACTORY;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import persistence.*;

import static test.utils.TestUtils.getUniqueName;

/**
 * Allgemeiner Testrahmen für Tests in Goja !
 */
public abstract class GojaTest {

    protected AdminService4Public adminService;
    protected StudentManager4Public studentManager;
    protected ProgramManager4Public programManager;

    protected CreditPoints4Public cp1;
    protected StudyProgram_PL4Public program;

    protected String drittelNoten = StringFACTORY.GradeFactory$FACTORY$RealGrades;
    protected String bestandenNichtBestanden = StringFACTORY.GradeFactory$FACTORY$PassedNotPassedGrades;
    protected static final String commentForNewGrade = "neueNoteHinzugefuegt";

    protected static final Fraction f5 = Fraction.parse("5");
    protected static final Fraction f6 = Fraction.parse("6");
    protected static final Fraction f9 = Fraction.parse("9");

    /**
     * Vorm Testen muss sich mit der Datenbank verbunden werden.
     */
    @BeforeClass
    public static void initDBandSingletons() throws Exception {
        TestSupport.prepareDatabase();
    }


    /**
     * Vor jedem Test muss ein nicht belasteter DB-, Singleton- und Cache-Zustand hergestellt werden.
     */
    @Before
    public void setUp() throws Exception {
        TestSupport.clearDatabase();
        TestSupport.prepareSingletons();
        Cache.getTheCache().reset$For$Test();
        initManagers();
        initUsefulObjekts();
        this.beforeMethod();
    }

    private void initUsefulObjekts() throws Exception {
        cp1 = CreditPoints.createCreditPoints(Fraction.One);
        program = programManager.createStudyProgram(getUniqueName());
    }

    private void initManagers() throws Exception {
        adminService = AdminService.createAdminService();
        studentManager = adminService.getStudentManager();
        programManager = adminService.getProgramManager();
    }

    @After
    public void tearDown() throws Exception {
        this.afterMethod();
    }

    protected abstract void beforeMethod() throws Exception;

    protected abstract void afterMethod() throws Exception;


}
