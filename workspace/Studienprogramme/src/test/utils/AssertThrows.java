package test.utils;

/**
 * Assert against exceptions that get thrown.
 */
public class AssertThrows {
    @SuppressWarnings("unchecked")
    public static <T extends Throwable> T assertThrows(Class<T> expectedType, Executable executable) {
        try {
            executable.execute();
        } catch (Throwable actualException) {
            if (expectedType.isInstance(actualException)) {
                return (T) actualException;
            } else {
                throw new AssertionError("Unexpected exception type thrown", actualException);
            }
        }
        throw new AssertionError(
                String.format("Expected %s to be thrown, but nothing was thrown.", expectedType.getSimpleName()));
    }
}
