package test.utils;

import model.*;
import model.visitor.ModuleOrModuleGroup_GLReturnVisitor;
import model.visitor.ModuleOrModuleGroup_SLReturnVisitor;
import persistence.*;

import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;

import static common.util.ListUtils.toList;

public class TestUtils {

    /**
     * Zähler für einzigartige Namen. Wird immer um 1 erhöht, bevor er zurückgeben wird.
     * Damit dieser bei 0 anfängt zu zählen, wird er mit -1 initalisiert.
     */
    private static BigInteger counterForUniqueNames = BigInteger.valueOf(-1);

    public static String getUniqueName() {
        increaseCounterByOne();
        return counterForUniqueNames.toString();
    }

    private static void increaseCounterByOne() {
        counterForUniqueNames = counterForUniqueNames.add(BigInteger.ONE);
    }


    public static boolean nameIsEquals(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public nameHavingThing, String name) throws PersistenceException {
        return nameHavingThing.getName().equals(name);
    }

    public static boolean nameIsEquals(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public nameHavingThing, String name) throws PersistenceException {
        return nameHavingThing.getName().equals(name);
    }

    public static boolean nameIsEqual(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public nameHavingThing, String name) throws PersistenceException {
        return nameHavingThing.getName().equals(name);
    }

    public static Student4Public createRandomStudent(StudentManager4Public studentManager, StudentGroup4Public studentGroup) throws PersistenceException, StateException {
        return studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), Date.valueOf(LocalDate.now()));
    }

    public static StudyProgram_SL4Public advanceToProgramSL(StudentManager4Public studentManager, StudyProgram_PL4Public program) throws StateException, PersistenceException {
        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());
        Student4Public student = createRandomStudent(studentManager, studentGroup);
        studentManager.addStudentsToGroup(studentGroup, toList(student.wrap()));

        studentGroup.makeActive();

        return getActiveStudyProgramFromStudent(student);
    }

    public static StudyProgram_SL4Public getActiveStudyProgramFromStudent(Student4Public student) throws PersistenceException {
        return student.getStudyPrograms().findFirst(studyProgram -> studyProgram.getActiveState().equals(Active.getTheActive()));
    }

    // TODO Methoden verschönern. Sind alle zu lang.

    public static ModuleGroup_GL4Public getModuleGroup(ModuleOrModuleGroup_GL4Public possibleGroup) throws PersistenceException {
        return possibleGroup.accept(new ModuleOrModuleGroup_GLReturnVisitor<ModuleGroup_GL4Public>() {
            @Override
            public ModuleGroup_GL4Public handleCONCModuleOrModuleGroup_GL(CONCModuleOrModuleGroup_GL4Public cONCModuleOrModuleGroup_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_GL4Public handleModuleGroup_GL(ModuleGroup_GL4Public moduleGroup_GL) throws PersistenceException {
                return moduleGroup_GL;
            }

            @Override
            public ModuleGroup_GL4Public handleCONCModule_GL(CONCModule_GL4Public cONCModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_GL4Public handleNotAtomicModule_GL(NotAtomicModule_GL4Public notAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_GL4Public handleBinaryAtomicModule_GL(BinaryAtomicModule_GL4Public binaryAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_GL4Public handleCONCAtomicModule_GL(CONCAtomicModule_GL4Public cONCAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_GL4Public handleTripleAtomicModule_GL(TripleAtomicModule_GL4Public tripleAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }
        });
    }

    public static NotAtomicModule_GL4Public getNotAtomicModule(ModuleOrModuleGroup_GL4Public possibleGroup) throws PersistenceException {
        return possibleGroup.accept(new ModuleOrModuleGroup_GLReturnVisitor<NotAtomicModule_GL4Public>() {
            @Override
            public NotAtomicModule_GL4Public handleCONCModuleOrModuleGroup_GL(CONCModuleOrModuleGroup_GL4Public cONCModuleOrModuleGroup_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_GL4Public handleModuleGroup_GL(ModuleGroup_GL4Public moduleGroup_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_GL4Public handleCONCModule_GL(CONCModule_GL4Public cONCModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_GL4Public handleNotAtomicModule_GL(NotAtomicModule_GL4Public notAtomicModule_GL) throws PersistenceException {
                return notAtomicModule_GL;
            }

            @Override
            public NotAtomicModule_GL4Public handleBinaryAtomicModule_GL(BinaryAtomicModule_GL4Public binaryAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_GL4Public handleCONCAtomicModule_GL(CONCAtomicModule_GL4Public cONCAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_GL4Public handleTripleAtomicModule_GL(TripleAtomicModule_GL4Public tripleAtomicModule_GL) throws PersistenceException {
                throw new Error("");
            }
        });
    }

    public static NotAtomicModule_SL4Public getNotAtomicModule(ModuleOrModuleGroup_SL4Public possibleGroup) throws PersistenceException {
        return possibleGroup.accept(new ModuleOrModuleGroup_SLReturnVisitor<NotAtomicModule_SL4Public>() {
            @Override
            public NotAtomicModule_SL4Public handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_SL4Public handleCONCAtomicModule_SL(CONCAtomicModule_SL4Public cONCAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_SL4Public handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_SL4Public handleCONCModule_SL(CONCModule_SL4Public cONCModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_SL4Public handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
                return notAtomicModule_SL;
            }

            @Override
            public NotAtomicModule_SL4Public handleCONCModuleOrModuleGroup_SL(CONCModuleOrModuleGroup_SL4Public cONCModuleOrModuleGroup_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public NotAtomicModule_SL4Public handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
                throw new Error("");
            }

        });

    }

    public static ModuleGroup_SL4Public getModuleGroup(ModuleOrModuleGroup_SL4Public possibleGroup) throws PersistenceException {
        return possibleGroup.accept(new ModuleOrModuleGroup_SLReturnVisitor<ModuleGroup_SL4Public>() {
            @Override
            public ModuleGroup_SL4Public handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleCONCAtomicModule_SL(CONCAtomicModule_SL4Public cONCAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleCONCModule_SL(CONCModule_SL4Public cONCModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleCONCModuleOrModuleGroup_SL(CONCModuleOrModuleGroup_SL4Public cONCModuleOrModuleGroup_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public ModuleGroup_SL4Public handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
                return moduleGroup_SL;
            }
        });
    }

    public static AtomicModule_SL4Public getAtomicModule(ModuleOrModuleGroup_SL4Public possibleGroup) throws PersistenceException {
        return possibleGroup.accept(new ModuleOrModuleGroup_SLReturnVisitor<AtomicModule_SL4Public>() {
            @Override
            public AtomicModule_SL4Public handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public atomicModule) throws PersistenceException {
                return atomicModule;
            }

            @Override
            public AtomicModule_SL4Public handleCONCAtomicModule_SL(CONCAtomicModule_SL4Public cONCAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public AtomicModule_SL4Public handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public atomicModule) throws PersistenceException {
                return atomicModule;
            }

            @Override
            public AtomicModule_SL4Public handleCONCModule_SL(CONCModule_SL4Public cONCModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public AtomicModule_SL4Public handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public AtomicModule_SL4Public handleCONCModuleOrModuleGroup_SL(CONCModuleOrModuleGroup_SL4Public cONCModuleOrModuleGroup_SL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public AtomicModule_SL4Public handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
                throw new Error("");
            }

        });

    }


}
