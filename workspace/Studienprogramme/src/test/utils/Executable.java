package test.utils;

@FunctionalInterface
public interface Executable {
    void execute() throws Throwable;
}