package test.concreteTests.adminServiceTests;

import model.NameAlreadyExistsException;
import model.StateException;
import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static test.utils.TestUtils.getUniqueName;

public class CreateModule extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }


    @Test
    /*
      Dieser Testfall testet die Operation CreateModule in der Klasse ProgramManager.
      Es werden zwei Module erstellt und dann geschaut ob sie beide in die Liste der Module des Programmanager aufgenommen werden.
     */
    public void testCreateModule() throws PersistenceException, NameAlreadyExistsException, StateException {
        // Ein Modul wird erstellt.
        NotAtomicModule_PL4Public modul1 = programManager.createModule(program, getUniqueName());
        // Der Programmanager muss nun das Modul1 in der Liste des Programms gespeichert haben.
        Assert.assertNotNull(program.getContainees().findFirst(containee -> containee.equals(modul1)));
        // Ein weiteres Modul wird erstellt.
        NotAtomicModule_PL4Public modul2 = programManager.createModule(program, getUniqueName());
        // Der Programmanager muss nun die Module(1,2) in der Liste des Programms gespeichert haben.
        Assert.assertNotNull(program.getContainees().findFirst(module -> module.equals(modul1)));
        Assert.assertNotNull(program.getContainees().findFirst(module -> module.equals(modul2)));
    }
}
