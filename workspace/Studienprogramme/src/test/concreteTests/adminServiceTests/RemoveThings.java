package test.concreteTests.adminServiceTests;

import common.util.HFW415Creator;
import model.StateException;
import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;
import view.objects.NotAtomicModule_PL;

public class RemoveThings extends GojaTest {

    private StudyProgram_PL4Public theProgram;
    private ModuleGroup_PL4Public wirtschaftsInformatik;
    private ModuleGroup_PL4Public grundlagen;
    private ModuleGroup_PL4Public betriebswirtschaft;
    private ModuleGroup_PL4Public praktischeInformatik;
    private NotAtomicModule_PL4Public notAtomicModule;
    private Unit_PL4Public unit;

    @Override
    protected void beforeMethod() throws Exception {
        theProgram = adminService.getProgramManager().createStudyProgram("theProgram");
        wirtschaftsInformatik = HFW415Creator.createWirtschaftsInformatik(adminService, theProgram);
        grundlagen = HFW415Creator.createGrundlagen(adminService, theProgram);
        betriebswirtschaft = HFW415Creator.createBetriebswirtschaft(adminService, theProgram);
        praktischeInformatik = HFW415Creator.createPraktischeInformatik(adminService, theProgram);
        notAtomicModule = ((NotAtomicModule_PL4Public) wirtschaftsInformatik.getContainees().findFirst(containee -> containee.getClassId() == NotAtomicModule_PL.getTypeId()));
        unit = notAtomicModule.getUnits().findFirst(unit -> true);
    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void testRemoveUnit() throws PersistenceException, StateException {
        long amountOfUnits = notAtomicModule.getUnits().getLength();
        adminService.getProgramManager().removeUnit(unit);
        Assert.assertEquals(amountOfUnits - 1, notAtomicModule.getUnits().getLength());
    }
}
