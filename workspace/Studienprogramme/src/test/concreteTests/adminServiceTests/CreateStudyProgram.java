package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Test;
import persistence.StudyProgram_PL4Public;
import test.GojaTest;

import static org.junit.Assert.assertNotNull;
import static test.utils.TestUtils.getUniqueName;

public class CreateStudyProgram extends GojaTest {

    private final String theSameName = getUniqueName();

    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    /**
     * Dieser Testfall ist etwas unsinnig, da bei GojaTest schon ein Programm erstellt wird. TODO überlegen
     */
    public void testCreation() throws Exception {

        StudyProgram_PL4Public program = programManager.createStudyProgram(getUniqueName());


        Assert.assertNotNull(programManager.getPrograms().findFirst(aProgram -> aProgram.equals(program)));
    }


}
