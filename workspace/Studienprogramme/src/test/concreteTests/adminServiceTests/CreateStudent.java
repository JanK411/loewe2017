package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import java.sql.Date;

import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class CreateStudent extends GojaTest {

        StudyProgram_PL4Public program;

    @Override
    protected void beforeMethod() throws Exception {
        program = programManager.createStudyProgram(getUniqueName());
    }

    @Override
    protected void afterMethod() throws Exception {

    }


    public static final String VORNAME = getUniqueName();
    public static final String NACHNAME = getUniqueName();

    /**
     * In diesem Testfall wird ein Student erstellt und einer neuen Studiengruppe hinzugef�gt.
     * �berpr�ft wird ob Vorname und Nachname des Studenten korrekt gespeichert werden.
     * Zus�tzlich wird �berpr�ft ob der erstellte Student in der Liste der Studenten der Studiengruppe vorkommt.
     */
    @Test
    public void createStudentAndAddToStudentGroup() throws Exception {
        // Ein Studienprogramm wird erstellt, welchem dann sp�ter Studenten hinzugef�gt werden k�nnen.
        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());

        // Ein Student wird zur Studiengruppe erstellt.
        Student4Public student = studentManager.createStudent(studentGroup, VORNAME, NACHNAME, new Date(System.currentTimeMillis()));


        // Nun muss der Student in der Liste der Studenten der Studiengruppe vorkommen.
        Assert.assertNotNull(studentGroup.getStudents().findFirst(aStudent -> aStudent.getWrappedStudent().equals(student)));

    }

    /**
     * In diesem Testfall werden mehrere Studenten erstellt und einer Studiengruppe hinzugef�gt.
     * �berpr�ft wird ob Vorname und Nachname der Studenten korrekt gespeichert werden.
     * Zus�tzlich wird �berpr�ft ob die erstellten Studenten in der Liste der Studenten der Studiengruppe vorkommen.
     */
    @Test
    public void createMultipleStudentsAndAddToStudentGroup() throws Exception {
        // Ein Studienprogramm wird erstellt, welchem dann sp�ter Studenten hinzugef�gt werden k�nnen.
        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());

        // Ein Student wird erstellt.
        Student4Public student1 = studentManager.createStudent(studentGroup, VORNAME, NACHNAME, new Date(System.currentTimeMillis()));
        Assert.assertEquals(VORNAME, student1.getFirstName());
        Assert.assertEquals(NACHNAME, student1.getName());
        // Ein Student wird erstellt.
        Student4Public student2 = studentManager.createStudent(studentGroup, VORNAME, NACHNAME, new Date(System.currentTimeMillis()));
        Assert.assertEquals(VORNAME, student2.getFirstName());
        Assert.assertEquals(NACHNAME, student2.getName());
        // Ein Student wird erstellt.
        Student4Public student3 = studentManager.createStudent(studentGroup, VORNAME, NACHNAME, new Date(System.currentTimeMillis()));
        Assert.assertEquals(VORNAME, student3.getFirstName());
        Assert.assertEquals(NACHNAME, student3.getName());


    }
}
