package test.concreteTests.adminServiceTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.concreteTests.adminServiceTests.createStudenGroup.CreateStudentGroup;
import test.concreteTests.adminServiceTests.makeGroupActive.MakeGroupActive;

@RunWith(Suite.class)
@Suite.SuiteClasses({AddModulesOrModuleGroup.class, AddStudentsToGroup.class, ChangeCreditPoints.class,
        CreateAtomicModule.class, CreateModule.class, CreateModuleGroup.class, CreateStudent.class,
        CreateStudentGroup.class, CreateStudyProgram.class, CreateUnit.class, DeleteStudentFromGroup.class,
        MakeGroupActive.class, RemoveThings.class})
/*
 * Diese Klasse f�hrt sequentiell alle definierten Testklassen aus.
 */
public final class AllAdminServiceTests {
}
