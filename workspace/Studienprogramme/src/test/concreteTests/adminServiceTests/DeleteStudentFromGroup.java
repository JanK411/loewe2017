package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import java.time.LocalDate;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class DeleteStudentFromGroup extends GojaTest {


    @Before
    public void beforeMethod() throws Exception {
    }


    @Override
    protected void afterMethod() throws Exception {
    }

    @Test
    public void testDeleteStudent() throws Exception {
        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());
        Student4Public student = studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), java.sql.Date.valueOf(LocalDate.now()));
        Assert.assertEquals(1, studentGroup.getStudents().getLength());
        adminService.getStudentManager().removeStudent(studentGroup.getStudents().findFirst(studentWrapper ->studentWrapper.getWrappedStudent().equals(student)));
        Assert.assertEquals(0, studentGroup.getStudents().getLength());
    }

}