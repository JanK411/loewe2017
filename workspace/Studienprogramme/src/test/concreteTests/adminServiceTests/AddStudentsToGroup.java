package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Test;
import persistence.Student4Public;
import persistence.StudentGroup4Public;
import test.GojaTest;

import java.sql.Date;

import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class AddStudentsToGroup extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void addStudents()throws Exception{
        //Studiengruppe erstellen
        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());

        //Studenten zur Studiengruppe erstellen
        Student4Public student1 = studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), new Date(System.currentTimeMillis()));
        Student4Public student3 = studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), new Date(System.currentTimeMillis()));
        Student4Public student2 = studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), new Date(System.currentTimeMillis()));

        // Nun m�ssen die Studenten in der Liste der Studenten der Studiengruppe vorkommen.
        Assert.assertNotNull(studentGroup.getStudents().findFirst(student -> student.getWrappedStudent().equals(student1)));
        Assert.assertNotNull(studentGroup.getStudents().findFirst(student -> student.getWrappedStudent().equals(student2)));
        Assert.assertNotNull(studentGroup.getStudents().findFirst(student -> student.getWrappedStudent().equals(student3)));
    }
}
