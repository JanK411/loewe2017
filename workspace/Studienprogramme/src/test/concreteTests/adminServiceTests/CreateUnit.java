package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static test.utils.TestUtils.getUniqueName;

public class CreateUnit extends GojaTest {

    NotAtomicModule_PL4Public module;

    @Override
    protected void beforeMethod() throws Exception {
        module = programManager.createModule(program, getUniqueName());

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    /**
     * Dieser Testfall testet die Operation CreateUnit in der Klasse ProgramManager.
     * Es werden zwei Units erstellt und dann geschaut ob sie in der Liste der Units des nicht atomaren Modules sind, für das sie erstellt wurden.
     */
    @Test
    public void testCreateUnit() throws Exception {
        // Unit erstellen
        Unit_PL4Public unit1 = programManager.createUnit(module, getUniqueName(), cp1);

        // Die Unit muss nun in der Liste der Units des nicht atomaren Moduls vorkommen
        Assert.assertEquals(unit1, module.getUnits().findFirst(unit -> unit.equals(unit1)));

        // Weitere Unit erstellen
        Unit_PL4Public unit2 = programManager.createUnit(module, getUniqueName(), cp1);

        // Die Units müssen nun in der Liste der Units des nicht atomaren Moduls vorkommen
        Assert.assertEquals(unit1, module.getUnits().findFirst(unit -> unit.equals(unit1)));
        Assert.assertEquals(unit2, module.getUnits().findFirst(unit -> unit.equals(unit2)));
    }
}
