package test.concreteTests.adminServiceTests;

import common.util.ListUtils;
import org.junit.Assert;
import org.junit.Test;
import persistence.ModuleGroup_PL4Public;
import test.GojaTest;

import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class CreateModuleGroup extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    /**
     * Dieser Testfall testet die Operation CreateModuleGroup in der Klasse ModuleOrModulegroupManager.
     * Es wird zwei Modulgruppen erstellt und einem Studienprogramm hinzugefügt.
     * Daraufhin müssen diese Modulgruppen in der Liste der containees des Studienprogramms vorkommen.
     */
    @Test
    public void testCreateModuleGroupAndAddToProgramm() throws Exception {
        // Modulgruppe erstellen.
        ModuleGroup_PL4Public modulGruppe1 = programManager.createModuleGroup(program, getUniqueName());
        // Das Programm muss nun die erstellte Modulgruppe enthalten.
        Assert.assertNotNull(program.getContainees().findFirst(x -> x.equals(modulGruppe1)));
        // Eine weitere Modulgruppe wird erstellt.
        ModuleGroup_PL4Public modulGruppe2 = programManager.createModuleGroup(program, getUniqueName());
        // Modulgruppe dem Programm hinzufügen
        programManager.addModulesOrModuleGroups(program, ListUtils.toList(modulGruppe2));
        // Das Programm muss jetzt beide Modulgruppen enthalten
        Assert.assertNotNull(program.getContainees().findFirst(moduleGroup -> moduleGroup.equals(modulGruppe1)));
        Assert.assertNotNull(program.getContainees().findFirst(moduleGroup -> moduleGroup.equals(modulGruppe2)));
    }

    /**
     * Dieser Testfall testet die Operation CreateModuleGroup in der Klasse ProgramManager.
     * Es werden drei Modulgruppen erstellt(1,2,3). Zwei Modulgruppen(2,3) werden Modulgruppe1 hinzugefügt.
     * Daraufhin müssen diese zwei Modulgruppen(1,2) in der Liste der containees der Modulgruppe1 vorkommen.
     */
    @Test
    public void testCreateModuleGroupAndAddToModuleGroup() throws Exception {
        // Modulgruppen erstellen.
        ModuleGroup_PL4Public modulGruppe1 = programManager.createModuleGroup(program, getUniqueName());
        ModuleGroup_PL4Public modulGruppe2 = programManager.createModuleGroup(modulGruppe1, getUniqueName());
        // Die Modulgruppe1 muss nun Modulgruppe2 enthalten.
        Assert.assertNotNull(modulGruppe1.getContainees().findFirst(x -> x.equals(modulGruppe2)));
        // Eine weitere Modulgruppe wird erstellt.
        ModuleGroup_PL4Public modulGruppe3 = programManager.createModuleGroup(modulGruppe1, getUniqueName());
        // Modulgruppe1 muss jetzt beide Modulgruppen(1,2) enthalten
        Assert.assertNotNull(modulGruppe1.getContainees().findFirst(moduleGroup -> moduleGroup.equals(modulGruppe2)));
        Assert.assertNotNull(modulGruppe1.getContainees().findFirst(moduleGroup -> moduleGroup.equals(modulGruppe3)));
    }


}
