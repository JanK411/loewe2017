package test.concreteTests.adminServiceTests;

import model.AlreadyInListException;
import org.junit.Test;
import persistence.ModuleGroup_PL4Public;
import persistence.StudyProgram_PL4Public;
import test.utils.AssertThrows;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class AddModulesOrModuleGroup extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void noDuplicateModuleGroups() throws Exception {
        //erstes Programm erstellen
        StudyProgram_PL4Public program1 = adminService.getProgramManager().createStudyProgram(getUniqueName());
        //zweites Programm erstellen
        StudyProgram_PL4Public program2 = adminService.getProgramManager().createStudyProgram(getUniqueName());

        //Modulgruppe in erstem Programm erstellen
        ModuleGroup_PL4Public group1 = adminService.getProgramManager().createModuleGroup(program1, getUniqueName());
        //andere Modulgruppe im zweiten Programm erstellen
        ModuleGroup_PL4Public group2 = adminService.getProgramManager().createModuleGroup(program2, getUniqueName());

        //die zweite Modulgruppe unter die erste legen
        adminService.getProgramManager().addModulesOrModuleGroups(group2, toList(group1));

        //die erste Modulgruppe darf nicht in doppelt in program1 vorkommen, was sie aber tun w�rde, wenn group2, die group1 enth�lt dem program1 hinzugef�gt wird.
        AssertThrows.assertThrows(AlreadyInListException.class, () -> adminService.getProgramManager().addModulesOrModuleGroups(program1, toList(group2)));
    }
}
