package test.concreteTests.adminServiceTests.createStudenGroup;

import common.util.HFW415Creator;
import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static test.utils.TestUtils.*;

public class CreateStudenGroupWithHFW415Grundlagen extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }


    @Test
    public void createStudentGroup() throws Exception{
        // Dem Programm werden die Grundlagen und überfachlichen Qualifikationen hinzugefügt.
        HFW415Creator.createGrundlagen(adminService, program);
        // Diese sind als Objektwelt unter test/documentation/GrundlagenObjektwelt dargestellt.
        // Studiengruppe erstellen
        StudentGroup4Public studiengruppe = studentManager.createStudentGroup(program, getUniqueName());
        // Das Studienprogramm auf der GL
        StudyProgram_GL4Public studyProgram = studiengruppe.getStudyProgram();
        // Testen ob die Struktur der Struktur auf der PL in GrundlagenObjektwelt entspricht.
        Assert.assertNotNull(studyProgram.getContainees().findFirst(containee -> nameIsEquals(containee,HFW415Creator.ModuleGroupNameGrundlagen)));
        // Grundlagen auf der GL holen, indem der Containee visitiert wird in getModuleGroup
        ModuleGroup_GL4Public grundlagenAufDerGL = getModuleGroup(studyProgram.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleGroupNameGrundlagen)));
        // Überprüfen ob auf der GL entsprechende Objekte mit den richtigen Namen existieren
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameMathematik1)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameMathematik2)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameStatistik)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameWirtschaftsrecht)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameEnglisch1)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameEnglisch2)));
        Assert.assertNotNull(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameSoko)));
        // Die nicht atomaren Module holen.
        NotAtomicModule_GL4Public wirtschaftsRechtAufDerGl = getNotAtomicModule(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameWirtschaftsrecht)));
        NotAtomicModule_GL4Public englisch1AufderGl = getNotAtomicModule(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameEnglisch1)));
        NotAtomicModule_GL4Public englisch2AufderGl = getNotAtomicModule(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameEnglisch2)));
        NotAtomicModule_GL4Public sokoAufderGl = getNotAtomicModule(grundlagenAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, HFW415Creator.ModuleNameSoko)));
        // Units der nicht atomaren Module testen.
        // Wirtschaftsrecht
        Assert.assertNotNull(wirtschaftsRechtAufDerGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameWirtschaftsrecht1)));
        Assert.assertNotNull(wirtschaftsRechtAufDerGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameWirtschaftsrecht2)));
        // Englisch 1
        Assert.assertNotNull(englisch1AufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameEnglisch1_1)));
        Assert.assertNotNull(englisch1AufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameEnglisch1_2)));
        // Englisch 2
        Assert.assertNotNull(englisch2AufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameEnglisch2_1)));
        Assert.assertNotNull(englisch2AufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameEnglisch2_2)));
        // Soko
        Assert.assertNotNull(sokoAufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameSoko1)));
        Assert.assertNotNull(sokoAufderGl.getUnits().findFirst(containee -> nameIsEquals(containee,HFW415Creator.UnitNameSoko2)));
    }

}
