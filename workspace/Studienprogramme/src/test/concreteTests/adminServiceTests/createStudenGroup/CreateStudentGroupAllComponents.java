package test.concreteTests.adminServiceTests.createStudenGroup;

import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static test.utils.TestUtils.*;
import static test.utils.TestUtils.getUniqueName;

public class CreateStudentGroupAllComponents extends GojaTest{

    @Override
    protected void beforeMethod() throws Exception {
    }

    @Override
    protected void afterMethod() throws Exception {
    }

    @Test
    public void testCreateStudentGroup() throws Exception {
        String moduleName = getUniqueName();
        programManager.createAtomicModule(program, moduleName, cp1, drittelNoten);

        StudentGroup4Public studentGroup = createStudentGroup();

        Assert.assertNotNull(studentGroup.getStudyProgram().getContainees().findFirst(containee -> nameIsEquals(containee, moduleName)));
    }


    @Test
    public void testMakeActiveWithNotAtomicModule() throws Exception {
        String moduleName = getUniqueName();
        String unitName1 = getUniqueName();
        String unitName2 = getUniqueName();
        NotAtomicModule_PL4Public module = programManager.createModule(program, moduleName);
        programManager.createUnit(module, unitName1, cp1);
        programManager.createUnit(module, unitName2, cp1);

        StudentGroup4Public studentGroup = createStudentGroup();
        Assert.assertNotNull(studentGroup.getStudyProgram().getContainees().findFirst(containee -> nameIsEquals(containee, moduleName)));
        // Check der Units
        NotAtomicModule_GL4Public moduleAufDerGL = getNotAtomicModule(studentGroup.getStudyProgram().getContainees().findFirst(containee -> nameIsEquals(containee, moduleName)));
        Assert.assertNotNull(moduleAufDerGL.getUnits().findFirst(containee -> nameIsEquals(containee, unitName1)));
        Assert.assertNotNull(moduleAufDerGL.getUnits().findFirst(containee -> nameIsEquals(containee, unitName2)));
    }

    @Test
    public void testMakeActiveWithModuleGroup() throws Exception {
        String notAtomicModuleName = getUniqueName();
        String atomicModuleName = getUniqueName();
        String unitName1 = getUniqueName();
        String unitName2 = getUniqueName();
        String moduleGroupName = getUniqueName();
        // Modulgruppe mit 2 Modulen erstellen.
        ModuleGroup_PL4Public moduleGroup = programManager.createModuleGroup(program, moduleGroupName);

        NotAtomicModule_PL4Public module = programManager.createModule(moduleGroup, notAtomicModuleName);
        programManager.createUnit(module, unitName1, cp1);
        programManager.createUnit(module, unitName2, cp1);

        programManager.createAtomicModule(moduleGroup, atomicModuleName, cp1, drittelNoten);

        StudentGroup4Public studentGroup = createStudentGroup();

        //Modulgruppe ist direkt im Programm enthalten
        Assert.assertNotNull(studentGroup.getStudyProgram().getContainees().findFirst(containee -> nameIsEquals(containee, moduleGroupName)));
        // Check der Module
        ModuleGroup_GL4Public moduleGroupAufDerGL = getModuleGroup(studentGroup.getStudyProgram().getContainees().findFirst(containee -> nameIsEquals(containee, moduleGroupName)));
        Assert.assertNotNull(moduleGroupAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, atomicModuleName)));
        Assert.assertNotNull(moduleGroupAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, notAtomicModuleName)));
        // Check der Units
        NotAtomicModule_GL4Public moduleAufDerGL = getNotAtomicModule(moduleGroupAufDerGL.getContainees().findFirst(containee -> nameIsEquals(containee, notAtomicModuleName)));
        Assert.assertNotNull(moduleAufDerGL.getUnits().findFirst(containee -> nameIsEquals(containee, unitName1)));
        Assert.assertNotNull(moduleAufDerGL.getUnits().findFirst(containee -> nameIsEquals(containee, unitName2)));
    }

    private StudentGroup4Public createStudentGroup() throws model.StateException, PersistenceException {
        return studentManager.createStudentGroup(program, getUniqueName());
    }
}
