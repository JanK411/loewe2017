package test.concreteTests.adminServiceTests.createStudenGroup;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertTrue;

@RunWith(Suite.class)
@Suite.SuiteClasses({CreateStudenGroupWithHFW415Grundlagen.class, CreateStudentGroupAllComponents.class})
/*
 * Diese Klasse f�hrt sequentiell alle definierten Testklassen aus.
 */
public final class CreateStudentGroup {
}
