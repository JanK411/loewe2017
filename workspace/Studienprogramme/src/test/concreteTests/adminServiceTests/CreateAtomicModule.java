package test.concreteTests.adminServiceTests;

import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class CreateAtomicModule extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }


    /*
      Dieser Testfall testet die Operation CreateAtomicModule in der Klasse ProgrammManager.
      Es werden zwei atomare Module erstellt und dann erwartet, dass sie in der Liste der Module des ProgrammManagers gepeichert sind.
     */
    @Test
    public void testCreateAtomicModule() throws Exception {
        // Atomares Modul erstellen
        AtomicModule_PL4Public modul1 = programManager.createAtomicModule(program, getUniqueName(), cp1, drittelNoten);
        // Das atomare Modul muss nun Teil der Liste der Module des ModuleManagers sein.
        Assert.assertNotNull(program.getContainees().findFirst(containee -> containee.equals(modul1)));


        // Ein weiteres atomares Modul erstellen
        AtomicModule_PL4Public modul2 = programManager.createAtomicModule(program, getUniqueName(), cp1, bestandenNichtBestanden);
        // Beide atomaren Modul müssen nun Teil der Liste der Module des ModuleManagers sein.
        Assert.assertNotNull(program.getContainees().findFirst(atomicModule -> atomicModule.equals(modul1)));
        Assert.assertNotNull(program.getContainees().findFirst(atomicModule -> atomicModule.equals(modul2)));
    }
}
