package test.concreteTests.adminServiceTests.makeGroupActive;

import common.util.HFW415Creator;
import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.*;

public class MakeGroupActiveWithHFW415Grundlagen extends GojaTest {

    private StudentGroup4Public studiengruppe;

    @Override
    protected void beforeMethod() throws Exception {
        // Dem Programm werden die Grundlagen und überfachlichen Qualifikationen hinzugefügt.
        HFW415Creator.createGrundlagen(adminService, program);
        // Diese sind als Objektwelt unter test/documentation/GrundlagenObjektwelt dargestellt.
        // Studiengruppe erstellen
        studiengruppe = studentManager.createStudentGroup(program, getUniqueName());
        // Studenten für die Studiengruppe erstellen und dieser hinzufügen
        Student4Public student1 = createRandomStudent(studentManager,studiengruppe);
        Student4Public student2 = createRandomStudent(studentManager,studiengruppe);
    }


    @Override
    protected void afterMethod() throws Exception {
    }

    @Test
    public void makeActive() throws Exception {
        // Studiengruppe aktiv schalten
        studiengruppe.makeActive();
        // Jeder Student muss ein Studienprogramm auf der SL bekommen.
        studiengruppe.getStudents().applyToAll(studentWrapper -> testIfStudentHasTheCorrectStudyProgram(studentWrapper.getWrappedStudent()));
    }

    private void testIfStudentHasTheCorrectStudyProgram(Student4Public student) throws PersistenceException {
        // Das Studienprogramm auf der SL
        StudyProgram_SL4Public studyProgramFromTheStudent = getActiveStudyProgramFromStudent(student);
        // Testen ob die Struktur der Struktur auf der PL in GrundlagenObjektwelt entspricht.
        Assert.assertNotNull(studyProgramFromTheStudent.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleGroupNameGrundlagen)));
        // Grundlagen auf der GL holen, indem der Containee visitiert wird in getModuleGroup
        ModuleGroup_SL4Public grundlagenAufDerSL = getModuleGroup(studyProgramFromTheStudent.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleGroupNameGrundlagen)));
        // Überprüfen ob auf der GL entsprechende Objekte mit den richtigen Namen existieren
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameMathematik1)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameMathematik2)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameStatistik)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameWirtschaftsrecht)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameEnglisch1)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameEnglisch2)));
        Assert.assertNotNull(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameSoko)));
        // Die nicht atomaren Module holen.
        NotAtomicModule_SL4Public wirtschaftsRechtAufDerGl = getNotAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameWirtschaftsrecht)));
        NotAtomicModule_SL4Public englisch1AufderSl = getNotAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameEnglisch1)));
        NotAtomicModule_SL4Public englisch2AufderSl = getNotAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameEnglisch2)));
        NotAtomicModule_SL4Public sokoAufderSl = getNotAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameSoko)));
        // Units der nicht atomaren Module testen.
        // Wirtschaftsrecht
        Assert.assertNotNull(wirtschaftsRechtAufDerGl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameWirtschaftsrecht1)));
        Assert.assertNotNull(wirtschaftsRechtAufDerGl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameWirtschaftsrecht2)));
        // Englisch 1
        Assert.assertNotNull(englisch1AufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameEnglisch1_1)));
        Assert.assertNotNull(englisch1AufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameEnglisch1_2)));
        // Englisch 2
        Assert.assertNotNull(englisch2AufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameEnglisch2_1)));
        Assert.assertNotNull(englisch2AufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameEnglisch2_2)));
        // Soko
        Assert.assertNotNull(sokoAufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameSoko1)));
        Assert.assertNotNull(sokoAufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameSoko2)));

    }
}
