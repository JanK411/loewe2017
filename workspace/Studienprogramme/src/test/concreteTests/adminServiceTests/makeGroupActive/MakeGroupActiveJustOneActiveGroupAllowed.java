package test.concreteTests.adminServiceTests.makeGroupActive;

import model.StateException;
import org.junit.Assert;
import org.junit.Test;
import persistence.Student4Public;
import persistence.StudentGroup4Public;
import test.GojaTest;
import test.utils.AssertThrows;

import static common.util.ListUtils.toList;
import static test.utils.TestUtils.createRandomStudent;
import static test.utils.TestUtils.getUniqueName;

public class MakeGroupActiveJustOneActiveGroupAllowed extends GojaTest {

    Student4Public studentWithActiveStudentGroup;

    @Override
    protected void beforeMethod() throws Exception {
        StudentGroup4Public activeGroup = studentManager.createStudentGroup(program, getUniqueName());
        studentWithActiveStudentGroup = createRandomStudent(studentManager, activeGroup);
        studentManager.addStudentsToGroup(activeGroup, toList(studentWithActiveStudentGroup.wrap()));
        activeGroup.makeActive();

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void testExceptionWhen2ActiveStudenGroups() throws Exception {

        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program, getUniqueName());
        studentManager.addStudentsToGroup(studentGroup, toList(studentWithActiveStudentGroup.wrap()));

        AssertThrows.assertThrows(StateException.class, () -> studentGroup.makeActive());
    }
}
