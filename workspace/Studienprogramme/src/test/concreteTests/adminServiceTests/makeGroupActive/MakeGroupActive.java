package test.concreteTests.adminServiceTests.makeGroupActive;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({MakeGroupActiveWithHFW415Grundlagen.class,MakeGroupActiveAllComponents.class,MakeGroupActiveJustOneActiveGroupAllowed.class})
/*
 * Diese Klasse f�hrt sequentiell alle definierten Testklassen aus.
 */
public final class MakeGroupActive {
}

