package test.concreteTests.adminServiceTests.makeGroupActive;

import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static test.utils.TestUtils.*;

public class MakeGroupActiveAllComponents extends GojaTest {

    @Override
    protected void beforeMethod() throws Exception {
    }

    @Override
    protected void afterMethod() throws Exception {
    }

    @Test
    public void testMakeActiveWithAtomicModule() throws Exception {
        String moduleName = getUniqueName();
        programManager.createAtomicModule(program, moduleName, cp1, drittelNoten);

        StudentGroup4Public studentGroup = createStudentGroup();
        Student4Public student = createRandomStudent(studentManager,studentGroup);
        addStudentToGroup(studentGroup, student);

        studentGroup.makeActive();

        Assert.assertNotNull(getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> nameIsEqual(containee, moduleName)));
    }


    @Test
    public void testMakeActiveWithNotAtomicModule() throws Exception {
        String moduleName = getUniqueName();
        String unitName1 = getUniqueName();
        String unitName2 = getUniqueName();
        NotAtomicModule_PL4Public module = programManager.createModule(program, moduleName);
        programManager.createUnit(module, unitName1, cp1);
        programManager.createUnit(module, unitName2, cp1);

        StudentGroup4Public studentGroup = createStudentGroup();
        Student4Public student = createRandomStudent(studentManager,studentGroup);
        addStudentToGroup(studentGroup, student);

        studentGroup.makeActive();

        Assert.assertNotNull(getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> nameIsEqual(containee, moduleName)));
        // Check der Units
        NotAtomicModule_SL4Public moduleAufDerSL = getNotAtomicModule(getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> nameIsEqual(containee, moduleName)));
        Assert.assertNotNull(moduleAufDerSL.getUnits().findFirst(containee -> nameIsEqual(containee, unitName1)));
        Assert.assertNotNull(moduleAufDerSL.getUnits().findFirst(containee -> nameIsEqual(containee, unitName2)));
    }

    @Test
    public void testMakeActiveWithModuleGroup() throws Exception {
        String notAtomicModuleName = getUniqueName();
        String atomicModuleName = getUniqueName();
        String unitName1 = getUniqueName();
        String unitName2 = getUniqueName();
        String moduleGroupName = getUniqueName();
        // Modulgruppe mit 2 Modulen erstellen.
        ModuleGroup_PL4Public moduleGroup = programManager.createModuleGroup(program, moduleGroupName);

        NotAtomicModule_PL4Public module = programManager.createModule(moduleGroup, notAtomicModuleName);
        programManager.createUnit(module, unitName1, cp1);
        programManager.createUnit(module, unitName2, cp1);

        programManager.createAtomicModule(moduleGroup, atomicModuleName, cp1, drittelNoten);

        StudentGroup4Public studentGroup = createStudentGroup();
        Student4Public student = createRandomStudent(studentManager,studentGroup);
        addStudentToGroup(studentGroup, student);

        studentGroup.makeActive();
        //Modulgruppe ist direkt im Programm enthalten
        Assert.assertNotNull(getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> nameIsEqual(containee, moduleGroupName)));
        // Check der Module
        ModuleGroup_SL4Public moduleGroupAufDerSL = getModuleGroup(getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> nameIsEqual(containee, moduleGroupName)));
        Assert.assertNotNull(moduleGroupAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, atomicModuleName)));
        Assert.assertNotNull(moduleGroupAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, notAtomicModuleName)));
        // Check der Units
        NotAtomicModule_SL4Public moduleAufDerSL = getNotAtomicModule(moduleGroupAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, notAtomicModuleName)));
        Assert.assertNotNull(moduleAufDerSL.getUnits().findFirst(containee -> nameIsEqual(containee, unitName1)));
        Assert.assertNotNull(moduleAufDerSL.getUnits().findFirst(containee -> nameIsEqual(containee, unitName2)));
    }

    private StudentGroup4Public createStudentGroup() throws model.StateException, PersistenceException {
        return studentManager.createStudentGroup(program, getUniqueName());
    }

    private void addStudentToGroup(StudentGroup4Public studentGroup, Student4Public student) throws model.StateException, PersistenceException {
        studentManager.addStudentsToGroup(studentGroup, toList(student.wrap()));
    }

}
