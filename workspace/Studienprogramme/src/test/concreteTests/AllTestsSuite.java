package test.concreteTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.concreteTests.adminServiceTests.AllAdminServiceTests;
import test.concreteTests.anforderungsTests.AllAnforderungsTests;
import test.concreteTests.anforderungsTests.Test_CPAggregation;

@RunWith(Suite.class)
@Suite.SuiteClasses({AllAdminServiceTests.class, AllAnforderungsTests.class, Test_CPAggregation.class})
/*
 * Diese Klasse führt sequentiell alle definierten Testklassen aus.
 */
public final class AllTestsSuite {
}
