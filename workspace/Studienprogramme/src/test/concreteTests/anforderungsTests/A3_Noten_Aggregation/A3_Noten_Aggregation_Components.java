package test.concreteTests.anforderungsTests.A3_Noten_Aggregation;

import model.*;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.*;

public class A3_Noten_Aggregation_Components extends GojaTest {


    @Override
    protected void beforeMethod() throws Exception {

    }


    @Test
    public void notAtomicModule() throws Exception {
        String moduleName = getUniqueName();
        String unitName1 = getUniqueName();
        String unitName2 = getUniqueName();

        NotAtomicModule_PL4Public atomicModule = programManager.createModule(program, moduleName);
        Unit_PL4Public unit1 = programManager.createUnit(atomicModule, unitName1, cp1);
        Unit_PL4Public unit2 = programManager.createUnit(atomicModule, unitName2, cp1);

        StudyProgram_SL4Public program_SL = advanceToProgramSL(studentManager, program);

        NotAtomicModule_SL4Public notAtomicModule_SL = getNotAtomicModule(program_SL.getContainees().findFirst(containee -> nameIsEqual(containee, moduleName)));
        Unit_SL4Public unit_SL1 = notAtomicModule_SL.getUnits().findFirst(aUnit -> nameIsEqual(aUnit, unitName1));
        Unit_SL4Public unit_SL2 = notAtomicModule_SL.getUnits().findFirst(aUnit -> nameIsEqual(aUnit, unitName2));
        unit_SL1.assignGrade(tg10.getThetg10(), commentForNewGrade);
        unit_SL2.assignGrade(tg17.getThetg17(), commentForNewGrade);


        assertEquals(tg13.getThetg13(), notAtomicModule_SL.getCalculatedStudentGrade());
        assertEquals(tg13.getThetg13(), program_SL.getCalculatedStudentGrade());
    }

    @Test
    public void atomicModule() throws Exception {

        String moduleName = getUniqueName();

        AtomicModule_PL4Public atomicModule = programManager.createAtomicModule(program, moduleName, cp1, drittelNoten);

        StudyProgram_SL4Public program_SL = advanceToProgramSL(studentManager, program);

        AtomicModule_SL4Public atomicModule_SL = getAtomicModule(program_SL.getContainees().findFirst(containee -> nameIsEqual(containee, moduleName)));

        atomicModule_SL.assignGrade(tg40.getThetg40(), commentForNewGrade);


        assertEquals(tg40.getThetg40(), atomicModule_SL.getCalculatedStudentGrade());
        assertEquals(tg40.getThetg40(), program_SL.getCalculatedStudentGrade());
    }

    @Test
    public void moduleGroup() throws Exception {

        String moduleGroupName = getUniqueName();
        String notAtomicModuleName = getUniqueName();
        String atomicModuleName = getUniqueName();
        String unitName = getUniqueName();

        ModuleGroup_PL4Public moduleGroup = programManager.createModuleGroup(program, moduleGroupName);

        NotAtomicModule_PL4Public notAtomicModule = programManager.createModule(moduleGroup, notAtomicModuleName);
        AtomicModule_PL4Public atomicModule = programManager.createAtomicModule(moduleGroup, atomicModuleName, cp1, drittelNoten);

        Unit_PL4Public unit = programManager.createUnit(notAtomicModule, unitName, cp1);


        StudyProgram_SL4Public program_SL = advanceToProgramSL(studentManager, program);

        ModuleGroup_SL4Public moduleGroup_SL = getModuleGroup(program_SL.getContainees().findFirst(containee -> nameIsEqual(containee, moduleGroupName)));
        NotAtomicModule_SL4Public notAtomicModule_SL = getNotAtomicModule(moduleGroup_SL.getContainees().findFirst(containee -> nameIsEqual(containee, notAtomicModuleName)));
        Unit_SL4Public unit_SL = notAtomicModule_SL.getUnits().findFirst(aUnit -> nameIsEqual(aUnit, unitName));
        AtomicModule_SL4Public atomicModule_SL = getAtomicModule(moduleGroup_SL.getContainees().findFirst(containee -> nameIsEqual(containee, atomicModuleName)));

        unit_SL.assignGrade(tg10.getThetg10(), commentForNewGrade);
        atomicModule_SL.assignGrade(tg13.getThetg13(), commentForNewGrade);


        assertEquals(tg13.getThetg13(), atomicModule_SL.getCalculatedStudentGrade());
        assertEquals(tg10.getThetg10(), notAtomicModule_SL.getCalculatedStudentGrade());

        // Modulegruppen- und Programmnoten werden auf Drittelnoten gerundet. 1.3 und 1.0 wird zu 1.1
        assertEquals(dg11.getThedg11(), moduleGroup_SL.getCalculatedStudentGrade());
        assertEquals(dg11.getThedg11(), program_SL.getCalculatedStudentGrade());

    }

    //TODO eigentlich w�re bei solchen Testf�llen irgendwie ein kleines Diagramm sch�n, welches Konstrukt aufgebaut wird... kann L�we da schon verstehen, ist aber halt auch mega aufw�ndig...
    @Test
    public void ignoreBinaryGrades() throws Exception {
        String notAtomicModuleName = getUniqueName();
        String atomicModuleName = getUniqueName();
        String unitName = getUniqueName();

        AtomicModule_PL4Public atomicModule = programManager.createAtomicModule(program, atomicModuleName, cp1, bestandenNichtBestanden);
        NotAtomicModule_PL4Public notAtomicModule = programManager.createModule(program, notAtomicModuleName);
        Unit_PL4Public unit = programManager.createUnit(notAtomicModule, unitName, cp1);

        StudyProgram_SL4Public program_SL = advanceToProgramSL(studentManager, program);

        AtomicModule_SL4Public atomicModule_SL = getAtomicModule(program_SL.getContainees().findFirst(containee -> nameIsEqual(containee, atomicModuleName)));
        NotAtomicModule_SL4Public notAtomicModule_SL = getNotAtomicModule(program_SL.getContainees().findFirst(containee -> nameIsEqual(containee, notAtomicModuleName)));
        Unit_SL4Public unit_SL = notAtomicModule_SL.getUnits().findFirst(aUnit -> nameIsEqual(aUnit, unitName));

        atomicModule_SL.assignGrade(Passed.getThePassed(), commentForNewGrade);

        assertEquals(Passed.getThePassed(), atomicModule_SL.getCalculatedStudentGrade());
        assertEquals(NoGrade.getTheNoGrade(), program_SL.getCalculatedStudentGrade());

        unit_SL.assignGrade(tg10.getThetg10(), commentForNewGrade);

        assertEquals(tg10.getThetg10(), program_SL.getCalculatedStudentGrade());
        assertEquals(tg10.getThetg10(), notAtomicModule_SL.getCalculatedStudentGrade());

    }


    @Override
    protected void afterMethod() throws Exception {

    }
}
