package test.concreteTests.anforderungsTests.A3_Noten_Aggregation;

import common.Fraction;
import common.util.HFW415Creator;
import model.*;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import java.sql.Date;
import java.time.Instant;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertEquals;
import static test.utils.TestUtils.*;

public class A3_Noten_Aggregation_HFW415Creator extends GojaTest {


    @Override
    protected void beforeMethod() throws Exception {

    }


    /**
     * In diesem Testfall werden die Grundlagen von der Studiengruppe HFW415 dem Programm hinzugefügt.
     * Diese Objektwelt ist in documentation/GrundlagenObjektwelt.pdf dargestellt.
     * <p>
     * Es werden Noten an einige Elemente dieses Studienprogramms verteilt und dann die Aggregation getestet.
     * Mathematik 1 und 2 bekommt die Note 3.0 und 4.0 .
     * In Soko gibt es für die Units die Noten 1.0 und 1.3 .
     * Die Gesamtnote des Programmes ergibt sich dann aus folgender Rechnung:
     * Gesamtnote = [(3.0 * 8)+(4.0 * 7)+(1.0 * 2,5)+(1.3 * 2,5)] / 20 = 2,8875 = 2.9
     * Alle anderen Elemente haben keine Noten und werden deswegen bei der Berechnung ignoriert.
     */
    @Test
    public void hfw415Creator() throws Exception {

        HFW415Creator.createGrundlagen(adminService, program);

        StudyProgram_SL4Public studyProgram_sl = advanceToProgramSL(studentManager, program);

        ModuleGroup_SL4Public grundlagenAufDerSL = getModuleGroup(studyProgram_sl.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleGroupNameGrundlagen)));

        AtomicModule_SL4Public mathe1 = getAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameMathematik1)));
        AtomicModule_SL4Public mathe2 = getAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameMathematik2)));

        NotAtomicModule_SL4Public sokoAufderSl = getNotAtomicModule(grundlagenAufDerSL.getContainees().findFirst(containee -> nameIsEqual(containee, HFW415Creator.ModuleNameSoko)));

        Unit_SL4Public soko1 = sokoAufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameSoko1));
        Unit_SL4Public soko2 = sokoAufderSl.getUnits().findFirst(containee -> nameIsEqual(containee, HFW415Creator.UnitNameSoko2));


        // Mathematik 1 und 2 bekommt die Note 3.0 und 4.0 .
        mathe1.assignGrade(tg30.getThetg30(), commentForNewGrade);
        mathe2.assignGrade(tg40.getThetg40(), commentForNewGrade);

        assertEquals(dg35.getThedg35(), grundlagenAufDerSL.getCalculatedStudentGrade());

        // In Soko gibt es für die Units die Noten 1.0 und 1.3 .
        soko1.assignGrade(tg10.getThetg10(), commentForNewGrade);
        soko2.assignGrade(tg13.getThetg13(), commentForNewGrade);

        assertEquals(tg10.getThetg10(), sokoAufderSl.getCalculatedStudentGrade());

        //  Gesamtnote = [(3.0 * 8)+(4.0 * 7)+(1.0 * 2,5)+(1.3 * 2,5)] / 20 = 2,8875 = 2.9
        assertEquals(dg29.getThedg29(), studyProgram_sl.getCalculatedStudentGrade());


        //hiernach müssste der cache von mathe1 und allem darüberliegenden gecleared worden sein
        mathe1.assignGrade(tg17.getThetg17(), "da hat der Student wohl doch eine etwas bessere Note in Mathe geschrieben");

        assertEquals(grundlagenAufDerSL.calculateGrade(), grundlagenAufDerSL.getCalculatedStudentGrade());


    }




    @Override
    protected void afterMethod() throws Exception {

    }
}
