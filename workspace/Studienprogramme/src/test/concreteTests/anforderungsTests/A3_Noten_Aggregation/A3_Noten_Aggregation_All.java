package test.concreteTests.anforderungsTests.A3_Noten_Aggregation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding.TestRounding;

@RunWith(Suite.class)
@Suite.SuiteClasses({A3_Noten_Aggregation_Components.class, A3_Noten_Aggregation_HFW415Creator.class, A3_Noten_Aggregation_Caching.class, TestRounding.class})
/*
 * Diese Klasse führt sequentiell alle definierten Testklassen aus.
 */
public final class A3_Noten_Aggregation_All {
}
