package test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding;


import common.Fraction;
import common.util.NotenFractions;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding.TestConstantsForRounding.*;


public class TestRoundingDrittelNoten {

	@Test
	public void testRoundZero() {
		assertEquals(NotenFractions.f50, Fraction.Null.roundToDrittelnoten());
	}

	@Test
	public void test10() {
		assertEquals(NotenFractions.f10, NotenFractions.f10.roundToDrittelnoten());
		assertEquals(NotenFractions.f10, NotenFractions.f11.roundToDrittelnoten());
		assertEquals(NotenFractions.f10, f115.roundToDrittelnoten());
	}

	@Test
	public void test13() {
		assertEquals(NotenFractions.f13, NotenFractions.f12.roundToDrittelnoten());
		assertEquals(NotenFractions.f13, NotenFractions.f13.roundToDrittelnoten());
		assertEquals(NotenFractions.f13, NotenFractions.f14.roundToDrittelnoten());
		assertEquals(NotenFractions.f13, NotenFractions.f15.roundToDrittelnoten());
	}

	@Test
	public void test17() {
		assertEquals(NotenFractions.f17, NotenFractions.f16.roundToDrittelnoten());
		assertEquals(NotenFractions.f17, NotenFractions.f17.roundToDrittelnoten());
		assertEquals(NotenFractions.f17, NotenFractions.f18.roundToDrittelnoten());
		assertEquals(NotenFractions.f17, f185.roundToDrittelnoten());
	}

	@Test
	public void test20() {
		assertEquals(NotenFractions.f20, NotenFractions.f19.roundToDrittelnoten());
		assertEquals(NotenFractions.f20, NotenFractions.f20.roundToDrittelnoten());
		assertEquals(NotenFractions.f20, NotenFractions.f21.roundToDrittelnoten());
		assertEquals(NotenFractions.f20, f215.roundToDrittelnoten());
	}

	@Test
	public void test23() {
		assertEquals(NotenFractions.f23, NotenFractions.f22.roundToDrittelnoten());
		assertEquals(NotenFractions.f23, NotenFractions.f23.roundToDrittelnoten());
		assertEquals(NotenFractions.f23, NotenFractions.f24.roundToDrittelnoten());
		assertEquals(NotenFractions.f23, NotenFractions.f25.roundToDrittelnoten());
	}

	@Test
	public void test27() {
		assertEquals(NotenFractions.f27, NotenFractions.f26.roundToDrittelnoten());
		assertEquals(NotenFractions.f27, NotenFractions.f27.roundToDrittelnoten());
		assertEquals(NotenFractions.f27, NotenFractions.f28.roundToDrittelnoten());
		assertEquals(NotenFractions.f27, f285.roundToDrittelnoten());
	}

	@Test
	public void test30() {
		assertEquals(NotenFractions.f30, NotenFractions.f29.roundToDrittelnoten());
		assertEquals(NotenFractions.f30, NotenFractions.f30.roundToDrittelnoten());
		assertEquals(NotenFractions.f30, NotenFractions.f31.roundToDrittelnoten());
		assertEquals(NotenFractions.f30, f315.roundToDrittelnoten());
	}

	@Test
	public void test33() {
		assertEquals(NotenFractions.f33, NotenFractions.f32.roundToDrittelnoten());
		assertEquals(NotenFractions.f33, NotenFractions.f33.roundToDrittelnoten());
		assertEquals(NotenFractions.f33, NotenFractions.f34.roundToDrittelnoten());
		assertEquals(NotenFractions.f33, NotenFractions.f35.roundToDrittelnoten());
	}

	@Test
	public void test37() {
		assertEquals(NotenFractions.f37, NotenFractions.f36.roundToDrittelnoten());
		assertEquals(NotenFractions.f37, NotenFractions.f37.roundToDrittelnoten());
		assertEquals(NotenFractions.f37, NotenFractions.f38.roundToDrittelnoten());
		assertEquals(NotenFractions.f37, f385.roundToDrittelnoten());
	}

	@Test
	public void test40() {
		assertEquals(NotenFractions.f40, NotenFractions.f39.roundToDrittelnoten());
		assertEquals(NotenFractions.f40, NotenFractions.f40.roundToDrittelnoten());
	}

}
