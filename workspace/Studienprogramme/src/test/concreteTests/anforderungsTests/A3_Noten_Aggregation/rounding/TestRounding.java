package test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestRoundingDrittelNoten.class, TestRoundingZehntelNoten.class})
/*
 * Diese Klasse führt sequentiell alle definierten Testklassen aus.
 */
public final class TestRounding {
}
