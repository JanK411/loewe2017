package test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding;

import static org.junit.Assert.assertEquals;
import static test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding.TestConstantsForRounding.*;

import common.Fraction;
import common.util.NotenFractions;
import org.junit.Test;


public class TestRoundingZehntelNoten {


	@Test
	public void testRoundZero() {
		assertEquals(NotenFractions.f50, Fraction.Null.roundToZehntelnoten());
	}

	@Test
	public void testNoRounding() {
		assertEquals(NotenFractions.f10, NotenFractions.f10.roundToZehntelnoten());
		assertEquals(NotenFractions.f11, NotenFractions.f11.roundToZehntelnoten());
		assertEquals(NotenFractions.f12, NotenFractions.f12.roundToZehntelnoten());
		assertEquals(NotenFractions.f13, NotenFractions.f13.roundToZehntelnoten());
		assertEquals(NotenFractions.f14, NotenFractions.f14.roundToZehntelnoten());
		assertEquals(NotenFractions.f15, NotenFractions.f15.roundToZehntelnoten());
		assertEquals(NotenFractions.f16, NotenFractions.f16.roundToZehntelnoten());
		assertEquals(NotenFractions.f17, NotenFractions.f17.roundToZehntelnoten());
		assertEquals(NotenFractions.f18, NotenFractions.f18.roundToZehntelnoten());
		assertEquals(NotenFractions.f19, NotenFractions.f19.roundToZehntelnoten());
		assertEquals(NotenFractions.f20, NotenFractions.f20.roundToZehntelnoten());
		assertEquals(NotenFractions.f21, NotenFractions.f21.roundToZehntelnoten());
		assertEquals(NotenFractions.f22, NotenFractions.f22.roundToZehntelnoten());
		assertEquals(NotenFractions.f23, NotenFractions.f23.roundToZehntelnoten());
		assertEquals(NotenFractions.f24, NotenFractions.f24.roundToZehntelnoten());
		assertEquals(NotenFractions.f25, NotenFractions.f25.roundToZehntelnoten());
		assertEquals(NotenFractions.f26, NotenFractions.f26.roundToZehntelnoten());
		assertEquals(NotenFractions.f27, NotenFractions.f27.roundToZehntelnoten());
		assertEquals(NotenFractions.f28, NotenFractions.f28.roundToZehntelnoten());
		assertEquals(NotenFractions.f29, NotenFractions.f29.roundToZehntelnoten());
		assertEquals(NotenFractions.f30, NotenFractions.f30.roundToZehntelnoten());
		assertEquals(NotenFractions.f31, NotenFractions.f31.roundToZehntelnoten());
		assertEquals(NotenFractions.f32, NotenFractions.f32.roundToZehntelnoten());
		assertEquals(NotenFractions.f33, NotenFractions.f33.roundToZehntelnoten());
		assertEquals(NotenFractions.f34, NotenFractions.f34.roundToZehntelnoten());
		assertEquals(NotenFractions.f35, NotenFractions.f35.roundToZehntelnoten());
		assertEquals(NotenFractions.f36, NotenFractions.f36.roundToZehntelnoten());
		assertEquals(NotenFractions.f37, NotenFractions.f37.roundToZehntelnoten());
		assertEquals(NotenFractions.f38, NotenFractions.f38.roundToZehntelnoten());
		assertEquals(NotenFractions.f39, NotenFractions.f39.roundToZehntelnoten());
		assertEquals(NotenFractions.f40, NotenFractions.f40.roundToZehntelnoten());
	}

	@Test
	public void testGrenzfaelle() {
		assertEquals(NotenFractions.f10, f105.roundToZehntelnoten());
		assertEquals(NotenFractions.f11, f115.roundToZehntelnoten());
		assertEquals(NotenFractions.f12, f125.roundToZehntelnoten());
		assertEquals(NotenFractions.f13, f135.roundToZehntelnoten());
		assertEquals(NotenFractions.f14, f145.roundToZehntelnoten());
		assertEquals(NotenFractions.f15, f155.roundToZehntelnoten());
		assertEquals(NotenFractions.f16, f165.roundToZehntelnoten());
		assertEquals(NotenFractions.f17, f175.roundToZehntelnoten());
		assertEquals(NotenFractions.f18, f185.roundToZehntelnoten());
		assertEquals(NotenFractions.f19, f195.roundToZehntelnoten());
		assertEquals(NotenFractions.f20, f205.roundToZehntelnoten());
		assertEquals(NotenFractions.f21, f215.roundToZehntelnoten());
		assertEquals(NotenFractions.f22, f225.roundToZehntelnoten());
		assertEquals(NotenFractions.f23, f235.roundToZehntelnoten());
		assertEquals(NotenFractions.f24, f245.roundToZehntelnoten());
		assertEquals(NotenFractions.f25, f255.roundToZehntelnoten());
		assertEquals(NotenFractions.f26, f265.roundToZehntelnoten());
		assertEquals(NotenFractions.f27, f275.roundToZehntelnoten());
		assertEquals(NotenFractions.f28, f285.roundToZehntelnoten());
		assertEquals(NotenFractions.f29, f295.roundToZehntelnoten());
		assertEquals(NotenFractions.f30, f305.roundToZehntelnoten());
		assertEquals(NotenFractions.f31, f315.roundToZehntelnoten());
		assertEquals(NotenFractions.f32, f325.roundToZehntelnoten());
		assertEquals(NotenFractions.f33, f335.roundToZehntelnoten());
		assertEquals(NotenFractions.f34, f345.roundToZehntelnoten());
		assertEquals(NotenFractions.f35, f355.roundToZehntelnoten());
		assertEquals(NotenFractions.f36, f365.roundToZehntelnoten());
		assertEquals(NotenFractions.f37, f375.roundToZehntelnoten());
		assertEquals(NotenFractions.f38, f385.roundToZehntelnoten());
		assertEquals(NotenFractions.f39, f395.roundToZehntelnoten());
	}

}
