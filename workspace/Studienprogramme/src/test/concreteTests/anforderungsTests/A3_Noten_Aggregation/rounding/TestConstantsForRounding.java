package test.concreteTests.anforderungsTests.A3_Noten_Aggregation.rounding;

import common.Fraction;

public class TestConstantsForRounding {


    static final Fraction f105 = Fraction.parse("1,05");
    static final Fraction f115 = Fraction.parse("1,15");
    static final Fraction f125 = Fraction.parse("1,25");
    static final Fraction f135 = Fraction.parse("1,35");
    static final Fraction f145 = Fraction.parse("1,45");
    static final Fraction f155 = Fraction.parse("1,55");
    static final Fraction f165 = Fraction.parse("1,65");
    static final Fraction f175 = Fraction.parse("1,75");
    static final Fraction f185 = Fraction.parse("1,85");
    static final Fraction f195 = Fraction.parse("1,95");
    static final Fraction f205 = Fraction.parse("2,05");
    static final Fraction f215 = Fraction.parse("2,15");
    static final Fraction f225 = Fraction.parse("2,25");
    static final Fraction f235 = Fraction.parse("2,35");
    static final Fraction f245 = Fraction.parse("2,45");
    static final Fraction f255 = Fraction.parse("2,55");
    static final Fraction f265 = Fraction.parse("2,65");
    static final Fraction f275 = Fraction.parse("2,75");
    static final Fraction f285 = Fraction.parse("2,85");
    static final Fraction f295 = Fraction.parse("2,95");
    static final Fraction f305 = Fraction.parse("3,05");
    static final Fraction f315 = Fraction.parse("3,15");
    static final Fraction f325 = Fraction.parse("3,25");
    static final Fraction f335 = Fraction.parse("3,35");
    static final Fraction f345 = Fraction.parse("3,45");
    static final Fraction f355 = Fraction.parse("3,55");
    static final Fraction f365 = Fraction.parse("3,65");
    static final Fraction f375 = Fraction.parse("3,75");
    static final Fraction f385 = Fraction.parse("3,85");
    static final Fraction f395 = Fraction.parse("3,95");
}
