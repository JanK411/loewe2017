package test.concreteTests.anforderungsTests.A3_Noten_Aggregation;

import common.Fraction;
import common.Literals;
import common.util.HFW415Creator;
import model.*;
import model.visitor.ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import java.sql.Date;
import java.time.Instant;

import static org.junit.Assert.assertEquals;
import static test.utils.TestUtils.getUniqueName;

public class A3_Noten_Aggregation_Caching extends GojaTest {
    private static final String neue_note = "neue note";

    private StudyProgram_SL4Public program;
    private BinaryAtomicModule_SL4Public binary_atomic_module;
    private ModuleGroup_SL4Public grundlagen_und_ueberfachliche_qualifikationen;
    private NotAtomicModule_SL4Public not_atomic_module_soko;
    private Unit_SL4Public unit_in_soko;
    private TripleAtomicModule_SL4Public mathe_1;

    @Override
    protected void beforeMethod() throws Exception {
        //bauen von HFW415
        StudyProgram_PL4Public program_pl = programManager.createStudyProgram("program");
        HFW415Creator.createWirtschaftsInformatik(adminService, program_pl);
        HFW415Creator.createGrundlagen(adminService, program_pl);
        HFW415Creator.createBetriebswirtschaft(adminService, program_pl);
        HFW415Creator.createPraktischeInformatik(adminService, program_pl);
        // atomicModules caching testen, deshalb auch ein paar atomate module in das studyprogram
        adminService.getProgramManager().createAtomicModule(program_pl, "atomicBnBModule", CreditPointsManager.getTheCreditPointsManager().getCP(Fraction.parse("5")), bestandenNichtBestanden);

        StudentGroup4Public studentGroup = studentManager.createStudentGroup(program_pl, getUniqueName());
        Student4Public student = studentManager.createStudent(studentGroup, getUniqueName(), getUniqueName(), new Date(Instant.now().toEpochMilli()));
        studentManager.makeGroupActive(studentGroup);

        //Das Studienprogramm f�r genau den einen Studenten
        program = student.getStudyPrograms().findFirst(prog -> true);
        binary_atomic_module = (BinaryAtomicModule_SL4Public) program.getContainees().findFirst(containee -> containee.getClassId() == BinaryAtomicModule_SL.getTypeId());
        grundlagen_und_ueberfachliche_qualifikationen = (ModuleGroup_SL4Public) program.getContainees().findFirst(containee -> containee.getName().equals("Grundlagen und ueberfachliche Qualifikationen"));
        not_atomic_module_soko = ((NotAtomicModule_SL4Public) grundlagen_und_ueberfachliche_qualifikationen.getContainees().findFirst(containee -> containee.getName().equals("Grundlagen sozialer Kompetenz")));
        unit_in_soko = not_atomic_module_soko.getUnits().findFirst(unit -> true);
        mathe_1 = ((TripleAtomicModule_SL4Public) grundlagen_und_ueberfachliche_qualifikationen.getContainees().findFirst(containee -> containee.getName().equals("Mathematik 1")));
    }

    @Test
    public void testGradeCaching() throws UserException, PersistenceException {
        //anfangs d�rfte noch keine Note existieren
        assertEquals(NoGrade.getTheNoGrade(), program.getCalculatedStudentGrade());
        //das Programm hat jetzt cached Grades
        assertEquals(GradeCachedState_SL.getTypeId(), program.getState().getClassId());

        testCorrectGrades();

        //eine Noten verteilen
        studentManager.assignGrade(mathe_1, tg10.getThetg10(), neue_note);
        testCorrectGrades();
        studentManager.assignGrade(binary_atomic_module, NotPassed.getTheNotPassed(), neue_note);
        testCorrectGrades();
        studentManager.assignGrade(unit_in_soko, tg40.getThetg40(), neue_note);
        testCorrectGrades();

        //jetzt auch Noten �berschreiben
        studentManager.assignGrade(binary_atomic_module, Passed.getThePassed(), neue_note);
        testCorrectGrades();
        studentManager.assignGrade(mathe_1, tg30.getThetg30(), neue_note);
        testCorrectGrades();
        studentManager.assignGrade(unit_in_soko, tg23.getThetg23(), neue_note);
        testCorrectGrades();


    }

    private void testCorrectGrades() throws PersistenceException {
        //alle Notenberechnungen sollten sowohl mit als auch ohne cache korrekt sein
        assertEquals(program.calculateGrade(), program.getCalculatedStudentGrade());
        assertEquals(binary_atomic_module.calculateGrade(), binary_atomic_module.getCalculatedStudentGrade());
        assertEquals(grundlagen_und_ueberfachliche_qualifikationen.calculateGrade(), grundlagen_und_ueberfachliche_qualifikationen.getCalculatedStudentGrade());
        assertEquals(not_atomic_module_soko.calculateGrade(), not_atomic_module_soko.getCalculatedStudentGrade());
        assertEquals(unit_in_soko.calculateGrade(), unit_in_soko.getCalculatedStudentGrade());
        assertEquals(mathe_1.calculateGrade(), mathe_1.getCalculatedStudentGrade());

        //das ganze 2x, weil am anfang vielleicht nicht cached, aber jetzt
        assertEquals(program.calculateGrade(), program.getCalculatedStudentGrade());
        assertEquals(binary_atomic_module.calculateGrade(), binary_atomic_module.getCalculatedStudentGrade());
        assertEquals(grundlagen_und_ueberfachliche_qualifikationen.calculateGrade(), grundlagen_und_ueberfachliche_qualifikationen.getCalculatedStudentGrade());
        assertEquals(not_atomic_module_soko.calculateGrade(), not_atomic_module_soko.getCalculatedStudentGrade());
        assertEquals(unit_in_soko.calculateGrade(), unit_in_soko.getCalculatedStudentGrade());
        assertEquals(mathe_1.calculateGrade(), mathe_1.getCalculatedStudentGrade());
    }

    @Override
    protected void afterMethod() throws Exception {

    }
}
