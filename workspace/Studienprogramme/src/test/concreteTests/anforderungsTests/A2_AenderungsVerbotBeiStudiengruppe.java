package test.concreteTests.anforderungsTests;

import common.Fraction;
import common.util.HFW415Creator;
import model.*;
import model.visitor.ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor;
import org.junit.Assert;
import org.junit.Test;
import persistence.*;
import test.utils.AssertThrows;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class A2_AenderungsVerbotBeiStudiengruppe extends GojaTest {


    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void testIfStudyProgramPLisNotEditableAnymore() throws Exception {
        // Program mit den Grundlagen aus hfw415 erstellen.
        ModuleGroup_PL4Public grundlagen = HFW415Creator.createGrundlagen(adminService, program);
        programManager.addModulesOrModuleGroups(program, toList(grundlagen));

        testIfEverythingInsideIsEditable(program);

        StudentGroup4Public hfw415 = studentManager.createStudentGroup(program, getUniqueName());

        testIfEverythingInsideIsNotEditable(program);

    }

    @Test
    public void testTryToEditNotEditableProgram() throws Exception {
        NotAtomicModule_PL4Public notAtomicModule = programManager.createModule(program, getUniqueName());
        Unit_PL4Public unit = programManager.createUnit(notAtomicModule, getUniqueName(), cp1);

        AtomicModule_PL4Public atomicModule = programManager.createAtomicModule(program, getUniqueName(), cp1, drittelNoten);
        ModuleGroup_PL4Public moduleGroup = programManager.createModuleGroup(program, getUniqueName());


        StudentGroup4Public hfw415 = studentManager.createStudentGroup(program, getUniqueName());


        AssertThrows.assertThrows(StateException.class, () -> programManager.createUnit(notAtomicModule, getUniqueName(), cp1));
        AssertThrows.assertThrows(StateException.class, () -> programManager.addModulesOrModuleGroups(program, toList(notAtomicModule)));
        AssertThrows.assertThrows(StateException.class, () -> programManager.addModulesOrModuleGroups(moduleGroup, toList(notAtomicModule)));


        AssertThrows.assertThrows(StateException.class, () -> programManager.changeCreditPoints(atomicModule, f5));
        AssertThrows.assertThrows(StateException.class, () -> programManager.changeCreditPoints(unit, f5));

    }

    private void testIfEverythingInsideIsEditable(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public thing) throws PersistenceException, UserException {
        this.testIfEverythingInsideIsEditableOrIneditable(thing, true);
    }

    private void testIfEverythingInsideIsNotEditable(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public thing) throws PersistenceException, UserException {
        this.testIfEverythingInsideIsEditableOrIneditable(thing, false);
    }

    /**
     * Diese Methode untersucht ob 'thing' vollkommen ineditable ist. D.h. handelt es sich um eine Modulgruppe eine NichtAtomareUnit oder ein Studienprogramm,
     * so werden auch alle Bestandteile dieses Objekts auf ineditable überprüft.
     */
    private void testIfEverythingInsideIsEditableOrIneditable(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public thing, boolean editable) throws PersistenceException, UserException {
        Assert.assertEquals(thing.getEditable().value(), editable);
        thing.accept(new ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<UserException>() {
            @Override
            public void handleStudyProgram_PL(StudyProgram_PL4Public studyProgram_PL) throws PersistenceException, UserException {
                studyProgram_PL.getContainees().applyToAllException(containee -> testIfEverythingInsideIsEditableOrIneditable(containee, editable));
            }

            @Override
            public void handleModuleGroup_PL(ModuleGroup_PL4Public moduleGroup_PL) throws PersistenceException, UserException {
                moduleGroup_PL.getContainees().applyToAllException(containee -> testIfEverythingInsideIsEditableOrIneditable(containee, editable));
            }

            @Override
            public void handleBinaryAtomicModule_PL(BinaryAtomicModule_PL4Public binaryAtomicModule_PL) throws PersistenceException {
            }

            @Override
            public void handleTripleAtomicModule_PL(TripleAtomicModule_PL4Public tripleAtomicModule_PL) throws PersistenceException {
            }

            @Override
            public void handleNotAtomicModule_PL(NotAtomicModule_PL4Public notAtomicModule_PL) throws PersistenceException, UserException {
                notAtomicModule_PL.getUnits().applyToAllException(unit -> testIfEverythingInsideIsEditableOrIneditable(unit, editable));
            }

            @Override
            public void handleUnit_PL(Unit_PL4Public unit_PL) throws PersistenceException {
            }

            @Override
            public void handleCONCCreditPointHavingElement_PL(CONCCreditPointHavingElement_PL4Public cONCCreditPointHavingElement_PL) throws PersistenceException, UserException {
                throw new Error("");
            }

            @Override
            public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCModuleOrModuleGroup_PL(CONCModuleOrModuleGroup_PL4Public cONCModuleOrModuleGroup_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCModuleGroupOrStudyProgram_PL(CONCModuleGroupOrStudyProgram_PL4Public cONCModuleGroupOrStudyProgram_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCAtomicModule_PL(CONCAtomicModule_PL4Public cONCAtomicModule_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCModule_PL(CONCModule_PL4Public cONCModule_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleNoProgram_PL(NoProgram_PL4Public noProgram_PL) throws PersistenceException, UserException {
                throw new Error("");
            }
        });
    }

}
