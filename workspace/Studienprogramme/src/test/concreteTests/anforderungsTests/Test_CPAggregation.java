package test.concreteTests.anforderungsTests;

import common.Fraction;
import common.util.HFW415Creator;
import model.*;
import model.visitor.ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import javax.management.Query;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertEquals;
import static test.utils.TestUtils.getUniqueName;

public class Test_CPAggregation extends GojaTest {
    private CreditPointsManager4Public cpManager;

    @Override
    protected void beforeMethod() throws Exception {
        cpManager = CreditPointsManager.getTheCreditPointsManager();
    }

    @Override
    protected void afterMethod() throws Exception {
    }

    @Test
    public void testAggregateUnitsInMod() throws Exception {
        NotAtomicModule_PL4Public mod1 = programManager.createModule(program, getUniqueName());
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("2")));
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("3")));
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("6")));

        assertEquals(Fraction.parse("11"), mod1.getCalculatedCP().getValue());

        program.addContainees(toList(mod1));
        assertEquals(Fraction.parse("11"), program.getCalculatedCP().getValue());
    }

    @Test
    public void testAggregateAtomicmodulesInModulegroup() throws Exception {
        ModuleGroup_PL4Public modgroup1 = programManager.createModuleGroup(program, getUniqueName());
        AtomicModule_PL4Public atom1 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("2")), drittelNoten);
        AtomicModule_PL4Public atom2 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("3")), drittelNoten);
        AtomicModule_PL4Public atom3 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("6")), bestandenNichtBestanden);
        AtomicModule_PL4Public atom4 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("12")), bestandenNichtBestanden);
        modgroup1.addContainees(toList(atom1, atom2, atom3, atom4));

        assertEquals(Fraction.parse("23"), modgroup1.getCalculatedCP().getValue());

        program.addContainees(toList(modgroup1));
        assertEquals(Fraction.parse("23"), program.getCalculatedCP().getValue());
    }

    @Test
    public void testAggregateModulegroupWithOnlyAtomicModulesInModulegroup() throws Exception {
        ModuleGroup_PL4Public modgroup1 = programManager.createModuleGroup(program, getUniqueName());
        AtomicModule_PL4Public atom1 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("2")), drittelNoten);
        AtomicModule_PL4Public atom3 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("6")), bestandenNichtBestanden);

        ModuleGroup_PL4Public modgroup2 = programManager.createModuleGroup(program, getUniqueName());
        AtomicModule_PL4Public atom2 = programManager.createAtomicModule(modgroup2, getUniqueName(), cpManager.getCP(Fraction.parse("3")), drittelNoten);
        AtomicModule_PL4Public atom4 = programManager.createAtomicModule(modgroup2, getUniqueName(), cpManager.getCP(Fraction.parse("12")), bestandenNichtBestanden);

        ModuleGroup_PL4Public modgroup3 = programManager.createModuleGroup(programManager.createStudyProgram(getUniqueName()), getUniqueName());
        modgroup3.addContainees(toList(modgroup1, modgroup2));

        assertEquals(Fraction.parse("23"), modgroup3.getCalculatedCP().getValue());

    }

    @Test
    public void testAggregateModulegroupWithOnlyNotAtomicModulesInModulegroup() throws Exception {
        ModuleGroup_PL4Public modgroup1 = programManager.createModuleGroup(program, getUniqueName());
        NotAtomicModule_PL4Public mod1 = programManager.createModule(modgroup1, getUniqueName());
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("2")));
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("3")));

        NotAtomicModule_PL4Public mod2 = programManager.createModule(modgroup1, getUniqueName());
        programManager.createUnit(mod2, getUniqueName(), cpManager.getCP(Fraction.parse("6")));
        programManager.createUnit(mod2, getUniqueName(), cpManager.getCP(Fraction.parse("12")));

        assertEquals(Fraction.parse("23"), modgroup1.getCalculatedCP().getValue());

        ModuleGroup_PL4Public modgroup2 = programManager.createModuleGroup(program, getUniqueName());
        NotAtomicModule_PL4Public mod3 = programManager.createModule(modgroup2, getUniqueName());
        programManager.createUnit(mod3, getUniqueName(), cpManager.getCP(Fraction.parse("2")));
        programManager.createUnit(mod3, getUniqueName(), cpManager.getCP(Fraction.parse("3")));

        NotAtomicModule_PL4Public mod4 = programManager.createModule(modgroup2, getUniqueName());
        programManager.createUnit(mod4, getUniqueName(), cpManager.getCP(Fraction.parse("6")));
        programManager.createUnit(mod4, getUniqueName(), cpManager.getCP(Fraction.parse("12")));

        assertEquals(Fraction.parse("23"), modgroup2.getCalculatedCP().getValue());

        ModuleGroup_PL4Public modgroup3 = programManager.createModuleGroup(programManager.createStudyProgram(getUniqueName()), getUniqueName());
        programManager.addModulesOrModuleGroups(modgroup3, toList(modgroup1, modgroup2));
        assertEquals(Fraction.parse("46"), modgroup3.getCalculatedCP().getValue());

    }

    @Test
    public void testAggregateModulegroupWithMixedModulesInModulegroup() throws Exception {
        ModuleGroup_PL4Public modgroup1 = programManager.createModuleGroup(program, getUniqueName());
        NotAtomicModule_PL4Public mod1 = programManager.createModule(modgroup1, getUniqueName());
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("2")));
        programManager.createUnit(mod1, getUniqueName(), cpManager.getCP(Fraction.parse("3")));

        AtomicModule_PL4Public atom1 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("2")), drittelNoten);
        AtomicModule_PL4Public atom2 = programManager.createAtomicModule(modgroup1, getUniqueName(), cpManager.getCP(Fraction.parse("6")), bestandenNichtBestanden);

        assertEquals(Fraction.parse("13"), modgroup1.getCalculatedCP().getValue());

        ModuleGroup_PL4Public modgroup2 = programManager.createModuleGroup(program, getUniqueName());
        NotAtomicModule_PL4Public mod2 = programManager.createModule(modgroup2, getUniqueName());
        programManager.createUnit(mod2, getUniqueName(), cpManager.getCP(Fraction.parse("2")));
        programManager.createUnit(mod2, getUniqueName(), cpManager.getCP(Fraction.parse("3")));

        AtomicModule_PL4Public atom3 = programManager.createAtomicModule(modgroup2, getUniqueName(), cpManager.getCP(Fraction.parse("3")), drittelNoten);
        AtomicModule_PL4Public atom4 = programManager.createAtomicModule(modgroup2, getUniqueName(), cpManager.getCP(Fraction.parse("12")), bestandenNichtBestanden);
        modgroup2.addContainees(toList(mod2, atom3, atom4));

        assertEquals(Fraction.parse("20"), modgroup2.getCalculatedCP().getValue());

        ModuleGroup_PL4Public modgroup3 = programManager.createModuleGroup(programManager.createStudyProgram(getUniqueName()), getUniqueName());
        modgroup3.addContainees(toList(modgroup1, modgroup2));

        assertEquals(Fraction.parse("33"), modgroup3.getCalculatedCP().getValue());
    }

    @Test
    public void testCpCaching() throws UserException, PersistenceException {
        //bauen von HFW415
        StudyProgram_PL4Public theProgram = adminService.getProgramManager().createStudyProgram("theProgram");
        ModuleGroup_PL4Public wirtschaftsInformatik = HFW415Creator.createWirtschaftsInformatik(adminService, theProgram);
        ModuleGroup_PL4Public grundlagen = HFW415Creator.createGrundlagen(adminService, theProgram);
        ModuleGroup_PL4Public betriebswirtschaft = HFW415Creator.createBetriebswirtschaft(adminService, theProgram);
        ModuleGroup_PL4Public praktischeInformatik = HFW415Creator.createPraktischeInformatik(adminService, theProgram);
        // atomicModules caching testen, deshalb auch ein paar atomate module in das studyprogram
        ModuleGroup_PL4Public atomicModuleContainer = adminService.getProgramManager().createModuleGroup(theProgram, "atomicModuleContainer");
        AtomicModule_PL4Public atomicBnBModule = adminService.getProgramManager().createAtomicModule(atomicModuleContainer, "atomicBnBModule", cpManager.getCP(Fraction.parse("5")), bestandenNichtBestanden);
        AtomicModule_PL4Public atomicDrittelModule = adminService.getProgramManager().createAtomicModule(atomicModuleContainer, "atomicDrittelModule", cpManager.getCP(Fraction.parse("5")), drittelNoten);

        //Creditpoints sollten noch nicht gecached sein
        testCachedStateRecursively(theProgram, NothingCachedState_PL.getTypeId());

        //Creditpoints testen
        assertEquals(Fraction.parse("152"), theProgram.getCalculatedCP().getValue());
        //Creditpoints sollten jetzt gecached sein
        testCachedStateRecursively(theProgram, CreditPointsCachedState_PL.getTypeId());
        //weitere CP testen
        assertEquals(Fraction.parse("18"), wirtschaftsInformatik.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("41"), grundlagen.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("42"), betriebswirtschaft.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("41"), praktischeInformatik.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("10"), atomicModuleContainer.getCalculatedCP().getValue());
        assertEquals(theProgram.getCalculatedCP().getValue(), theProgram.getContainees().aggregate(Fraction.Null, (cp, containee) -> cp.add(containee.getCalculatedCP().getValue())));

        //creditpoints in Database Programming �ndern
        NotAtomicModule_PL4Public datenbanken1 = (NotAtomicModule_PL4Public) praktischeInformatik.getContainees().findFirst(containee -> containee.getName().equals("Datenbanken 1"));
        Unit_PL4Public databaseProgramming = (Unit_PL4Public) datenbanken1.getUnits().findFirst(unit -> unit.getName().equals("Database Programming"));
        databaseProgramming.changeInitialCP(f5);

        //alles �ber Database-Programming sollte nun nicht mehr gecached sein, die anderen Sachen allerdings schon
        assertEquals(NothingCachedState_PL.getTypeId(), databaseProgramming.getState().getClassId());
        assertEquals(CreditPointsCachedState_PL.getTypeId(), datenbanken1.getUnits().findFirst(unit -> !unit.getName().equals("Datenbanken 1")).getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), datenbanken1.getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), praktischeInformatik.getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), theProgram.getState().getClassId());
        testCachedStateRecursively(wirtschaftsInformatik, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(betriebswirtschaft, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(grundlagen, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(atomicModuleContainer, CreditPointsCachedState_PL.getTypeId());

        //es gibt jetzt einen cp1 mehr im Compositum
        assertEquals(Fraction.parse("153"), theProgram.getCalculatedCP().getValue());
        //Creditpoints sollten jetzt gecached sein
        testCachedStateRecursively(theProgram, CreditPointsCachedState_PL.getTypeId());
        //weitere CP testen
        assertEquals(theProgram.getCalculatedCP().getValue(), theProgram.getContainees().aggregate(Fraction.Null, (cp, containee) -> cp.add(containee.getCalculatedCP().getValue())));
        assertEquals(Fraction.parse("18"), wirtschaftsInformatik.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("41"), grundlagen.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("42"), betriebswirtschaft.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("42"), praktischeInformatik.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("10"), atomicModuleContainer.getCalculatedCP().getValue());

        //l�schen der Unit (-5cp insgesamt)
        adminService.getProgramManager().removeUnit(databaseProgramming);
        assertEquals(Fraction.parse("37"), praktischeInformatik.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("148"), theProgram.getCalculatedCP().getValue());

        // �ndern von CP in den AtomicModules
        atomicBnBModule.changeInitialCP(f6);
        atomicDrittelModule.changeInitialCP(f9);

        // sollte jetzt nicht mehr cached sein
        testCachedStateRecursively(atomicModuleContainer, NothingCachedState_PL.getTypeId());

        // jetzt ist alles wieder cached
        assertEquals(Fraction.parse("15"), atomicModuleContainer.getCalculatedCP().getValue());
        testCachedStateRecursively(atomicModuleContainer, CreditPointsCachedState_PL.getTypeId());

        // caching sollte jetzt verloren gegangen sein f�r atomicModuleContainer und dar�ber beim erstellen eines neuen Modules in einer Gruppe
        AtomicModule_PL4Public drittesAtomicModule = adminService.getProgramManager().createAtomicModule(atomicModuleContainer, "drittesAtomicModule", cpManager.getCP(Fraction.parse("5")), bestandenNichtBestanden);
        assertEquals(CreditPointsCachedState_PL.getTypeId(), atomicBnBModule.getState().getClassId());
        assertEquals(CreditPointsCachedState_PL.getTypeId(), atomicDrittelModule.getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), drittesAtomicModule.getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), atomicModuleContainer.getState().getClassId());
        assertEquals(NothingCachedState_PL.getTypeId(), theProgram.getState().getClassId());
        testCachedStateRecursively(wirtschaftsInformatik, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(betriebswirtschaft, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(grundlagen, CreditPointsCachedState_PL.getTypeId());
        testCachedStateRecursively(praktischeInformatik, CreditPointsCachedState_PL.getTypeId());

        // jetzt ist bestenfalls alles wieder cached und die Werte stimmen
        assertEquals(Fraction.parse("20"), atomicModuleContainer.getCalculatedCP().getValue());
        assertEquals(Fraction.parse("158"), theProgram.getCalculatedCP().getValue());
        testCachedStateRecursively(theProgram, CreditPointsCachedState_PL.getTypeId());

        //TODO Jan/ Jeremy.. vielleicht :') module entfernen implementierten, testen und schauen, ob caching tut wie es soll
    }

    private void testCachedStateRecursively(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public element, long typeId) throws PersistenceException {
        assertEquals(typeId, element.getState().getClassId());
        element.accept(new ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor() {
            @Override
            public void handleModuleGroup_PL(ModuleGroup_PL4Public moduleGroup_PL) throws PersistenceException {
                moduleGroup_PL.getContainees().applyToAll(containee -> testCachedStateRecursively(containee, typeId));
            }

            @Override
            public void handleStudyProgram_PL(StudyProgram_PL4Public studyProgram_PL) throws PersistenceException {
                studyProgram_PL.getContainees().applyToAll(containee -> testCachedStateRecursively(containee, typeId));
            }

            @Override
            public void handleCONCModule_PL(CONCModule_PL4Public cONCModule_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleNotAtomicModule_PL(NotAtomicModule_PL4Public notAtomicModule_PL) throws PersistenceException {
                notAtomicModule_PL.getUnits().applyToAll(unit -> testCachedStateRecursively(unit, typeId));
            }

            @Override
            public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCModuleOrModuleGroup_PL(CONCModuleOrModuleGroup_PL4Public cONCModuleOrModuleGroup_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleCONCCreditPointHavingElement_PL(CONCCreditPointHavingElement_PL4Public cONCCreditPointHavingElement_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleUnit_PL(Unit_PL4Public unit_PL) throws PersistenceException {
                //nothing
            }

            @Override
            public void handleBinaryAtomicModule_PL(BinaryAtomicModule_PL4Public binaryAtomicModule_PL) throws PersistenceException {
                //nothing
            }

            @Override
            public void handleCONCAtomicModule_PL(CONCAtomicModule_PL4Public cONCAtomicModule_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleTripleAtomicModule_PL(TripleAtomicModule_PL4Public tripleAtomicModule_PL) throws PersistenceException {
                //nothing
            }

            @Override
            public void handleCONCModuleGroupOrStudyProgram_PL(CONCModuleGroupOrStudyProgram_PL4Public cONCModuleGroupOrStudyProgram_PL) throws PersistenceException {
                throw new Error("");
            }

            @Override
            public void handleNoProgram_PL(NoProgram_PL4Public noProgram_PL) throws PersistenceException {
                throw new Error("");
            }
        });
    }


}