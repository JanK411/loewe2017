package test.concreteTests.anforderungsTests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import test.concreteTests.anforderungsTests.A3_Noten_Aggregation.A3_Noten_Aggregation_All;

@RunWith(Suite.class)
@Suite.SuiteClasses({A1_PaarweiseVerschiedenheit.class, A2_AenderungsVerbotBeiStudiengruppe.class, A3_Noten_Aggregation_All.class,
        A4_Notenhistorie.class, A5_NoDuplicateElementsInStudyPrograms.class, Test_CPAggregation.class})
/*
 * Diese Klasse führt sequentiell alle definierten Testklassen aus.
 */
public final class AllAnforderungsTests {
}
