package test.concreteTests.anforderungsTests;

import common.util.HFW415Creator;
import model.*;
import org.junit.Test;
import persistence.*;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getActiveStudyProgramFromStudent;
import static test.utils.TestUtils.getUniqueName;

public class A4_Notenhistorie extends GojaTest {

    private Student4Public student;
    private ModuleGroup_SL4Public moduleGroup;
    private NotAtomicModule_SL4Public notAtomicModule;
    private Unit_SL4Public unit;
    private TripleAtomicModule_SL4Public tripleAtomicModule;
    private BinaryAtomicModule_SL4Public binaryAtomicModule;

    @Override
    protected void beforeMethod() throws Exception {
        StudyProgram_PL4Public studyProgram = adminService.getProgramManager().createStudyProgram(getUniqueName());
        ModuleGroup_PL4Public praktischeInformatik = HFW415Creator.createPraktischeInformatik(adminService, studyProgram);
        HFW415Creator.createWirtschaftsInformatik(adminService, studyProgram);
        HFW415Creator.createGrundlagen(adminService, studyProgram);
        HFW415Creator.createBetriebswirtschaft(adminService, studyProgram);
        AtomicModule_PL4Public binaryatomicmodule = adminService.getProgramManager().createAtomicModule(praktischeInformatik, getUniqueName(), cp1, bestandenNichtBestanden);

        StudentGroup4Public hfw415 = adminService.getStudentManager().createStudentGroup(studyProgram, getUniqueName());
        HFW415Creator.addDevStudentsToGroup(adminService, hfw415);
        adminService.getStudentManager().makeGroupActive(hfw415);

        // TODO Das Ermitteln des studenten ist echt schlecht gel�st hier
        student = hfw415.getStudents().iterator().next().getWrappedStudent();
        // TODO dieses class id zeug finde ich auch nicht sch�n. Bitte fixxen
        // TODO wieso genau wird hier hfw415 ben�tigt? Inwiefern wird das genutzt?
        moduleGroup = (ModuleGroup_SL4Public) getActiveStudyProgramFromStudent(student).getContainees().findFirst(containee -> containee.getClassId() == ModuleGroup_SL.getTypeId() && containee.getName().equals(HFW415Creator.ModuleGroupNameInformatik));
        notAtomicModule = ((NotAtomicModule_SL4Public) moduleGroup.getContainees().findFirst(containee -> containee.getClassId() == NotAtomicModule_SL.getTypeId()));
        unit = notAtomicModule.getUnits().findFirst(unit -> true);
        tripleAtomicModule = ((TripleAtomicModule_SL4Public) moduleGroup.getContainees().findFirst(containee -> containee.getClassId() == TripleAtomicModule_SL.getTypeId()));
        binaryAtomicModule = ((BinaryAtomicModule_SL4Public) moduleGroup.getContainees().findFirst(containee -> containee.getClassId() == BinaryAtomicModule_SL.getTypeId()));

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    @Test
    public void testNotenEingabeUnit() throws PersistenceException, InterruptedException {
        adminService.getStudentManager().assignGrade(unit, tg13.getThetg13(), commentForNewGrade);
        assertEquals(tg13.getThetg13(), unit.getCurrentGrade());
        adminService.getStudentManager().assignGrade(unit, tg17.getThetg17(), commentForNewGrade);
        assertEquals(tg17.getThetg17(), unit.getCurrentGrade());
    }


    @Test
    public void testNotenEingabeTripleAtomicModule() throws PersistenceException {
        adminService.getStudentManager().assignGrade(tripleAtomicModule, tg20.getThetg20(), commentForNewGrade);
        assertEquals(tg20.getThetg20(), tripleAtomicModule.getCurrentGrade());

        adminService.getStudentManager().assignGrade(tripleAtomicModule, tg23.getThetg23(), commentForNewGrade);
        assertEquals(tg23.getThetg23(), tripleAtomicModule.getCurrentGrade());
    }


    @Test
    public void testNotenEingabeBinaryAtomicModule() throws PersistenceException {
        adminService.getStudentManager().assignGrade(binaryAtomicModule, Passed.getThePassed(), commentForNewGrade);
        assertEquals(Passed.getThePassed(), binaryAtomicModule.getCurrentGrade());
    }


}





