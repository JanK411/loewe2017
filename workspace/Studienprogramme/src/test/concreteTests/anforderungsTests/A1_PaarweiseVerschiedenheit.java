package test.concreteTests.anforderungsTests;

import model.NameAlreadyExistsException;
import org.junit.Test;
import persistence.NotAtomicModule_PL4Public;
import test.utils.AssertThrows;
import test.GojaTest;

import static org.junit.Assert.assertTrue;
import static test.utils.TestUtils.getUniqueName;

public class A1_PaarweiseVerschiedenheit extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {

    }

    @Override
    protected void afterMethod() throws Exception {

    }

    /**
     * Dieser Testfall testet die Anforderung, dass die Namen folgender Objekte paarweise verschieden sein müssen:
     * - Module(Atomar und nicht atomar),Studienprogramme,Modulgruppen und Units.
     * Wird trotzdem probiert eines dieser Objekte mit einem "belegten" Namen zu erstellen, so wird eine NameAlreadyExistsException erwartet.
     */
    @Test
    public void testCheckForUniqueNames() throws Exception {
        String sameName = getUniqueName();
        // Als erstes muss es ein Objekt geben, welches den Namen "belegt".
        NotAtomicModule_PL4Public namensBelegerModul = programManager.createModule(program, sameName);

        // Studienprogramme mit dem Namen dürfen nicht erstellt werden.
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createStudyProgram(sameName));
        // Nicht atomare Module mit dem Namen dürfen nicht erstellt werden.
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createModule(program, sameName));
        // Atomare Module mit dem Namen dürfen nicht erstellt werden.
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createAtomicModule(program, sameName, cp1, drittelNoten));
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createAtomicModule(program, sameName, cp1, bestandenNichtBestanden));
        // Modulgruppen mit dem Namen dürfen nicht erstellt werden.
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createModuleGroup(program, sameName));
        // Units mit dem Namen dürfen nicht erstellt werden.
        AssertThrows.assertThrows(NameAlreadyExistsException.class, () -> programManager.createUnit(namensBelegerModul, sameName, cp1));
    }
}

