package test.concreteTests.anforderungsTests;

import model.AlreadyInListException;
import org.junit.Assert;
import org.junit.Test;
import persistence.AtomicModule_PL4Public;
import persistence.ModuleGroup_PL4Public;
import persistence.NotAtomicModule_PL4Public;
import test.utils.AssertThrows;
import test.GojaTest;

import static common.util.ListUtils.toList;
import static test.utils.TestUtils.getUniqueName;

public class A5_NoDuplicateElementsInStudyPrograms extends GojaTest {
    @Override
    protected void beforeMethod() throws Exception {
    }

    @Override
    protected void afterMethod() throws Exception {
    }

    /**
     * Es wird versucht ein Atomares Modul/Modulgruppe/Nicht atomares Modul mehrfach in ein Studienprogramm hinzuzufügen.
     */
    @Test
    public void testNoDoubleModulesAndModuleGroupsInAProgramm() throws Exception {

        final ModuleGroup_PL4Public doubleGroup = programManager.createModuleGroup(program, getUniqueName());
        final AtomicModule_PL4Public doubleAtomicModule = programManager.createAtomicModule(program, getUniqueName(), cp1, drittelNoten);
        final NotAtomicModule_PL4Public doubleNotAtmomicModule = programManager.createModule(program, getUniqueName());

        // Beim Zweiten hinzufügen muss eine Exception ausgelöst werden (Auch wenn alles in die doubleGroup eingefügt wird, so existieren die Objekte bereits eine Ebene höher im Programm)
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(doubleGroup, toList(doubleGroup)));
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(doubleGroup, toList(doubleAtomicModule)));
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(doubleGroup, toList(doubleNotAtmomicModule)));
    }

    /**
     * Dieser Testfall testet die Operation AddContainees in der Klasse ModuleOrModulegroupManager.
     * Es wird versucht ein Atomares Modul/Modulgruppe/Nicht atomares Modul mehrfach in ein Studienprogramm hinzuzufügen.
     * Diese Versuche müssen mit AlreadyInListExceptions bestraft werden.
     */
    @Test
    public void testModuleImplicitelyDoubleInProgramIsNotAllowed() throws Exception {
        final ModuleGroup_PL4Public group1 = programManager.createModuleGroup(program, getUniqueName());
        final ModuleGroup_PL4Public group2 = programManager.createModuleGroup(program, getUniqueName());

        program.addContainees(toList(group1));
        program.addContainees(toList(group2));
        final AtomicModule_PL4Public doppelModul = programManager.createAtomicModule(group1, getUniqueName(), cp1, drittelNoten);

        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(program, toList(doppelModul)));
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(group2, toList(doppelModul)));
    }


    /**
     * Dieser Testfall testet die Operation AddContainees in der Klasse ModuleOrModulegroupManager.
     * Es wird versucht reflexiv und transitiv einen Zyklus zu erstellen.
     * Diese Versuche müssen mit CycleExceptions bestraft werden.
     */
    @Test
    public void testForCycles() throws Exception {
        //Modulgruppe zu Studienprogramm erstellen
        ModuleGroup_PL4Public modulgruppe1 = programManager.createModuleGroup(program, getUniqueName());
        // Reflexiver Zyklus muss erkannt und mit einer CycleException bestraft werden.
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(modulgruppe1, toList(modulgruppe1)));
        // Modulgruppe2 erstellen
        ModuleGroup_PL4Public modulgruppe2 = programManager.createModuleGroup(modulgruppe1, getUniqueName());
        // Modulgruppe1 muss nun Modulgruppe2 enthalten
        Assert.assertNotNull(modulgruppe1.getContainees().findFirst(x -> x.equals(modulgruppe2)));
        // Modulgruppe3 erstellen
        ModuleGroup_PL4Public modulgruppe3 = programManager.createModuleGroup(modulgruppe2, getUniqueName());
        // Nun enthält Modulgruppe2 direkt Modulgruppe3
        Assert.assertNotNull(modulgruppe2.getContainees().findFirst(x -> x.equals(modulgruppe3)));
        // TODO ein BIld hierzu malen ??? Ja nein ?
        // Modulgruppe 3 darf nicht mehr Modulgruppe1 enthalten (transitiver Zyklus Modulgruppe1 enthält Modulgruppe3 über Modulgruppe2) --> Exception
        AssertThrows.assertThrows(AlreadyInListException.class, () -> programManager.addModulesOrModuleGroups(modulgruppe3, toList(modulgruppe1)));
    }

}
