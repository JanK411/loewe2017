
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NothingCachedState_PL extends PersistentObject implements PersistentNothingCachedState_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static NothingCachedState_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade.getClass(objectId);
        return (NothingCachedState_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static NothingCachedState_PL4Public createNothingCachedState_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement) throws PersistenceException{
        return createNothingCachedState_PL(cachingElement,false);
    }
    
    public static NothingCachedState_PL4Public createNothingCachedState_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement,boolean delayed$Persistence) throws PersistenceException {
        PersistentNothingCachedState_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade
                .newDelayedNothingCachedState_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade
                .newNothingCachedState_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static NothingCachedState_PL4Public createNothingCachedState_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement,boolean delayed$Persistence,NothingCachedState_PL4Public This) throws PersistenceException {
        PersistentNothingCachedState_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade
                .newDelayedNothingCachedState_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade
                .newNothingCachedState_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCState_PL = (AbstractPersistentRoot)this.getMyCONCState_PL();
            if (myCONCState_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCState_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, true, true);
                result.put("myCONCState_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public NothingCachedState_PL provideCopy() throws PersistenceException{
        NothingCachedState_PL result = this;
        result = new NothingCachedState_PL(this.getId());
        result.This = this.This;
        result.myCONCState_PL = this.myCONCState_PL;
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentNothingCachedState_PL This;
    protected PersistentState_PL myCONCState_PL;
    
    public NothingCachedState_PL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -302;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -302) ConnectionHandler.getTheConnectionHandler().theNothingCachedState_PLFacade
            .newNothingCachedState_PL(this.getId());
        super.store();
        
    }
    
    protected void setThis(PersistentNothingCachedState_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentNothingCachedState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentState_PL getMyCONCState_PL() throws PersistenceException {
        return this.myCONCState_PL;
    }
    public void setMyCONCState_PL(PersistentState_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCState_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCState_PL = (PersistentState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentNothingCachedState_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNothingCachedState_PL result = (PersistentNothingCachedState_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNothingCachedState_PL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getCachingElement() throws PersistenceException {
        return ((PersistentState_PL)this.getMyCONCState_PL()).getCachingElement();
    }
    public void setCachingElement(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentState_PL)this.getMyCONCState_PL()).setCachingElement(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCState_PL().delete$Me();
    }
    
    public void accept(State_PLVisitor visitor) throws PersistenceException {
        visitor.handleNothingCachedState_PL(this);
    }
    public <R> R accept(State_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNothingCachedState_PL(this);
    }
    public <E extends model.UserException>  void accept(State_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNothingCachedState_PL(this);
    }
    public <R, E extends model.UserException> R accept(State_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNothingCachedState_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNothingCachedState_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNothingCachedState_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNothingCachedState_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNothingCachedState_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getCreditPoints() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNothingCachedState_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCState_PL myCONCState_PL = (PersistentCONCState_PL) model.CONCState_PL.createCONCState_PL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"), this.isDelayed$Persistence(), (PersistentNothingCachedState_PL)This);
			this.setMyCONCState_PL(myCONCState_PL);
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public CreditPoints4Public getCreditPoints() 
				throws PersistenceException{
        CreditPoints4Public creditpoints = getThis().getCachingElement().calculateCP();
        getThis().getCachingElement().setState(CreditPointsCachedState_PL.createCreditPointsCachedState_PL(creditpoints, getThis().getCachingElement()));
        return creditpoints;
    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
