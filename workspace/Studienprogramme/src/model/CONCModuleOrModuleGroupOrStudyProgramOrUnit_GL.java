
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL extends model.ModuleOrModuleGroupOrStudyProgramOrUnit_GL implements PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL{
    
    
    public static CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public This) throws PersistenceException {
        PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade
                .newDelayedCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade
                .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL provideCopy() throws PersistenceException{
        CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL result = this;
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this.instanceOf, 
                                                                    this.This, 
                                                                    this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL instanceOf,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)instanceOf,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)This,id);        
    }
    
    static public long getTypeId() {
        return 105;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 105) ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade
            .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL result = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL)this.This;
    }
    
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL)This);
		if(this.isTheSameAs(This)){
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getActiveState();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
