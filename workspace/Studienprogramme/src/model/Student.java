
package model;

import persistence.*;
import model.visitor.*;
import view.objects.NoProgram_SL;
import view.objects.StudentWrapper;

import java.sql.Timestamp;
import java.time.Instant;


/* Additional import section end */

public class Student extends PersistentObject implements PersistentStudent{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Student4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudentFacade.getClass(objectId);
        return (Student4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static Student4Public createStudent(String firstName,String name,java.sql.Date dateOfBirth) throws PersistenceException{
        return createStudent(firstName,name,dateOfBirth,false);
    }
    
    public static Student4Public createStudent(String firstName,String name,java.sql.Date dateOfBirth,boolean delayed$Persistence) throws PersistenceException {
        if (firstName == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if (name == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentStudent result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentFacade
                .newDelayedStudent(firstName,name,dateOfBirth);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentFacade
                .newStudent(firstName,name,dateOfBirth,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("firstName", firstName);
        final$$Fields.put("name", name);
        final$$Fields.put("dateOfBirth", dateOfBirth);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static Student4Public createStudent(String firstName,String name,java.sql.Date dateOfBirth,boolean delayed$Persistence,Student4Public This) throws PersistenceException {
        if (firstName == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if (name == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentStudent result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentFacade
                .newDelayedStudent(firstName,name,dateOfBirth);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentFacade
                .newStudent(firstName,name,dateOfBirth,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("firstName", firstName);
        final$$Fields.put("name", name);
        final$$Fields.put("dateOfBirth", dateOfBirth);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("firstName", this.getFirstName());
            result.put("name", this.getName());
            result.put("dateOfBirth", this.getDateOfBirth());
            result.put("matNr", this.getMatNr());
            result.put("studyPrograms", this.getStudyPrograms().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            result.put("wrappers", this.getWrappers().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
        }
        return result;
    }
    
    public static StudentSearchList getStudentByFirstName(String firstName) throws PersistenceException{
        return ConnectionHandler.getTheConnectionHandler().theStudentFacade
            .getStudentByFirstName(firstName);
    }
    
    public static StudentSearchList getStudentByName(String name) throws PersistenceException{
        return ConnectionHandler.getTheConnectionHandler().theStudentFacade
            .getStudentByName(name);
    }
    
    public Student provideCopy() throws PersistenceException{
        Student result = this;
        result = new Student(this.firstName, 
                             this.name, 
                             this.dateOfBirth, 
                             this.This, 
                             this.getId());
        result.studyPrograms = this.studyPrograms.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected String firstName;
    protected String name;
    protected java.sql.Date dateOfBirth;
    protected Student_StudyProgramsProxi studyPrograms;
    protected PersistentStudent This;
    
    public Student(String firstName,String name,java.sql.Date dateOfBirth,PersistentStudent This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.firstName = firstName;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.studyPrograms = new Student_StudyProgramsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 139;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 139) ConnectionHandler.getTheConnectionHandler().theStudentFacade
            .newStudent(firstName,name,dateOfBirth,this.getId());
        super.store();
        this.getStudyPrograms().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudentFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public String getFirstName() throws PersistenceException {
        return this.firstName;
    }
    public void setFirstName(String newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theStudentFacade.firstNameSet(this.getId(), newValue);
        this.firstName = newValue;
    }
    public String getName() throws PersistenceException {
        return this.name;
    }
    public void setName(String newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theStudentFacade.nameSet(this.getId(), newValue);
        this.name = newValue;
    }
    public java.sql.Date getDateOfBirth() throws PersistenceException {
        return this.dateOfBirth;
    }
    public void setDateOfBirth(java.sql.Date newValue) throws PersistenceException {
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theStudentFacade.dateOfBirthSet(this.getId(), newValue);
        this.dateOfBirth = newValue;
    }
    public Student_StudyProgramsProxi getStudyPrograms() throws PersistenceException {
        return this.studyPrograms;
    }
    protected void setThis(PersistentStudent newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudent)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentStudent getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudent result = (PersistentStudent)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudent)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudent(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudent(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudent(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudent(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getStudyPrograms().getLength() > 0) return 1;
        return 0;
    }
    
    
    public StudentWrapperSearchList getWrappers() 
				throws PersistenceException{
        StudentWrapperSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
										.inverseGetWrappedStudent(getThis().getId(), getThis().getClassId());
		return result;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudent)This);
		if(this.isTheSameAs(This)){
			this.setFirstName((String)final$$Fields.get("firstName"));
			this.setName((String)final$$Fields.get("name"));
			this.setDateOfBirth((java.sql.Date)final$$Fields.get("dateOfBirth"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addStudyProgram(final StudyProgram_SL4Public studyProgram) 
				throws PersistenceException{
        getThis().getStudyPrograms().add(studyProgram);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createStudyProgramWithGrades(final StudyProgram_GL4Public programm) 
				throws PersistenceException{
        StudyProgram_SL4Public studyProgram = StudyProgram_SL.createStudyProgram_SL(programm);
        programm.populateProgramSL(studyProgram);
        getThis().addStudyProgram(studyProgram);
    }
    public String getMatNr() 
				throws PersistenceException{
        return String.valueOf(getThis().getId());
    }
    public Bool4Public hasActiveStudentGroup() 
				throws PersistenceException{
        StudentWrapper4Public activeWrapper = getThis().getWrappers().findFirst(wrapper -> wrapper.getStudentGroup().getActiveState().equals(Active.getTheActive()));
        return activeWrapper != null ? Verum.getTheVerum() : Falsum.getTheFalsum();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        Server4Public server = Server.createServer("password", getThis().getMatNr(), 0, Timestamp.from(Instant.now()), false);
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        ServerSearchList server = Server.getServerByUser(getThis().getMatNr());
    }
    public StudentWrapper4Public wrap() 
				throws PersistenceException{
        return model.StudentWrapper.createStudentWrapper(getThis());
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
