
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NoStudentGroup extends model.StudentGroup implements PersistentNoStudentGroup{
    
    private static NoStudentGroup4Public theNoStudentGroup = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static NoStudentGroup4Public getTheNoStudentGroup() throws PersistenceException{
        if (theNoStudentGroup == null || reset$For$Test){
            if (reset$For$Test) theNoStudentGroup = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            NoStudentGroup4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theNoStudentGroupFacade.getTheNoStudentGroup();
                            theNoStudentGroup = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                NoStudentGroup4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theNoStudentGroup== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theNoStudentGroup;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theNoStudentGroup;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public NoStudentGroup provideCopy() throws PersistenceException{
        NoStudentGroup result = this;
        result = new NoStudentGroup(this.name, 
                                    this.studyProgram, 
                                    this.activeState, 
                                    this.This, 
                                    this.getId());
        result.students = this.students.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public NoStudentGroup(String name,PersistentStudyProgram_GL studyProgram,PersistentActiveState activeState,PersistentStudentGroup This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((String)name,(PersistentStudyProgram_GL)studyProgram,(PersistentActiveState)activeState,(PersistentStudentGroup)This,id);        
    }
    
    static public long getTypeId() {
        return 146;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentNoStudentGroup getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNoStudentGroup result = (PersistentNoStudentGroup)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNoStudentGroup)this.This;
    }
    
    public void accept(StudentGroupVisitor visitor) throws PersistenceException {
        visitor.handleNoStudentGroup(this);
    }
    public <R> R accept(StudentGroupReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoStudentGroup(this);
    }
    public <E extends model.UserException>  void accept(StudentGroupExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoStudentGroup(this);
    }
    public <R, E extends model.UserException> R accept(StudentGroupReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoStudentGroup(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNoStudentGroup(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoStudentGroup(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoStudentGroup(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoStudentGroup(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getStudyProgram() != null) return 1;
        if (this.getActiveState() != null) return 1;
        if (this.getStudents().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNoStudentGroup)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
