
package model;

import common.Fraction;
import common.Literals;
import model.visitor.ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor;
import model.visitor.State_SLReturnVisitor;
import persistence.*;


/* Additional import section end */

public abstract class ModuleOrModuleGroupOrStudyProgramOrUnit_SL extends PersistentObject implements PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade.getClass(objectId);
        return (ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("alreadyEarnedCP", this.alreadyEarnedCPAsString());
            AbstractPersistentRoot state = (AbstractPersistentRoot)this.getState();
            if (state != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    state, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, true, false);
                result.put("state", proxiInformation);
                
            }
            result.put("name", this.getName());
            result.put("cp", this.cpAsString());
            AbstractPersistentRoot calculatedStudentGrade = (AbstractPersistentRoot)this.getCalculatedStudentGrade();
            if (calculatedStudentGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    calculatedStudentGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("calculatedStudentGrade", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleOrModuleGroupOrStudyProgramOrUnit_SL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL instanceOf;
    protected PersistentState_SL state;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL This;
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL instanceOf,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.instanceOf = instanceOf;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return -198;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(this.getInstanceOf() != null){
            this.getInstanceOf().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade.instanceOfSet(this.getId(), getInstanceOf());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return this.instanceOf;
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.instanceOf)) return;
        if(getThis().getInstanceOf() != null)throw new PersistenceException("Final field instanceOf in type ModuleOrModuleGroupOrStudyProgramOrUnit_SL has been set already!",0);
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.instanceOf = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade.instanceOfSet(this.getId(), newValue);
        }
    }
    public State_SL4Public getState() throws PersistenceException {
        return this.state;
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.state)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.state = (PersistentState_SL)PersistentProxi.createProxi(objectId, classId);
    }
    protected void setThis(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getThis() throws PersistenceException ;
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)This);
		if(this.isTheSameAs(This)){
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().setState(NothingCachedState_SL.createNothingCachedState_SL(getThis()));
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        GradeAggregator4Public aggregator = GradeAggregator.createGradeAggregator(true);
        getThis().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
        Fraction sumAsFraction = aggregator.sumOfAllCPs();
        CreditPoints4Public cp = CreditPointsManager.getTheCreditPointsManager().getCP(sumAsFraction);
        return cp;
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getState().getGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getInstanceOf().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getInstanceOf().getName();
    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().setState(getThis().getState().accept(new State_SLReturnVisitor<State_SL4Public>() {
            @Override
            public State_SL4Public handleCONCState_SL(CONCState_SL4Public cONCState_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public State_SL4Public handleGradeCachedState_SL(GradeCachedState_SL4Public gradeCachedState_SL) throws PersistenceException {
                return NothingCachedState_SL.createNothingCachedState_SL(getThis());
            }

            @Override
            public State_SL4Public handleNothingCachedState_SL(NothingCachedState_SL4Public nothingCachedState_SL) throws PersistenceException {
                return NothingCachedState_SL.createNothingCachedState_SL(getThis());
            }
        }));
        //hier wird entschieden, ob nach oben genotified werden muss...
        getThis().accept(new ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor() {
            @Override
            public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public cONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleCONCCreditPointHavingElement_SL(CONCCreditPointHavingElement_SL4Public cONCCreditPointHavingElement_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleUnit_SL(Unit_SL4Public unit_SL) throws PersistenceException {
                unit_SL.getContainingModule().updateGradeCache();
            }

            @Override
            public void handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
                binaryAtomicModule_SL.getContainingModuleGroupOrStudyProgram().updateGradeCache();
            }

            @Override
            public void handleCONCModuleGroupOrStudyProgram_SL(CONCModuleGroupOrStudyProgram_SL4Public cONCModuleGroupOrStudyProgram_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
                moduleGroup_SL.getContainingModuleGroupOrStudyProgram().updateGradeCache();
            }

            @Override
            public void handleStudyProgram_SL(StudyProgram_SL4Public studyProgram_SL) throws PersistenceException {
                //hier muss nichts getan werden, da das StudyProgram nicht in irgendetwas enthalten ist und somit niemandem Rechenschaft schuldet
            }

            @Override
            public void handleCONCModuleOrModuleGroup_SL(CONCModuleOrModuleGroup_SL4Public cONCModuleOrModuleGroup_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleCONCModule_SL(CONCModule_SL4Public cONCModule_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
                notAtomicModule_SL.getContainingModuleGroupOrStudyProgram().updateGradeCache();
            }

            @Override
            public void handleCONCAtomicModule_SL(CONCAtomicModule_SL4Public cONCAtomicModule_SL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public void handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
                tripleAtomicModule_SL.getContainingModuleGroupOrStudyProgram().updateGradeCache();
            }

            @Override
            public void handleNoProgram_SL(NoProgram_SL4Public noProgram_SL) throws PersistenceException {
                throw new Error("Das NoProgram wird hier wohl auch niemals auftreten.");
            }
        });
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
