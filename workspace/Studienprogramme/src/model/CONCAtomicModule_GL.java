
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCAtomicModule_GL extends model.AtomicModule_GL implements PersistentCONCAtomicModule_GL{
    
    
    public static CONCAtomicModule_GL4Public createCONCAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP,boolean delayed$Persistence,CONCAtomicModule_GL4Public This) throws PersistenceException {
        PersistentCONCAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_GLFacade
                .newDelayedCONCAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_GLFacade
                .newCONCAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        final$$Fields.put("studentGroupCP", studentGroupCP);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCAtomicModule_GL provideCopy() throws PersistenceException{
        CONCAtomicModule_GL result = this;
        result = new CONCAtomicModule_GL(this.This, 
                                         this.myCONCModule_GL, 
                                         this.myCONCCreditPointHavingElement_GL, 
                                         this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCAtomicModule_GL(PersistentAtomicModule_GL This,PersistentModule_GL myCONCModule_GL,PersistentCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentAtomicModule_GL)This,(PersistentModule_GL)myCONCModule_GL,(PersistentCreditPointHavingElement_GL)myCONCCreditPointHavingElement_GL,id);        
    }
    
    static public long getTypeId() {
        return 120;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 120) ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_GLFacade
            .newCONCAtomicModule_GL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCAtomicModule_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCAtomicModule_GL result = (PersistentCONCAtomicModule_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCAtomicModule_GL)this.This;
    }
    
    public void accept(AtomicModule_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(AtomicModule_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public void accept(CreditPointHavingElement_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(CreditPointHavingElement_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public void accept(Module_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(Module_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(Module_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(Module_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCAtomicModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL = (PersistentCONCCreditPointHavingElement_GL) model.CONCCreditPointHavingElement_GL.createCONCCreditPointHavingElement_GL((PersistentCreditPoints)final$$Fields.get("studentGroupCP"), (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_GL)This);
			PersistentCONCModule_GL myCONCModule_GL = (PersistentCONCModule_GL) model.CONCModule_GL.createCONCModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_GL)This);
			this.setMyCONCCreditPointHavingElement_GL(myCONCCreditPointHavingElement_GL);
			this.setMyCONCModule_GL(myCONCModule_GL);
			myCONCCreditPointHavingElement_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			myCONCModule_GL.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        getThis().createSLEquivalentAndAdd(container);
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getActiveState();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
