
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class GradeHistoryElement extends PersistentObject implements PersistentGradeHistoryElement{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static GradeHistoryElement4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.getClass(objectId);
        return (GradeHistoryElement4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static GradeHistoryElement4Public createGradeHistoryElement(Grade4Public grade,String comment,java.sql.Timestamp timestamp) throws PersistenceException{
        return createGradeHistoryElement(grade,comment,timestamp,false);
    }
    
    public static GradeHistoryElement4Public createGradeHistoryElement(Grade4Public grade,String comment,java.sql.Timestamp timestamp,boolean delayed$Persistence) throws PersistenceException {
        if (comment == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentGradeHistoryElement result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade
                .newDelayedGradeHistoryElement(comment,timestamp);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade
                .newGradeHistoryElement(comment,timestamp,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("grade", grade);
        final$$Fields.put("comment", comment);
        final$$Fields.put("timestamp", timestamp);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getGrade() == null)throw new PersistenceException("Field grade in type GradeHistoryElement has not been initialized!",0);
        return result;
    }
    
    public static GradeHistoryElement4Public createGradeHistoryElement(Grade4Public grade,String comment,java.sql.Timestamp timestamp,boolean delayed$Persistence,GradeHistoryElement4Public This) throws PersistenceException {
        if (comment == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentGradeHistoryElement result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade
                .newDelayedGradeHistoryElement(comment,timestamp);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade
                .newGradeHistoryElement(comment,timestamp,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("grade", grade);
        final$$Fields.put("comment", comment);
        final$$Fields.put("timestamp", timestamp);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot grade = (AbstractPersistentRoot)this.getGrade();
            if (grade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    grade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("grade", proxiInformation);
                
            }
            result.put("comment", this.getComment());
            result.put("timestamp", this.getTimestamp());
        }
        return result;
    }
    
    public GradeHistoryElement provideCopy() throws PersistenceException{
        GradeHistoryElement result = this;
        result = new GradeHistoryElement(this.grade, 
                                         this.comment, 
                                         this.timestamp, 
                                         this.This, 
                                         this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentGrade grade;
    protected String comment;
    protected java.sql.Timestamp timestamp;
    protected PersistentGradeHistoryElement This;
    
    public GradeHistoryElement(PersistentGrade grade,String comment,java.sql.Timestamp timestamp,PersistentGradeHistoryElement This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.grade = grade;
        this.comment = comment;
        this.timestamp = timestamp;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 224;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 224) ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade
            .newGradeHistoryElement(comment,timestamp,this.getId());
        super.store();
        if(this.getGrade() != null){
            this.getGrade().store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.gradeSet(this.getId(), getGrade());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public Grade4Public getGrade() throws PersistenceException {
        return this.grade;
    }
    public void setGrade(Grade4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.grade)) return;
        if(getThis().getGrade() != null)throw new PersistenceException("Final field grade in type GradeHistoryElement has been set already!",0);
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.grade = (PersistentGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.gradeSet(this.getId(), newValue);
        }
    }
    public String getComment() throws PersistenceException {
        return this.comment;
    }
    public void setComment(String newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.commentSet(this.getId(), newValue);
        this.comment = newValue;
    }
    public java.sql.Timestamp getTimestamp() throws PersistenceException {
        return this.timestamp;
    }
    public void setTimestamp(java.sql.Timestamp newValue) throws PersistenceException {
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.timestampSet(this.getId(), newValue);
        this.timestamp = newValue;
    }
    protected void setThis(PersistentGradeHistoryElement newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentGradeHistoryElement)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryElementFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentGradeHistoryElement getThis() throws PersistenceException {
        if(this.This == null){
            PersistentGradeHistoryElement result = (PersistentGradeHistoryElement)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentGradeHistoryElement)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleGradeHistoryElement(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleGradeHistoryElement(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleGradeHistoryElement(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleGradeHistoryElement(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentGradeHistoryElement)This);
		if(this.isTheSameAs(This)){
			this.setGrade((PersistentGrade)final$$Fields.get("grade"));
			this.setComment((String)final$$Fields.get("comment"));
			this.setTimestamp((java.sql.Timestamp)final$$Fields.get("timestamp"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
