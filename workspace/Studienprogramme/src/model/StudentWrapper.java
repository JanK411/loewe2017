
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class StudentWrapper extends PersistentObject implements PersistentStudentWrapper{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static StudentWrapper4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade.getClass(objectId);
        return (StudentWrapper4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static StudentWrapper4Public createStudentWrapper(Student4Public wrappedStudent) throws PersistenceException{
        return createStudentWrapper(wrappedStudent,false);
    }
    
    public static StudentWrapper4Public createStudentWrapper(Student4Public wrappedStudent,boolean delayed$Persistence) throws PersistenceException {
        PersistentStudentWrapper result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
                .newDelayedStudentWrapper();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
                .newStudentWrapper(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("wrappedStudent", wrappedStudent);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static StudentWrapper4Public createStudentWrapper(Student4Public wrappedStudent,boolean delayed$Persistence,StudentWrapper4Public This) throws PersistenceException {
        PersistentStudentWrapper result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
                .newDelayedStudentWrapper();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
                .newStudentWrapper(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("wrappedStudent", wrappedStudent);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot wrappedStudent = (AbstractPersistentRoot)this.getWrappedStudent();
            if (wrappedStudent != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    wrappedStudent, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("wrappedStudent", proxiInformation);
                
            }
            result.put("firstName", this.getFirstName());
            result.put("name", this.getName());
            result.put("dateOfBirth", this.getDateOfBirth());
            result.put("matNr", this.getMatNr());
            AbstractPersistentRoot studentGroup = (AbstractPersistentRoot)this.getStudentGroup();
            if (studentGroup != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    studentGroup, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("studentGroup", proxiInformation);
                
            }
        }
        return result;
    }
    
    public StudentWrapper provideCopy() throws PersistenceException{
        StudentWrapper result = this;
        result = new StudentWrapper(this.wrappedStudent, 
                                    this.This, 
                                    this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentStudent wrappedStudent;
    protected PersistentStudentWrapper This;
    
    public StudentWrapper(PersistentStudent wrappedStudent,PersistentStudentWrapper This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.wrappedStudent = wrappedStudent;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 272;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 272) ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade
            .newStudentWrapper(this.getId());
        super.store();
        if(this.getWrappedStudent() != null){
            this.getWrappedStudent().store();
            ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade.wrappedStudentSet(this.getId(), getWrappedStudent());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public Student4Public getWrappedStudent() throws PersistenceException {
        return this.wrappedStudent;
    }
    public void setWrappedStudent(Student4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.wrappedStudent)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.wrappedStudent = (PersistentStudent)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade.wrappedStudentSet(this.getId(), newValue);
        }
    }
    protected void setThis(PersistentStudentWrapper newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudentWrapper)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentWrapperFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentStudentWrapper getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudentWrapper result = (PersistentStudentWrapper)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudentWrapper)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudentWrapper(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudentWrapper(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudentWrapper(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudentWrapper(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getWrappedStudent() != null && this.getWrappedStudent().getTheObject().getLeafInfo() != 0) return 1;
        return 0;
    }
    
    
    public StudentGroup4Public getStudentGroup() 
				throws PersistenceException{
        StudentGroupSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
										.inverseGetStudents(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudentWrapper)This);
		if(this.isTheSameAs(This)){
			this.setWrappedStudent((PersistentStudent)final$$Fields.get("wrappedStudent"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public java.sql.Date getDateOfBirth() 
				throws PersistenceException{
        return getThis().getWrappedStudent().getDateOfBirth();
    }
    public String getFirstName() 
				throws PersistenceException{
        return getThis().getWrappedStudent().getFirstName();
    }
    public String getMatNr() 
				throws PersistenceException{
        return getThis().getWrappedStudent().getMatNr();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getWrappedStudent().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
