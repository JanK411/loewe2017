
package model;

import common.Fraction;
import persistence.*;

import java.sql.Timestamp;
import java.time.Instant;


/* Additional import section end */

public abstract class CreditPointHavingElement_SL extends PersistentObject implements PersistentCreditPointHavingElement_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static CreditPointHavingElement_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.getClass(objectId);
        return (CreditPointHavingElement_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot currentGrade = (AbstractPersistentRoot)this.getCurrentGrade();
            if (currentGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    currentGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("currentGrade", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
            if (myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract CreditPointHavingElement_SL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentGradeHistory gradeHistory;
    protected PersistentCreditPointHavingElement_SL This;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;
    
    public CreditPointHavingElement_SL(PersistentGradeHistory gradeHistory,PersistentCreditPointHavingElement_SL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.gradeHistory = gradeHistory;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;        
    }
    
    static public long getTypeId() {
        return 125;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(this.getGradeHistory() != null){
            this.getGradeHistory().store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.gradeHistorySet(this.getId(), getGradeHistory());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() != null){
            this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLSet(this.getId(), getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL());
        }
        
    }
    
    public GradeHistory4Public getGradeHistory() throws PersistenceException {
        return this.gradeHistory;
    }
    public void setGradeHistory(GradeHistory4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.gradeHistory)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.gradeHistory = (PersistentGradeHistory)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.gradeHistorySet(this.getId(), newValue);
        }
    }
    protected void setThis(PersistentCreditPointHavingElement_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentCreditPointHavingElement_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointHavingElement_SLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentCreditPointHavingElement_SL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).setState(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCreditPointHavingElement_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCreditPointHavingElement_SL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().initializeOnCreation();
        getThis().setGradeHistory(GradeHistory.createGradeHistory());
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    /*Gradehistoryelement wird erstellt und in die Historie hinzugef�gt*/
    public void assignGrade(final Grade4Public grade, final String comment) 
				throws PersistenceException{
        GradeHistoryElement4Public gradeHistoryElement = GradeHistoryElement.createGradeHistoryElement(grade, comment, Timestamp.from(Instant.now()));
        getThis().getGradeHistory().addElement(gradeHistoryElement);
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().updateGradeCache();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        aggregator.addElementIfRelevantWithBinaryModule(getThis());
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        aggregator.addElementIfRelevant(getThis());
    }
    public Grade4Public getCurrentGrade() 
				throws PersistenceException{
        return getThis().getGradeHistory().getLastGrade();
    }
    public GradeHistory4Public showHistory() 
				throws PersistenceException{
        return getThis().getGradeHistory();
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
