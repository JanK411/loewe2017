
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CreditPoints extends PersistentObject implements PersistentCreditPoints{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static CreditPoints4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade.getClass(objectId);
        return (CreditPoints4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static CreditPoints4Public createCreditPoints(common.Fraction value) throws PersistenceException{
        return createCreditPoints(value,false);
    }
    
    public static CreditPoints4Public createCreditPoints(common.Fraction value,boolean delayed$Persistence) throws PersistenceException {
        PersistentCreditPoints result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade
                .newDelayedCreditPoints(value);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade
                .newCreditPoints(value,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("value", value);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static CreditPoints4Public createCreditPoints(common.Fraction value,boolean delayed$Persistence,CreditPoints4Public This) throws PersistenceException {
        PersistentCreditPoints result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade
                .newDelayedCreditPoints(value);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade
                .newCreditPoints(value,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("value", value);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("value", this.getValue().toString());
        }
        return result;
    }
    
    public CreditPoints provideCopy() throws PersistenceException{
        CreditPoints result = this;
        result = new CreditPoints(this.value, 
                                  this.This, 
                                  this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected common.Fraction value;
    protected PersistentCreditPoints This;
    
    public CreditPoints(common.Fraction value,PersistentCreditPoints This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.value = value;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 223;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 223) ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade
            .newCreditPoints(value,this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public common.Fraction getValue() throws PersistenceException {
        return this.value;
    }
    public void setValue(common.Fraction newValue) throws PersistenceException {
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade.valueSet(this.getId(), newValue);
        this.value = newValue;
    }
    protected void setThis(PersistentCreditPoints newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentCreditPoints)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointsFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPoints getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCreditPoints result = (PersistentCreditPoints)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCreditPoints)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCreditPoints(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCreditPoints(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCreditPoints(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCreditPoints(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCreditPoints)This);
		if(this.isTheSameAs(This)){
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
