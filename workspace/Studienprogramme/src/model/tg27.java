
package model;

import common.Fraction;
import common.util.NotenFractions;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class tg27 extends model.TripleGrade implements Persistenttg27{
    
    private static tg274Public thetg27 = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static tg274Public getThetg27() throws PersistenceException{
        if (thetg27 == null || reset$For$Test){
            if (reset$For$Test) thetg27 = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            tg274Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().thetg27Facade.getThetg27();
                            thetg27 = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                tg274Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && thetg27== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return thetg27;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return thetg27;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public tg27 provideCopy() throws PersistenceException{
        tg27 result = this;
        result = new tg27(this.This, 
                          this.myCONCDecimalGrade, 
                          this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public tg27(PersistentTripleGrade This,PersistentDecimalGrade myCONCDecimalGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentTripleGrade)This,(PersistentDecimalGrade)myCONCDecimalGrade,id);        
    }
    
    static public long getTypeId() {
        return 222;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public Persistenttg27 getThis() throws PersistenceException {
        if(this.This == null){
            Persistenttg27 result = (Persistenttg27)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (Persistenttg27)this.This;
    }
    
    public void accept(TripleGradeVisitor visitor) throws PersistenceException {
        visitor.handletg27(this);
    }
    public <R> R accept(TripleGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg27(this);
    }
    public <E extends model.UserException>  void accept(TripleGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg27(this);
    }
    public <R, E extends model.UserException> R accept(TripleGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg27(this);
    }
    public void accept(DecimalGradeVisitor visitor) throws PersistenceException {
        visitor.handletg27(this);
    }
    public <R> R accept(DecimalGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg27(this);
    }
    public <E extends model.UserException>  void accept(DecimalGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg27(this);
    }
    public <R, E extends model.UserException> R accept(DecimalGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg27(this);
    }
    public void accept(CalculatableGradeVisitor visitor) throws PersistenceException {
        visitor.handletg27(this);
    }
    public <R> R accept(CalculatableGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg27(this);
    }
    public <E extends model.UserException>  void accept(CalculatableGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg27(this);
    }
    public <R, E extends model.UserException> R accept(CalculatableGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg27(this);
    }
    public void accept(GradeVisitor visitor) throws PersistenceException {
        visitor.handletg27(this);
    }
    public <R> R accept(GradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg27(this);
    }
    public <E extends model.UserException>  void accept(GradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg27(this);
    }
    public <R, E extends model.UserException> R accept(GradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg27(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handletg27(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg27(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg27(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg27(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((Persistenttg27)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (Persistenttg27)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (Persistenttg27)This);
			PersistentCONCDecimalGrade myCONCDecimalGrade = (PersistentCONCDecimalGrade) model.CONCDecimalGrade.createCONCDecimalGrade(common.Fraction.Null, this.isDelayed$Persistence(), (Persistenttg27)This);
			this.setMyCONCDecimalGrade(myCONCDecimalGrade);
			myCONCDecimalGrade.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
        
        
    }
    public common.Fraction fetchGradeValue() 
				throws PersistenceException{
                        return getThis().getMyCONCCalculatableGrade().getValue();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
        getThis().setValue(NotenFractions.f27);

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
		
    }
    public Bool4Public isNotNoGrade() 
				throws PersistenceException{return getThis().getMyCONCCalculatableGrade().isNotNoGrade();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
