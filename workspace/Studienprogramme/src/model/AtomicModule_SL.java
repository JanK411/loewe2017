
package model;

import persistence.*;


/* Additional import section end */

public abstract class AtomicModule_SL extends PersistentObject implements PersistentAtomicModule_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static AtomicModule_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.getClass(objectId);
        return (AtomicModule_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModule_SL = (AbstractPersistentRoot)this.getMyCONCModule_SL();
            if (myCONCModule_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_SL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCCreditPointHavingElement_SL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_SL();
            if (myCONCCreditPointHavingElement_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract AtomicModule_SL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentAtomicModule_SL This;
    protected PersistentModule_SL myCONCModule_SL;
    protected PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL;
    
    public AtomicModule_SL(PersistentAtomicModule_SL This,PersistentModule_SL myCONCModule_SL,PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_SL = myCONCModule_SL;
        this.myCONCCreditPointHavingElement_SL = myCONCCreditPointHavingElement_SL;        
    }
    
    static public long getTypeId() {
        return 217;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_SL() != null){
            this.getMyCONCModule_SL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.myCONCModule_SLSet(this.getId(), getMyCONCModule_SL());
        }
        if(this.getMyCONCCreditPointHavingElement_SL() != null){
            this.getMyCONCCreditPointHavingElement_SL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.myCONCCreditPointHavingElement_SLSet(this.getId(), getMyCONCCreditPointHavingElement_SL());
        }
        
    }
    
    protected void setThis(PersistentAtomicModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentAtomicModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_SL getMyCONCModule_SL() throws PersistenceException {
        return this.myCONCModule_SL;
    }
    public void setMyCONCModule_SL(PersistentModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_SL = (PersistentModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.myCONCModule_SLSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_SL getMyCONCCreditPointHavingElement_SL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_SL;
    }
    public void setMyCONCCreditPointHavingElement_SL(PersistentCreditPointHavingElement_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_SL = (PersistentCreditPointHavingElement_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_SLFacade.myCONCCreditPointHavingElement_SLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentAtomicModule_SL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setState(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public PersistentModuleOrModuleGroup_SL getMyCONCModuleOrModuleGroup_SL() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getMyCONCModuleOrModuleGroup_SL();
    }
    public void setMyCONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setMyCONCModuleOrModuleGroup_SL(newValue);
    }
    public GradeHistory4Public getGradeHistory() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).getGradeHistory();
    }
    public void setGradeHistory(GradeHistory4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setGradeHistory(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_SL().delete$Me();
        this.getMyCONCCreditPointHavingElement_SL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentAtomicModule_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_SL)This);
			PersistentCONCModule_SL myCONCModule_SL = (PersistentCONCModule_SL) model.CONCModule_SL.createCONCModule_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_SL)This);
			PersistentCONCCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL = (PersistentCONCCreditPointHavingElement_SL) model.CONCCreditPointHavingElement_SL.createCONCCreditPointHavingElement_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_SL)This);
			this.setMyCONCCreditPointHavingElement_SL(myCONCCreditPointHavingElement_SL);
			this.setMyCONCModule_SL(myCONCModule_SL);
			myCONCCreditPointHavingElement_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			myCONCModule_SL.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().getCurrentGrade();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getName();
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
