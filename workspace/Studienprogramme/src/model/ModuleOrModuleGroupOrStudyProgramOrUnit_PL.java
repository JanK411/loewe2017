
package model;

import common.Literals;
import model.visitor.State_PLReturnVisitor;
import persistence.*;


/* Additional import section end */

public abstract class ModuleOrModuleGroupOrStudyProgramOrUnit_PL extends PersistentObject implements PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.getClass(objectId);
        return (ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot editable = (AbstractPersistentRoot)this.getEditable();
            if (editable != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    editable, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("editable", proxiInformation);
                
            }
            AbstractPersistentRoot state = (AbstractPersistentRoot)this.getState();
            if (state != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    state, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, true, false);
                result.put("state", proxiInformation);
                
            }
            result.put("name", this.getName());
            AbstractPersistentRoot calculatedCP = (AbstractPersistentRoot)this.getCalculatedCP();
            if (calculatedCP != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    calculatedCP, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("calculatedCP", proxiInformation);
                
            }
        }
        return result;
    }
    
    public static ModuleOrModuleGroupOrStudyProgramOrUnit_PLSearchList getModuleOrModuleGroupOrStudyProgramOrUnit_PLByName(String name) throws PersistenceException{
        return ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade
            .getModuleOrModuleGroupOrStudyProgramOrUnit_PLByName(name);
    }
    
    public abstract ModuleOrModuleGroupOrStudyProgramOrUnit_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentBool editable;
    protected PersistentState_PL state;
    protected String name;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL This;
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentBool editable,String name,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.editable = editable;
        this.name = name;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return -162;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(this.getEditable() != null){
            this.getEditable().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.editableSet(this.getId(), getEditable());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public Bool4Public getEditable() throws PersistenceException {
        return this.editable;
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.editable)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.editable = (PersistentBool)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.editableSet(this.getId(), newValue);
        }
    }
    public State_PL4Public getState() throws PersistenceException {
        return this.state;
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.state)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.state = (PersistentState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public String getName() throws PersistenceException {
        return this.name;
    }
    public void setName(String newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.nameSet(this.getId(), newValue);
        this.name = newValue;
    }
    protected void setThis(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getThis() throws PersistenceException ;
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)This);
		if(this.isTheSameAs(This)){
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().setEditable(Verum.getTheVerum());
        getThis().setState(NothingCachedState_PL.createNothingCachedState_PL(getThis()));
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getState().getCreditPoints();
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().setEditable(Falsum.getTheFalsum());
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        //TODO hier braucht man theoretisch ja keinen Visitor, aber vielleicht wird ja doch irgendwann nochmal was anderes gecached, also vielleicht ja doch lieber einer?
        // sicherheitshalber oder was?
        getThis().setState(getThis().getState().accept(new State_PLReturnVisitor<State_PL4Public>() {

            @Override
            public State_PL4Public handleCONCState_PL(CONCState_PL4Public cONCState_PL) throws PersistenceException {
                throw new Error(Literals.cONCVisitorMessage());
            }

            @Override
            public State_PL4Public handleCreditPointsCachedState_PL(CreditPointsCachedState_PL4Public creditPointsCachedState_PL) throws PersistenceException {
                return NothingCachedState_PL.createNothingCachedState_PL(getThis());
            }

            @Override
            public State_PL4Public handleNothingCachedState_PL(NothingCachedState_PL4Public nothingCachedState_PL) throws PersistenceException {
                return NothingCachedState_PL.createNothingCachedState_PL(getThis());
            }
        }));
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
