
package model;

import common.Fraction;
import persistence.*;
import model.visitor.*;

import java.lang.reflect.Array;
import java.util.Iterator;


/* Additional import section end */

public class CONCModuleGroupOrStudyProgram_PL extends model.ModuleGroupOrStudyProgram_PL implements PersistentCONCModuleGroupOrStudyProgram_PL{
    
    
    public static CONCModuleGroupOrStudyProgram_PL4Public createCONCModuleGroupOrStudyProgram_PL(String name,boolean delayed$Persistence,CONCModuleGroupOrStudyProgram_PL4Public This) throws PersistenceException {
        PersistentCONCModuleGroupOrStudyProgram_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_PLFacade
                .newDelayedCONCModuleGroupOrStudyProgram_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_PLFacade
                .newCONCModuleGroupOrStudyProgram_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleGroupOrStudyProgram_PL provideCopy() throws PersistenceException{
        CONCModuleGroupOrStudyProgram_PL result = this;
        result = new CONCModuleGroupOrStudyProgram_PL(this.This, 
                                                      this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL, 
                                                      this.getId());
        result.containees = this.containees.copy(result);
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCModuleGroupOrStudyProgram_PL(PersistentModuleGroupOrStudyProgram_PL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModuleGroupOrStudyProgram_PL)This,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,id);        
    }
    
    static public long getTypeId() {
        return 101;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 101) ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_PLFacade
            .newCONCModuleGroupOrStudyProgram_PL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleGroupOrStudyProgram_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleGroupOrStudyProgram_PL result = (PersistentCONCModuleGroupOrStudyProgram_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleGroupOrStudyProgram_PL)this.This;
    }
    
    public void accept(ModuleGroupOrStudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleGroupOrStudyProgram_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentCONCModuleGroupOrStudyProgram_PL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public Bool4Public alreadyContainsModuleOrModuleGroup(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        return getThis().alreadyContainsModuleOrModuleGroup(container);
    }
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part);
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        return getThis().containsh_PL(part, visited);
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getCalculatedCP();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().setInEditable();
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy);
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, visited);
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */

    /* End of protected part that is not overridden by persistence generator */
    
}
