
package model;

import persistence.*;


/* Additional import section end */

public abstract class State_PL extends PersistentObject implements PersistentState_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static State_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theState_PLFacade.getClass(objectId);
        return (State_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot creditPoints = (AbstractPersistentRoot)this.getCreditPoints();
            if (creditPoints != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    creditPoints, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("creditPoints", proxiInformation);
                
            }
            AbstractPersistentRoot cachingElement = (AbstractPersistentRoot)this.getCachingElement();
            if (cachingElement != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    cachingElement, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("cachingElement", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract State_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL cachingElement;
    protected PersistentState_PL This;
    
    public State_PL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -301;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        
    }
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getCachingElement() throws PersistenceException {
        return this.cachingElement;
    }
    public void setCachingElement(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.cachingElement)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.cachingElement = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(objectId, classId);
    }
    protected void setThis(PersistentState_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public abstract PersistentState_PL getThis() throws PersistenceException ;
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentState_PL)This);
		if(this.isTheSameAs(This)){
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
