
package model;

import common.Fraction;
import common.util.NotenFractions;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class dg36 extends model.DecimalGrade implements Persistentdg36{
    
    private static dg364Public thedg36 = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static dg364Public getThedg36() throws PersistenceException{
        if (thedg36 == null || reset$For$Test){
            if (reset$For$Test) thedg36 = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            dg364Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().thedg36Facade.getThedg36();
                            thedg36 = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                dg364Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && thedg36== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return thedg36;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return thedg36;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public dg36 provideCopy() throws PersistenceException{
        dg36 result = this;
        result = new dg36(this.This, 
                          this.myCONCCalculatableGrade, 
                          this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public dg36(PersistentDecimalGrade This,PersistentCalculatableGrade myCONCCalculatableGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentDecimalGrade)This,(PersistentCalculatableGrade)myCONCCalculatableGrade,id);        
    }
    
    static public long getTypeId() {
        return 107;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public Persistentdg36 getThis() throws PersistenceException {
        if(this.This == null){
            Persistentdg36 result = (Persistentdg36)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (Persistentdg36)this.This;
    }
    
    public void accept(DecimalGradeVisitor visitor) throws PersistenceException {
        visitor.handledg36(this);
    }
    public <R> R accept(DecimalGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handledg36(this);
    }
    public <E extends model.UserException>  void accept(DecimalGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handledg36(this);
    }
    public <R, E extends model.UserException> R accept(DecimalGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handledg36(this);
    }
    public void accept(CalculatableGradeVisitor visitor) throws PersistenceException {
        visitor.handledg36(this);
    }
    public <R> R accept(CalculatableGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handledg36(this);
    }
    public <E extends model.UserException>  void accept(CalculatableGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handledg36(this);
    }
    public <R, E extends model.UserException> R accept(CalculatableGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handledg36(this);
    }
    public void accept(GradeVisitor visitor) throws PersistenceException {
        visitor.handledg36(this);
    }
    public <R> R accept(GradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handledg36(this);
    }
    public <E extends model.UserException>  void accept(GradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handledg36(this);
    }
    public <R, E extends model.UserException> R accept(GradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handledg36(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handledg36(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handledg36(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handledg36(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handledg36(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((Persistentdg36)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (Persistentdg36)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (Persistentdg36)This);
			this.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{


    }
    public common.Fraction fetchGradeValue() 
				throws PersistenceException{
        return getThis().getMyCONCCalculatableGrade().getValue();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
        getThis().setValue(NotenFractions.f36);
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();

    }
    public Bool4Public isNotNoGrade() 
				throws PersistenceException{
        return getThis().getMyCONCCalculatableGrade().isNotNoGrade();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
