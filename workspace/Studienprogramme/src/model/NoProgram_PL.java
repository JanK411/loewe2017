
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NoProgram_PL extends model.StudyProgram_PL implements PersistentNoProgram_PL{
    
    private static NoProgram_PL4Public theNoProgram_PL = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static NoProgram_PL4Public getTheNoProgram_PL() throws PersistenceException{
        if (theNoProgram_PL == null || reset$For$Test){
            if (reset$For$Test) theNoProgram_PL = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            NoProgram_PL4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theNoProgram_PLFacade.getTheNoProgram_PL();
                            theNoProgram_PL = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                NoProgram_PL4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theNoProgram_PL== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theNoProgram_PL;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theNoProgram_PL;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public NoProgram_PL provideCopy() throws PersistenceException{
        NoProgram_PL result = this;
        result = new NoProgram_PL(this.This, 
                                  this.myCONCModuleGroupOrStudyProgram_PL, 
                                  this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public NoProgram_PL(PersistentStudyProgram_PL This,PersistentModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentStudyProgram_PL)This,(PersistentModuleGroupOrStudyProgram_PL)myCONCModuleGroupOrStudyProgram_PL,id);        
    }
    
    static public long getTypeId() {
        return 271;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentNoProgram_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNoProgram_PL result = (PersistentNoProgram_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNoProgram_PL)this.This;
    }
    
    public void accept(StudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_PL(this);
    }
    public <R> R accept(StudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(StudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(StudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_PL(this);
    }
    public void accept(ModuleGroupOrStudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_PL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNoProgram_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL = (PersistentCONCModuleGroupOrStudyProgram_PL) model.CONCModuleGroupOrStudyProgram_PL.createCONCModuleGroupOrStudyProgram_PL("", this.isDelayed$Persistence(), (PersistentNoProgram_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentNoProgram_PL)This);
			this.setMyCONCModuleGroupOrStudyProgram_PL(myCONCModuleGroupOrStudyProgram_PL);
			myCONCModuleGroupOrStudyProgram_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
