
package model;

import common.Fraction;
import persistence.*;


/* Additional import section end */

public abstract class TripleGrade extends PersistentObject implements PersistentTripleGrade{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static TripleGrade4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theTripleGradeFacade.getClass(objectId);
        return (TripleGrade4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCDecimalGrade = (AbstractPersistentRoot)this.getMyCONCDecimalGrade();
            if (myCONCDecimalGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCDecimalGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCDecimalGrade", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract TripleGrade provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentTripleGrade This;
    protected PersistentDecimalGrade myCONCDecimalGrade;
    
    public TripleGrade(PersistentTripleGrade This,PersistentDecimalGrade myCONCDecimalGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCDecimalGrade = myCONCDecimalGrade;        
    }
    
    static public long getTypeId() {
        return 117;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theTripleGradeFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCDecimalGrade() != null){
            this.getMyCONCDecimalGrade().store();
            ConnectionHandler.getTheConnectionHandler().theTripleGradeFacade.myCONCDecimalGradeSet(this.getId(), getMyCONCDecimalGrade());
        }
        
    }
    
    protected void setThis(PersistentTripleGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentTripleGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theTripleGradeFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentDecimalGrade getMyCONCDecimalGrade() throws PersistenceException {
        return this.myCONCDecimalGrade;
    }
    public void setMyCONCDecimalGrade(PersistentDecimalGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCDecimalGrade)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCDecimalGrade = (PersistentDecimalGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theTripleGradeFacade.myCONCDecimalGradeSet(this.getId(), newValue);
        }
    }
    public abstract PersistentTripleGrade getThis() throws PersistenceException ;
    public common.Fraction getValue() throws PersistenceException {
        return ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).getValue();
    }
    public void setValue(common.Fraction newValue) throws PersistenceException {
        ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).setValue(newValue);
    }
    public PersistentGrade getMyCONCGrade() throws PersistenceException {
        return ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).getMyCONCGrade();
    }
    public void setMyCONCGrade(PersistentGrade newValue) throws PersistenceException {
        ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).setMyCONCGrade(newValue);
    }
    public PersistentCalculatableGrade getMyCONCCalculatableGrade() throws PersistenceException {
        return ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).getMyCONCCalculatableGrade();
    }
    public void setMyCONCCalculatableGrade(PersistentCalculatableGrade newValue) throws PersistenceException {
        ((PersistentDecimalGrade)this.getMyCONCDecimalGrade()).setMyCONCCalculatableGrade(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCDecimalGrade().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentTripleGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (PersistentTripleGrade)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentTripleGrade)This);
			PersistentCONCDecimalGrade myCONCDecimalGrade = (PersistentCONCDecimalGrade) model.CONCDecimalGrade.createCONCDecimalGrade(common.Fraction.Null, this.isDelayed$Persistence(), (PersistentTripleGrade)This);
			this.setMyCONCDecimalGrade(myCONCDecimalGrade);
			myCONCDecimalGrade.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
