
package model;

import model.visitor.*;
import persistence.*;


/* Additional import section end */

public class StudyProgram_PL extends PersistentObject implements PersistentStudyProgram_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static StudyProgram_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade.getClass(objectId);
        return (StudyProgram_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static StudyProgram_PL4Public createStudyProgram_PL(String name) throws PersistenceException{
        return createStudyProgram_PL(name,false);
    }
    
    public static StudyProgram_PL4Public createStudyProgram_PL(String name,boolean delayed$Persistence) throws PersistenceException {
        PersistentStudyProgram_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade
                .newDelayedStudyProgram_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade
                .newStudyProgram_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static StudyProgram_PL4Public createStudyProgram_PL(String name,boolean delayed$Persistence,StudyProgram_PL4Public This) throws PersistenceException {
        PersistentStudyProgram_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade
                .newDelayedStudyProgram_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade
                .newStudyProgram_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleGroupOrStudyProgram_PL = (AbstractPersistentRoot)this.getMyCONCModuleGroupOrStudyProgram_PL();
            if (myCONCModuleGroupOrStudyProgram_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleGroupOrStudyProgram_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleGroupOrStudyProgram_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public StudyProgram_PL provideCopy() throws PersistenceException{
        StudyProgram_PL result = this;
        result = new StudyProgram_PL(this.This, 
                                     this.myCONCModuleGroupOrStudyProgram_PL, 
                                     this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentStudyProgram_PL This;
    protected PersistentModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL;
    
    public StudyProgram_PL(PersistentStudyProgram_PL This,PersistentModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleGroupOrStudyProgram_PL = myCONCModuleGroupOrStudyProgram_PL;        
    }
    
    static public long getTypeId() {
        return 233;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 233) ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade
            .newStudyProgram_PL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleGroupOrStudyProgram_PL() != null){
            this.getMyCONCModuleGroupOrStudyProgram_PL().store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade.myCONCModuleGroupOrStudyProgram_PLSet(this.getId(), getMyCONCModuleGroupOrStudyProgram_PL());
        }
        
    }
    
    protected void setThis(PersistentStudyProgram_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudyProgram_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroupOrStudyProgram_PL getMyCONCModuleGroupOrStudyProgram_PL() throws PersistenceException {
        return this.myCONCModuleGroupOrStudyProgram_PL;
    }
    public void setMyCONCModuleGroupOrStudyProgram_PL(PersistentModuleGroupOrStudyProgram_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleGroupOrStudyProgram_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleGroupOrStudyProgram_PL = (PersistentModuleGroupOrStudyProgram_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_PLFacade.myCONCModuleGroupOrStudyProgram_PLSet(this.getId(), newValue);
        }
    }
    public PersistentStudyProgram_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudyProgram_PL result = (PersistentStudyProgram_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudyProgram_PL)this.This;
    }
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setName(newValue);
    }
    public ModuleGroupOrStudyProgram_PL_ContaineesProxi getContainees() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getContainees();
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleGroupOrStudyProgram_PL().delete$Me();
    }
    
    public void accept(StudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_PL(this);
    }
    public <R> R accept(StudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(StudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(StudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_PL(this);
    }
    public void accept(ModuleGroupOrStudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_PL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part, new java.util.HashSet<h_PLHIERARCHY>());
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        if(getThis().equals(part)) return true;
		if(visited.contains(getThis())) return false;
		java.util.Iterator<ModuleOrModuleGroup_PL4Public> iterator0 = getThis().getContainees().iterator();
		while(iterator0.hasNext())
			if(((h_PLHIERARCHY)iterator0.next()).containsh_PL(part, visited)) return true; 
		visited.add(getThis());
		return false;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudyProgram_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL = (PersistentCONCModuleGroupOrStudyProgram_PL) model.CONCModuleGroupOrStudyProgram_PL.createCONCModuleGroupOrStudyProgram_PL("", this.isDelayed$Persistence(), (PersistentStudyProgram_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentStudyProgram_PL)This);
			this.setMyCONCModuleGroupOrStudyProgram_PL(myCONCModuleGroupOrStudyProgram_PL);
			myCONCModuleGroupOrStudyProgram_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, new java.util.HashMap<h_PLHIERARCHY,T>());
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        if (visited.containsKey(getThis())) return visited.get(getThis());
		T result$$containees$$StudyProgram_PL = strategy.StudyProgram_PL$$containees$$$initialize(getThis());
		java.util.Iterator<?> iterator$$ = getThis().getContainees().iterator();
		while (iterator$$.hasNext()){
			ModuleOrModuleGroup_PL4Public current$$Field = (ModuleOrModuleGroup_PL4Public)iterator$$.next();
			T current$$ = current$$Field.strategyh_PL(strategy, visited);
			result$$containees$$StudyProgram_PL = strategy.StudyProgram_PL$$containees$$consolidate(getThis(), result$$containees$$StudyProgram_PL, current$$);
		}
		T result = strategy.StudyProgram_PL$$finalize(getThis() ,result$$containees$$StudyProgram_PL);
		visited.put(getThis(),result);
		return result;
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addContainees(final ModuleOrModuleGroup_PLSearchList containees) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        this.getMyCONCModuleGroupOrStudyProgram_PL().addContainees(containees);
    }
    public void addContainee(final ModuleOrModuleGroup_PL4Public containee) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        this.getMyCONCModuleGroupOrStudyProgram_PL().addContainee(containee);
    }
    public Bool4Public alreadyContainsModuleOrModuleGroupAndTheirContainees(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCModuleGroupOrStudyProgram_PL().alreadyContainsModuleOrModuleGroupAndTheirContainees(container);
    }
    public Bool4Public alreadyContainsModuleOrModuleGroup(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        if (getThis().containsh_PL(container)) return Verum.getTheVerum();
        return Falsum.getTheFalsum();
    }
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCModuleGroupOrStudyProgram_PL().calculateCP();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().getCalculatedCP();
    }
    public ModuleOrModuleGroup_PLSearchList getOneLayerContainees() 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCModuleGroupOrStudyProgram_PL().getOneLayerContainees();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public void populateProgramGL(final StudyProgram_GL4Public program) 
				throws PersistenceException{
        getThis().getContainees().applyToAll(x -> {
            x.createGlEquivalentAndAdd(program);
        });
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
