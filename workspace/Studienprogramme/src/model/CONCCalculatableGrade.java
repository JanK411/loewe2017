
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCCalculatableGrade extends model.CalculatableGrade implements PersistentCONCCalculatableGrade{
    
    
    public static CONCCalculatableGrade4Public createCONCCalculatableGrade(common.Fraction value,boolean delayed$Persistence,CONCCalculatableGrade4Public This) throws PersistenceException {
        PersistentCONCCalculatableGrade result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCCalculatableGradeFacade
                .newDelayedCONCCalculatableGrade(value);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCCalculatableGradeFacade
                .newCONCCalculatableGrade(value,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("value", value);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCCalculatableGrade provideCopy() throws PersistenceException{
        CONCCalculatableGrade result = this;
        result = new CONCCalculatableGrade(this.value, 
                                           this.This, 
                                           this.myCONCGrade, 
                                           this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCCalculatableGrade(common.Fraction value,PersistentCalculatableGrade This,PersistentGrade myCONCGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((common.Fraction)value,(PersistentCalculatableGrade)This,(PersistentGrade)myCONCGrade,id);        
    }
    
    static public long getTypeId() {
        return 229;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 229) ConnectionHandler.getTheConnectionHandler().theCONCCalculatableGradeFacade
            .newCONCCalculatableGrade(value,this.getId());
        super.store();
        
    }
    
    public PersistentCONCCalculatableGrade getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCCalculatableGrade result = (PersistentCONCCalculatableGrade)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCCalculatableGrade)this.This;
    }
    
    public void accept(CalculatableGradeVisitor visitor) throws PersistenceException {
        visitor.handleCONCCalculatableGrade(this);
    }
    public <R> R accept(CalculatableGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public <E extends model.UserException>  void accept(CalculatableGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCalculatableGrade(this);
    }
    public <R, E extends model.UserException> R accept(CalculatableGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public void accept(GradeVisitor visitor) throws PersistenceException {
        visitor.handleCONCCalculatableGrade(this);
    }
    public <R> R accept(GradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public <E extends model.UserException>  void accept(GradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCalculatableGrade(this);
    }
    public <R, E extends model.UserException> R accept(GradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCCalculatableGrade(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCalculatableGrade(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCalculatableGrade(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCCalculatableGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentCONCCalculatableGrade)This);
			this.setMyCONCGrade(myCONCGrade);
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public common.Fraction fetchGradeValue() 
				throws PersistenceException{
        return getThis().fetchGradeValue();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
