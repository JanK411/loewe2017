
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModuleOrModuleGroup_SL extends model.ModuleOrModuleGroup_SL implements PersistentCONCModuleOrModuleGroup_SL{
    
    
    public static CONCModuleOrModuleGroup_SL4Public createCONCModuleOrModuleGroup_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,CONCModuleOrModuleGroup_SL4Public This) throws PersistenceException {
        PersistentCONCModuleOrModuleGroup_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroup_SLFacade
                .newDelayedCONCModuleOrModuleGroup_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroup_SLFacade
                .newCONCModuleOrModuleGroup_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleOrModuleGroup_SL provideCopy() throws PersistenceException{
        CONCModuleOrModuleGroup_SL result = this;
        result = new CONCModuleOrModuleGroup_SL(this.This, 
                                                this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL, 
                                                this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModuleOrModuleGroup_SL)This,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL,id);        
    }
    
    static public long getTypeId() {
        return 236;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 236) ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroup_SLFacade
            .newCONCModuleOrModuleGroup_SL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleOrModuleGroup_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleOrModuleGroup_SL result = (PersistentCONCModuleOrModuleGroup_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleOrModuleGroup_SL)this.This;
    }
    
    public void accept(ModuleOrModuleGroup_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroup_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleOrModuleGroup_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCModuleOrModuleGroup_SL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().calculateGrade();
    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP();
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getCp();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().updateGradeCache();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
