
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCState_PL extends model.State_PL implements PersistentCONCState_PL{
    
    
    public static CONCState_PL4Public createCONCState_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement,boolean delayed$Persistence,CONCState_PL4Public This) throws PersistenceException {
        PersistentCONCState_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCState_PLFacade
                .newDelayedCONCState_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCState_PLFacade
                .newCONCState_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCState_PL provideCopy() throws PersistenceException{
        CONCState_PL result = this;
        result = new CONCState_PL(this.getId());
        result.cachingElement = this.cachingElement;
        result.This = this.This;
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public CONCState_PL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -297;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -297) ConnectionHandler.getTheConnectionHandler().theCONCState_PLFacade
            .newCONCState_PL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCState_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCState_PL result = (PersistentCONCState_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCState_PL)this.This;
    }
    
    public void accept(State_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCState_PL(this);
    }
    public <R> R accept(State_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCState_PL(this);
    }
    public <E extends model.UserException>  void accept(State_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCState_PL(this);
    }
    public <R, E extends model.UserException> R accept(State_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCState_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCState_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCState_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCState_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCState_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getCreditPoints() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCState_PL)This);
		if(this.isTheSameAs(This)){
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public getCreditPoints() 
				throws PersistenceException{
        return getThis().getCreditPoints();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
