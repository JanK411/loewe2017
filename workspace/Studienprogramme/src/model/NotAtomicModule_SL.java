
package model;

import common.Fraction;
import model.meta.StringFACTORY;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NotAtomicModule_SL extends PersistentObject implements PersistentNotAtomicModule_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static NotAtomicModule_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade.getClass(objectId);
        return (NotAtomicModule_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static NotAtomicModule_SL4Public createNotAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf) throws PersistenceException{
        return createNotAtomicModule_SL(instanceOf,false);
    }
    
    public static NotAtomicModule_SL4Public createNotAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentNotAtomicModule_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
                .newDelayedNotAtomicModule_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
                .newNotAtomicModule_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type NotAtomicModule_SL has not been initialized!",0);
        return result;
    }
    
    public static NotAtomicModule_SL4Public createNotAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,NotAtomicModule_SL4Public This) throws PersistenceException {
        PersistentNotAtomicModule_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
                .newDelayedNotAtomicModule_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
                .newNotAtomicModule_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("units", this.getUnits().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModule_SL = (AbstractPersistentRoot)this.getMyCONCModule_SL();
            if (myCONCModule_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public NotAtomicModule_SL provideCopy() throws PersistenceException{
        NotAtomicModule_SL result = this;
        result = new NotAtomicModule_SL(this.This, 
                                        this.myCONCModule_SL, 
                                        this.getId());
        result.units = this.units.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected NotAtomicModule_SL_UnitsProxi units;
    protected PersistentNotAtomicModule_SL This;
    protected PersistentModule_SL myCONCModule_SL;
    
    public NotAtomicModule_SL(PersistentNotAtomicModule_SL This,PersistentModule_SL myCONCModule_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.units = new NotAtomicModule_SL_UnitsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_SL = myCONCModule_SL;        
    }
    
    static public long getTypeId() {
        return 113;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 113) ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
            .newNotAtomicModule_SL(this.getId());
        super.store();
        this.getUnits().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_SL() != null){
            this.getMyCONCModule_SL().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade.myCONCModule_SLSet(this.getId(), getMyCONCModule_SL());
        }
        
    }
    
    public NotAtomicModule_SL_UnitsProxi getUnits() throws PersistenceException {
        return this.units;
    }
    protected void setThis(PersistentNotAtomicModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentNotAtomicModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_SL getMyCONCModule_SL() throws PersistenceException {
        return this.myCONCModule_SL;
    }
    public void setMyCONCModule_SL(PersistentModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_SL = (PersistentModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade.myCONCModule_SLSet(this.getId(), newValue);
        }
    }
    public PersistentNotAtomicModule_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNotAtomicModule_SL result = (PersistentNotAtomicModule_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNotAtomicModule_SL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setState(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public PersistentModuleOrModuleGroup_SL getMyCONCModuleOrModuleGroup_SL() throws PersistenceException {
        return ((PersistentModule_SL)this.getMyCONCModule_SL()).getMyCONCModuleOrModuleGroup_SL();
    }
    public void setMyCONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL newValue) throws PersistenceException {
        ((PersistentModule_SL)this.getMyCONCModule_SL()).setMyCONCModuleOrModuleGroup_SL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_SL().delete$Me();
    }
    
    public void accept(Module_SLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_SL(this);
    }
    public <R> R accept(Module_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(Module_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(Module_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroup_SLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        if (this.getUnits().getLength() > 0) return 1;
        return 0;
    }
    
    
    public ModuleGroupOrStudyProgram_SL4Public getContainingModuleGroupOrStudyProgram() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_SLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNotAtomicModule_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_SL)This);
			PersistentCONCModule_SL myCONCModule_SL = (PersistentCONCModule_SL) model.CONCModule_SL.createCONCModule_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_SL)This);
			this.setMyCONCModule_SL(myCONCModule_SL);
			myCONCModule_SL.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addUnit(final Unit_SL4Public unit) 
				throws PersistenceException{
        getThis().getUnits().add(unit);
    }
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        GradeAggregator4Public aggregator = GradeAggregator.createGradeAggregator(true);
        getThis().getUnits().applyToAll(aggregator::addElementIfRelevant);
        return aggregator.calcTripleGrade();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getUnits().applyToAll(unit -> unit.fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator));
        
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getUnits().applyToAll(unit -> unit.fetchAllRelevantUnitsAndAtomicModules(aggregator));
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getAlreadyEarnedCP();
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCp();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().updateGradeCache();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */

    /* End of protected part that is not overridden by persistence generator */
    
}
