
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NoProgram_SL extends model.StudyProgram_SL implements PersistentNoProgram_SL{
    
    private static NoProgram_SL4Public theNoProgram_SL = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static NoProgram_SL4Public getTheNoProgram_SL() throws PersistenceException{
        if (theNoProgram_SL == null || reset$For$Test){
            if (reset$For$Test) theNoProgram_SL = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            NoProgram_SL4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theNoProgram_SLFacade.getTheNoProgram_SL();
                            theNoProgram_SL = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                NoProgram_SL4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theNoProgram_SL== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theNoProgram_SL;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theNoProgram_SL;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public NoProgram_SL provideCopy() throws PersistenceException{
        NoProgram_SL result = this;
        result = new NoProgram_SL(this.This, 
                                  this.myCONCModuleGroupOrStudyProgram_SL, 
                                  this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public NoProgram_SL(PersistentStudyProgram_SL This,PersistentModuleGroupOrStudyProgram_SL myCONCModuleGroupOrStudyProgram_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentStudyProgram_SL)This,(PersistentModuleGroupOrStudyProgram_SL)myCONCModuleGroupOrStudyProgram_SL,id);        
    }
    
    static public long getTypeId() {
        return 206;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentNoProgram_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNoProgram_SL result = (PersistentNoProgram_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNoProgram_SL)this.This;
    }
    
    public void accept(StudyProgram_SLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_SL(this);
    }
    public <R> R accept(StudyProgram_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_SL(this);
    }
    public <E extends model.UserException>  void accept(StudyProgram_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_SL(this);
    }
    public <R, E extends model.UserException> R accept(StudyProgram_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_SL(this);
    }
    public void accept(ModuleGroupOrStudyProgram_SLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_SL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNoProgram_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNoProgram_SL)This);
			PersistentCONCModuleGroupOrStudyProgram_SL myCONCModuleGroupOrStudyProgram_SL = (PersistentCONCModuleGroupOrStudyProgram_SL) model.CONCModuleGroupOrStudyProgram_SL.createCONCModuleGroupOrStudyProgram_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNoProgram_SL)This);
			this.setMyCONCModuleGroupOrStudyProgram_SL(myCONCModuleGroupOrStudyProgram_SL);
			myCONCModuleGroupOrStudyProgram_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
        getThis().setInstanceOf(NoProgram_GL.getTheNoProgram_GL());
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
