
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCDecimalGrade extends model.DecimalGrade implements PersistentCONCDecimalGrade{
    
    
    public static CONCDecimalGrade4Public createCONCDecimalGrade(common.Fraction value,boolean delayed$Persistence,CONCDecimalGrade4Public This) throws PersistenceException {
        PersistentCONCDecimalGrade result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCDecimalGradeFacade
                .newDelayedCONCDecimalGrade();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCDecimalGradeFacade
                .newCONCDecimalGrade(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("value", value);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCDecimalGrade provideCopy() throws PersistenceException{
        CONCDecimalGrade result = this;
        result = new CONCDecimalGrade(this.This, 
                                      this.myCONCCalculatableGrade, 
                                      this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCDecimalGrade(PersistentDecimalGrade This,PersistentCalculatableGrade myCONCCalculatableGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentDecimalGrade)This,(PersistentCalculatableGrade)myCONCCalculatableGrade,id);        
    }
    
    static public long getTypeId() {
        return 274;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 274) ConnectionHandler.getTheConnectionHandler().theCONCDecimalGradeFacade
            .newCONCDecimalGrade(this.getId());
        super.store();
        
    }
    
    public PersistentCONCDecimalGrade getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCDecimalGrade result = (PersistentCONCDecimalGrade)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCDecimalGrade)this.This;
    }
    
    public void accept(DecimalGradeVisitor visitor) throws PersistenceException {
        visitor.handleCONCDecimalGrade(this);
    }
    public <R> R accept(DecimalGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCDecimalGrade(this);
    }
    public <E extends model.UserException>  void accept(DecimalGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCDecimalGrade(this);
    }
    public <R, E extends model.UserException> R accept(DecimalGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCDecimalGrade(this);
    }
    public void accept(CalculatableGradeVisitor visitor) throws PersistenceException {
        visitor.handleCONCDecimalGrade(this);
    }
    public <R> R accept(CalculatableGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCDecimalGrade(this);
    }
    public <E extends model.UserException>  void accept(CalculatableGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCDecimalGrade(this);
    }
    public <R, E extends model.UserException> R accept(CalculatableGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCDecimalGrade(this);
    }
    public void accept(GradeVisitor visitor) throws PersistenceException {
        visitor.handleCONCDecimalGrade(this);
    }
    public <R> R accept(GradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCDecimalGrade(this);
    }
    public <E extends model.UserException>  void accept(GradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCDecimalGrade(this);
    }
    public <R, E extends model.UserException> R accept(GradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCDecimalGrade(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCDecimalGrade(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCDecimalGrade(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCDecimalGrade(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCDecimalGrade(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCDecimalGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (PersistentCONCDecimalGrade)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentCONCDecimalGrade)This);
			this.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public common.Fraction fetchGradeValue() 
				throws PersistenceException{
        return getThis().fetchGradeValue();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public Bool4Public isNotNoGrade() 
				throws PersistenceException{
        return getThis().isNotNoGrade();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
