
package model;

import common.Literals;
import persistence.*;
import model.visitor.*;

import java.util.Iterator;


/* Additional import section end */

public class StudentGroup extends PersistentObject implements PersistentStudentGroup{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static StudentGroup4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.getClass(objectId);
        return (StudentGroup4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static StudentGroup4Public createStudentGroup(String name,StudyProgram_GL4Public studyProgram) throws PersistenceException{
        return createStudentGroup(name,studyProgram,false);
    }
    
    public static StudentGroup4Public createStudentGroup(String name,StudyProgram_GL4Public studyProgram,boolean delayed$Persistence) throws PersistenceException {
        if (name == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentStudentGroup result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
                .newDelayedStudentGroup(name);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
                .newStudentGroup(name,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        final$$Fields.put("studyProgram", studyProgram);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static StudentGroup4Public createStudentGroup(String name,StudyProgram_GL4Public studyProgram,boolean delayed$Persistence,StudentGroup4Public This) throws PersistenceException {
        if (name == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        PersistentStudentGroup result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
                .newDelayedStudentGroup(name);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
                .newStudentGroup(name,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        final$$Fields.put("studyProgram", studyProgram);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("name", this.getName());
            AbstractPersistentRoot studyProgram = (AbstractPersistentRoot)this.getStudyProgram();
            if (studyProgram != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    studyProgram, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("studyProgram", proxiInformation);
                
            }
            result.put("students", this.getStudents().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            result.put("activeState", this.activeStateAsString());
        }
        return result;
    }
    
    public StudentGroup provideCopy() throws PersistenceException{
        StudentGroup result = this;
        result = new StudentGroup(this.name, 
                                  this.studyProgram, 
                                  this.activeState, 
                                  this.This, 
                                  this.getId());
        result.students = this.students.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected String name;
    protected PersistentStudyProgram_GL studyProgram;
    protected StudentGroup_StudentsProxi students;
    protected PersistentActiveState activeState;
    protected PersistentStudentGroup This;
    
    public StudentGroup(String name,PersistentStudyProgram_GL studyProgram,PersistentActiveState activeState,PersistentStudentGroup This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.name = name;
        this.studyProgram = studyProgram;
        this.students = new StudentGroup_StudentsProxi(this);
        this.activeState = activeState;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 174;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 174) ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
            .newStudentGroup(name,this.getId());
        super.store();
        if(this.getStudyProgram() != null){
            this.getStudyProgram().store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.studyProgramSet(this.getId(), getStudyProgram());
        }
        this.getStudents().store();
        if(this.getActiveState() != null){
            this.getActiveState().store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.activeStateSet(this.getId(), getActiveState());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public String getName() throws PersistenceException {
        return this.name;
    }
    public void setName(String newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null not allowed for persistent strings, since null = \"\" in Oracle!", 0);
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.nameSet(this.getId(), newValue);
        this.name = newValue;
    }
    public StudyProgram_GL4Public getStudyProgram() throws PersistenceException {
        return this.studyProgram;
    }
    public void setStudyProgram(StudyProgram_GL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.studyProgram)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.studyProgram = (PersistentStudyProgram_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.studyProgramSet(this.getId(), newValue);
        }
    }
    public StudentGroup_StudentsProxi getStudents() throws PersistenceException {
        return this.students;
    }
    public ActiveState4Public getActiveState() throws PersistenceException {
        return this.activeState;
    }
    public void setActiveState(ActiveState4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.activeState)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.activeState = (PersistentActiveState)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.activeStateSet(this.getId(), newValue);
        }
    }
    protected void setThis(PersistentStudentGroup newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudentGroup)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentStudentGroup getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudentGroup result = (PersistentStudentGroup)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudentGroup)this.This;
    }
    
    public void accept(StudentGroupVisitor visitor) throws PersistenceException {
        visitor.handleStudentGroup(this);
    }
    public <R> R accept(StudentGroupReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudentGroup(this);
    }
    public <E extends model.UserException>  void accept(StudentGroupExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudentGroup(this);
    }
    public <R, E extends model.UserException> R accept(StudentGroupReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudentGroup(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudentGroup(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudentGroup(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudentGroup(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudentGroup(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getStudyProgram() != null) return 1;
        if (this.getActiveState() != null) return 1;
        if (this.getStudents().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudentGroup)This);
		if(this.isTheSameAs(This)){
			this.setName((String)final$$Fields.get("name"));
			this.setStudyProgram((PersistentStudyProgram_GL)final$$Fields.get("studyProgram"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String activeStateAsString() 
				throws PersistenceException{
        return getThis().getActiveState().toString();
    }
    public void addStudent(final StudentWrapper4Public student) 
				throws model.StateException, PersistenceException{
        //TODO das darf nat�rlich nur geschehen, wenn die Studiengruppe NotYetStudied ist. Das ist auch der Fall, da sie nur checked aufgerufen werden kann.
        //nur k�nnte ja irgendjemand auf die Idee kommen, noch einen anderen Weg zu implementieren. Das w�re dann ja gar nicht sch�n
        if (studentDoesNotExist(student)) {
            getThis().getStudents().add(student);
        }
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void finishesStudying() 
				throws model.StateException, PersistenceException{
        if (!allStudentsHaveEarnedAllCps()) {
            throw new StateException(Literals.notAllStudentsHaveEarnedAllCps);
        }
        getThis().getActiveState().accept(new ActiveStateExceptionVisitor<StateException>() {
            @Override
            public void handleActive(Active4Public active) throws PersistenceException, StateException {
                getThis().setActiveState(Finished.getTheFinished());
            }

            @Override
            public void handleFinished(Finished4Public finished) throws PersistenceException, StateException {
                // Should not occure since we implement checked.
                throw new StateException(Literals.NotActiveMessage(getThis(), getThis().getActiveState()));
            }

            @Override
            public void handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException, StateException {
                // Should not occure since we implement checked.
                throw new StateException(Literals.NotActiveMessage(getThis(), getThis().getActiveState()));
            }
        });
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().setActiveState(NotYetStudied.getTheNotYetStudied());
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public void makeActive() 
				throws model.StateException, PersistenceException{
        getThis().getStudents().applyToAllException(studentWrapper -> {
            if (studentWrapper.getWrappedStudent().hasActiveStudentGroup().value())
                throw new StateException(Literals.ActiveStudentMessage(studentWrapper.getWrappedStudent()));
            studentWrapper.getWrappedStudent().createStudyProgramWithGrades(getThis().getStudyProgram());
        });
        getThis().setActiveState(Active.getTheActive());
    }
    public void removeStudent(final StudentWrapper4Public student) 
				throws PersistenceException{
        getThis().getStudents().removeFirst(student);
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */


    private boolean allStudentsHaveEarnedAllCps() throws PersistenceException {
        Bool4Public allStudentsHaveEarnedAllCps = Verum.getTheVerum();
        Iterator<StudentWrapper4Public> it = getThis().getStudents().iterator();
        while (it.hasNext()) {
            StudentWrapper4Public student = it.next();
            StudyProgram_SL4Public activeProgram = student.getWrappedStudent().getStudyPrograms().findFirst(program -> program.getActiveState().equals(Active.getTheActive()));
            if (!activeProgram.getAlreadyEarnedCP().equals(activeProgram.getCp()))
                allStudentsHaveEarnedAllCps = Falsum.getTheFalsum();
        }
        return allStudentsHaveEarnedAllCps.value();
    }


    private boolean studentDoesNotExist(StudentWrapper4Public student) throws PersistenceException {
        StudentWrapper4Public foundStudent = getThis().getStudents().findFirst(current -> current.equals(student));
        return foundStudent == null;
    }
    /* End of protected part that is not overridden by persistence generator */
    
}
