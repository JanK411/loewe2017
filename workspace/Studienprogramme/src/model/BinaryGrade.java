
package model;

import common.Fraction;
import persistence.*;


/* Additional import section end */

public abstract class BinaryGrade extends PersistentObject implements PersistentBinaryGrade{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static BinaryGrade4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theBinaryGradeFacade.getClass(objectId);
        return (BinaryGrade4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCGrade = (AbstractPersistentRoot)this.getMyCONCGrade();
            if (myCONCGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCGrade", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract BinaryGrade provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentBinaryGrade This;
    protected PersistentGrade myCONCGrade;
    
    public BinaryGrade(PersistentBinaryGrade This,PersistentGrade myCONCGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCGrade = myCONCGrade;        
    }
    
    static public long getTypeId() {
        return 225;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryGradeFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCGrade() != null){
            this.getMyCONCGrade().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryGradeFacade.myCONCGradeSet(this.getId(), getMyCONCGrade());
        }
        
    }
    
    protected void setThis(PersistentBinaryGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentBinaryGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryGradeFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentGrade getMyCONCGrade() throws PersistenceException {
        return this.myCONCGrade;
    }
    public void setMyCONCGrade(PersistentGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCGrade)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCGrade = (PersistentGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryGradeFacade.myCONCGradeSet(this.getId(), newValue);
        }
    }
    public abstract PersistentBinaryGrade getThis() throws PersistenceException ;
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCGrade().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentBinaryGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentBinaryGrade)This);
			this.setMyCONCGrade(myCONCGrade);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
