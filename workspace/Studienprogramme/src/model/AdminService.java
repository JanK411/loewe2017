
package model;

import com.sun.org.apache.xml.internal.utils.XMLStringFactory;
import common.Literals;
import model.meta.StringFACTORY;
import model.visitor.*;
import persistence.*;


/* Additional import section end */

public class AdminService extends model.Service implements PersistentAdminService{
    
    
    public static AdminService4Public createAdminService() throws PersistenceException{
        return createAdminService(false);
    }
    
    public static AdminService4Public createAdminService(boolean delayed$Persistence) throws PersistenceException {
        PersistentAdminService result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade
                .newDelayedAdminService();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade
                .newAdminService(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static AdminService4Public createAdminService(boolean delayed$Persistence,AdminService4Public This) throws PersistenceException {
        PersistentAdminService result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade
                .newDelayedAdminService();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade
                .newAdminService(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot programManager = (AbstractPersistentRoot)this.getProgramManager();
            if (programManager != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    programManager, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("programManager", proxiInformation);
                
            }
            AbstractPersistentRoot studentManager = (AbstractPersistentRoot)this.getStudentManager();
            if (studentManager != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    studentManager, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("studentManager", proxiInformation);
                
            }
        }
        return result;
    }
    
    public AdminService provideCopy() throws PersistenceException{
        AdminService result = this;
        result = new AdminService(this.This, 
                                  this.programManager, 
                                  this.studentManager, 
                                  this.getId());
        result.errors = this.errors.copy(result);
        result.errors = this.errors.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentProgramManager programManager;
    protected PersistentStudentManager studentManager;
    
    public AdminService(PersistentService This,PersistentProgramManager programManager,PersistentStudentManager studentManager,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentService)This,id);
        this.programManager = programManager;
        this.studentManager = studentManager;        
    }
    
    static public long getTypeId() {
        return -177;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -177) ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade
            .newAdminService(this.getId());
        super.store();
        if(this.getProgramManager() != null){
            this.getProgramManager().store();
            ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade.programManagerSet(this.getId(), getProgramManager());
        }
        if(this.getStudentManager() != null){
            this.getStudentManager().store();
            ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade.studentManagerSet(this.getId(), getStudentManager());
        }
        
    }
    
    public ProgramManager4Public getProgramManager() throws PersistenceException {
        return this.programManager;
    }
    public void setProgramManager(ProgramManager4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.programManager)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.programManager = (PersistentProgramManager)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade.programManagerSet(this.getId(), newValue);
        }
    }
    public StudentManager4Public getStudentManager() throws PersistenceException {
        return this.studentManager;
    }
    public void setStudentManager(StudentManager4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.studentManager)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.studentManager = (PersistentStudentManager)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAdminServiceFacade.studentManagerSet(this.getId(), newValue);
        }
    }
    public PersistentAdminService getThis() throws PersistenceException {
        if(this.This == null){
            PersistentAdminService result = (PersistentAdminService)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentAdminService)this.This;
    }
    
    public void accept(ServiceVisitor visitor) throws PersistenceException {
        visitor.handleAdminService(this);
    }
    public <R> R accept(ServiceReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleAdminService(this);
    }
    public <E extends model.UserException>  void accept(ServiceExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleAdminService(this);
    }
    public <R, E extends model.UserException> R accept(ServiceReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleAdminService(this);
    }
    public void accept(InvokerVisitor visitor) throws PersistenceException {
        visitor.handleAdminService(this);
    }
    public <R> R accept(InvokerReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleAdminService(this);
    }
    public <E extends model.UserException>  void accept(InvokerExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleAdminService(this);
    }
    public <R, E extends model.UserException> R accept(InvokerReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleAdminService(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleAdminService(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleAdminService(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleAdminService(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleAdminService(this);
    }
    public void accept(RemoteVisitor visitor) throws PersistenceException {
        visitor.handleAdminService(this);
    }
    public <R> R accept(RemoteReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleAdminService(this);
    }
    public <E extends model.UserException>  void accept(RemoteExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleAdminService(this);
    }
    public <R, E extends model.UserException> R accept(RemoteReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleAdminService(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getProgramManager() != null) return 1;
        if (this.getStudentManager() != null) return 1;
        return 0;
    }
    
    
    public String adminService_Menu_Filter(final Anything anything) 
				throws PersistenceException{
        String result = "+++";
		if(anything instanceof StudentWrapper4Public) {
			if(this.filter_removeStudent((StudentWrapper4Public)anything)) result = result + "removeStudentPRMTRStudentWrapperPRMTR+++";
		}
		if(anything instanceof NotAtomicModule_GL4Public) {
			if(this.filter_transferCP((NotAtomicModule_GL4Public)anything)) result = result + "transferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTR+++";
		}
		if(anything instanceof CreditPointHavingElement_PL4Public) {
			if(this.filter_changeCreditPoints((CreditPointHavingElement_PL4Public)anything)) result = result + "changeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTR+++";
		}
		if(anything instanceof StudentGroup4Public) {
			if(this.filter_createStudent((StudentGroup4Public)anything)) result = result + "createStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTR+++";
			if(this.filter_addStudentsToGroup((StudentGroup4Public)anything)) result = result + "addStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTR+++";
			if(this.filter_makeGroupActive((StudentGroup4Public)anything)) result = result + "makeGroupActivePRMTRStudentGroupPRMTR+++";
			if(this.filter_finishesStudying((StudentGroup4Public)anything)) result = result + "finishesStudyingPRMTRStudentGroupPRMTR+++";
		}
		return result;
    }
    public ProgramManager4Public containees_Path_In_AddModulesOrModuleGroups() 
				throws model.UserException, PersistenceException{
        	return getThis().getProgramManager();
    }
    public Unit_GLSearchList fromUnit_Path_In_TransferCP(final NotAtomicModule_GL4Public toBeChanged) 
				throws model.UserException, PersistenceException{
        	return new Unit_GLSearchList(toBeChanged.
                getUnits().getList());
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentAdminService)This);
		if(this.isTheSameAs(This)){
		}
    }
    public StudentManager4Public students_Path_In_AddStudentsToGroup() 
				throws model.UserException, PersistenceException{
        	return getThis().getStudentManager();
    }
    public Unit_GLSearchList toUnit_Path_In_TransferCP(final NotAtomicModule_GL4Public toBeChanged) 
				throws model.UserException, PersistenceException{
        	return new Unit_GLSearchList(toBeChanged.
                getUnits().getList());
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addModulesOrModuleGroups(final ModuleGroupOrStudyProgram_PL4Public container, final ModuleOrModuleGroup_PLSearchList containees) 
				throws model.CycleException, model.AlreadyInListException, PersistenceException{
        getThis().getProgramManager().addModulesOrModuleGroups(container, containees, getThis());
    }
    public void addStudentsToGroup(final StudentGroup4Public group, final StudentWrapperSearchList students) 
				throws model.AlreadyInListException, PersistenceException{
        getThis().getStudentManager().addStudentsToGroup(group, students, getThis());
    }
    public void assignGradeForBinaryAtomicModule(final BinaryAtomicModule_SL4Public amodule, final String grade, final String comment) 
				throws PersistenceException{
        BinaryGrade4Public realGrade = StringFACTORY.createObjectBySubTypeNameForBinaryGrade(grade);
        getThis().getStudentManager().assignGrade(amodule, realGrade, comment, getThis());

    }
    public void assignGradeForTripleAtomicModule(final TripleAtomicModule_SL4Public amodule, final String grade, final String comment) 
				throws PersistenceException{
        // aus L�we-String wird eine TrippleGrade erstellt
        TripleGrade4Public realGrade = StringFACTORY.createObjectBySubTypeNameForTripleGrade(grade);
        getThis().getStudentManager().assignGrade(amodule, realGrade, comment, getThis());
    }
    public void assignTrippleGradeToUnit(final Unit_SL4Public unit, final String grade, final String comment) 
				throws PersistenceException{
        TripleGrade4Public realGrade = StringFACTORY.createObjectBySubTypeNameForTripleGrade(grade);
        getThis().getStudentManager().assignGrade(unit, realGrade, comment, getThis());

    }
    public void changeCreditPoints(final CreditPointHavingElement_PL4Public element, final common.Fraction newCP) 
				throws PersistenceException{
        getThis().getProgramManager().changeCreditPoints(element, newCP, getThis());
    }
    public void clearError(final ErrorDisplay4Public error) 
				throws PersistenceException{
        getThis().getErrors().filter(argument -> !argument.equals(error));
        getThis().signalChanged(true);
    }
    public void connected(final String user) 
				throws PersistenceException{

    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createAtomicModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name, final common.Fraction cp, final String notenschema) 
				throws model.NameAlreadyExistsException, PersistenceException{
        getThis().getProgramManager().createAtomicModule(container, name, CreditPointsManager.getTheCreditPointsManager().getCP(cp), notenschema, getThis());
    }
    public void createModuleGroup(final ModuleGroupOrStudyProgram_PL4Public container, final String name) 
				throws model.NameAlreadyExistsException, PersistenceException{
        getThis().getProgramManager().createModuleGroup(container, name, getThis());
    }
    public void createModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name) 
				throws model.NameAlreadyExistsException, PersistenceException{
        getThis().getProgramManager().createModule(container, name, getThis());
    }
    public void createStudentGroup(final StudyProgram_PL4Public program, final String name) 
				throws model.StateException, PersistenceException{
        getThis().getStudentManager().createStudentGroup(program, name, getThis());
    }
    public void createStudent(final StudentGroup4Public studentGroup, final String firstName, final String name, final java.sql.Date date) 
				throws model.StateException, PersistenceException{
        getThis().getStudentManager().createStudent(studentGroup, firstName, name, date, getThis());
    }
    public void createStudyProgram(final ProgramManager4Public manager, final String name) 
				throws model.NameAlreadyExistsException, PersistenceException{
        manager.createStudyProgram(name, getThis());
    }
    public void createUnit(final NotAtomicModule_PL4Public module, final String name, final common.Fraction cp) 
				throws model.NameAlreadyExistsException, PersistenceException{
        getThis().getProgramManager().createUnit(module, name, CreditPointsManager.getTheCreditPointsManager().getCP(cp), getThis());
    }
    public void disconnected() 
				throws PersistenceException{

    }
    public StudentSearchList findStudent(final String criteria) 
				throws PersistenceException{
        return getThis().getStudentManager().findStudent(criteria);
    }
    public void finishesStudying(final StudentGroup4Public group) 
				throws model.StateException, PersistenceException{
        getThis().getStudentManager().finishesStudying(group, getThis());
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
        getThis().setProgramManager(ProgramManager.createProgramManager());
        getThis().setStudentManager(StudentManager.createStudentManager());
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public void makeGroupActive(final StudentGroup4Public group) 
				throws model.StateException, PersistenceException{
        getThis().getStudentManager().makeGroupActive(group, getThis());
    }
    public void removeStudent(final StudentWrapper4Public studentWrapper) 
				throws model.StateException, PersistenceException{
        getThis().getStudentManager().removeStudent(studentWrapper, getThis());
    }
    public void removeUnit(final Unit_PL4Public unit) 
				throws model.StateException, PersistenceException{
        getThis().getProgramManager().removeUnit(unit, getThis());
    }
    public void resetPassword(final StudentWrapper4Public student) 
				throws PersistenceException{
        //TODO sollte vielleicht über den Studentmanager aufgerufen werden und eine active operation sein
        Server4Public server = Server.getServerByUser(student.getMatNr()).iterator().next();
        try {
            server.changePassword("unknown", "password", Verum.getTheVerum());
        } catch (PasswordException e) {
            throw new Error("cannot occur");
        }
    }
    public GradeHistory4Public showHistorie(final CreditPointHavingElement_SL4Public element) 
				throws PersistenceException{
        return element.showHistory();
    }
    public void transferCP(final NotAtomicModule_GL4Public toBeChanged, final Unit_GL4Public fromUnit, final Unit_GL4Public toUnit, final common.Fraction amountToTransfer) 
				throws model.InvalidParameterException, PersistenceException{
        getThis().getProgramManager().transferCP(toBeChanged, fromUnit, toUnit, amountToTransfer, getThis());
    }
    
    
    // Start of section that contains overridden operations only.
    
    public void handleException(final Command command, final PersistenceException exception) 
				throws PersistenceException{

    }
    public void handleResult(final Command command) 
				throws PersistenceException{
        new Thread(new Runnable() {
            public void  /*INTERNAL*/  run() {
                try {
                    try {
                        command.checkException();
                        //Handle result!
                        signalChanged(true);
                    } catch (model.UserException e) {
                        model.UserExceptionToDisplayVisitor visitor = new model.UserExceptionToDisplayVisitor();
                        e.accept(visitor);
                        getErrors().add(visitor.getResult());
                        signalChanged(true);
                    }
                } catch (PersistenceException e) {
                    //Handle fatal exception!
                }
            }
        }).start();
    }
    public boolean hasChanged() 
				throws PersistenceException{
        boolean result = this.changed;
        this.changed = false;
        return result;
    }

    /* Start of protected part that is not overridden by persistence generator */

    private boolean filter_transferCP(NotAtomicModule_GL4Public module_gl4Public) throws PersistenceException {
        return module_gl4Public.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return true;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return true;
            }
        });
    }

    private boolean filter_makeGroupActive(StudentGroup4Public studentGroup) throws PersistenceException {
        return studentGroup.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return true;
            }
        });
    }

    private boolean filter_removeStudent(StudentGroup4Public group) throws PersistenceException {
        return group.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                // Falls es keine Studenten in der Gruppe gibt, benötigt man auch die Funktion remove nicht.
                if (group.getStudents().getLength() == 0) return false;
                return true;
            }
        });
    }


    private boolean filter_createStudent(StudentGroup4Public anything) throws PersistenceException {
        return anything.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return true;
            }
        });
    }

    private boolean filter_addStudentsToGroup(StudentGroup4Public studentGroup) throws PersistenceException {
        return studentGroup.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return true;
            }
        });
    }

    private boolean filter_changeCreditPoints(CreditPointHavingElement_PL4Public creditPointHavingElement_pl4Public) throws PersistenceException {
        return creditPointHavingElement_pl4Public.getEditable().value();
    }

    private boolean filter_removeStudent(StudentWrapper4Public wrapper) throws PersistenceException {
        return wrapper.getStudentGroup().getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return true;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return true;
            }
        });
    }

    private boolean filter_finishesStudying(StudentGroup4Public group) throws PersistenceException {
        return group.getActiveState().accept(new ActiveStateReturnVisitor<Boolean>() {
            @Override
            public Boolean handleActive(Active4Public active) throws PersistenceException {
                return true;
            }

            @Override
            public Boolean handleFinished(Finished4Public finished) throws PersistenceException {
                return false;
            }

            @Override
            public Boolean handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                return false;
            }
        });
    }
    
    /* End of protected part that is not overridden by persistence generator */
    
}
