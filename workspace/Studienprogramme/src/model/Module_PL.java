
package model;

import persistence.*;


/* Additional import section end */

public abstract class Module_PL extends PersistentObject implements PersistentModule_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Module_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModule_PLFacade.getClass(objectId);
        return (Module_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleOrModuleGroup_PL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_PL();
            if (myCONCModuleOrModuleGroup_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract Module_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModule_PL This;
    protected PersistentModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL;
    
    public Module_PL(PersistentModule_PL This,PersistentModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroup_PL = myCONCModuleOrModuleGroup_PL;        
    }
    
    static public long getTypeId() {
        return 164;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModule_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroup_PL() != null){
            this.getMyCONCModuleOrModuleGroup_PL().store();
            ConnectionHandler.getTheConnectionHandler().theModule_PLFacade.myCONCModuleOrModuleGroup_PLSet(this.getId(), getMyCONCModuleOrModuleGroup_PL());
        }
        
    }
    
    protected void setThis(PersistentModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_PL getMyCONCModuleOrModuleGroup_PL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_PL;
    }
    public void setMyCONCModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_PL = (PersistentModuleOrModuleGroup_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_PLFacade.myCONCModuleOrModuleGroup_PLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModule_PL getThis() throws PersistenceException ;
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).setName(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroup_PL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModule_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentModule_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentModule_PL)This);
			this.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
