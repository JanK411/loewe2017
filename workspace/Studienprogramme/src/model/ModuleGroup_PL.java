
package model;

import common.Fraction;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class ModuleGroup_PL extends PersistentObject implements PersistentModuleGroup_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroup_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.getClass(objectId);
        return (ModuleGroup_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static ModuleGroup_PL4Public createModuleGroup_PL(String name) throws PersistenceException{
        return createModuleGroup_PL(name,false);
    }
    
    public static ModuleGroup_PL4Public createModuleGroup_PL(String name,boolean delayed$Persistence) throws PersistenceException {
        PersistentModuleGroup_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade
                .newDelayedModuleGroup_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade
                .newModuleGroup_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static ModuleGroup_PL4Public createModuleGroup_PL(String name,boolean delayed$Persistence,ModuleGroup_PL4Public This) throws PersistenceException {
        PersistentModuleGroup_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade
                .newDelayedModuleGroup_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade
                .newModuleGroup_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleGroupOrStudyProgram_PL = (AbstractPersistentRoot)this.getMyCONCModuleGroupOrStudyProgram_PL();
            if (myCONCModuleGroupOrStudyProgram_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleGroupOrStudyProgram_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleGroupOrStudyProgram_PL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCModuleOrModuleGroup_PL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_PL();
            if (myCONCModuleOrModuleGroup_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public ModuleGroup_PL provideCopy() throws PersistenceException{
        ModuleGroup_PL result = this;
        result = new ModuleGroup_PL(this.This, 
                                    this.myCONCModuleGroupOrStudyProgram_PL, 
                                    this.myCONCModuleOrModuleGroup_PL, 
                                    this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModuleGroup_PL This;
    protected PersistentModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL;
    protected PersistentModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL;
    
    public ModuleGroup_PL(PersistentModuleGroup_PL This,PersistentModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL,PersistentModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleGroupOrStudyProgram_PL = myCONCModuleGroupOrStudyProgram_PL;
        this.myCONCModuleOrModuleGroup_PL = myCONCModuleOrModuleGroup_PL;        
    }
    
    static public long getTypeId() {
        return 121;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 121) ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade
            .newModuleGroup_PL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleGroupOrStudyProgram_PL() != null){
            this.getMyCONCModuleGroupOrStudyProgram_PL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.myCONCModuleGroupOrStudyProgram_PLSet(this.getId(), getMyCONCModuleGroupOrStudyProgram_PL());
        }
        if(this.getMyCONCModuleOrModuleGroup_PL() != null){
            this.getMyCONCModuleOrModuleGroup_PL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.myCONCModuleOrModuleGroup_PLSet(this.getId(), getMyCONCModuleOrModuleGroup_PL());
        }
        
    }
    
    protected void setThis(PersistentModuleGroup_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroup_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroupOrStudyProgram_PL getMyCONCModuleGroupOrStudyProgram_PL() throws PersistenceException {
        return this.myCONCModuleGroupOrStudyProgram_PL;
    }
    public void setMyCONCModuleGroupOrStudyProgram_PL(PersistentModuleGroupOrStudyProgram_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleGroupOrStudyProgram_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleGroupOrStudyProgram_PL = (PersistentModuleGroupOrStudyProgram_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.myCONCModuleGroupOrStudyProgram_PLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_PL getMyCONCModuleOrModuleGroup_PL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_PL;
    }
    public void setMyCONCModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_PL = (PersistentModuleOrModuleGroup_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_PLFacade.myCONCModuleOrModuleGroup_PLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroup_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentModuleGroup_PL result = (PersistentModuleGroup_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentModuleGroup_PL)this.This;
    }
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setName(newValue);
    }
    public ModuleGroupOrStudyProgram_PL_ContaineesProxi getContainees() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getContainees();
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_PL)this.getMyCONCModuleGroupOrStudyProgram_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
        ((PersistentModuleOrModuleGroup_PL)this.getMyCONCModuleOrModuleGroup_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleGroupOrStudyProgram_PL().delete$Me();
        this.getMyCONCModuleOrModuleGroup_PL().delete$Me();
    }
    
    public void accept(ModuleGroupOrStudyProgram_PLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_PL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_PL(this);
    }
    public void accept(ModuleOrModuleGroup_PLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part, new java.util.HashSet<h_PLHIERARCHY>());
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        if(getThis().equals(part)) return true;
		if(visited.contains(getThis())) return false;
		java.util.Iterator<ModuleOrModuleGroup_PL4Public> iterator0 = getThis().getContainees().iterator();
		while(iterator0.hasNext())
			if(((h_PLHIERARCHY)iterator0.next()).containsh_PL(part, visited)) return true; 
		visited.add(getThis());
		return false;
    }
    public ModuleGroupOrStudyProgram_PLSearchList getContainers() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroup_PL().getContainers();
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroup_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentModuleGroup_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentModuleGroup_PL)This);
			PersistentCONCModuleGroupOrStudyProgram_PL myCONCModuleGroupOrStudyProgram_PL = (PersistentCONCModuleGroupOrStudyProgram_PL) model.CONCModuleGroupOrStudyProgram_PL.createCONCModuleGroupOrStudyProgram_PL("", this.isDelayed$Persistence(), (PersistentModuleGroup_PL)This);
			this.setMyCONCModuleGroupOrStudyProgram_PL(myCONCModuleGroupOrStudyProgram_PL);
			this.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleGroupOrStudyProgram_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, new java.util.HashMap<h_PLHIERARCHY,T>());
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        if (visited.containsKey(getThis())) return visited.get(getThis());
		T result$$containees$$ModuleGroup_PL = strategy.ModuleGroup_PL$$containees$$$initialize(getThis());
		java.util.Iterator<?> iterator$$ = getThis().getContainees().iterator();
		while (iterator$$.hasNext()){
			ModuleOrModuleGroup_PL4Public current$$Field = (ModuleOrModuleGroup_PL4Public)iterator$$.next();
			T current$$ = current$$Field.strategyh_PL(strategy, visited);
			result$$containees$$ModuleGroup_PL = strategy.ModuleGroup_PL$$containees$$consolidate(getThis(), result$$containees$$ModuleGroup_PL, current$$);
		}
		T result = strategy.ModuleGroup_PL$$finalize(getThis() ,result$$containees$$ModuleGroup_PL);
		visited.put(getThis(),result);
		return result;
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addContainees(final ModuleOrModuleGroup_PLSearchList containees) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        this.getMyCONCModuleGroupOrStudyProgram_PL().addContainees(containees);
    }
    public void addContainee(final ModuleOrModuleGroup_PL4Public containee) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        this.getMyCONCModuleGroupOrStudyProgram_PL().addContainee(containee);
    }
    public Bool4Public alreadyContainsModuleOrModuleGroupAndTheirContainees(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        return this.getMyCONCModuleGroupOrStudyProgram_PL().alreadyContainsModuleOrModuleGroupAndTheirContainees(container);
    }
    public Bool4Public alreadyContainsModuleOrModuleGroup(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        return getThis().getContainers().aggregate(new Aggregtion<ModuleGroupOrStudyProgram_PL4Public, Bool4Public>() {
            @Override
            public Bool4Public neutral() throws PersistenceException {
                return Falsum.getTheFalsum();
            }

            @Override
            public Bool4Public compose(Bool4Public bool, ModuleGroupOrStudyProgram_PL4Public moduleGroupOrStudyProgram) throws PersistenceException {
                return bool.lazyOr(moduleGroupOrStudyProgram.alreadyContainsModuleOrModuleGroup(container));
            }
        });
    }
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCModuleGroupOrStudyProgram_PL().calculateCP();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createGlEquivalentAndAdd(final ModuleGroupOrStudyProgram_GL4Public container) 
				throws PersistenceException{
        getThis().setEditable(Falsum.getTheFalsum());
        ModuleGroup_GL4Public moduleGroup_gl = ModuleGroup_GL.createModuleGroup_GL(getThis());
        getThis().getContainees().applyToAll(x -> x.createGlEquivalentAndAdd(moduleGroup_gl));
        container.addContainee(moduleGroup_gl);
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().getCalculatedCP();
    }
    public ModuleOrModuleGroup_PLSearchList getOneLayerContainees() 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCModuleGroupOrStudyProgram_PL().getOneLayerContainees();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().updateCreditPointCace();
        getThis().getMyCONCModuleOrModuleGroup_PL().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
