package model;

import common.Constants;
import common.MyIconInfo$Visitor;
import model.visitor.ActiveStateVisitor;
import model.visitor.BoolVisitor;
import persistence.*;

public class GetIconInfo$Visitor extends MyIconInfo$Visitor {

    @Override
    public void handleModuleGroup_PL(ModuleGroup_PL4Public moduleGroup_PL) throws PersistenceException {
        moduleGroup_PL.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.ModuleGroupIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.ModuleGroupEditableIconNumber;
            }
        });
    }

    @Override
    public void handleModuleGroup_GL(ModuleGroup_GL4Public moduleGroup_GL) throws PersistenceException {
        result = Constants.ModuleGroupIconNumber;
    }

    @Override
    public void handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
        result = Constants.ModuleGroupIconNumber;
    }

    @Override
    public void handleStudent(Student4Public student) throws PersistenceException {
        result = Constants.StudentIconNumber;
    }

    @Override
    public void handleStudentWrapper(StudentWrapper4Public studentWrapper) throws PersistenceException {
        result = Constants.StudentIconNumber;
    }

    @Override
    public void handleStudentGroup(StudentGroup4Public studentGroup) throws PersistenceException {
        studentGroup.getActiveState().accept(new ActiveStateVisitor() {
            @Override
            public void handleActive(Active4Public active) throws PersistenceException {
                result = Constants.StudentGroupIconNumber;
            }

            @Override
            public void handleFinished(Finished4Public finished) throws PersistenceException {
                result = Constants.StudentGroup_FinishedIconNumber;
            }

            @Override
            public void handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
                result = Constants.StudentGroup_NotYetStudiedIconNumber;
            }
        });
    }

    @Override
    public void handleStudyProgram_PL(StudyProgram_PL4Public studyProgram) throws PersistenceException {
        studyProgram.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.StudyProgrammIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.StudyProgrammEditableIconNumber;
            }
        });
    }

    @Override
    public void handleStudyProgram_GL(StudyProgram_GL4Public studyProgram) throws PersistenceException {
        result = Constants.StudyProgrammIconNumber;
    }

    @Override
    public void handleUnit_PL(Unit_PL4Public unit_PL) throws PersistenceException {
        unit_PL.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.UnitIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.UnitEditableIconNumber;
            }
        });
    }

    @Override
    public void handleUnit_GL(Unit_GL4Public unit_PL) throws PersistenceException {
        result = Constants.UnitIconNumber;
    }

    @Override
    public void handleUnit_SL(Unit_SL4Public unit_SL) throws PersistenceException {
        result = Constants.UnitIconNumber;
    }

    @Override
    public void handleNotAtomicModule_PL(NotAtomicModule_PL4Public notAtomicModule_PL) throws PersistenceException {
        notAtomicModule_PL.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.NotAtomicModuleIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.NotAtomicModuleEditableIconNumber;
            }
        });
    }

    @Override
    public void handleNotAtomicModule_GL(NotAtomicModule_GL4Public notAtomicModule_GL) throws PersistenceException {
        result = Constants.NotAtomicModuleIconNumber;
    }

    @Override
    public void handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
        result = Constants.NotAtomicModuleIconNumber;
    }


    @Override
    public void handleStudyProgram_SL(StudyProgram_SL4Public studyProgram_SL) throws PersistenceException {
        result = Constants.StudyProgrammIconNumber;
    }

    @Override
    public void handleBinaryAtomicModule_PL(BinaryAtomicModule_PL4Public binaryAtomicModule_PL) throws PersistenceException {
        binaryAtomicModule_PL.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.AtomicModuleIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.AtomicModuleEditableIconNumber;
            }
        });
    }

    @Override
    public void handleBinaryAtomicModule_GL(BinaryAtomicModule_GL4Public binaryAtomicModule_GL) throws PersistenceException {
        result = Constants.AtomicModuleIconNumber;
    }

    @Override
    public void handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
        result = Constants.AtomicModuleIconNumber;
    }

    @Override
    public void handleTripleAtomicModule_PL(TripleAtomicModule_PL4Public tripleAtomicModule_PL) throws PersistenceException {
        tripleAtomicModule_PL.getEditable().accept(new BoolVisitor() {
            @Override
            public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
                result = Constants.AtomicModuleIconNumber;
            }

            @Override
            public void handleVerum(Verum4Public verum) throws PersistenceException {
                result = Constants.AtomicModuleEditableIconNumber;
            }
        });
    }

    @Override
    public void handleTripleAtomicModule_GL(TripleAtomicModule_GL4Public tripleAtomicModule_GL) throws PersistenceException {
        result = Constants.AtomicModuleIconNumber;
    }

    @Override
    public void handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
        result = Constants.AtomicModuleIconNumber;
    }

    @Override
    public void handleCreditPoints(CreditPoints4Public creditPoints) throws PersistenceException {
        result = Constants.CreditPointIconNumber;
    }

    @Override
    protected void handleManager(Anything manager) throws PersistenceException {
        result = Constants.ManagerIconNumber;
    }

    @Override
    public void handleErrorDisplay(ErrorDisplay4Public errorDisplay) throws PersistenceException {
        result = Constants.ErrorDisplayIconNumber;
    }

    @Override
    public void handleGradeHistory(GradeHistory4Public gradeHistory) throws PersistenceException {
        result = Constants.GradeHistoryIconNumber;
    }

    @Override
    public void handleGradeHistoryElement(GradeHistoryElement4Public gradeHistoryElement) throws PersistenceException {
        result = Constants.GradeHistoryElementIconNumber;
    }
}
