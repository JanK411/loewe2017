
package model;

import common.Fraction;
import model.meta.StringFACTORY;
import persistence.*;


/* Additional import section end */

public abstract class ModuleGroupOrStudyProgram_SL extends PersistentObject implements PersistentModuleGroupOrStudyProgram_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroupOrStudyProgram_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade.getClass(objectId);
        return (ModuleGroupOrStudyProgram_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("containees", this.getContainees().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
            if (myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleGroupOrStudyProgram_SL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected ModuleGroupOrStudyProgram_SL_ContaineesProxi containees;
    protected PersistentModuleGroupOrStudyProgram_SL This;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;
    
    public ModuleGroupOrStudyProgram_SL(PersistentModuleGroupOrStudyProgram_SL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.containees = new ModuleGroupOrStudyProgram_SL_ContaineesProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;        
    }
    
    static public long getTypeId() {
        return 154;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        this.getContainees().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() != null){
            this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLSet(this.getId(), getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL());
        }
        
    }
    
    public ModuleGroupOrStudyProgram_SL_ContaineesProxi getContainees() throws PersistenceException {
        return this.containees;
    }
    protected void setThis(PersistentModuleGroupOrStudyProgram_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroupOrStudyProgram_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL;
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleGroupOrStudyProgram_SL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL()).setState(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroupOrStudyProgram_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroupOrStudyProgram_SL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public void addContainee(final ModuleOrModuleGroup_SL4Public containee) 
				throws PersistenceException{
        getThis().getContainees().add(containee);
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        GradeAggregator4Public aggregator = GradeAggregator.createGradeAggregator(true);
        getThis().fetchAllRelevantUnitsAndAtomicModules(aggregator);
        Grade4Public grade = aggregator.calcDecimalGrade();
        getThis().setState(GradeCachedState_SL.createGradeCachedState_SL(grade, getThis()));
        return grade;
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getContainees().applyToAll(containee -> containee.fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator));
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getContainees().applyToAll(containee -> containee.fetchAllRelevantUnitsAndAtomicModules(aggregator));
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
