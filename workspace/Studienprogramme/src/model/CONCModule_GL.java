
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModule_GL extends model.Module_GL implements PersistentCONCModule_GL{
    
    
    public static CONCModule_GL4Public createCONCModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,CONCModule_GL4Public This) throws PersistenceException {
        PersistentCONCModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModule_GLFacade
                .newDelayedCONCModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModule_GLFacade
                .newCONCModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModule_GL provideCopy() throws PersistenceException{
        CONCModule_GL result = this;
        result = new CONCModule_GL(this.This, 
                                   this.myCONCModuleOrModuleGroup_GL, 
                                   this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCModule_GL(PersistentModule_GL This,PersistentModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModule_GL)This,(PersistentModuleOrModuleGroup_GL)myCONCModuleOrModuleGroup_GL,id);        
    }
    
    static public long getTypeId() {
        return 181;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 181) ConnectionHandler.getTheConnectionHandler().theCONCModule_GLFacade
            .newCONCModule_GL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModule_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModule_GL result = (PersistentCONCModule_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModule_GL)this.This;
    }
    
    public void accept(Module_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_GL(this);
    }
    public <R> R accept(Module_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_GL(this);
    }
    public <E extends model.UserException>  void accept(Module_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(Module_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCModule_GL)This);
			this.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        getThis().createSLEquivalentAndAdd(container);
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
