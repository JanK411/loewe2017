
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCState_SL extends model.State_SL implements PersistentCONCState_SL{
    
    
    public static CONCState_SL4Public createCONCState_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public cachingElement,boolean delayed$Persistence,CONCState_SL4Public This) throws PersistenceException {
        PersistentCONCState_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCState_SLFacade
                .newDelayedCONCState_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCState_SLFacade
                .newCONCState_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCState_SL provideCopy() throws PersistenceException{
        CONCState_SL result = this;
        result = new CONCState_SL(this.getId());
        result.cachingElement = this.cachingElement;
        result.This = this.This;
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public CONCState_SL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -300;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -300) ConnectionHandler.getTheConnectionHandler().theCONCState_SLFacade
            .newCONCState_SL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCState_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCState_SL result = (PersistentCONCState_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCState_SL)this.This;
    }
    
    public void accept(State_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCState_SL(this);
    }
    public <R> R accept(State_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCState_SL(this);
    }
    public <E extends model.UserException>  void accept(State_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCState_SL(this);
    }
    public <R, E extends model.UserException> R accept(State_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCState_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCState_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCState_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCState_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCState_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getGrade() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCState_SL)This);
		if(this.isTheSameAs(This)){
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public Grade4Public getGrade() 
				throws PersistenceException{
        return getThis().getGrade();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
