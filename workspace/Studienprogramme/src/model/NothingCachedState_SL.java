
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NothingCachedState_SL extends PersistentObject implements PersistentNothingCachedState_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static NothingCachedState_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade.getClass(objectId);
        return (NothingCachedState_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static NothingCachedState_SL4Public createNothingCachedState_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public cachingElement) throws PersistenceException{
        return createNothingCachedState_SL(cachingElement,false);
    }
    
    public static NothingCachedState_SL4Public createNothingCachedState_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public cachingElement,boolean delayed$Persistence) throws PersistenceException {
        PersistentNothingCachedState_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade
                .newDelayedNothingCachedState_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade
                .newNothingCachedState_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static NothingCachedState_SL4Public createNothingCachedState_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public cachingElement,boolean delayed$Persistence,NothingCachedState_SL4Public This) throws PersistenceException {
        PersistentNothingCachedState_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade
                .newDelayedNothingCachedState_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade
                .newNothingCachedState_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCState_SL = (AbstractPersistentRoot)this.getMyCONCState_SL();
            if (myCONCState_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCState_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, true, true);
                result.put("myCONCState_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public NothingCachedState_SL provideCopy() throws PersistenceException{
        NothingCachedState_SL result = this;
        result = new NothingCachedState_SL(this.getId());
        result.This = this.This;
        result.myCONCState_SL = this.myCONCState_SL;
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentNothingCachedState_SL This;
    protected PersistentState_SL myCONCState_SL;
    
    public NothingCachedState_SL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -296;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -296) ConnectionHandler.getTheConnectionHandler().theNothingCachedState_SLFacade
            .newNothingCachedState_SL(this.getId());
        super.store();
        
    }
    
    protected void setThis(PersistentNothingCachedState_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentNothingCachedState_SL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentState_SL getMyCONCState_SL() throws PersistenceException {
        return this.myCONCState_SL;
    }
    public void setMyCONCState_SL(PersistentState_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCState_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCState_SL = (PersistentState_SL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentNothingCachedState_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNothingCachedState_SL result = (PersistentNothingCachedState_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNothingCachedState_SL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public getCachingElement() throws PersistenceException {
        return ((PersistentState_SL)this.getMyCONCState_SL()).getCachingElement();
    }
    public void setCachingElement(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public newValue) throws PersistenceException {
        ((PersistentState_SL)this.getMyCONCState_SL()).setCachingElement(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCState_SL().delete$Me();
    }
    
    public void accept(State_SLVisitor visitor) throws PersistenceException {
        visitor.handleNothingCachedState_SL(this);
    }
    public <R> R accept(State_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNothingCachedState_SL(this);
    }
    public <E extends model.UserException>  void accept(State_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNothingCachedState_SL(this);
    }
    public <R, E extends model.UserException> R accept(State_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNothingCachedState_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNothingCachedState_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNothingCachedState_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNothingCachedState_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNothingCachedState_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getGrade() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNothingCachedState_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCState_SL myCONCState_SL = (PersistentCONCState_SL) model.CONCState_SL.createCONCState_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)final$$Fields.get("cachingElement"), this.isDelayed$Persistence(), (PersistentNothingCachedState_SL)This);
			this.setMyCONCState_SL(myCONCState_SL);
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public Grade4Public getGrade() 
				throws PersistenceException{
        Grade4Public grade = getThis().getCachingElement().calculateGrade();
        getThis().getCachingElement().setState(GradeCachedState_SL.createGradeCachedState_SL(grade, getThis().getCachingElement()));
        return grade;
    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
