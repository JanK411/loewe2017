
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class ModuleGroup_SL extends PersistentObject implements PersistentModuleGroup_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroup_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.getClass(objectId);
        return (ModuleGroup_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static ModuleGroup_SL4Public createModuleGroup_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf) throws PersistenceException{
        return createModuleGroup_SL(instanceOf,false);
    }
    
    public static ModuleGroup_SL4Public createModuleGroup_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentModuleGroup_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade
                .newDelayedModuleGroup_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade
                .newModuleGroup_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type ModuleGroup_SL has not been initialized!",0);
        return result;
    }
    
    public static ModuleGroup_SL4Public createModuleGroup_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,ModuleGroup_SL4Public This) throws PersistenceException {
        PersistentModuleGroup_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade
                .newDelayedModuleGroup_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade
                .newModuleGroup_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleGroupOrStudyProgram_SL = (AbstractPersistentRoot)this.getMyCONCModuleGroupOrStudyProgram_SL();
            if (myCONCModuleGroupOrStudyProgram_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleGroupOrStudyProgram_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleGroupOrStudyProgram_SL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCModuleOrModuleGroup_SL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_SL();
            if (myCONCModuleOrModuleGroup_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public ModuleGroup_SL provideCopy() throws PersistenceException{
        ModuleGroup_SL result = this;
        result = new ModuleGroup_SL(this.This, 
                                    this.myCONCModuleGroupOrStudyProgram_SL, 
                                    this.myCONCModuleOrModuleGroup_SL, 
                                    this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModuleGroup_SL This;
    protected PersistentModuleGroupOrStudyProgram_SL myCONCModuleGroupOrStudyProgram_SL;
    protected PersistentModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL;
    
    public ModuleGroup_SL(PersistentModuleGroup_SL This,PersistentModuleGroupOrStudyProgram_SL myCONCModuleGroupOrStudyProgram_SL,PersistentModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleGroupOrStudyProgram_SL = myCONCModuleGroupOrStudyProgram_SL;
        this.myCONCModuleOrModuleGroup_SL = myCONCModuleOrModuleGroup_SL;        
    }
    
    static public long getTypeId() {
        return 202;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 202) ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade
            .newModuleGroup_SL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleGroupOrStudyProgram_SL() != null){
            this.getMyCONCModuleGroupOrStudyProgram_SL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.myCONCModuleGroupOrStudyProgram_SLSet(this.getId(), getMyCONCModuleGroupOrStudyProgram_SL());
        }
        if(this.getMyCONCModuleOrModuleGroup_SL() != null){
            this.getMyCONCModuleOrModuleGroup_SL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.myCONCModuleOrModuleGroup_SLSet(this.getId(), getMyCONCModuleOrModuleGroup_SL());
        }
        
    }
    
    protected void setThis(PersistentModuleGroup_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroup_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroupOrStudyProgram_SL getMyCONCModuleGroupOrStudyProgram_SL() throws PersistenceException {
        return this.myCONCModuleGroupOrStudyProgram_SL;
    }
    public void setMyCONCModuleGroupOrStudyProgram_SL(PersistentModuleGroupOrStudyProgram_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleGroupOrStudyProgram_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleGroupOrStudyProgram_SL = (PersistentModuleGroupOrStudyProgram_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.myCONCModuleGroupOrStudyProgram_SLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_SL getMyCONCModuleOrModuleGroup_SL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_SL;
    }
    public void setMyCONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_SL = (PersistentModuleOrModuleGroup_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_SLFacade.myCONCModuleOrModuleGroup_SLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroup_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentModuleGroup_SL result = (PersistentModuleGroup_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentModuleGroup_SL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).setState(newValue);
    }
    public ModuleGroupOrStudyProgram_SL_ContaineesProxi getContainees() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).getContainees();
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_SL)this.getMyCONCModuleGroupOrStudyProgram_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
        ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleGroupOrStudyProgram_SL().delete$Me();
        this.getMyCONCModuleOrModuleGroup_SL().delete$Me();
    }
    
    public void accept(ModuleGroupOrStudyProgram_SLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_SL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_SL(this);
    }
    public void accept(ModuleOrModuleGroup_SLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public ModuleGroupOrStudyProgram_SL4Public getContainingModuleGroupOrStudyProgram() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroup_SL().getContainingModuleGroupOrStudyProgram();
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroup_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleGroupOrStudyProgram_SL myCONCModuleGroupOrStudyProgram_SL = (PersistentCONCModuleGroupOrStudyProgram_SL) model.CONCModuleGroupOrStudyProgram_SL.createCONCModuleGroupOrStudyProgram_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_SL)This);
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_SL)This);
			this.setMyCONCModuleGroupOrStudyProgram_SL(myCONCModuleGroupOrStudyProgram_SL);
			this.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleGroupOrStudyProgram_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addContainee(final ModuleOrModuleGroup_SL4Public containee) 
				throws PersistenceException{
        this.getMyCONCModuleGroupOrStudyProgram_SL().addContainee(containee);
    }
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().getMyCONCModuleGroupOrStudyProgram_SL().calculateGrade();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCModuleGroupOrStudyProgram_SL().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
        
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCModuleGroupOrStudyProgram_SL().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getAlreadyEarnedCP();
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCp();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().updateGradeCache();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
