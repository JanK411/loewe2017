
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class TripleAtomicModule_GL extends PersistentObject implements PersistentTripleAtomicModule_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static TripleAtomicModule_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade.getClass(objectId);
        return (TripleAtomicModule_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static TripleAtomicModule_GL4Public createTripleAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP) throws PersistenceException{
        return createTripleAtomicModule_GL(instanceOf,studentGroupCP,false);
    }
    
    public static TripleAtomicModule_GL4Public createTripleAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP,boolean delayed$Persistence) throws PersistenceException {
        PersistentTripleAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade
                .newDelayedTripleAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade
                .newTripleAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        final$$Fields.put("studentGroupCP", studentGroupCP);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type TripleAtomicModule_GL has not been initialized!",0);
        return result;
    }
    
    public static TripleAtomicModule_GL4Public createTripleAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP,boolean delayed$Persistence,TripleAtomicModule_GL4Public This) throws PersistenceException {
        PersistentTripleAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade
                .newDelayedTripleAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade
                .newTripleAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        final$$Fields.put("studentGroupCP", studentGroupCP);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCAtomicModule_GL = (AbstractPersistentRoot)this.getMyCONCAtomicModule_GL();
            if (myCONCAtomicModule_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCAtomicModule_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCAtomicModule_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public TripleAtomicModule_GL provideCopy() throws PersistenceException{
        TripleAtomicModule_GL result = this;
        result = new TripleAtomicModule_GL(this.This, 
                                           this.myCONCAtomicModule_GL, 
                                           this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentTripleAtomicModule_GL This;
    protected PersistentAtomicModule_GL myCONCAtomicModule_GL;
    
    public TripleAtomicModule_GL(PersistentTripleAtomicModule_GL This,PersistentAtomicModule_GL myCONCAtomicModule_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCAtomicModule_GL = myCONCAtomicModule_GL;        
    }
    
    static public long getTypeId() {
        return 204;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 204) ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade
            .newTripleAtomicModule_GL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCAtomicModule_GL() != null){
            this.getMyCONCAtomicModule_GL().store();
            ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade.myCONCAtomicModule_GLSet(this.getId(), getMyCONCAtomicModule_GL());
        }
        
    }
    
    protected void setThis(PersistentTripleAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentTripleAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentAtomicModule_GL getMyCONCAtomicModule_GL() throws PersistenceException {
        return this.myCONCAtomicModule_GL;
    }
    public void setMyCONCAtomicModule_GL(PersistentAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCAtomicModule_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCAtomicModule_GL = (PersistentAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theTripleAtomicModule_GLFacade.myCONCAtomicModule_GLSet(this.getId(), newValue);
        }
    }
    public PersistentTripleAtomicModule_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentTripleAtomicModule_GL result = (PersistentTripleAtomicModule_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentTripleAtomicModule_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setInstanceOf(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModuleOrModuleGroup_GL();
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModuleOrModuleGroup_GL(newValue);
    }
    public CreditPoints4Public getStudentGroupCP() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getStudentGroupCP();
    }
    public void setStudentGroupCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setStudentGroupCP(newValue);
    }
    public PersistentModule_GL getMyCONCModule_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModule_GL();
    }
    public void setMyCONCModule_GL(PersistentModule_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModule_GL(newValue);
    }
    public PersistentCreditPointHavingElement_GL getMyCONCCreditPointHavingElement_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCCreditPointHavingElement_GL();
    }
    public void setMyCONCCreditPointHavingElement_GL(PersistentCreditPointHavingElement_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCCreditPointHavingElement_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCAtomicModule_GL().delete$Me();
    }
    
    public void accept(AtomicModule_GLVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(AtomicModule_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public void accept(CreditPointHavingElement_GLVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(CreditPointHavingElement_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public void accept(Module_GLVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(Module_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(Module_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(Module_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleTripleAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleTripleAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleTripleAtomicModule_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentTripleAtomicModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL = (PersistentCONCCreditPointHavingElement_GL) model.CONCCreditPointHavingElement_GL.createCONCCreditPointHavingElement_GL((PersistentCreditPoints)final$$Fields.get("studentGroupCP"), (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentTripleAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentTripleAtomicModule_GL)This);
			PersistentCONCAtomicModule_GL myCONCAtomicModule_GL = (PersistentCONCAtomicModule_GL) model.CONCAtomicModule_GL.createCONCAtomicModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), (PersistentCreditPoints)final$$Fields.get("studentGroupCP"), this.isDelayed$Persistence(), (PersistentTripleAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentTripleAtomicModule_GL)This);
			PersistentCONCModule_GL myCONCModule_GL = (PersistentCONCModule_GL) model.CONCModule_GL.createCONCModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentTripleAtomicModule_GL)This);
			this.setMyCONCAtomicModule_GL(myCONCAtomicModule_GL);
			myCONCAtomicModule_GL.setMyCONCCreditPointHavingElement_GL(myCONCCreditPointHavingElement_GL);
			myCONCCreditPointHavingElement_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			myCONCAtomicModule_GL.setMyCONCModule_GL(myCONCModule_GL);
			myCONCModule_GL.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        container.addContainee(TripleAtomicModule_SL.createTripleAtomicModule_SL(getThis()));
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().inverseGetContainees().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
