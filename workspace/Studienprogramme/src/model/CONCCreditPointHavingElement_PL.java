
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCCreditPointHavingElement_PL extends model.CreditPointHavingElement_PL implements PersistentCONCCreditPointHavingElement_PL{
    
    
    public static CONCCreditPointHavingElement_PL4Public createCONCCreditPointHavingElement_PL(CreditPoints4Public initialCP,String name,boolean delayed$Persistence,CONCCreditPointHavingElement_PL4Public This) throws PersistenceException {
        PersistentCONCCreditPointHavingElement_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_PLFacade
                .newDelayedCONCCreditPointHavingElement_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_PLFacade
                .newCONCCreditPointHavingElement_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("initialCP", initialCP);
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCCreditPointHavingElement_PL provideCopy() throws PersistenceException{
        CONCCreditPointHavingElement_PL result = this;
        result = new CONCCreditPointHavingElement_PL(this.initialCP, 
                                                     this.This, 
                                                     this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL, 
                                                     this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCCreditPointHavingElement_PL(PersistentCreditPoints initialCP,PersistentCreditPointHavingElement_PL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentCreditPoints)initialCP,(PersistentCreditPointHavingElement_PL)This,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,id);        
    }
    
    static public long getTypeId() {
        return 182;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 182) ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_PLFacade
            .newCONCCreditPointHavingElement_PL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCCreditPointHavingElement_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCCreditPointHavingElement_PL result = (PersistentCONCCreditPointHavingElement_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCCreditPointHavingElement_PL)this.This;
    }
    
    public void accept(CreditPointHavingElement_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R> R accept(CreditPointHavingElement_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getInitialCP() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCCreditPointHavingElement_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentCONCCreditPointHavingElement_PL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setInitialCP((PersistentCreditPoints)final$$Fields.get("initialCP"));
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getCalculatedCP();
    }
    public String initialCPAsString() 
				throws PersistenceException{
        return getThis().getInitialCP().toString();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
