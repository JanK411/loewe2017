
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModule_PL extends model.Module_PL implements PersistentCONCModule_PL{
    
    
    public static CONCModule_PL4Public createCONCModule_PL(String name,boolean delayed$Persistence,CONCModule_PL4Public This) throws PersistenceException {
        PersistentCONCModule_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModule_PLFacade
                .newDelayedCONCModule_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModule_PLFacade
                .newCONCModule_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModule_PL provideCopy() throws PersistenceException{
        CONCModule_PL result = this;
        result = new CONCModule_PL(this.This, 
                                   this.myCONCModuleOrModuleGroup_PL, 
                                   this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCModule_PL(PersistentModule_PL This,PersistentModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModule_PL)This,(PersistentModuleOrModuleGroup_PL)myCONCModuleOrModuleGroup_PL,id);        
    }
    
    static public long getTypeId() {
        return 173;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 173) ConnectionHandler.getTheConnectionHandler().theCONCModule_PLFacade
            .newCONCModule_PL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModule_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModule_PL result = (PersistentCONCModule_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModule_PL)this.This;
    }
    
    public void accept(Module_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_PL(this);
    }
    public <R> R accept(Module_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_PL(this);
    }
    public <E extends model.UserException>  void accept(Module_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(Module_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_PL(this);
    }
    public void accept(ModuleOrModuleGroup_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleCONCModule_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModule_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModule_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public ModuleGroupOrStudyProgram_PLSearchList getContainers() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_PLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		return result;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModule_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentCONCModule_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentCONCModule_PL)This);
			this.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        return getThis().calculateCP();
    }
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part);
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        return getThis().containsh_PL(part, visited);
    }
    public void createGlEquivalentAndAdd(final ModuleGroupOrStudyProgram_GL4Public container) 
				throws PersistenceException{
        getThis().createGlEquivalentAndAdd(container);
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getCalculatedCP();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().setInEditable();
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy);
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, visited);
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
