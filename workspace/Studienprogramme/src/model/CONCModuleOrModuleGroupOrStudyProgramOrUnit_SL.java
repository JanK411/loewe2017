
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL extends model.ModuleOrModuleGroupOrStudyProgramOrUnit_SL implements PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL{
    
    
    public static CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public This) throws PersistenceException {
        PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade
                .newDelayedCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade
                .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL provideCopy() throws PersistenceException{
        CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL result = this;
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this.instanceOf, 
                                                                    this.This, 
                                                                    this.getId());
        result.state = this.state;
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL instanceOf,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)instanceOf,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL)This,id);        
    }
    
    static public long getTypeId() {
        return -186;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -186) ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_SLFacade
            .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL result = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL)this.This;
    }
    
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL)This);
		if(this.isTheSameAs(This)){
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().calculateGrade();
    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
