
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class Unit_PL extends PersistentObject implements PersistentUnit_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Unit_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade.getClass(objectId);
        return (Unit_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static Unit_PL4Public createUnit_PL(CreditPoints4Public initialCP,String name) throws PersistenceException{
        return createUnit_PL(initialCP,name,false);
    }
    
    public static Unit_PL4Public createUnit_PL(CreditPoints4Public initialCP,String name,boolean delayed$Persistence) throws PersistenceException {
        PersistentUnit_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade
                .newDelayedUnit_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade
                .newUnit_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("initialCP", initialCP);
        final$$Fields.put("name", name);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static Unit_PL4Public createUnit_PL(CreditPoints4Public initialCP,String name,boolean delayed$Persistence,Unit_PL4Public This) throws PersistenceException {
        PersistentUnit_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade
                .newDelayedUnit_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade
                .newUnit_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("initialCP", initialCP);
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot container = (AbstractPersistentRoot)this.getContainer();
            if (container != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    container, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("container", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCCreditPointHavingElement_PL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_PL();
            if (myCONCCreditPointHavingElement_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public Unit_PL provideCopy() throws PersistenceException{
        Unit_PL result = this;
        result = new Unit_PL(this.This, 
                             this.myCONCCreditPointHavingElement_PL, 
                             this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentUnit_PL This;
    protected PersistentCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL;
    
    public Unit_PL(PersistentUnit_PL This,PersistentCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCCreditPointHavingElement_PL = myCONCCreditPointHavingElement_PL;        
    }
    
    static public long getTypeId() {
        return 159;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 159) ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade
            .newUnit_PL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCCreditPointHavingElement_PL() != null){
            this.getMyCONCCreditPointHavingElement_PL().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade.myCONCCreditPointHavingElement_PLSet(this.getId(), getMyCONCCreditPointHavingElement_PL());
        }
        
    }
    
    protected void setThis(PersistentUnit_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentUnit_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_PL getMyCONCCreditPointHavingElement_PL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_PL;
    }
    public void setMyCONCCreditPointHavingElement_PL(PersistentCreditPointHavingElement_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_PL = (PersistentCreditPointHavingElement_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_PLFacade.myCONCCreditPointHavingElement_PLSet(this.getId(), newValue);
        }
    }
    public PersistentUnit_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentUnit_PL result = (PersistentUnit_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentUnit_PL)this.This;
    }
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setName(newValue);
    }
    public CreditPoints4Public getInitialCP() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getInitialCP();
    }
    public void setInitialCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setInitialCP(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCCreditPointHavingElement_PL().delete$Me();
    }
    
    public void accept(CreditPointHavingElement_PLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_PL(this);
    }
    public <R> R accept(CreditPointHavingElement_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_PL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_PL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleUnit_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getInitialCP() != null) return 1;
        return 0;
    }
    
    
    public NotAtomicModule_PL4Public getContainer() 
				throws PersistenceException{
        NotAtomicModule_PLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
										.inverseGetUnits(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentUnit_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentUnit_PL)This);
			PersistentCONCCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL = (PersistentCONCCreditPointHavingElement_PL) model.CONCCreditPointHavingElement_PL.createCONCCreditPointHavingElement_PL((PersistentCreditPoints)final$$Fields.get("initialCP"), "", this.isDelayed$Persistence(), (PersistentUnit_PL)This);
			this.setMyCONCCreditPointHavingElement_PL(myCONCCreditPointHavingElement_PL);
			myCONCCreditPointHavingElement_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setInitialCP((PersistentCreditPoints)final$$Fields.get("initialCP"));
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        //TODO Check delegation to abstract class and overwrite if necessary!
        return this.getMyCONCCreditPointHavingElement_PL().calculateCP();
    }
    public void changeInitialCP(final common.Fraction newCP) 
				throws model.StateException, PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_PL().changeInitialCP(newCP);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public Unit_GL4Public createGLEquivalent() 
				throws PersistenceException{
        getThis().setEditable(Falsum.getTheFalsum());
        return Unit_GL.createUnit_GL(getThis().getInitialCP(), getThis());
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().getCalculatedCP();
    }
    public String initialCPAsString() 
				throws PersistenceException{
        return this.getMyCONCCreditPointHavingElement_PL().initialCPAsString();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public void setInEditable() 
				throws PersistenceException{
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().updateCreditPointCace();
        getThis().getContainer().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
