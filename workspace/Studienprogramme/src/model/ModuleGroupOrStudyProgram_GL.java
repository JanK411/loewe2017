
package model;

import persistence.*;

import java.util.Iterator;


/* Additional import section end */

public abstract class ModuleGroupOrStudyProgram_GL extends PersistentObject implements PersistentModuleGroupOrStudyProgram_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroupOrStudyProgram_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade.getClass(objectId);
        return (ModuleGroupOrStudyProgram_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("containees", this.getContainees().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
            if (myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleGroupOrStudyProgram_GL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected ModuleGroupOrStudyProgram_GL_ContaineesProxi containees;
    protected PersistentModuleGroupOrStudyProgram_GL This;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL;
    
    public ModuleGroupOrStudyProgram_GL(PersistentModuleGroupOrStudyProgram_GL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.containees = new ModuleGroupOrStudyProgram_GL_ContaineesProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL;        
    }
    
    static public long getTypeId() {
        return 228;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        this.getContainees().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() != null){
            this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GLSet(this.getId(), getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL());
        }
        
    }
    
    public ModuleGroupOrStudyProgram_GL_ContaineesProxi getContainees() throws PersistenceException {
        return this.containees;
    }
    protected void setThis(PersistentModuleGroupOrStudyProgram_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroupOrStudyProgram_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL;
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleGroupOrStudyProgram_GL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL()).setInstanceOf(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroupOrStudyProgram_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroupOrStudyProgram_GL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    
    public void addContainee(final ModuleOrModuleGroup_GL4Public containee) 
				throws PersistenceException{
        getThis().getContainees().add(containee);
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
