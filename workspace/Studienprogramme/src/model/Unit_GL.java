
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class Unit_GL extends PersistentObject implements PersistentUnit_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Unit_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade.getClass(objectId);
        return (Unit_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static Unit_GL4Public createUnit_GL(CreditPoints4Public studentGroupCP,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf) throws PersistenceException{
        return createUnit_GL(studentGroupCP,instanceOf,false);
    }
    
    public static Unit_GL4Public createUnit_GL(CreditPoints4Public studentGroupCP,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentUnit_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade
                .newDelayedUnit_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade
                .newUnit_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("studentGroupCP", studentGroupCP);
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type Unit_GL has not been initialized!",0);
        return result;
    }
    
    public static Unit_GL4Public createUnit_GL(CreditPoints4Public studentGroupCP,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,Unit_GL4Public This) throws PersistenceException {
        PersistentUnit_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade
                .newDelayedUnit_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade
                .newUnit_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("studentGroupCP", studentGroupCP);
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCCreditPointHavingElement_GL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_GL();
            if (myCONCCreditPointHavingElement_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public Unit_GL provideCopy() throws PersistenceException{
        Unit_GL result = this;
        result = new Unit_GL(this.This, 
                             this.myCONCCreditPointHavingElement_GL, 
                             this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentUnit_GL This;
    protected PersistentCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL;
    
    public Unit_GL(PersistentUnit_GL This,PersistentCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCCreditPointHavingElement_GL = myCONCCreditPointHavingElement_GL;        
    }
    
    static public long getTypeId() {
        return 151;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 151) ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade
            .newUnit_GL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCCreditPointHavingElement_GL() != null){
            this.getMyCONCCreditPointHavingElement_GL().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade.myCONCCreditPointHavingElement_GLSet(this.getId(), getMyCONCCreditPointHavingElement_GL());
        }
        
    }
    
    protected void setThis(PersistentUnit_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentUnit_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_GL getMyCONCCreditPointHavingElement_GL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_GL;
    }
    public void setMyCONCCreditPointHavingElement_GL(PersistentCreditPointHavingElement_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_GL = (PersistentCreditPointHavingElement_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_GLFacade.myCONCCreditPointHavingElement_GLSet(this.getId(), newValue);
        }
    }
    public PersistentUnit_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentUnit_GL result = (PersistentUnit_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentUnit_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).setInstanceOf(newValue);
    }
    public CreditPoints4Public getStudentGroupCP() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).getStudentGroupCP();
    }
    public void setStudentGroupCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).setStudentGroupCP(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCCreditPointHavingElement_GL().delete$Me();
    }
    
    public void accept(CreditPointHavingElement_GLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_GL(this);
    }
    public <R> R accept(CreditPointHavingElement_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_GL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_GL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleUnit_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentUnit_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL = (PersistentCONCCreditPointHavingElement_GL) model.CONCCreditPointHavingElement_GL.createCONCCreditPointHavingElement_GL((PersistentCreditPoints)final$$Fields.get("studentGroupCP"), (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentUnit_GL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentUnit_GL)This);
			this.setMyCONCCreditPointHavingElement_GL(myCONCCreditPointHavingElement_GL);
			myCONCCreditPointHavingElement_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    public NotAtomicModule_GL4Public inverseGetUnits() 
				throws PersistenceException{
        NotAtomicModule_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
										.inverseGetUnits(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void changeStudentGroupCP(final CreditPoints4Public newCP) 
				throws PersistenceException{
        this.getMyCONCCreditPointHavingElement_GL().setStudentGroupCP(newCP);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public Unit_SL4Public createSLEquivalent() 
				throws PersistenceException{
        return Unit_SL.createUnit_SL(getThis());
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().inverseGetUnits().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
