
package model;

import common.Fraction;
import common.Literals;
import common.NameAlreadyExistsHelper;
import persistence.*;
import model.visitor.*;

import java.text.MessageFormat;


/* Additional import section end */

public class NotAtomicModule_PL extends PersistentObject implements PersistentNotAtomicModule_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static NotAtomicModule_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade.getClass(objectId);
        return (NotAtomicModule_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static NotAtomicModule_PL4Public createNotAtomicModule_PL(String name) throws PersistenceException{
        return createNotAtomicModule_PL(name,false);
    }
    
    public static NotAtomicModule_PL4Public createNotAtomicModule_PL(String name,boolean delayed$Persistence) throws PersistenceException {
        PersistentNotAtomicModule_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
                .newDelayedNotAtomicModule_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
                .newNotAtomicModule_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static NotAtomicModule_PL4Public createNotAtomicModule_PL(String name,boolean delayed$Persistence,NotAtomicModule_PL4Public This) throws PersistenceException {
        PersistentNotAtomicModule_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
                .newDelayedNotAtomicModule_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
                .newNotAtomicModule_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("units", this.getUnits().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModule_PL = (AbstractPersistentRoot)this.getMyCONCModule_PL();
            if (myCONCModule_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public NotAtomicModule_PL provideCopy() throws PersistenceException{
        NotAtomicModule_PL result = this;
        result = new NotAtomicModule_PL(this.This, 
                                        this.myCONCModule_PL, 
                                        this.getId());
        result.units = this.units.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected NotAtomicModule_PL_UnitsProxi units;
    protected PersistentNotAtomicModule_PL This;
    protected PersistentModule_PL myCONCModule_PL;
    
    public NotAtomicModule_PL(PersistentNotAtomicModule_PL This,PersistentModule_PL myCONCModule_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.units = new NotAtomicModule_PL_UnitsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_PL = myCONCModule_PL;        
    }
    
    static public long getTypeId() {
        return 168;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 168) ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade
            .newNotAtomicModule_PL(this.getId());
        super.store();
        this.getUnits().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_PL() != null){
            this.getMyCONCModule_PL().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade.myCONCModule_PLSet(this.getId(), getMyCONCModule_PL());
        }
        
    }
    
    public NotAtomicModule_PL_UnitsProxi getUnits() throws PersistenceException {
        return this.units;
    }
    protected void setThis(PersistentNotAtomicModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentNotAtomicModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_PL getMyCONCModule_PL() throws PersistenceException {
        return this.myCONCModule_PL;
    }
    public void setMyCONCModule_PL(PersistentModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_PL = (PersistentModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_PLFacade.myCONCModule_PLSet(this.getId(), newValue);
        }
    }
    public PersistentNotAtomicModule_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNotAtomicModule_PL result = (PersistentNotAtomicModule_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNotAtomicModule_PL)this.This;
    }
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setName(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public PersistentModuleOrModuleGroup_PL getMyCONCModuleOrModuleGroup_PL() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getMyCONCModuleOrModuleGroup_PL();
    }
    public void setMyCONCModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setMyCONCModuleOrModuleGroup_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_PL().delete$Me();
    }
    
    public void accept(Module_PLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_PL(this);
    }
    public <R> R accept(Module_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(Module_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(Module_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public void accept(ModuleOrModuleGroup_PLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getUnits().getLength() > 0) return 1;
        return 0;
    }
    
    
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part, new java.util.HashSet<h_PLHIERARCHY>());
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        if(getThis().equals(part)) return true;
		if(visited.contains(getThis())) return false;
		visited.add(getThis());
		return false;
    }
    public ModuleGroupOrStudyProgram_PLSearchList getContainers() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_PLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		return result;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNotAtomicModule_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModule_PL myCONCModule_PL = (PersistentCONCModule_PL) model.CONCModule_PL.createCONCModule_PL("", this.isDelayed$Persistence(), (PersistentNotAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentNotAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentNotAtomicModule_PL)This);
			this.setMyCONCModule_PL(myCONCModule_PL);
			myCONCModule_PL.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, new java.util.HashMap<h_PLHIERARCHY,T>());
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        if (visited.containsKey(getThis())) return visited.get(getThis());
		T result = strategy.NotAtomicModule_PL$$finalize(getThis() );
		visited.put(getThis(),result);
		return result;
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addUnit(final Unit_PL4Public unit) 
				throws model.StateException, PersistenceException{
        if (!getThis().getEditable().value()) throw new StateException(Literals.IneditableMessage(getThis()));
        Unit_PL4Public foundSameUnit = getThis().getUnits().findFirst(x -> x.equals(unit));
        if (foundSameUnit == null) getThis().getUnits().add(unit);
        updateCreditPointCace();
    }
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        return getThis().getUnits().aggregate(CreditPointsManager.getTheCreditPointsManager().getCP(Fraction.Null), (prevCP, unit) ->
                CreditPointsManager.getTheCreditPointsManager().getCP(prevCP.getValue().add(unit.getCalculatedCP().getValue())));
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createGlEquivalentAndAdd(final ModuleGroupOrStudyProgram_GL4Public container) 
				throws PersistenceException{
        // TODO Template method setEditable wird immer gemacht
        // TODO createGLEquivalent sollte auch in die oberste Klasse
        getThis().setEditable(Falsum.getTheFalsum());
        NotAtomicModule_GL4Public notAtomicModule_gl = NotAtomicModule_GL.createNotAtomicModule_GL(getThis());
        getThis().getUnits().applyToAll(x -> notAtomicModule_gl.addUnit(x.createGLEquivalent()));
        container.addContainee(notAtomicModule_gl);
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().getCalculatedCP();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public void removeUnit(final Unit_PL4Public unit) 
				throws model.StateException, PersistenceException{
        //TODO falls das hier nur conditional (checked) angeboten wird, kann das mit der Exception wegfallen
        if (!getThis().getEditable().value()) throw new StateException(Literals.IneditableMessage(getThis()));
        getThis().getUnits().removeFirst(unit);
        getThis().updateCreditPointCace();
    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().updateCreditPointCace();
        getThis().getMyCONCModuleOrModuleGroup_PL().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
