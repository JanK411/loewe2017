
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCCreditPointHavingElement_GL extends model.CreditPointHavingElement_GL implements PersistentCONCCreditPointHavingElement_GL{
    
    
    public static CONCCreditPointHavingElement_GL4Public createCONCCreditPointHavingElement_GL(CreditPoints4Public studentGroupCP,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,CONCCreditPointHavingElement_GL4Public This) throws PersistenceException {
        PersistentCONCCreditPointHavingElement_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_GLFacade
                .newDelayedCONCCreditPointHavingElement_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_GLFacade
                .newCONCCreditPointHavingElement_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("studentGroupCP", studentGroupCP);
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCCreditPointHavingElement_GL provideCopy() throws PersistenceException{
        CONCCreditPointHavingElement_GL result = this;
        result = new CONCCreditPointHavingElement_GL(this.studentGroupCP, 
                                                     this.This, 
                                                     this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL, 
                                                     this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCCreditPointHavingElement_GL(PersistentCreditPoints studentGroupCP,PersistentCreditPointHavingElement_GL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentCreditPoints)studentGroupCP,(PersistentCreditPointHavingElement_GL)This,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL,id);        
    }
    
    static public long getTypeId() {
        return 193;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 193) ConnectionHandler.getTheConnectionHandler().theCONCCreditPointHavingElement_GLFacade
            .newCONCCreditPointHavingElement_GL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCCreditPointHavingElement_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCCreditPointHavingElement_GL result = (PersistentCONCCreditPointHavingElement_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCCreditPointHavingElement_GL)this.This;
    }
    
    public void accept(CreditPointHavingElement_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R> R accept(CreditPointHavingElement_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCCreditPointHavingElement_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCCreditPointHavingElement_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCCreditPointHavingElement_GL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
