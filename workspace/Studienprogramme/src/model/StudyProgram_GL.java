
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class StudyProgram_GL extends PersistentObject implements PersistentStudyProgram_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static StudyProgram_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade.getClass(objectId);
        return (StudyProgram_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static StudyProgram_GL4Public createStudyProgram_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf) throws PersistenceException{
        return createStudyProgram_GL(instanceOf,false);
    }
    
    public static StudyProgram_GL4Public createStudyProgram_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentStudyProgram_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade
                .newDelayedStudyProgram_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade
                .newStudyProgram_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type StudyProgram_GL has not been initialized!",0);
        return result;
    }
    
    public static StudyProgram_GL4Public createStudyProgram_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,StudyProgram_GL4Public This) throws PersistenceException {
        PersistentStudyProgram_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade
                .newDelayedStudyProgram_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade
                .newStudyProgram_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot studentGroup = (AbstractPersistentRoot)this.getStudentGroup();
            if (studentGroup != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    studentGroup, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("studentGroup", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCModuleGroupOrStudyProgram_GL = (AbstractPersistentRoot)this.getMyCONCModuleGroupOrStudyProgram_GL();
            if (myCONCModuleGroupOrStudyProgram_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleGroupOrStudyProgram_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleGroupOrStudyProgram_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public StudyProgram_GL provideCopy() throws PersistenceException{
        StudyProgram_GL result = this;
        result = new StudyProgram_GL(this.This, 
                                     this.myCONCModuleGroupOrStudyProgram_GL, 
                                     this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentStudyProgram_GL This;
    protected PersistentModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL;
    
    public StudyProgram_GL(PersistentStudyProgram_GL This,PersistentModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleGroupOrStudyProgram_GL = myCONCModuleGroupOrStudyProgram_GL;        
    }
    
    static public long getTypeId() {
        return 220;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 220) ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade
            .newStudyProgram_GL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleGroupOrStudyProgram_GL() != null){
            this.getMyCONCModuleGroupOrStudyProgram_GL().store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade.myCONCModuleGroupOrStudyProgram_GLSet(this.getId(), getMyCONCModuleGroupOrStudyProgram_GL());
        }
        
    }
    
    protected void setThis(PersistentStudyProgram_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudyProgram_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroupOrStudyProgram_GL getMyCONCModuleGroupOrStudyProgram_GL() throws PersistenceException {
        return this.myCONCModuleGroupOrStudyProgram_GL;
    }
    public void setMyCONCModuleGroupOrStudyProgram_GL(PersistentModuleGroupOrStudyProgram_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleGroupOrStudyProgram_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleGroupOrStudyProgram_GL = (PersistentModuleGroupOrStudyProgram_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudyProgram_GLFacade.myCONCModuleGroupOrStudyProgram_GLSet(this.getId(), newValue);
        }
    }
    public PersistentStudyProgram_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudyProgram_GL result = (PersistentStudyProgram_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudyProgram_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).setInstanceOf(newValue);
    }
    public ModuleGroupOrStudyProgram_GL_ContaineesProxi getContainees() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getContainees();
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleGroupOrStudyProgram_GL().delete$Me();
    }
    
    public void accept(StudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_GL(this);
    }
    public <R> R accept(StudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(StudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(StudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_GL(this);
    }
    public void accept(ModuleGroupOrStudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_GL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudyProgram_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudyProgram_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public StudentGroup4Public getStudentGroup() 
				throws PersistenceException{
        StudentGroupSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theStudentGroupFacade
										.inverseGetStudyProgram(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudyProgram_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentStudyProgram_GL)This);
			PersistentCONCModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL = (PersistentCONCModuleGroupOrStudyProgram_GL) model.CONCModuleGroupOrStudyProgram_GL.createCONCModuleGroupOrStudyProgram_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentStudyProgram_GL)This);
			this.setMyCONCModuleGroupOrStudyProgram_GL(myCONCModuleGroupOrStudyProgram_GL);
			myCONCModuleGroupOrStudyProgram_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addContainee(final ModuleOrModuleGroup_GL4Public containee) 
				throws PersistenceException{
        this.getMyCONCModuleGroupOrStudyProgram_GL().addContainee(containee);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
        

    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getStudentGroup().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getInstanceOf().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        

    }
    public void populateProgramSL(final StudyProgram_SL4Public program) 
				throws PersistenceException{
        getThis().getContainees().applyToAll(x -> x.createSLEquivalentAndAdd(program));
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
