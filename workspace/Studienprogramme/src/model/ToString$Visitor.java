package model;

import common.MyToString$Visitor;
import persistence.*;

import static common.Literals.Note;

public class ToString$Visitor extends MyToString$Visitor {


    private static final String openBracketWithSpace = " (";
    private static final String spaceCpCloseBracked = " cp)";
    private static final String slash = "/";

    public ToString$Visitor() {
    }


    protected void standardHandling(Anything anything) {
        result = anything.getClassId() + ";" + anything.getId();
    }


    @Override
    public void handleServer(Server4Public server) throws PersistenceException {

    }


    @Override
    public void handlePassedNotPassedGrades(PassedNotPassedGrades4Public passedNotPassedGrades) throws PersistenceException {

    }


    @Override
    public void handleGradeHistory(GradeHistory4Public gradeHistory) throws PersistenceException {
        result = "Notenhistorie:";
    }

    @Override
    public void handleStudentManager(StudentManager4Public studentManager) throws PersistenceException {
        result = "Studiengruppen";
    }

    @Override
    public void handleGradeHistoryElement(GradeHistoryElement4Public gradeHistoryElement) throws PersistenceException {
        result = "Note: " + gradeHistoryElement.getGrade().toString(true) +
                " Kommentar: " + gradeHistoryElement.getComment() + " Erfassungsdatum: " + gradeHistoryElement.getTimestamp();
    }

    @Override
    public void handleNoGrade(NoGrade4Public noGrade) throws PersistenceException {
        result = "keine Note";
    }

    @Override
    public void handleNotPassed(NotPassed4Public notPassed) throws PersistenceException {
        result = "Nicht bestanden";
    }

    @Override
    public void handleNothingCachedState_PL(NothingCachedState_PL4Public nothingCachedState_PL) throws PersistenceException {
        result = "nothing cached";
    }


    @Override
    public void handleStudent(Student4Public student) throws PersistenceException {
        result = student.getMatNr() + openBracketWithSpace + student.getName() + ", " + student.getFirstName() + ")";
    }

    @Override
    public void handleGradeCachedState_SL(GradeCachedState_SL4Public gradeCachedState_SL) throws PersistenceException {
        result = "grade cached";
    }

    @Override
    public void handleNoStudentGroup(NoStudentGroup4Public noStudentGroup) throws PersistenceException {
        result = "Keine Studiengruppe";
    }


    @Override
    public void handleCreditPointsManager(CreditPointsManager4Public creditPointsManager) throws PersistenceException {
        result = "CreditPointsManager";
    }

    @Override
    public void handleStudentGroup(StudentGroup4Public studentGroup) throws PersistenceException {
        result = studentGroup.getName();
    }

    @Override
    public void handlePassed(Passed4Public passed) throws PersistenceException {
        result = "Bestanden";
    }

    @Override
    public void handleGradeAggregator(GradeAggregator4Public gradeAggregator) throws PersistenceException {

    }

    @Override
    public void handleAdminService(AdminService4Public adminService) throws PersistenceException {

    }


    @Override
    public void handleProgramManager(ProgramManager4Public programManager) throws PersistenceException {
        result = "Programme";
    }

    @Override
    public void handleStudentService(StudentService4Public studentService) throws PersistenceException {

    }

    @Override
    public void handleVerum(Verum4Public verum) throws PersistenceException {
        result = "true";
    }

    @Override
    public void handleCreditPoints(CreditPoints4Public creditPoints) throws PersistenceException {
        result = creditPoints.getValue().toString();
    }


    @Override
    public void handleNoProgram_SL(NoProgram_SL4Public noProgram_SL) throws PersistenceException {
        result = "Kein Studienprogramm";

    }

    @Override
    public void handleCreditPointsCachedState_PL(CreditPointsCachedState_PL4Public creditPointsCachedState_PL) throws PersistenceException {
        result = "creditpoints cached";
    }

    @Override
    public void handleFinished(Finished4Public finished) throws PersistenceException {
        result = "fertig studiert";
    }

    @Override
    public void handleActive(Active4Public active) throws PersistenceException {
        result = "aktiv am Studieren";
    }


    @Override
    public void handleStudentWrapper(StudentWrapper4Public studentWrapper) throws PersistenceException {
        result = studentWrapper.getWrappedStudent().toString(true);
    }

    @Override
    public void handleNotYetStudied(NotYetStudied4Public notYetStudied) throws PersistenceException {
        result = "inaktiv nicht am Studieren";
    }

    @Override
    public void handleFalsum(Falsum4Public falsum) throws PersistenceException {
        result = "false";
    }

    @Override
    public void handleNoProgram_GL(NoProgram_GL4Public noProgram_GL) throws PersistenceException {
        result = "Kein Studienprogramm";
    }

    @Override
    public void handleRealGrades(RealGrades4Public realGrades) throws PersistenceException {

    }

    @Override
    public void handleNothingCachedState_SL(NothingCachedState_SL4Public nothingCachedState_SL) throws PersistenceException {
        result = "nothing cached";
    }

    @Override
    protected void handleGrade(CalculatableGrade4Public grade) throws PersistenceException {
        result = grade.getValue().toString();
    }

    @Override
    protected void handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public element) throws PersistenceException {
        result = element.getName() + openBracketWithSpace + element.getCalculatedCP() + spaceCpCloseBracked;
    }


    @Override
    protected void handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public element) throws PersistenceException {
        result = element.getName() + openBracketWithSpace + element.getCalculatedStudentGroupCP() + spaceCpCloseBracked;
    }

    @Override
    protected void handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public element) throws PersistenceException {
        result = element.getName() + openBracketWithSpace + element.getAlreadyEarnedCP()+ slash + element.getCp() + spaceCpCloseBracked + Note + element.getCalculatedStudentGrade().toString(true);
    }
}
