
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class BinaryAtomicModule_GL extends PersistentObject implements PersistentBinaryAtomicModule_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static BinaryAtomicModule_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade.getClass(objectId);
        return (BinaryAtomicModule_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static BinaryAtomicModule_GL4Public createBinaryAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP) throws PersistenceException{
        return createBinaryAtomicModule_GL(instanceOf,studentGroupCP,false);
    }
    
    public static BinaryAtomicModule_GL4Public createBinaryAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP,boolean delayed$Persistence) throws PersistenceException {
        PersistentBinaryAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade
                .newDelayedBinaryAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade
                .newBinaryAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        final$$Fields.put("studentGroupCP", studentGroupCP);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type BinaryAtomicModule_GL has not been initialized!",0);
        return result;
    }
    
    public static BinaryAtomicModule_GL4Public createBinaryAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,CreditPoints4Public studentGroupCP,boolean delayed$Persistence,BinaryAtomicModule_GL4Public This) throws PersistenceException {
        PersistentBinaryAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade
                .newDelayedBinaryAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade
                .newBinaryAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        final$$Fields.put("studentGroupCP", studentGroupCP);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCAtomicModule_GL = (AbstractPersistentRoot)this.getMyCONCAtomicModule_GL();
            if (myCONCAtomicModule_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCAtomicModule_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCAtomicModule_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public BinaryAtomicModule_GL provideCopy() throws PersistenceException{
        BinaryAtomicModule_GL result = this;
        result = new BinaryAtomicModule_GL(this.This, 
                                           this.myCONCAtomicModule_GL, 
                                           this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentBinaryAtomicModule_GL This;
    protected PersistentAtomicModule_GL myCONCAtomicModule_GL;
    
    public BinaryAtomicModule_GL(PersistentBinaryAtomicModule_GL This,PersistentAtomicModule_GL myCONCAtomicModule_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCAtomicModule_GL = myCONCAtomicModule_GL;        
    }
    
    static public long getTypeId() {
        return 230;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 230) ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade
            .newBinaryAtomicModule_GL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCAtomicModule_GL() != null){
            this.getMyCONCAtomicModule_GL().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade.myCONCAtomicModule_GLSet(this.getId(), getMyCONCAtomicModule_GL());
        }
        
    }
    
    protected void setThis(PersistentBinaryAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentBinaryAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentAtomicModule_GL getMyCONCAtomicModule_GL() throws PersistenceException {
        return this.myCONCAtomicModule_GL;
    }
    public void setMyCONCAtomicModule_GL(PersistentAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCAtomicModule_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCAtomicModule_GL = (PersistentAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_GLFacade.myCONCAtomicModule_GLSet(this.getId(), newValue);
        }
    }
    public PersistentBinaryAtomicModule_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentBinaryAtomicModule_GL result = (PersistentBinaryAtomicModule_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentBinaryAtomicModule_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setInstanceOf(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModuleOrModuleGroup_GL();
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModuleOrModuleGroup_GL(newValue);
    }
    public CreditPoints4Public getStudentGroupCP() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getStudentGroupCP();
    }
    public void setStudentGroupCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setStudentGroupCP(newValue);
    }
    public PersistentModule_GL getMyCONCModule_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCModule_GL();
    }
    public void setMyCONCModule_GL(PersistentModule_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCModule_GL(newValue);
    }
    public PersistentCreditPointHavingElement_GL getMyCONCCreditPointHavingElement_GL() throws PersistenceException {
        return ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).getMyCONCCreditPointHavingElement_GL();
    }
    public void setMyCONCCreditPointHavingElement_GL(PersistentCreditPointHavingElement_GL newValue) throws PersistenceException {
        ((PersistentAtomicModule_GL)this.getMyCONCAtomicModule_GL()).setMyCONCCreditPointHavingElement_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCAtomicModule_GL().delete$Me();
    }
    
    public void accept(AtomicModule_GLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(AtomicModule_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public void accept(CreditPointHavingElement_GLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(CreditPointHavingElement_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public void accept(Module_GLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(Module_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(Module_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(Module_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentBinaryAtomicModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL = (PersistentCONCCreditPointHavingElement_GL) model.CONCCreditPointHavingElement_GL.createCONCCreditPointHavingElement_GL((PersistentCreditPoints)final$$Fields.get("studentGroupCP"), (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_GL)This);
			PersistentCONCAtomicModule_GL myCONCAtomicModule_GL = (PersistentCONCAtomicModule_GL) model.CONCAtomicModule_GL.createCONCAtomicModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), (PersistentCreditPoints)final$$Fields.get("studentGroupCP"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_GL)This);
			PersistentCONCModule_GL myCONCModule_GL = (PersistentCONCModule_GL) model.CONCModule_GL.createCONCModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_GL)This);
			this.setMyCONCAtomicModule_GL(myCONCAtomicModule_GL);
			myCONCAtomicModule_GL.setMyCONCCreditPointHavingElement_GL(myCONCCreditPointHavingElement_GL);
			myCONCCreditPointHavingElement_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			myCONCAtomicModule_GL.setMyCONCModule_GL(myCONCModule_GL);
			myCONCModule_GL.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        container.addContainee(BinaryAtomicModule_SL.createBinaryAtomicModule_SL(getThis()));
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().inverseGetContainees().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
