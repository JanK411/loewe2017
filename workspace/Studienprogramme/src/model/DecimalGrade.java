
package model;

import persistence.*;


/* Additional import section end */

public abstract class DecimalGrade extends PersistentObject implements PersistentDecimalGrade{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static DecimalGrade4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theDecimalGradeFacade.getClass(objectId);
        return (DecimalGrade4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCCalculatableGrade = (AbstractPersistentRoot)this.getMyCONCCalculatableGrade();
            if (myCONCCalculatableGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCalculatableGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCalculatableGrade", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract DecimalGrade provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentDecimalGrade This;
    protected PersistentCalculatableGrade myCONCCalculatableGrade;
    
    public DecimalGrade(PersistentDecimalGrade This,PersistentCalculatableGrade myCONCCalculatableGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCCalculatableGrade = myCONCCalculatableGrade;        
    }
    
    static public long getTypeId() {
        return 156;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theDecimalGradeFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCCalculatableGrade() != null){
            this.getMyCONCCalculatableGrade().store();
            ConnectionHandler.getTheConnectionHandler().theDecimalGradeFacade.myCONCCalculatableGradeSet(this.getId(), getMyCONCCalculatableGrade());
        }
        
    }
    
    protected void setThis(PersistentDecimalGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentDecimalGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theDecimalGradeFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCalculatableGrade getMyCONCCalculatableGrade() throws PersistenceException {
        return this.myCONCCalculatableGrade;
    }
    public void setMyCONCCalculatableGrade(PersistentCalculatableGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCalculatableGrade)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCalculatableGrade = (PersistentCalculatableGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theDecimalGradeFacade.myCONCCalculatableGradeSet(this.getId(), newValue);
        }
    }
    public abstract PersistentDecimalGrade getThis() throws PersistenceException ;
    public common.Fraction getValue() throws PersistenceException {
        return ((PersistentCalculatableGrade)this.getMyCONCCalculatableGrade()).getValue();
    }
    public void setValue(common.Fraction newValue) throws PersistenceException {
        ((PersistentCalculatableGrade)this.getMyCONCCalculatableGrade()).setValue(newValue);
    }
    public PersistentGrade getMyCONCGrade() throws PersistenceException {
        return ((PersistentCalculatableGrade)this.getMyCONCCalculatableGrade()).getMyCONCGrade();
    }
    public void setMyCONCGrade(PersistentGrade newValue) throws PersistenceException {
        ((PersistentCalculatableGrade)this.getMyCONCCalculatableGrade()).setMyCONCGrade(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCCalculatableGrade().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentDecimalGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (PersistentDecimalGrade)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentDecimalGrade)This);
			this.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
