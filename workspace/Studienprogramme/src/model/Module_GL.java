
package model;

import persistence.*;


/* Additional import section end */

public abstract class Module_GL extends PersistentObject implements PersistentModule_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Module_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModule_GLFacade.getClass(objectId);
        return (Module_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleOrModuleGroup_GL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_GL();
            if (myCONCModuleOrModuleGroup_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract Module_GL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModule_GL This;
    protected PersistentModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL;
    
    public Module_GL(PersistentModule_GL This,PersistentModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroup_GL = myCONCModuleOrModuleGroup_GL;        
    }
    
    static public long getTypeId() {
        return 190;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModule_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroup_GL() != null){
            this.getMyCONCModuleOrModuleGroup_GL().store();
            ConnectionHandler.getTheConnectionHandler().theModule_GLFacade.myCONCModuleOrModuleGroup_GLSet(this.getId(), getMyCONCModuleOrModuleGroup_GL());
        }
        
    }
    
    protected void setThis(PersistentModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_GL;
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_GL = (PersistentModuleOrModuleGroup_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_GLFacade.myCONCModuleOrModuleGroup_GLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModule_GL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_GL)this.getMyCONCModuleOrModuleGroup_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_GL)this.getMyCONCModuleOrModuleGroup_GL()).setInstanceOf(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_GL)this.getMyCONCModuleOrModuleGroup_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_GL)this.getMyCONCModuleOrModuleGroup_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroup_GL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModule_GL)This);
			this.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
