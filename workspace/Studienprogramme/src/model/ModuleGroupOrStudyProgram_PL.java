
package model;

import common.Fraction;
import common.Literals;
import model.visitor.ModuleOrModuleGroup_PLReturnVisitor;
import model.visitor.ModuleOrModuleGroup_PLVisitor;
import persistence.*;

import java.text.MessageFormat;
import java.util.Iterator;


/* Additional import section end */

public abstract class ModuleGroupOrStudyProgram_PL extends PersistentObject implements PersistentModuleGroupOrStudyProgram_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroupOrStudyProgram_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade.getClass(objectId);
        return (ModuleGroupOrStudyProgram_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("containees", this.getContainees().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
            if (myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleGroupOrStudyProgram_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected ModuleGroupOrStudyProgram_PL_ContaineesProxi containees;
    protected PersistentModuleGroupOrStudyProgram_PL This;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;
    
    public ModuleGroupOrStudyProgram_PL(PersistentModuleGroupOrStudyProgram_PL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.containees = new ModuleGroupOrStudyProgram_PL_ContaineesProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;        
    }
    
    static public long getTypeId() {
        return 197;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        this.getContainees().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() != null){
            this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLSet(this.getId(), getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL());
        }
        
    }
    
    public ModuleGroupOrStudyProgram_PL_ContaineesProxi getContainees() throws PersistenceException {
        return this.containees;
    }
    protected void setThis(PersistentModuleGroupOrStudyProgram_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroupOrStudyProgram_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleGroupOrStudyProgram_PL getThis() throws PersistenceException ;
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setName(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroupOrStudyProgram_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentModuleGroupOrStudyProgram_PL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    
    public void addContainees(final ModuleOrModuleGroup_PLSearchList containees) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        for (ModuleOrModuleGroup_PL4Public current : containees) {
            getThis().addContainee(current);
        }
    }
    public void addContainee(final ModuleOrModuleGroup_PL4Public containee) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        if (getThis().getEditable().equals(Falsum.getTheFalsum()))
            throw new StateException(Literals.IneditableMessage(getThis()));

        if (!this.alreadyInContainees(containee)) {
            if (getThis().alreadyContainsModuleOrModuleGroupAndTheirContainees(containee).value()) {
                throw new AlreadyInListException(Literals.AlreadyInListMessage(containee, getThis()));
            }
            // Hier sollte die Hierarchy cycles erkennen, da bin ich mir aber nicht sicher TODO hierarchy ???
            if (containee.containsh_PL(getThis())) throw new CycleException("Cycle detected");
            getThis().getContainees().add(containee);
        }
        getThis().updateCreditPointCace();
    }
    public Bool4Public alreadyContainsModuleOrModuleGroupAndTheirContainees(final ModuleOrModuleGroup_PL4Public container) 
				throws PersistenceException{
        return container.accept(new ModuleOrModuleGroup_PLReturnVisitor<Bool4Public>() {

            @Override
            public Bool4Public handleModuleGroup_PL(ModuleGroup_PL4Public moduleGroup_PL) throws PersistenceException {
                if (getThis().alreadyContainsModuleOrModuleGroup(moduleGroup_PL).value()) return Verum.getTheVerum();
                return moduleGroup_PL.getContainees().aggregate(new Aggregtion<ModuleOrModuleGroup_PL4Public, Bool4Public>() {
                    @Override
                    public Bool4Public neutral() throws PersistenceException {
                        return Falsum.getTheFalsum();
                    }

                    @Override
                    public Bool4Public compose(Bool4Public bool, ModuleOrModuleGroup_PL4Public moduleOrModuleGroup_pl4Public) throws PersistenceException {
                        return bool.lazyOr(getThis().alreadyContainsModuleOrModuleGroupAndTheirContainees(moduleOrModuleGroup_pl4Public));
                    }
                });
//                Iterator<ModuleOrModuleGroup_PL4Public> it = moduleGroup_PL.getContainees().iterator();
//                Falsum4Public aggregateElement = Falsum.getTheFalsum();
//                while (it.hasNext()) {
//                    ModuleOrModuleGroup_PL4Public current = it.next();
//                    aggregateElement.lazyOr(getThis().alreadyContainsModuleOrModuleGroupAndTheirContainees(current));
//                }
//                return aggregateElement;
            }


            @Override
            public Bool4Public handleNotAtomicModule_PL(NotAtomicModule_PL4Public notAtomicModule_PL) throws PersistenceException {
                return getThis().alreadyContainsModuleOrModuleGroup(notAtomicModule_PL);
            }

            @Override
            public Bool4Public handleBinaryAtomicModule_PL(BinaryAtomicModule_PL4Public binaryAtomicModule_PL) throws PersistenceException {

                return getThis().alreadyContainsModuleOrModuleGroup(binaryAtomicModule_PL);
            }


            @Override
            public Bool4Public handleTripleAtomicModule_PL(TripleAtomicModule_PL4Public tripleAtomicModule_PL) throws PersistenceException {

                return getThis().alreadyContainsModuleOrModuleGroup(tripleAtomicModule_PL);
            }

            @Override
            public Bool4Public handleCONCModuleOrModuleGroup_PL(CONCModuleOrModuleGroup_PL4Public cONCModuleOrModuleGroup_PL) throws PersistenceException {
                // Should not occure TODO irgendwie nervt das ein wenig
                throw new Error("");
            }

            @Override
            public Bool4Public handleCONCModule_PL(CONCModule_PL4Public cONCModule_PL) throws PersistenceException {
                // Should not occure TODO irgendwie nervt das ein wenig
                throw new Error("");
            }

            @Override
            public Bool4Public handleCONCAtomicModule_PL(CONCAtomicModule_PL4Public cONCAtomicModule_PL) throws PersistenceException {
                // Should not occure TODO irgendwie nervt das ein wenig
                throw new Error("");
            }
        });
    }
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        return getThis().getContainees().aggregate(CreditPointsManager.getTheCreditPointsManager().getCP(Fraction.Null),
                (prevCP, containee) -> CreditPointsManager.getTheCreditPointsManager().getCP(prevCP.getValue().add(containee.getCalculatedCP().getValue())));
    }
    public ModuleOrModuleGroup_PLSearchList getOneLayerContainees() 
				throws PersistenceException{
        return getThis().getContainees().getList();
    }

    /* Start of protected part that is not overridden by persistence generator */

    private boolean alreadyInContainees(ModuleOrModuleGroup_PL4Public current) throws PersistenceException {
        ModuleOrModuleGroup_PL4Public found = getThis().getContainees().findFirst(x -> x.equals(current));
        if (found == null) return false;
        return true;
    }

    /* End of protected part that is not overridden by persistence generator */
    
}
