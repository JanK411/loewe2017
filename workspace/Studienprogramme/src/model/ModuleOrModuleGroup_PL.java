
package model;

import persistence.*;


/* Additional import section end */

public abstract class ModuleOrModuleGroup_PL extends PersistentObject implements PersistentModuleOrModuleGroup_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleOrModuleGroup_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroup_PLFacade.getClass(objectId);
        return (ModuleOrModuleGroup_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("containers", this.getContainers().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
            if (myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleOrModuleGroup_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModuleOrModuleGroup_PL This;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;
    
    public ModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;        
    }
    
    static public long getTypeId() {
        return 235;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroup_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() != null){
            this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroup_PLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLSet(this.getId(), getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL());
        }
        
    }
    
    protected void setThis(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleOrModuleGroup_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroup_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL;
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroup_PLFacade.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleOrModuleGroup_PL getThis() throws PersistenceException ;
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL()).setName(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().delete$Me();
    }
    
    
    
    public ModuleGroupOrStudyProgram_PLSearchList getContainers() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_PLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		return result;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleOrModuleGroup_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentModuleOrModuleGroup_PL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getContainers().applyToAll(container -> container.updateCreditPointCace());
    }

    /* Start of protected part that is not overridden by persistence generator */

    
    /* End of protected part that is not overridden by persistence generator */
    
}
