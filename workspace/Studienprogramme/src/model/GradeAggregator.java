
package model;

import common.Fraction;
import model.meta.DecimalGradeSwitchPARAMETER;
import model.meta.StringFACTORY;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class GradeAggregator extends PersistentObject implements PersistentGradeAggregator{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static GradeAggregator4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade.getClass(objectId);
        return (GradeAggregator4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static GradeAggregator4Public createGradeAggregator() throws PersistenceException{
        return createGradeAggregator(false);
    }
    
    public static GradeAggregator4Public createGradeAggregator(boolean delayed$Persistence) throws PersistenceException {
        PersistentGradeAggregator result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade
                .newDelayedGradeAggregator();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade
                .newGradeAggregator(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static GradeAggregator4Public createGradeAggregator(boolean delayed$Persistence,GradeAggregator4Public This) throws PersistenceException {
        PersistentGradeAggregator result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade
                .newDelayedGradeAggregator();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade
                .newGradeAggregator(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("elements", this.getElements().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
        }
        return result;
    }
    
    public GradeAggregator provideCopy() throws PersistenceException{
        GradeAggregator result = this;
        result = new GradeAggregator(this.This, 
                                     this.getId());
        result.elements = this.elements.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected GradeAggregator_ElementsProxi elements;
    protected PersistentGradeAggregator This;
    
    public GradeAggregator(PersistentGradeAggregator This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.elements = new GradeAggregator_ElementsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 281;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 281) ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade
            .newGradeAggregator(this.getId());
        super.store();
        this.getElements().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public GradeAggregator_ElementsProxi getElements() throws PersistenceException {
        return this.elements;
    }
    protected void setThis(PersistentGradeAggregator newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentGradeAggregator)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theGradeAggregatorFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentGradeAggregator getThis() throws PersistenceException {
        if(this.This == null){
            PersistentGradeAggregator result = (PersistentGradeAggregator)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentGradeAggregator)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleGradeAggregator(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleGradeAggregator(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleGradeAggregator(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleGradeAggregator(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getElements().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentGradeAggregator)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addElementIfRelevantWithBinaryModule(final CreditPointHavingElement_SL4Public element) 
				throws PersistenceException{
        Bool4Public relevant = isRelevant(element, Verum.getTheVerum());
        if (relevant.value()) {
            getThis().getElements().add(element);
        }

    }
    public void addElementIfRelevant(final CreditPointHavingElement_SL4Public element) 
				throws PersistenceException{
        // Ein Element ist genau dann "relevant", wenn es sich um ein TripleAtomicModule oder eine Unit handelt. Zusätzlich muss es bereits eine Note zu diesem Element geben.
        Bool4Public relevant = isRelevant(element, Falsum.getTheFalsum());
        if (relevant.value()) {
            getThis().getElements().add(element);
        }
    }
    public Grade4Public calcDecimalGrade() 
				throws PersistenceException{
        if (noElements()) {
            return NoGrade.getTheNoGrade();
        }
        Fraction mittel = calcArithmetischedMittel();
        Fraction zehntelnote = mittel.roundToZehntelnoten();
        return createDecimalGradeFromFraction(zehntelnote);

    }
    public Grade4Public calcTripleGrade() 
				throws PersistenceException{
        if (noElements()) {
            return NoGrade.getTheNoGrade();
        }
        Fraction mittel = calcArithmetischedMittel();
        Fraction drittelnote = mittel.roundToDrittelnoten();
        return createTripleGradeFromFraction(drittelnote);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public common.Fraction sumOfAllCPs() 
				throws PersistenceException{
        Fraction sumOfAllCps = getThis().getElements().aggregate(new Aggregtion<CreditPointHavingElement_SL4Public, Fraction>() {
            @Override
            public Fraction neutral() throws PersistenceException {
                return Fraction.Null;
            }

            @Override
            public Fraction compose(Fraction fraction, CreditPointHavingElement_SL4Public element) throws PersistenceException {
                Fraction cpAsFraction = element.getCp().getValue();
                return fraction.add(cpAsFraction);
            }
        });
        return sumOfAllCps;
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */

    private Fraction calcArithmetischedMittel() throws PersistenceException {
        Fraction sumOfAllCPs = this.sumOfAllCPs();
        Fraction gradesMulCps = this.mulAllGradedWithCPs();

        return gradesMulCps.div(sumOfAllCPs);

    }


    private common.Fraction mulAllGradedWithCPs()
            throws PersistenceException {
        Fraction allGradesMulCPs = getThis().getElements().aggregate(new Aggregtion<CreditPointHavingElement_SL4Public, Fraction>() {
            @Override
            public Fraction neutral() throws PersistenceException {
                return Fraction.Null;
            }

            @Override
            public Fraction compose(Fraction fraction, CreditPointHavingElement_SL4Public element) throws PersistenceException {
                Fraction gradeMulCP = element.getCurrentGrade().fetchGradeValue().mul(element.getCp().getValue());
                return fraction.add(gradeMulCP);
            }
        });
        return allGradesMulCPs;
    }


    private boolean noElements() throws PersistenceException {
        return getThis().getElements().getLength() == 0;
    }


    private DecimalGrade4Public createTripleGradeFromFraction(Fraction drittelnote) throws PersistenceException {
        String drittelnoteAlsString = drittelnote.toStringWithPoint();
        return StringFACTORY.createObjectBySubTypeNameForTripleGrade(drittelnoteAlsString);
    }

    private DecimalGrade4Public createDecimalGradeFromFraction(Fraction zehntelnote) throws PersistenceException {
        String zehntelNoteAlsString = zehntelnote.toStringWithPoint();
        return StringFACTORY.createObjectBySubTypeNameForDecimalGrade(zehntelNoteAlsString, new DecimalGradeSwitchPARAMETER() {
            @Override
            public CONCDecimalGrade4Public handleCONCDecimalGrade() throws PersistenceException {
                throw new Error("");
            }
        });
    }

    private Bool4Public isRelevant(CreditPointHavingElement_SL4Public element, Bool4Public withBinary) throws PersistenceException {
        return element.accept(new CreditPointHavingElement_SLReturnVisitor<Bool4Public>() {
            @Override
            public Bool4Public handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
                if(withBinary.value() && binaryAtomicModule_SL.getCurrentGrade().equals(Passed.getThePassed())) {
                    return Verum.getTheVerum();
                }
                return Falsum.getTheFalsum();
            }


            @Override
            public Bool4Public handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
                return tripleAtomicModule_SL.getCalculatedStudentGrade().isNotNoGrade();
            }

            @Override
            public Bool4Public handleUnit_SL(Unit_SL4Public unit_SL) throws PersistenceException {
                return unit_SL.getCalculatedStudentGrade().isNotNoGrade();
            }

            @Override
            public Bool4Public handleCONCAtomicModule_SL(CONCAtomicModule_SL4Public cONCAtomicModule_SL) throws PersistenceException {
                return Falsum.getTheFalsum();
            }


            @Override
            public Bool4Public handleCONCCreditPointHavingElement_SL(CONCCreditPointHavingElement_SL4Public cONCCreditPointHavingElement_SL) throws PersistenceException {
                return Falsum.getTheFalsum();
            }
        });
    }

    /* End of protected part that is not overridden by persistence generator */
    
}
