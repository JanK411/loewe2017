
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class ModuleGroup_GL extends PersistentObject implements PersistentModuleGroup_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleGroup_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.getClass(objectId);
        return (ModuleGroup_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static ModuleGroup_GL4Public createModuleGroup_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf) throws PersistenceException{
        return createModuleGroup_GL(instanceOf,false);
    }
    
    public static ModuleGroup_GL4Public createModuleGroup_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentModuleGroup_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade
                .newDelayedModuleGroup_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade
                .newModuleGroup_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type ModuleGroup_GL has not been initialized!",0);
        return result;
    }
    
    public static ModuleGroup_GL4Public createModuleGroup_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,ModuleGroup_GL4Public This) throws PersistenceException {
        PersistentModuleGroup_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade
                .newDelayedModuleGroup_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade
                .newModuleGroup_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleGroupOrStudyProgram_GL = (AbstractPersistentRoot)this.getMyCONCModuleGroupOrStudyProgram_GL();
            if (myCONCModuleGroupOrStudyProgram_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleGroupOrStudyProgram_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleGroupOrStudyProgram_GL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCModuleOrModuleGroup_GL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_GL();
            if (myCONCModuleOrModuleGroup_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public ModuleGroup_GL provideCopy() throws PersistenceException{
        ModuleGroup_GL result = this;
        result = new ModuleGroup_GL(this.This, 
                                    this.myCONCModuleGroupOrStudyProgram_GL, 
                                    this.myCONCModuleOrModuleGroup_GL, 
                                    this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModuleGroup_GL This;
    protected PersistentModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL;
    protected PersistentModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL;
    
    public ModuleGroup_GL(PersistentModuleGroup_GL This,PersistentModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL,PersistentModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleGroupOrStudyProgram_GL = myCONCModuleGroupOrStudyProgram_GL;
        this.myCONCModuleOrModuleGroup_GL = myCONCModuleOrModuleGroup_GL;        
    }
    
    static public long getTypeId() {
        return 127;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 127) ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade
            .newModuleGroup_GL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleGroupOrStudyProgram_GL() != null){
            this.getMyCONCModuleGroupOrStudyProgram_GL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.myCONCModuleGroupOrStudyProgram_GLSet(this.getId(), getMyCONCModuleGroupOrStudyProgram_GL());
        }
        if(this.getMyCONCModuleOrModuleGroup_GL() != null){
            this.getMyCONCModuleOrModuleGroup_GL().store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.myCONCModuleOrModuleGroup_GLSet(this.getId(), getMyCONCModuleOrModuleGroup_GL());
        }
        
    }
    
    protected void setThis(PersistentModuleGroup_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleGroup_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroupOrStudyProgram_GL getMyCONCModuleGroupOrStudyProgram_GL() throws PersistenceException {
        return this.myCONCModuleGroupOrStudyProgram_GL;
    }
    public void setMyCONCModuleGroupOrStudyProgram_GL(PersistentModuleGroupOrStudyProgram_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleGroupOrStudyProgram_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleGroupOrStudyProgram_GL = (PersistentModuleGroupOrStudyProgram_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.myCONCModuleGroupOrStudyProgram_GLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_GL;
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_GL = (PersistentModuleOrModuleGroup_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleGroup_GLFacade.myCONCModuleOrModuleGroup_GLSet(this.getId(), newValue);
        }
    }
    public PersistentModuleGroup_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentModuleGroup_GL result = (PersistentModuleGroup_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentModuleGroup_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).setInstanceOf(newValue);
    }
    public ModuleGroupOrStudyProgram_GL_ContaineesProxi getContainees() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getContainees();
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentModuleGroupOrStudyProgram_GL)this.getMyCONCModuleGroupOrStudyProgram_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
        ((PersistentModuleOrModuleGroup_GL)this.getMyCONCModuleOrModuleGroup_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleGroupOrStudyProgram_GL().delete$Me();
        this.getMyCONCModuleOrModuleGroup_GL().delete$Me();
    }
    
    public void accept(ModuleGroupOrStudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_GL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleModuleGroup_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleModuleGroup_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleModuleGroup_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleModuleGroup_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleGroup_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_GL)This);
			PersistentCONCModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL = (PersistentCONCModuleGroupOrStudyProgram_GL) model.CONCModuleGroupOrStudyProgram_GL.createCONCModuleGroupOrStudyProgram_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModuleGroup_GL)This);
			this.setMyCONCModuleGroupOrStudyProgram_GL(myCONCModuleGroupOrStudyProgram_GL);
			this.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleGroupOrStudyProgram_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroup_GL().inverseGetContainees();
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addContainee(final ModuleOrModuleGroup_GL4Public containee) 
				throws PersistenceException{
        this.getMyCONCModuleGroupOrStudyProgram_GL().addContainee(containee);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        ModuleGroup_SL4Public moduleGroup_sl = ModuleGroup_SL.createModuleGroup_SL(getThis());
        getThis().getContainees().applyToAll(x -> x.createSLEquivalentAndAdd(moduleGroup_sl));
        container.addContainee(moduleGroup_sl);
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().inverseGetContainees().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
