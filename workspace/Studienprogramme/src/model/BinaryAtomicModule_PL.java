
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class BinaryAtomicModule_PL extends PersistentObject implements PersistentBinaryAtomicModule_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static BinaryAtomicModule_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade.getClass(objectId);
        return (BinaryAtomicModule_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static BinaryAtomicModule_PL4Public createBinaryAtomicModule_PL(String name,CreditPoints4Public initialCP) throws PersistenceException{
        return createBinaryAtomicModule_PL(name,initialCP,false);
    }
    
    public static BinaryAtomicModule_PL4Public createBinaryAtomicModule_PL(String name,CreditPoints4Public initialCP,boolean delayed$Persistence) throws PersistenceException {
        PersistentBinaryAtomicModule_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade
                .newDelayedBinaryAtomicModule_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade
                .newBinaryAtomicModule_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        final$$Fields.put("initialCP", initialCP);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static BinaryAtomicModule_PL4Public createBinaryAtomicModule_PL(String name,CreditPoints4Public initialCP,boolean delayed$Persistence,BinaryAtomicModule_PL4Public This) throws PersistenceException {
        PersistentBinaryAtomicModule_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade
                .newDelayedBinaryAtomicModule_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade
                .newBinaryAtomicModule_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        final$$Fields.put("initialCP", initialCP);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCAtomicModule_PL = (AbstractPersistentRoot)this.getMyCONCAtomicModule_PL();
            if (myCONCAtomicModule_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCAtomicModule_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCAtomicModule_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public BinaryAtomicModule_PL provideCopy() throws PersistenceException{
        BinaryAtomicModule_PL result = this;
        result = new BinaryAtomicModule_PL(this.This, 
                                           this.myCONCAtomicModule_PL, 
                                           this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentBinaryAtomicModule_PL This;
    protected PersistentAtomicModule_PL myCONCAtomicModule_PL;
    
    public BinaryAtomicModule_PL(PersistentBinaryAtomicModule_PL This,PersistentAtomicModule_PL myCONCAtomicModule_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCAtomicModule_PL = myCONCAtomicModule_PL;        
    }
    
    static public long getTypeId() {
        return 219;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 219) ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade
            .newBinaryAtomicModule_PL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCAtomicModule_PL() != null){
            this.getMyCONCAtomicModule_PL().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade.myCONCAtomicModule_PLSet(this.getId(), getMyCONCAtomicModule_PL());
        }
        
    }
    
    protected void setThis(PersistentBinaryAtomicModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentBinaryAtomicModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentAtomicModule_PL getMyCONCAtomicModule_PL() throws PersistenceException {
        return this.myCONCAtomicModule_PL;
    }
    public void setMyCONCAtomicModule_PL(PersistentAtomicModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCAtomicModule_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCAtomicModule_PL = (PersistentAtomicModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_PLFacade.myCONCAtomicModule_PLSet(this.getId(), newValue);
        }
    }
    public PersistentBinaryAtomicModule_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentBinaryAtomicModule_PL result = (PersistentBinaryAtomicModule_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentBinaryAtomicModule_PL)this.This;
    }
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setName(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public PersistentModuleOrModuleGroup_PL getMyCONCModuleOrModuleGroup_PL() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getMyCONCModuleOrModuleGroup_PL();
    }
    public void setMyCONCModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setMyCONCModuleOrModuleGroup_PL(newValue);
    }
    public CreditPoints4Public getInitialCP() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getInitialCP();
    }
    public void setInitialCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setInitialCP(newValue);
    }
    public PersistentModule_PL getMyCONCModule_PL() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getMyCONCModule_PL();
    }
    public void setMyCONCModule_PL(PersistentModule_PL newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setMyCONCModule_PL(newValue);
    }
    public PersistentCreditPointHavingElement_PL getMyCONCCreditPointHavingElement_PL() throws PersistenceException {
        return ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).getMyCONCCreditPointHavingElement_PL();
    }
    public void setMyCONCCreditPointHavingElement_PL(PersistentCreditPointHavingElement_PL newValue) throws PersistenceException {
        ((PersistentAtomicModule_PL)this.getMyCONCAtomicModule_PL()).setMyCONCCreditPointHavingElement_PL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCAtomicModule_PL().delete$Me();
    }
    
    public void accept(AtomicModule_PLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(AtomicModule_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(CreditPointHavingElement_PLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(CreditPointHavingElement_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(Module_PLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(Module_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(Module_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(Module_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(ModuleOrModuleGroup_PLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public void accept(h_PLHIERARCHYVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R> R accept(h_PLHIERARCHYReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public <E extends model.UserException>  void accept(h_PLHIERARCHYExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_PL(this);
    }
    public <R, E extends model.UserException> R accept(h_PLHIERARCHYReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getInitialCP() != null) return 1;
        return 0;
    }
    
    
    public boolean containsh_PL(final h_PLHIERARCHY part) 
				throws PersistenceException{
        return getThis().containsh_PL(part, new java.util.HashSet<h_PLHIERARCHY>());
    }
    public boolean containsh_PL(final h_PLHIERARCHY part, final java.util.HashSet<h_PLHIERARCHY> visited) 
				throws PersistenceException{
        if(getThis().equals(part)) return true;
		if(visited.contains(getThis())) return false;
		visited.add(getThis());
		return false;
    }
    public ModuleGroupOrStudyProgram_PLSearchList getContainers() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_PLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_PLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		return result;
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentBinaryAtomicModule_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModule_PL myCONCModule_PL = (PersistentCONCModule_PL) model.CONCModule_PL.createCONCModule_PL("", this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_PL)This);
			PersistentCONCAtomicModule_PL myCONCAtomicModule_PL = (PersistentCONCAtomicModule_PL) model.CONCAtomicModule_PL.createCONCAtomicModule_PL("", (PersistentCreditPoints)final$$Fields.get("initialCP"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_PL)This);
			PersistentCONCCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL = (PersistentCONCCreditPointHavingElement_PL) model.CONCCreditPointHavingElement_PL.createCONCCreditPointHavingElement_PL((PersistentCreditPoints)final$$Fields.get("initialCP"), "", this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_PL)This);
			this.setMyCONCAtomicModule_PL(myCONCAtomicModule_PL);
			myCONCAtomicModule_PL.setMyCONCModule_PL(myCONCModule_PL);
			myCONCModule_PL.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			myCONCAtomicModule_PL.setMyCONCCreditPointHavingElement_PL(myCONCCreditPointHavingElement_PL);
			myCONCCreditPointHavingElement_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
			this.setInitialCP((PersistentCreditPoints)final$$Fields.get("initialCP"));
		}
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy) 
				throws PersistenceException{
        return getThis().strategyh_PL(strategy, new java.util.HashMap<h_PLHIERARCHY,T>());
    }
    public <T> T strategyh_PL(final h_PLHIERARCHYStrategy<T> strategy, final java.util.HashMap<h_PLHIERARCHY,T> visited) 
				throws PersistenceException{
        if (visited.containsKey(getThis())) return visited.get(getThis());
		T result = strategy.BinaryAtomicModule_PL$$finalize(getThis() );
		visited.put(getThis(),result);
		return result;
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        return getThis().getMyCONCCreditPointHavingElement_PL().calculateCP();
    }
    public void changeInitialCP(final common.Fraction newCP) 
				throws model.StateException, PersistenceException{
        getThis().getMyCONCAtomicModule_PL().changeInitialCP(newCP);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createGlEquivalentAndAdd(final ModuleGroupOrStudyProgram_GL4Public container) 
				throws PersistenceException{
        BinaryAtomicModule_GL4Public binaryAtomicModule_gl = BinaryAtomicModule_GL.createBinaryAtomicModule_GL(getThis(), getThis().getInitialCP());
        container.addContainee(binaryAtomicModule_gl);
    }
    public CreditPoints4Public getCalculatedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().getCalculatedCP();
    }
    public String initialCPAsString() 
				throws PersistenceException{
        return getThis().getInitialCP().toString();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public void setInEditable() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().setInEditable();
    }
    public void updateCreditPointCace() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL().updateCreditPointCace();
        getThis().getMyCONCModuleOrModuleGroup_PL().updateCreditPointCace();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
