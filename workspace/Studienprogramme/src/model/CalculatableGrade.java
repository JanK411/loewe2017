
package model;

import common.Fraction;
import persistence.*;


/* Additional import section end */

public abstract class CalculatableGrade extends PersistentObject implements PersistentCalculatableGrade{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static CalculatableGrade4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.getClass(objectId);
        return (CalculatableGrade4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("value", this.getValue().toString());
            AbstractPersistentRoot myCONCGrade = (AbstractPersistentRoot)this.getMyCONCGrade();
            if (myCONCGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCGrade", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract CalculatableGrade provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected common.Fraction value;
    protected PersistentCalculatableGrade This;
    protected PersistentGrade myCONCGrade;
    
    public CalculatableGrade(common.Fraction value,PersistentCalculatableGrade This,PersistentGrade myCONCGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.value = value;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCGrade = myCONCGrade;        
    }
    
    static public long getTypeId() {
        return 241;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCGrade() != null){
            this.getMyCONCGrade().store();
            ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.myCONCGradeSet(this.getId(), getMyCONCGrade());
        }
        
    }
    
    public common.Fraction getValue() throws PersistenceException {
        return this.value;
    }
    public void setValue(common.Fraction newValue) throws PersistenceException {
        if(!this.isDelayed$Persistence()) ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.valueSet(this.getId(), newValue);
        this.value = newValue;
    }
    protected void setThis(PersistentCalculatableGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentCalculatableGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentGrade getMyCONCGrade() throws PersistenceException {
        return this.myCONCGrade;
    }
    public void setMyCONCGrade(PersistentGrade newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCGrade)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCGrade = (PersistentGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCalculatableGradeFacade.myCONCGradeSet(this.getId(), newValue);
        }
    }
    public abstract PersistentCalculatableGrade getThis() throws PersistenceException ;
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCGrade().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCalculatableGrade)This);
		if(this.isTheSameAs(This)){
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (PersistentCalculatableGrade)This);
			this.setMyCONCGrade(myCONCGrade);
			this.setValue((common.Fraction)final$$Fields.get("value"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public Bool4Public isNotNoGrade() 
				throws PersistenceException{
        return Verum.getTheVerum();
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
