
package model;

import common.Fraction;
import common.util.NotenFractions;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class tg17 extends model.TripleGrade implements Persistenttg17{
    
    private static tg174Public thetg17 = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static tg174Public getThetg17() throws PersistenceException{
        if (thetg17 == null || reset$For$Test){
            if (reset$For$Test) thetg17 = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            tg174Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().thetg17Facade.getThetg17();
                            thetg17 = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                tg174Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && thetg17== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return thetg17;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return thetg17;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public tg17 provideCopy() throws PersistenceException{
        tg17 result = this;
        result = new tg17(this.This, 
                          this.myCONCDecimalGrade, 
                          this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public tg17(PersistentTripleGrade This,PersistentDecimalGrade myCONCDecimalGrade,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentTripleGrade)This,(PersistentDecimalGrade)myCONCDecimalGrade,id);        
    }
    
    static public long getTypeId() {
        return 239;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public Persistenttg17 getThis() throws PersistenceException {
        if(this.This == null){
            Persistenttg17 result = (Persistenttg17)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (Persistenttg17)this.This;
    }
    
    public void accept(TripleGradeVisitor visitor) throws PersistenceException {
        visitor.handletg17(this);
    }
    public <R> R accept(TripleGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg17(this);
    }
    public <E extends model.UserException>  void accept(TripleGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg17(this);
    }
    public <R, E extends model.UserException> R accept(TripleGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg17(this);
    }
    public void accept(DecimalGradeVisitor visitor) throws PersistenceException {
        visitor.handletg17(this);
    }
    public <R> R accept(DecimalGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg17(this);
    }
    public <E extends model.UserException>  void accept(DecimalGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg17(this);
    }
    public <R, E extends model.UserException> R accept(DecimalGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg17(this);
    }
    public void accept(CalculatableGradeVisitor visitor) throws PersistenceException {
        visitor.handletg17(this);
    }
    public <R> R accept(CalculatableGradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg17(this);
    }
    public <E extends model.UserException>  void accept(CalculatableGradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg17(this);
    }
    public <R, E extends model.UserException> R accept(CalculatableGradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg17(this);
    }
    public void accept(GradeVisitor visitor) throws PersistenceException {
        visitor.handletg17(this);
    }
    public <R> R accept(GradeReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg17(this);
    }
    public <E extends model.UserException>  void accept(GradeExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg17(this);
    }
    public <R, E extends model.UserException> R accept(GradeReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg17(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handletg17(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handletg17(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handletg17(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handletg17(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((Persistenttg17)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCalculatableGrade myCONCCalculatableGrade = (PersistentCONCCalculatableGrade) model.CONCCalculatableGrade.createCONCCalculatableGrade(common.Fraction.Null, this.isDelayed$Persistence(), (Persistenttg17)This);
			PersistentCONCGrade myCONCGrade = (PersistentCONCGrade) model.CONCGrade.createCONCGrade(this.isDelayed$Persistence(), (Persistenttg17)This);
			PersistentCONCDecimalGrade myCONCDecimalGrade = (PersistentCONCDecimalGrade) model.CONCDecimalGrade.createCONCDecimalGrade(common.Fraction.Null, this.isDelayed$Persistence(), (Persistenttg17)This);
			this.setMyCONCDecimalGrade(myCONCDecimalGrade);
			myCONCDecimalGrade.setMyCONCCalculatableGrade(myCONCCalculatableGrade);
			myCONCCalculatableGrade.setMyCONCGrade(myCONCGrade);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
        
        
    }
    public common.Fraction fetchGradeValue() 
				throws PersistenceException{
                        return getThis().getMyCONCCalculatableGrade().getValue();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
        getThis().setValue(NotenFractions.f17);

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
		
    }
    public Bool4Public isNotNoGrade() 
				throws PersistenceException{return getThis().getMyCONCCalculatableGrade().isNotNoGrade();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
