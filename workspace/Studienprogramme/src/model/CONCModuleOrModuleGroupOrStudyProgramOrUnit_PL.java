
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL extends model.ModuleOrModuleGroupOrStudyProgramOrUnit_PL implements PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL{
    
    
    public static CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(String name,boolean delayed$Persistence,CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public This) throws PersistenceException {
        PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade
                .newDelayedCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(name);
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade
                .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(name,-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("name", name);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL provideCopy() throws PersistenceException{
        CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL result = this;
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this.editable, 
                                                                    this.name, 
                                                                    this.This, 
                                                                    this.getId());
        result.state = this.state;
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentBool editable,String name,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentBool)editable,(String)name,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)This,id);        
    }
    
    static public long getTypeId() {
        return -135;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -135) ConnectionHandler.getTheConnectionHandler().theCONCModuleOrModuleGroupOrStudyProgramOrUnit_PLFacade
            .newCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(name,this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL result = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL)this.This;
    }
    
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL)This);
		if(this.isTheSameAs(This)){
			this.setName((String)final$$Fields.get("name"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public CreditPoints4Public calculateCP() 
				throws PersistenceException{
        return getThis().calculateCP();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
