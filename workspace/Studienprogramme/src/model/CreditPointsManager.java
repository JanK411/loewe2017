
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CreditPointsManager extends PersistentObject implements PersistentCreditPointsManager{
    
    private static CreditPointsManager4Public theCreditPointsManager = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static CreditPointsManager4Public getTheCreditPointsManager() throws PersistenceException{
        if (theCreditPointsManager == null || reset$For$Test){
            if (reset$For$Test) theCreditPointsManager = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            CreditPointsManager4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theCreditPointsManagerFacade.getTheCreditPointsManager();
                            theCreditPointsManager = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                CreditPointsManager4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theCreditPointsManager== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theCreditPointsManager;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theCreditPointsManager;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CreditPointsManager provideCopy() throws PersistenceException{
        CreditPointsManager result = this;
        result = new CreditPointsManager(this.This, 
                                         this.getId());
        result.cps = this.cps.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected CreditPointsManager_CpsProxi cps;
    protected PersistentCreditPointsManager This;
    
    public CreditPointsManager(PersistentCreditPointsManager This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.cps = new CreditPointsManager_CpsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 147;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public CreditPointsManager_CpsProxi getCps() throws PersistenceException {
        return this.cps;
    }
    protected void setThis(PersistentCreditPointsManager newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentCreditPointsManager)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theCreditPointsManagerFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointsManager getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCreditPointsManager result = (PersistentCreditPointsManager)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCreditPointsManager)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCreditPointsManager(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCreditPointsManager(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCreditPointsManager(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCreditPointsManager(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCreditPointsManager)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public CreditPoints4Public getCP(final common.Fraction cpValue) 
				throws PersistenceException{
        CreditPoints4Public cp = this.getCps().get(cpValue);
        if(cp == null) {
            CreditPoints4Public createdCP = CreditPoints.createCreditPoints(cpValue);
            this.getCps().put(cpValue, createdCP);
            return createdCP;
        }
        return cp;
    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
