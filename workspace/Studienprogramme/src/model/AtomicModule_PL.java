
package model;

import persistence.*;


/* Additional import section end */

public abstract class AtomicModule_PL extends PersistentObject implements PersistentAtomicModule_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static AtomicModule_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.getClass(objectId);
        return (AtomicModule_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModule_PL = (AbstractPersistentRoot)this.getMyCONCModule_PL();
            if (myCONCModule_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_PL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCCreditPointHavingElement_PL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_PL();
            if (myCONCCreditPointHavingElement_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract AtomicModule_PL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentAtomicModule_PL This;
    protected PersistentModule_PL myCONCModule_PL;
    protected PersistentCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL;
    
    public AtomicModule_PL(PersistentAtomicModule_PL This,PersistentModule_PL myCONCModule_PL,PersistentCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_PL = myCONCModule_PL;
        this.myCONCCreditPointHavingElement_PL = myCONCCreditPointHavingElement_PL;        
    }
    
    static public long getTypeId() {
        return 167;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_PL() != null){
            this.getMyCONCModule_PL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.myCONCModule_PLSet(this.getId(), getMyCONCModule_PL());
        }
        if(this.getMyCONCCreditPointHavingElement_PL() != null){
            this.getMyCONCCreditPointHavingElement_PL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.myCONCCreditPointHavingElement_PLSet(this.getId(), getMyCONCCreditPointHavingElement_PL());
        }
        
    }
    
    protected void setThis(PersistentAtomicModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentAtomicModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_PL getMyCONCModule_PL() throws PersistenceException {
        return this.myCONCModule_PL;
    }
    public void setMyCONCModule_PL(PersistentModule_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_PL = (PersistentModule_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.myCONCModule_PLSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_PL getMyCONCCreditPointHavingElement_PL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_PL;
    }
    public void setMyCONCCreditPointHavingElement_PL(PersistentCreditPointHavingElement_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_PL = (PersistentCreditPointHavingElement_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_PLFacade.myCONCCreditPointHavingElement_PLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentAtomicModule_PL getThis() throws PersistenceException ;
    public Bool4Public getEditable() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getEditable();
    }
    public void setEditable(Bool4Public newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setEditable(newValue);
    }
    public State_PL4Public getState() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getState();
    }
    public void setState(State_PL4Public newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setState(newValue);
    }
    public String getName() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getName();
    }
    public void setName(String newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setName(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(newValue);
    }
    public PersistentModuleOrModuleGroup_PL getMyCONCModuleOrModuleGroup_PL() throws PersistenceException {
        return ((PersistentModule_PL)this.getMyCONCModule_PL()).getMyCONCModuleOrModuleGroup_PL();
    }
    public void setMyCONCModuleOrModuleGroup_PL(PersistentModuleOrModuleGroup_PL newValue) throws PersistenceException {
        ((PersistentModule_PL)this.getMyCONCModule_PL()).setMyCONCModuleOrModuleGroup_PL(newValue);
    }
    public CreditPoints4Public getInitialCP() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).getInitialCP();
    }
    public void setInitialCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_PL)this.getMyCONCCreditPointHavingElement_PL()).setInitialCP(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_PL().delete$Me();
        this.getMyCONCCreditPointHavingElement_PL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentAtomicModule_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModule_PL myCONCModule_PL = (PersistentCONCModule_PL) model.CONCModule_PL.createCONCModule_PL("", this.isDelayed$Persistence(), (PersistentAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroup_PL myCONCModuleOrModuleGroup_PL = (PersistentCONCModuleOrModuleGroup_PL) model.CONCModuleOrModuleGroup_PL.createCONCModuleOrModuleGroup_PL("", this.isDelayed$Persistence(), (PersistentAtomicModule_PL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL("", this.isDelayed$Persistence(), (PersistentAtomicModule_PL)This);
			PersistentCONCCreditPointHavingElement_PL myCONCCreditPointHavingElement_PL = (PersistentCONCCreditPointHavingElement_PL) model.CONCCreditPointHavingElement_PL.createCONCCreditPointHavingElement_PL((PersistentCreditPoints)final$$Fields.get("initialCP"), "", this.isDelayed$Persistence(), (PersistentAtomicModule_PL)This);
			this.setMyCONCModule_PL(myCONCModule_PL);
			this.setMyCONCCreditPointHavingElement_PL(myCONCCreditPointHavingElement_PL);
			myCONCModule_PL.setMyCONCModuleOrModuleGroup_PL(myCONCModuleOrModuleGroup_PL);
			myCONCModuleOrModuleGroup_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			myCONCCreditPointHavingElement_PL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL);
			this.setName((String)final$$Fields.get("name"));
			this.setInitialCP((PersistentCreditPoints)final$$Fields.get("initialCP"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public void changeInitialCP(final common.Fraction newCP) 
				throws model.StateException, PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_PL().changeInitialCP(newCP);
        getThis().getContainers().applyToAll(container -> container.updateCreditPointCace());
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
