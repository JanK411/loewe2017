
package model;

import common.Fraction;
import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class BinaryAtomicModule_SL extends PersistentObject implements PersistentBinaryAtomicModule_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static BinaryAtomicModule_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade.getClass(objectId);
        return (BinaryAtomicModule_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static BinaryAtomicModule_SL4Public createBinaryAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf) throws PersistenceException{
        return createBinaryAtomicModule_SL(instanceOf,false);
    }
    
    public static BinaryAtomicModule_SL4Public createBinaryAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentBinaryAtomicModule_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade
                .newDelayedBinaryAtomicModule_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade
                .newBinaryAtomicModule_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type BinaryAtomicModule_SL has not been initialized!",0);
        return result;
    }
    
    public static BinaryAtomicModule_SL4Public createBinaryAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,BinaryAtomicModule_SL4Public This) throws PersistenceException {
        PersistentBinaryAtomicModule_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade
                .newDelayedBinaryAtomicModule_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade
                .newBinaryAtomicModule_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCAtomicModule_SL = (AbstractPersistentRoot)this.getMyCONCAtomicModule_SL();
            if (myCONCAtomicModule_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCAtomicModule_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCAtomicModule_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public BinaryAtomicModule_SL provideCopy() throws PersistenceException{
        BinaryAtomicModule_SL result = this;
        result = new BinaryAtomicModule_SL(this.This, 
                                           this.myCONCAtomicModule_SL, 
                                           this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentBinaryAtomicModule_SL This;
    protected PersistentAtomicModule_SL myCONCAtomicModule_SL;
    
    public BinaryAtomicModule_SL(PersistentBinaryAtomicModule_SL This,PersistentAtomicModule_SL myCONCAtomicModule_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCAtomicModule_SL = myCONCAtomicModule_SL;        
    }
    
    static public long getTypeId() {
        return 169;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 169) ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade
            .newBinaryAtomicModule_SL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCAtomicModule_SL() != null){
            this.getMyCONCAtomicModule_SL().store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade.myCONCAtomicModule_SLSet(this.getId(), getMyCONCAtomicModule_SL());
        }
        
    }
    
    protected void setThis(PersistentBinaryAtomicModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentBinaryAtomicModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentAtomicModule_SL getMyCONCAtomicModule_SL() throws PersistenceException {
        return this.myCONCAtomicModule_SL;
    }
    public void setMyCONCAtomicModule_SL(PersistentAtomicModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCAtomicModule_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCAtomicModule_SL = (PersistentAtomicModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theBinaryAtomicModule_SLFacade.myCONCAtomicModule_SLSet(this.getId(), newValue);
        }
    }
    public PersistentBinaryAtomicModule_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentBinaryAtomicModule_SL result = (PersistentBinaryAtomicModule_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentBinaryAtomicModule_SL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setState(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public PersistentModuleOrModuleGroup_SL getMyCONCModuleOrModuleGroup_SL() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getMyCONCModuleOrModuleGroup_SL();
    }
    public void setMyCONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setMyCONCModuleOrModuleGroup_SL(newValue);
    }
    public GradeHistory4Public getGradeHistory() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getGradeHistory();
    }
    public void setGradeHistory(GradeHistory4Public newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setGradeHistory(newValue);
    }
    public PersistentModule_SL getMyCONCModule_SL() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getMyCONCModule_SL();
    }
    public void setMyCONCModule_SL(PersistentModule_SL newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setMyCONCModule_SL(newValue);
    }
    public PersistentCreditPointHavingElement_SL getMyCONCCreditPointHavingElement_SL() throws PersistenceException {
        return ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).getMyCONCCreditPointHavingElement_SL();
    }
    public void setMyCONCCreditPointHavingElement_SL(PersistentCreditPointHavingElement_SL newValue) throws PersistenceException {
        ((PersistentAtomicModule_SL)this.getMyCONCAtomicModule_SL()).setMyCONCCreditPointHavingElement_SL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCAtomicModule_SL().delete$Me();
    }
    
    public void accept(AtomicModule_SLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(AtomicModule_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public void accept(CreditPointHavingElement_SLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(CreditPointHavingElement_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public void accept(Module_SLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(Module_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(Module_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(Module_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroup_SLVisitor visitor) throws PersistenceException {
        visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleBinaryAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleBinaryAtomicModule_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        return 0;
    }
    
    
    public ModuleGroupOrStudyProgram_SL4Public getContainingModuleGroupOrStudyProgram() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_SLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentBinaryAtomicModule_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCAtomicModule_SL myCONCAtomicModule_SL = (PersistentCONCAtomicModule_SL) model.CONCAtomicModule_SL.createCONCAtomicModule_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_SL)This);
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_SL)This);
			PersistentCONCModule_SL myCONCModule_SL = (PersistentCONCModule_SL) model.CONCModule_SL.createCONCModule_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_SL)This);
			PersistentCONCCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL = (PersistentCONCCreditPointHavingElement_SL) model.CONCCreditPointHavingElement_SL.createCONCCreditPointHavingElement_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentBinaryAtomicModule_SL)This);
			this.setMyCONCAtomicModule_SL(myCONCAtomicModule_SL);
			myCONCAtomicModule_SL.setMyCONCCreditPointHavingElement_SL(myCONCCreditPointHavingElement_SL);
			myCONCCreditPointHavingElement_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			myCONCAtomicModule_SL.setMyCONCModule_SL(myCONCModule_SL);
			myCONCModule_SL.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public void assignGrade(final Grade4Public grade, final String comment) 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().assignGrade(grade, comment);
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().getCurrentGrade();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        this.getMyCONCAtomicModule_SL().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
        
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        this.getMyCONCAtomicModule_SL().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        if(getThis().getCurrentGrade().equals(Passed.getThePassed())) {
            return getThis().getCp();
        }
        return CreditPointsManager.getTheCreditPointsManager().getCP(Fraction.Null);
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCp();
    }
    public Grade4Public getCurrentGrade() 
				throws PersistenceException{
        return getThis().getMyCONCCreditPointHavingElement_SL().getCurrentGrade();

    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public GradeHistory4Public showHistory() 
				throws PersistenceException{
        return this.getMyCONCCreditPointHavingElement_SL().showHistory();
    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().updateGradeCache();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
