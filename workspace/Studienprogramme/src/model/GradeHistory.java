
package model;

import persistence.*;
import model.visitor.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;


/* Additional import section end */

public class GradeHistory extends PersistentObject implements PersistentGradeHistory{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static GradeHistory4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade.getClass(objectId);
        return (GradeHistory4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static GradeHistory4Public createGradeHistory() throws PersistenceException{
        return createGradeHistory(false);
    }
    
    public static GradeHistory4Public createGradeHistory(boolean delayed$Persistence) throws PersistenceException {
        PersistentGradeHistory result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade
                .newDelayedGradeHistory();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade
                .newGradeHistory(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static GradeHistory4Public createGradeHistory(boolean delayed$Persistence,GradeHistory4Public This) throws PersistenceException {
        PersistentGradeHistory result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade
                .newDelayedGradeHistory();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade
                .newGradeHistory(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot lastGrade = (AbstractPersistentRoot)this.getLastGrade();
            if (lastGrade != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    lastGrade, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, false);
                result.put("lastGrade", proxiInformation);
                
            }
            result.put("allElements", this.getAllElements().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
        }
        return result;
    }
    
    public GradeHistory provideCopy() throws PersistenceException{
        GradeHistory result = this;
        result = new GradeHistory(this.lastGrade, 
                                  this.This, 
                                  this.getId());
        result.allElements = this.allElements.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentGrade lastGrade;
    protected GradeHistory_AllElementsProxi allElements;
    protected PersistentGradeHistory This;
    
    public GradeHistory(PersistentGrade lastGrade,PersistentGradeHistory This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.lastGrade = lastGrade;
        this.allElements = new GradeHistory_AllElementsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 130;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 130) ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade
            .newGradeHistory(this.getId());
        super.store();
        if(this.getLastGrade() != null){
            this.getLastGrade().store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade.lastGradeSet(this.getId(), getLastGrade());
        }
        this.getAllElements().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public Grade4Public getLastGrade() throws PersistenceException {
        return this.lastGrade;
    }
    public void setLastGrade(Grade4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.lastGrade)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.lastGrade = (PersistentGrade)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade.lastGradeSet(this.getId(), newValue);
        }
    }
    public GradeHistory_AllElementsProxi getAllElements() throws PersistenceException {
        return this.allElements;
    }
    protected void setThis(PersistentGradeHistory newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentGradeHistory)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theGradeHistoryFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentGradeHistory getThis() throws PersistenceException {
        if(this.This == null){
            PersistentGradeHistory result = (PersistentGradeHistory)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentGradeHistory)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleGradeHistory(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleGradeHistory(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleGradeHistory(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleGradeHistory(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAllElements().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentGradeHistory)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addElement(final GradeHistoryElement4Public element) 
				throws PersistenceException{
        getThis().getAllElements().add(element);
        getThis().setLastGrade(element.getGrade());

    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().setLastGrade(NoGrade.getTheNoGrade());
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
