
package model;

import persistence.*;
import model.visitor.*;

import java.util.HashMap;


/* Additional import section end */

public class StudentManager extends PersistentObject implements PersistentStudentManager{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static StudentManager4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade.getClass(objectId);
        return (StudentManager4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static StudentManager4Public createStudentManager() throws PersistenceException{
        return createStudentManager(false);
    }
    
    public static StudentManager4Public createStudentManager(boolean delayed$Persistence) throws PersistenceException {
        PersistentStudentManager result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade
                .newDelayedStudentManager();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade
                .newStudentManager(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static StudentManager4Public createStudentManager(boolean delayed$Persistence,StudentManager4Public This) throws PersistenceException {
        PersistentStudentManager result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade
                .newDelayedStudentManager();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade
                .newStudentManager(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("studentGroups", this.getStudentGroups().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
        }
        return result;
    }
    
    public StudentManager provideCopy() throws PersistenceException{
        StudentManager result = this;
        result = new StudentManager(this.This, 
                                    this.getId());
        result.studentGroups = this.studentGroups.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected StudentManager_StudentGroupsProxi studentGroups;
    protected PersistentStudentManager This;
    
    public StudentManager(PersistentStudentManager This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.studentGroups = new StudentManager_StudentGroupsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 137;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 137) ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade
            .newStudentManager(this.getId());
        super.store();
        this.getStudentGroups().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public StudentManager_StudentGroupsProxi getStudentGroups() throws PersistenceException {
        return this.studentGroups;
    }
    protected void setThis(PersistentStudentManager newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentStudentManager)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theStudentManagerFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentStudentManager getThis() throws PersistenceException {
        if(this.This == null){
            PersistentStudentManager result = (PersistentStudentManager)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentStudentManager)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleStudentManager(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleStudentManager(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleStudentManager(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleStudentManager(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getStudentGroups().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void addStudentsToGroup(final StudentGroup4Public group, final StudentWrapperSearchList students, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		AddStudentsToGroupCommand4Public command = model.meta.AddStudentsToGroupCommand.createAddStudentsToGroupCommand(nw, d1170);
		command.setGroup(group);
		java.util.Iterator<StudentWrapper4Public> studentsIterator = students.iterator();
		while(studentsIterator.hasNext()){
			command.getStudents().add(studentsIterator.next());
		}
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void assignGrade(final CreditPointHavingElement_SL4Public entity, final Grade4Public grade, final String comment, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		AssignGradeCommand4Public command = model.meta.AssignGradeCommand.createAssignGradeCommand(comment, nw, d1170);
		command.setEntity(entity);
		command.setGrade(grade);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createStudentGroup(final StudyProgram_PL4Public program, final String name, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateStudentGroupCommand4Public command = model.meta.CreateStudentGroupCommand.createCreateStudentGroupCommand(name, nw, d1170);
		command.setProgram(program);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createStudent(final StudentGroup4Public studentGroup, final String firstName, final String name, final java.sql.Date date, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateStudentCommand4Public command = model.meta.CreateStudentCommand.createCreateStudentCommand(firstName, name, date, nw, d1170);
		command.setStudentGroup(studentGroup);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void findStudent(final String criteria, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		FindStudentCommand4Public command = model.meta.FindStudentCommand.createFindStudentCommand(criteria, nw, d1170);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void finishesStudying(final StudentGroup4Public group, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		FinishesStudyingCommand4Public command = model.meta.FinishesStudyingCommand.createFinishesStudyingCommand(nw, d1170);
		command.setGroup(group);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentStudentManager)This);
		if(this.isTheSameAs(This)){
		}
    }
    public void makeGroupActive(final StudentGroup4Public group, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		MakeGroupActiveCommand4Public command = model.meta.MakeGroupActiveCommand.createMakeGroupActiveCommand(nw, d1170);
		command.setGroup(group);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void removeStudent(final StudentWrapper4Public studentWrapper, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		RemoveStudentCommand4Public command = model.meta.RemoveStudentCommand.createRemoveStudentCommand(nw, d1170);
		command.setStudentWrapper(studentWrapper);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addStudentsToGroup(final StudentGroup4Public group, final StudentWrapperSearchList students) 
				throws model.StateException, PersistenceException{
        students.applyToAllException(group::addStudent);
    }
    public void assignGrade(final CreditPointHavingElement_SL4Public entity, final Grade4Public grade, final String comment) 
				throws PersistenceException{
        entity.assignGrade(grade, comment);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public StudentGroup4Public createStudentGroup(final StudyProgram_PL4Public program, final String name) 
				throws model.StateException, PersistenceException{
        final StudyProgram_GL4Public studyProgram_gl = StudyProgram_GL.createStudyProgram_GL(program);
        program.populateProgramGL(studyProgram_gl);
        final StudentGroup4Public studentGroup = StudentGroup.createStudentGroup(name, studyProgram_gl);
        getThis().getStudentGroups().add(studentGroup);
        return studentGroup;
    }
    public Student4Public createStudent(final StudentGroup4Public studentGroup, final String firstName, final String name, final java.sql.Date date) 
				throws model.StateException, PersistenceException{
        final Student4Public student = Student.createStudent(firstName, name, date);
        studentGroup.addStudent(student.wrap());
        return student;
    }
    public StudentSearchList findStudent(final String criteria) 
				throws PersistenceException{
        final StudentSearchList ret = Student.getStudentByFirstName(criteria);
        try {
            ret.addUnique(Student.getStudentByName(criteria));
        } catch (UserException e) {
            e.printStackTrace();
        }
        return ret;
    }
    public void finishesStudying(final StudentGroup4Public group) 
				throws model.StateException, PersistenceException{
        group.finishesStudying();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    public void makeGroupActive(final StudentGroup4Public group) 
				throws model.StateException, PersistenceException{
        group.makeActive();
    }
    public void removeStudent(final StudentWrapper4Public studentWrapper) 
				throws model.StateException, PersistenceException{
        studentWrapper.getStudentGroup().removeStudent(studentWrapper);
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */

    /* End of protected part that is not overridden by persistence generator */
    
}
