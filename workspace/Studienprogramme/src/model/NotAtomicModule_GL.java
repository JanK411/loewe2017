
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NotAtomicModule_GL extends PersistentObject implements PersistentNotAtomicModule_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static NotAtomicModule_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade.getClass(objectId);
        return (NotAtomicModule_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static NotAtomicModule_GL4Public createNotAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf) throws PersistenceException{
        return createNotAtomicModule_GL(instanceOf,false);
    }
    
    public static NotAtomicModule_GL4Public createNotAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentNotAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
                .newDelayedNotAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
                .newNotAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type NotAtomicModule_GL has not been initialized!",0);
        return result;
    }
    
    public static NotAtomicModule_GL4Public createNotAtomicModule_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,NotAtomicModule_GL4Public This) throws PersistenceException {
        PersistentNotAtomicModule_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
                .newDelayedNotAtomicModule_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
                .newNotAtomicModule_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("units", this.getUnits().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
            AbstractPersistentRoot myCONCModule_GL = (AbstractPersistentRoot)this.getMyCONCModule_GL();
            if (myCONCModule_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public NotAtomicModule_GL provideCopy() throws PersistenceException{
        NotAtomicModule_GL result = this;
        result = new NotAtomicModule_GL(this.This, 
                                        this.myCONCModule_GL, 
                                        this.getId());
        result.units = this.units.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected NotAtomicModule_GL_UnitsProxi units;
    protected PersistentNotAtomicModule_GL This;
    protected PersistentModule_GL myCONCModule_GL;
    
    public NotAtomicModule_GL(PersistentNotAtomicModule_GL This,PersistentModule_GL myCONCModule_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.units = new NotAtomicModule_GL_UnitsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_GL = myCONCModule_GL;        
    }
    
    static public long getTypeId() {
        return 178;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 178) ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade
            .newNotAtomicModule_GL(this.getId());
        super.store();
        this.getUnits().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_GL() != null){
            this.getMyCONCModule_GL().store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade.myCONCModule_GLSet(this.getId(), getMyCONCModule_GL());
        }
        
    }
    
    public NotAtomicModule_GL_UnitsProxi getUnits() throws PersistenceException {
        return this.units;
    }
    protected void setThis(PersistentNotAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentNotAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_GL getMyCONCModule_GL() throws PersistenceException {
        return this.myCONCModule_GL;
    }
    public void setMyCONCModule_GL(PersistentModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_GL = (PersistentModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_GLFacade.myCONCModule_GLSet(this.getId(), newValue);
        }
    }
    public PersistentNotAtomicModule_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNotAtomicModule_GL result = (PersistentNotAtomicModule_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNotAtomicModule_GL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setInstanceOf(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getMyCONCModuleOrModuleGroup_GL();
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setMyCONCModuleOrModuleGroup_GL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_GL().delete$Me();
    }
    
    public void accept(Module_GLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_GL(this);
    }
    public <R> R accept(Module_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(Module_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(Module_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroup_GLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNotAtomicModule_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotAtomicModule_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotAtomicModule_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getUnits().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNotAtomicModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_GL)This);
			PersistentCONCModule_GL myCONCModule_GL = (PersistentCONCModule_GL) model.CONCModule_GL.createCONCModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNotAtomicModule_GL)This);
			this.setMyCONCModule_GL(myCONCModule_GL);
			myCONCModule_GL.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    public ModuleGroupOrStudyProgram_GL4Public inverseGetContainees() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_GLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_GLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addUnit(final Unit_GL4Public unit) 
				throws PersistenceException{
        getThis().getUnits().add(unit);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void createSLEquivalentAndAdd(final ModuleGroupOrStudyProgram_SL4Public container) 
				throws PersistenceException{
        NotAtomicModule_SL4Public notAtomicModule_sl = NotAtomicModule_SL.createNotAtomicModule_SL(getThis());
        getThis().getUnits().applyToAll(x -> notAtomicModule_sl.addUnit(x.createSLEquivalent()));
        container.addContainee(notAtomicModule_sl);
    }
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().inverseGetContainees().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
