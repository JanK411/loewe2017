
package model;

import persistence.*;


/* Additional import section end */

public abstract class AtomicModule_GL extends PersistentObject implements PersistentAtomicModule_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static AtomicModule_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.getClass(objectId);
        return (AtomicModule_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModule_GL = (AbstractPersistentRoot)this.getMyCONCModule_GL();
            if (myCONCModule_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModule_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModule_GL", proxiInformation);
                
            }
            AbstractPersistentRoot myCONCCreditPointHavingElement_GL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_GL();
            if (myCONCCreditPointHavingElement_GL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_GL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_GL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract AtomicModule_GL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentAtomicModule_GL This;
    protected PersistentModule_GL myCONCModule_GL;
    protected PersistentCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL;
    
    public AtomicModule_GL(PersistentAtomicModule_GL This,PersistentModule_GL myCONCModule_GL,PersistentCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModule_GL = myCONCModule_GL;
        this.myCONCCreditPointHavingElement_GL = myCONCCreditPointHavingElement_GL;        
    }
    
    static public long getTypeId() {
        return 152;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModule_GL() != null){
            this.getMyCONCModule_GL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.myCONCModule_GLSet(this.getId(), getMyCONCModule_GL());
        }
        if(this.getMyCONCCreditPointHavingElement_GL() != null){
            this.getMyCONCCreditPointHavingElement_GL().store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.myCONCCreditPointHavingElement_GLSet(this.getId(), getMyCONCCreditPointHavingElement_GL());
        }
        
    }
    
    protected void setThis(PersistentAtomicModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentAtomicModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModule_GL getMyCONCModule_GL() throws PersistenceException {
        return this.myCONCModule_GL;
    }
    public void setMyCONCModule_GL(PersistentModule_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModule_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModule_GL = (PersistentModule_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.myCONCModule_GLSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_GL getMyCONCCreditPointHavingElement_GL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_GL;
    }
    public void setMyCONCCreditPointHavingElement_GL(PersistentCreditPointHavingElement_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_GL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_GL = (PersistentCreditPointHavingElement_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theAtomicModule_GLFacade.myCONCCreditPointHavingElement_GLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentAtomicModule_GL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setInstanceOf(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
        ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(newValue);
    }
    public PersistentModuleOrModuleGroup_GL getMyCONCModuleOrModuleGroup_GL() throws PersistenceException {
        return ((PersistentModule_GL)this.getMyCONCModule_GL()).getMyCONCModuleOrModuleGroup_GL();
    }
    public void setMyCONCModuleOrModuleGroup_GL(PersistentModuleOrModuleGroup_GL newValue) throws PersistenceException {
        ((PersistentModule_GL)this.getMyCONCModule_GL()).setMyCONCModuleOrModuleGroup_GL(newValue);
    }
    public CreditPoints4Public getStudentGroupCP() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).getStudentGroupCP();
    }
    public void setStudentGroupCP(CreditPoints4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_GL)this.getMyCONCCreditPointHavingElement_GL()).setStudentGroupCP(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModule_GL().delete$Me();
        this.getMyCONCCreditPointHavingElement_GL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentAtomicModule_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_GL myCONCCreditPointHavingElement_GL = (PersistentCONCCreditPointHavingElement_GL) model.CONCCreditPointHavingElement_GL.createCONCCreditPointHavingElement_GL((PersistentCreditPoints)final$$Fields.get("studentGroupCP"), (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_GL)This);
			PersistentCONCModuleOrModuleGroup_GL myCONCModuleOrModuleGroup_GL = (PersistentCONCModuleOrModuleGroup_GL) model.CONCModuleOrModuleGroup_GL.createCONCModuleOrModuleGroup_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_GL)This);
			PersistentCONCModule_GL myCONCModule_GL = (PersistentCONCModule_GL) model.CONCModule_GL.createCONCModule_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentAtomicModule_GL)This);
			this.setMyCONCCreditPointHavingElement_GL(myCONCCreditPointHavingElement_GL);
			this.setMyCONCModule_GL(myCONCModule_GL);
			myCONCCreditPointHavingElement_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			myCONCModule_GL.setMyCONCModuleOrModuleGroup_GL(myCONCModuleOrModuleGroup_GL);
			myCONCModuleOrModuleGroup_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
			this.setStudentGroupCP((PersistentCreditPoints)final$$Fields.get("studentGroupCP"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getStudentGroupCP();
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
