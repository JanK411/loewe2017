
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CreditPointsCachedState_PL extends PersistentObject implements PersistentCreditPointsCachedState_PL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static CreditPointsCachedState_PL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade.getClass(objectId);
        return (CreditPointsCachedState_PL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static CreditPointsCachedState_PL4Public createCreditPointsCachedState_PL(CreditPoints4Public cachedCreditPoints,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement) throws PersistenceException{
        return createCreditPointsCachedState_PL(cachedCreditPoints,cachingElement,false);
    }
    
    public static CreditPointsCachedState_PL4Public createCreditPointsCachedState_PL(CreditPoints4Public cachedCreditPoints,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement,boolean delayed$Persistence) throws PersistenceException {
        PersistentCreditPointsCachedState_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade
                .newDelayedCreditPointsCachedState_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade
                .newCreditPointsCachedState_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachedCreditPoints", cachedCreditPoints);
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static CreditPointsCachedState_PL4Public createCreditPointsCachedState_PL(CreditPoints4Public cachedCreditPoints,ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public cachingElement,boolean delayed$Persistence,CreditPointsCachedState_PL4Public This) throws PersistenceException {
        PersistentCreditPointsCachedState_PL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade
                .newDelayedCreditPointsCachedState_PL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade
                .newCreditPointsCachedState_PL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("cachedCreditPoints", cachedCreditPoints);
        final$$Fields.put("cachingElement", cachingElement);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCState_PL = (AbstractPersistentRoot)this.getMyCONCState_PL();
            if (myCONCState_PL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCState_PL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, true, true);
                result.put("myCONCState_PL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public CreditPointsCachedState_PL provideCopy() throws PersistenceException{
        CreditPointsCachedState_PL result = this;
        result = new CreditPointsCachedState_PL(this.getId());
        result.cachedCreditPoints = this.cachedCreditPoints;
        result.This = this.This;
        result.myCONCState_PL = this.myCONCState_PL;
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentCreditPoints cachedCreditPoints;
    protected PersistentCreditPointsCachedState_PL This;
    protected PersistentState_PL myCONCState_PL;
    
    public CreditPointsCachedState_PL(long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);        
    }
    
    static public long getTypeId() {
        return -299;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == -299) ConnectionHandler.getTheConnectionHandler().theCreditPointsCachedState_PLFacade
            .newCreditPointsCachedState_PL(this.getId());
        super.store();
        
    }
    
    public CreditPoints4Public getCachedCreditPoints() throws PersistenceException {
        return this.cachedCreditPoints;
    }
    public void setCachedCreditPoints(CreditPoints4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.cachedCreditPoints)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.cachedCreditPoints = (PersistentCreditPoints)PersistentProxi.createProxi(objectId, classId);
    }
    protected void setThis(PersistentCreditPointsCachedState_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentCreditPointsCachedState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentState_PL getMyCONCState_PL() throws PersistenceException {
        return this.myCONCState_PL;
    }
    public void setMyCONCState_PL(PersistentState_PL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCState_PL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCState_PL = (PersistentState_PL)PersistentProxi.createProxi(objectId, classId);
    }
    public PersistentCreditPointsCachedState_PL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCreditPointsCachedState_PL result = (PersistentCreditPointsCachedState_PL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCreditPointsCachedState_PL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getCachingElement() throws PersistenceException {
        return ((PersistentState_PL)this.getMyCONCState_PL()).getCachingElement();
    }
    public void setCachingElement(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        ((PersistentState_PL)this.getMyCONCState_PL()).setCachingElement(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCState_PL().delete$Me();
    }
    
    public void accept(State_PLVisitor visitor) throws PersistenceException {
        visitor.handleCreditPointsCachedState_PL(this);
    }
    public <R> R accept(State_PLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCreditPointsCachedState_PL(this);
    }
    public <E extends model.UserException>  void accept(State_PLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCreditPointsCachedState_PL(this);
    }
    public <R, E extends model.UserException> R accept(State_PLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCreditPointsCachedState_PL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCreditPointsCachedState_PL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCreditPointsCachedState_PL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCreditPointsCachedState_PL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCreditPointsCachedState_PL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getCreditPoints() != null) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCreditPointsCachedState_PL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCState_PL myCONCState_PL = (PersistentCONCState_PL) model.CONCState_PL.createCONCState_PL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"), this.isDelayed$Persistence(), (PersistentCreditPointsCachedState_PL)This);
			this.setMyCONCState_PL(myCONCState_PL);
			this.setCachedCreditPoints((PersistentCreditPoints)final$$Fields.get("cachedCreditPoints"));
			this.setCachingElement((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("cachingElement"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public CreditPoints4Public getCreditPoints() 
				throws PersistenceException{
        return getThis().getCachedCreditPoints();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
