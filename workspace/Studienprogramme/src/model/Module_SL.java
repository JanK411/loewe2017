
package model;

import persistence.*;


/* Additional import section end */

public abstract class Module_SL extends PersistentObject implements PersistentModule_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Module_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModule_SLFacade.getClass(objectId);
        return (Module_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCModuleOrModuleGroup_SL = (AbstractPersistentRoot)this.getMyCONCModuleOrModuleGroup_SL();
            if (myCONCModuleOrModuleGroup_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCModuleOrModuleGroup_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCModuleOrModuleGroup_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract Module_SL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentModule_SL This;
    protected PersistentModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL;
    
    public Module_SL(PersistentModule_SL This,PersistentModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCModuleOrModuleGroup_SL = myCONCModuleOrModuleGroup_SL;        
    }
    
    static public long getTypeId() {
        return 102;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModule_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCModuleOrModuleGroup_SL() != null){
            this.getMyCONCModuleOrModuleGroup_SL().store();
            ConnectionHandler.getTheConnectionHandler().theModule_SLFacade.myCONCModuleOrModuleGroup_SLSet(this.getId(), getMyCONCModuleOrModuleGroup_SL());
        }
        
    }
    
    protected void setThis(PersistentModule_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModule_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentModuleOrModuleGroup_SL getMyCONCModuleOrModuleGroup_SL() throws PersistenceException {
        return this.myCONCModuleOrModuleGroup_SL;
    }
    public void setMyCONCModuleOrModuleGroup_SL(PersistentModuleOrModuleGroup_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCModuleOrModuleGroup_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCModuleOrModuleGroup_SL = (PersistentModuleOrModuleGroup_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModule_SLFacade.myCONCModuleOrModuleGroup_SLSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModule_SL getThis() throws PersistenceException ;
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).setState(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentModuleOrModuleGroup_SL)this.getMyCONCModuleOrModuleGroup_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCModuleOrModuleGroup_SL().delete$Me();
    }
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModule_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModule_SL)This);
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentModule_SL)This);
			this.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
