
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCModuleGroupOrStudyProgram_GL extends model.ModuleGroupOrStudyProgram_GL implements PersistentCONCModuleGroupOrStudyProgram_GL{
    
    
    public static CONCModuleGroupOrStudyProgram_GL4Public createCONCModuleGroupOrStudyProgram_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public instanceOf,boolean delayed$Persistence,CONCModuleGroupOrStudyProgram_GL4Public This) throws PersistenceException {
        PersistentCONCModuleGroupOrStudyProgram_GL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_GLFacade
                .newDelayedCONCModuleGroupOrStudyProgram_GL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_GLFacade
                .newCONCModuleGroupOrStudyProgram_GL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCModuleGroupOrStudyProgram_GL provideCopy() throws PersistenceException{
        CONCModuleGroupOrStudyProgram_GL result = this;
        result = new CONCModuleGroupOrStudyProgram_GL(this.This, 
                                                      this.myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL, 
                                                      this.getId());
        result.containees = this.containees.copy(result);
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCModuleGroupOrStudyProgram_GL(PersistentModuleGroupOrStudyProgram_GL This,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentModuleGroupOrStudyProgram_GL)This,(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL,id);        
    }
    
    static public long getTypeId() {
        return 150;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 150) ConnectionHandler.getTheConnectionHandler().theCONCModuleGroupOrStudyProgram_GLFacade
            .newCONCModuleGroupOrStudyProgram_GL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCModuleGroupOrStudyProgram_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCModuleGroupOrStudyProgram_GL result = (PersistentCONCModuleGroupOrStudyProgram_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCModuleGroupOrStudyProgram_GL)this.This;
    }
    
    public void accept(ModuleGroupOrStudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCModuleGroupOrStudyProgram_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCModuleGroupOrStudyProgram_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCModuleGroupOrStudyProgram_GL)This);
			this.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public ActiveState4Public getActiveState() 
				throws PersistenceException{
        return getThis().getActiveState();
    }
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getCalculatedStudentGroupCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
