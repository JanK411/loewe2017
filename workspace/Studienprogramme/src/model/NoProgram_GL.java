
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NoProgram_GL extends model.StudyProgram_GL implements PersistentNoProgram_GL{
    
    private static NoProgram_GL4Public theNoProgram_GL = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static NoProgram_GL4Public getTheNoProgram_GL() throws PersistenceException{
        if (theNoProgram_GL == null || reset$For$Test){
            if (reset$For$Test) theNoProgram_GL = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            NoProgram_GL4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theNoProgram_GLFacade.getTheNoProgram_GL();
                            theNoProgram_GL = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                NoProgram_GL4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theNoProgram_GL== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theNoProgram_GL;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theNoProgram_GL;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public NoProgram_GL provideCopy() throws PersistenceException{
        NoProgram_GL result = this;
        result = new NoProgram_GL(this.This, 
                                  this.myCONCModuleGroupOrStudyProgram_GL, 
                                  this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public NoProgram_GL(PersistentStudyProgram_GL This,PersistentModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentStudyProgram_GL)This,(PersistentModuleGroupOrStudyProgram_GL)myCONCModuleGroupOrStudyProgram_GL,id);        
    }
    
    static public long getTypeId() {
        return 270;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentNoProgram_GL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNoProgram_GL result = (PersistentNoProgram_GL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNoProgram_GL)this.This;
    }
    
    public void accept(StudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_GL(this);
    }
    public <R> R accept(StudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(StudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(StudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_GL(this);
    }
    public void accept(ModuleGroupOrStudyProgram_GLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_GL(this);
    }
    public <R> R accept(ModuleGroupOrStudyProgram_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleGroupOrStudyProgram_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleGroupOrStudyProgram_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_GL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_GL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_GLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_GL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNoProgram_GL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNoProgram_GL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNoProgram_GL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNoProgram_GL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getContainees().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNoProgram_GL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNoProgram_GL)This);
			PersistentCONCModuleGroupOrStudyProgram_GL myCONCModuleGroupOrStudyProgram_GL = (PersistentCONCModuleGroupOrStudyProgram_GL) model.CONCModuleGroupOrStudyProgram_GL.createCONCModuleGroupOrStudyProgram_GL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentNoProgram_GL)This);
			this.setMyCONCModuleGroupOrStudyProgram_GL(myCONCModuleGroupOrStudyProgram_GL);
			myCONCModuleGroupOrStudyProgram_GL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL);
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
