
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class CONCAtomicModule_SL extends model.AtomicModule_SL implements PersistentCONCAtomicModule_SL{
    
    
    public static CONCAtomicModule_SL4Public createCONCAtomicModule_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,CONCAtomicModule_SL4Public This) throws PersistenceException {
        PersistentCONCAtomicModule_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_SLFacade
                .newDelayedCONCAtomicModule_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_SLFacade
                .newCONCAtomicModule_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public CONCAtomicModule_SL provideCopy() throws PersistenceException{
        CONCAtomicModule_SL result = this;
        result = new CONCAtomicModule_SL(this.This, 
                                         this.myCONCModule_SL, 
                                         this.myCONCCreditPointHavingElement_SL, 
                                         this.getId());
        
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    
    public CONCAtomicModule_SL(PersistentAtomicModule_SL This,PersistentModule_SL myCONCModule_SL,PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentAtomicModule_SL)This,(PersistentModule_SL)myCONCModule_SL,(PersistentCreditPointHavingElement_SL)myCONCCreditPointHavingElement_SL,id);        
    }
    
    static public long getTypeId() {
        return 189;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 189) ConnectionHandler.getTheConnectionHandler().theCONCAtomicModule_SLFacade
            .newCONCAtomicModule_SL(this.getId());
        super.store();
        
    }
    
    public PersistentCONCAtomicModule_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentCONCAtomicModule_SL result = (PersistentCONCAtomicModule_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentCONCAtomicModule_SL)this.This;
    }
    
    public void accept(AtomicModule_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(AtomicModule_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(AtomicModule_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(AtomicModule_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public void accept(CreditPointHavingElement_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(CreditPointHavingElement_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public void accept(Module_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(Module_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(Module_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(Module_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public void accept(ModuleOrModuleGroup_SLVisitor visitor) throws PersistenceException {
        visitor.handleCONCAtomicModule_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroup_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroup_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleCONCAtomicModule_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroup_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleCONCAtomicModule_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        return 0;
    }
    
    
    public ModuleGroupOrStudyProgram_SL4Public getContainingModuleGroupOrStudyProgram() 
				throws PersistenceException{
        ModuleGroupOrStudyProgram_SLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theModuleGroupOrStudyProgram_SLFacade
										.inverseGetContainees(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentCONCAtomicModule_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCModuleOrModuleGroup_SL myCONCModuleOrModuleGroup_SL = (PersistentCONCModuleOrModuleGroup_SL) model.CONCModuleOrModuleGroup_SL.createCONCModuleOrModuleGroup_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_SL)This);
			PersistentCONCModule_SL myCONCModule_SL = (PersistentCONCModule_SL) model.CONCModule_SL.createCONCModule_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_SL)This);
			PersistentCONCCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL = (PersistentCONCCreditPointHavingElement_SL) model.CONCCreditPointHavingElement_SL.createCONCCreditPointHavingElement_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentCONCAtomicModule_SL)This);
			this.setMyCONCCreditPointHavingElement_SL(myCONCCreditPointHavingElement_SL);
			this.setMyCONCModule_SL(myCONCModule_SL);
			myCONCCreditPointHavingElement_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			myCONCModule_SL.setMyCONCModuleOrModuleGroup_SL(myCONCModuleOrModuleGroup_SL);
			myCONCModuleOrModuleGroup_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public void assignGrade(final Grade4Public grade, final String comment) 
				throws PersistenceException{
        getThis().assignGrade(grade, comment);
    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP();
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getCp();
    }
    public Grade4Public getCurrentGrade() 
				throws PersistenceException{
        return getThis().getCurrentGrade();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    public GradeHistory4Public showHistory() 
				throws PersistenceException{
        return getThis().showHistory();
    }
    public void updateGradeCache() 
				throws PersistenceException{
        getThis().updateGradeCache();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
