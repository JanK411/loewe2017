
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class Unit_SL extends PersistentObject implements PersistentUnit_SL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static Unit_SL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade.getClass(objectId);
        return (Unit_SL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static Unit_SL4Public createUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf) throws PersistenceException{
        return createUnit_SL(instanceOf,false);
    }
    
    public static Unit_SL4Public createUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence) throws PersistenceException {
        PersistentUnit_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade
                .newDelayedUnit_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade
                .newUnit_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        if(result.getThis().getInstanceOf() == null)throw new PersistenceException("Field instanceOf in type Unit_SL has not been initialized!",0);
        return result;
    }
    
    public static Unit_SL4Public createUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public instanceOf,boolean delayed$Persistence,Unit_SL4Public This) throws PersistenceException {
        PersistentUnit_SL result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade
                .newDelayedUnit_SL();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade
                .newUnit_SL(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        final$$Fields.put("instanceOf", instanceOf);
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot myCONCCreditPointHavingElement_SL = (AbstractPersistentRoot)this.getMyCONCCreditPointHavingElement_SL();
            if (myCONCCreditPointHavingElement_SL != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    myCONCCreditPointHavingElement_SL, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, inDerived, false, true);
                result.put("myCONCCreditPointHavingElement_SL", proxiInformation);
                
            }
        }
        return result;
    }
    
    public Unit_SL provideCopy() throws PersistenceException{
        Unit_SL result = this;
        result = new Unit_SL(this.This, 
                             this.myCONCCreditPointHavingElement_SL, 
                             this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return true;
    }
    protected PersistentUnit_SL This;
    protected PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL;
    
    public Unit_SL(PersistentUnit_SL This,PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;
        this.myCONCCreditPointHavingElement_SL = myCONCCreditPointHavingElement_SL;        
    }
    
    static public long getTypeId() {
        return 240;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 240) ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade
            .newUnit_SL(this.getId());
        super.store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade.ThisSet(this.getId(), getThis());
        }
        if(this.getMyCONCCreditPointHavingElement_SL() != null){
            this.getMyCONCCreditPointHavingElement_SL().store();
            ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade.myCONCCreditPointHavingElement_SLSet(this.getId(), getMyCONCCreditPointHavingElement_SL());
        }
        
    }
    
    protected void setThis(PersistentUnit_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentUnit_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentCreditPointHavingElement_SL getMyCONCCreditPointHavingElement_SL() throws PersistenceException {
        return this.myCONCCreditPointHavingElement_SL;
    }
    public void setMyCONCCreditPointHavingElement_SL(PersistentCreditPointHavingElement_SL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.myCONCCreditPointHavingElement_SL)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.myCONCCreditPointHavingElement_SL = (PersistentCreditPointHavingElement_SL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theUnit_SLFacade.myCONCCreditPointHavingElement_SLSet(this.getId(), newValue);
        }
    }
    public PersistentUnit_SL getThis() throws PersistenceException {
        if(this.This == null){
            PersistentUnit_SL result = (PersistentUnit_SL)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentUnit_SL)this.This;
    }
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getInstanceOf() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).getInstanceOf();
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setInstanceOf(newValue);
    }
    public State_SL4Public getState() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).getState();
    }
    public void setState(State_SL4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setState(newValue);
    }
    public GradeHistory4Public getGradeHistory() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).getGradeHistory();
    }
    public void setGradeHistory(GradeHistory4Public newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setGradeHistory(newValue);
    }
    public PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL() throws PersistenceException {
        return ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL();
    }
    public void setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_SL newValue) throws PersistenceException {
        ((PersistentCreditPointHavingElement_SL)this.getMyCONCCreditPointHavingElement_SL()).setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(newValue);
    }
    public void delete$Me() throws PersistenceException{
        super.delete$Me();
        this.getMyCONCCreditPointHavingElement_SL().delete$Me();
    }
    
    public void accept(CreditPointHavingElement_SLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_SL(this);
    }
    public <R> R accept(CreditPointHavingElement_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_SL(this);
    }
    public <E extends model.UserException>  void accept(CreditPointHavingElement_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_SL(this);
    }
    public <R, E extends model.UserException> R accept(CreditPointHavingElement_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_SL(this);
    }
    public void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLVisitor visitor) throws PersistenceException {
        visitor.handleUnit_SL(this);
    }
    public <R> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_SL(this);
    }
    public <E extends model.UserException>  void accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_SL(this);
    }
    public <R, E extends model.UserException> R accept(ModuleOrModuleGroupOrStudyProgramOrUnit_SLReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_SL(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleUnit_SL(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleUnit_SL(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleUnit_SL(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleUnit_SL(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getAlreadyEarnedCP() != null) return 1;
        if (this.getCp() != null) return 1;
        return 0;
    }
    
    
    public NotAtomicModule_SL4Public getContainingModule() 
				throws PersistenceException{
        NotAtomicModule_SLSearchList result = null;
		if (result == null) result = ConnectionHandler.getTheConnectionHandler().theNotAtomicModule_SLFacade
										.inverseGetUnits(getThis().getId(), getThis().getClassId());
		try {
			return result.iterator().next();
		} catch (java.util.NoSuchElementException nsee){
			return null;
		}
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentUnit_SL)This);
		if(this.isTheSameAs(This)){
			PersistentCONCCreditPointHavingElement_SL myCONCCreditPointHavingElement_SL = (PersistentCONCCreditPointHavingElement_SL) model.CONCCreditPointHavingElement_SL.createCONCCreditPointHavingElement_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentUnit_SL)This);
			PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL = (PersistentCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL) model.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SL.createCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"), this.isDelayed$Persistence(), (PersistentUnit_SL)This);
			this.setMyCONCCreditPointHavingElement_SL(myCONCCreditPointHavingElement_SL);
			myCONCCreditPointHavingElement_SL.setMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(myCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL);
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public String alreadyEarnedCPAsString() 
				throws PersistenceException{
        return getThis().getAlreadyEarnedCP().toString();
    }
    public void assignGrade(final Grade4Public grade, final String comment) 
				throws PersistenceException{
        PersistentCreditPointHavingElement_SL myCONCCreditPointHavingElement_sl = this.getMyCONCCreditPointHavingElement_SL();
        myCONCCreditPointHavingElement_sl.assignGrade(grade, comment);
    }
    public Grade4Public calculateGrade() 
				throws PersistenceException{
        return getThis().getCurrentGrade();
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{


    }
    public String cpAsString() 
				throws PersistenceException{
        return getThis().getCp().toString();
    }
    public void fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().fetchAllRelevantUnitsAndAtomicModulesWithBinaryModules(aggregator);
        
    }
    public void fetchAllRelevantUnitsAndAtomicModules(final GradeAggregator4Public aggregator) 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().fetchAllRelevantUnitsAndAtomicModules(aggregator);
    }
    public CreditPoints4Public getAlreadyEarnedCP() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getAlreadyEarnedCP();
    }
    public Grade4Public getCalculatedStudentGrade() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCalculatedStudentGrade();
    }
    public CreditPoints4Public getCp() 
				throws PersistenceException{
        return getThis().getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getCp();
    }
    public Grade4Public getCurrentGrade() 
				throws PersistenceException{
        return this.getMyCONCCreditPointHavingElement_SL().getCurrentGrade();
    }
    public String getName() 
				throws PersistenceException{
        return this.getMyCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL().getName();
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getMyCONCCreditPointHavingElement_SL().initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{


    }
    public GradeHistory4Public showHistory() 
				throws PersistenceException{
        return this.getMyCONCCreditPointHavingElement_SL().showHistory();
    }
    public void updateGradeCache() 
				throws PersistenceException{
        //TODO: implement method: updateGradeCache
        
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
