
package model;

import persistence.*;


/* Additional import section end */

public abstract class ModuleOrModuleGroupOrStudyProgramOrUnit_GL extends PersistentObject implements PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade.getClass(objectId);
        return (ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            AbstractPersistentRoot activeState = (AbstractPersistentRoot)this.getActiveState();
            if (activeState != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    activeState, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("activeState", proxiInformation);
                
            }
            result.put("name", this.getName());
            AbstractPersistentRoot calculatedStudentGroupCP = (AbstractPersistentRoot)this.getCalculatedStudentGroupCP();
            if (calculatedStudentGroupCP != null) {
                String proxiInformation = SearchListRoot.calculateProxiInfoAndRecursiveGet(
                    calculatedStudentGroupCP, allResults, depth, essentialLevel, forGUI, false, essentialLevel <= 1, true, false, false);
                result.put("calculatedStudentGroupCP", proxiInformation);
                
            }
        }
        return result;
    }
    
    public abstract ModuleOrModuleGroupOrStudyProgramOrUnit_GL provideCopy() throws PersistenceException;
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL instanceOf;
    protected PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL This;
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_GL(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL instanceOf,PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.instanceOf = instanceOf;
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 144;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        super.store();
        if(this.getInstanceOf() != null){
            this.getInstanceOf().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade.instanceOfSet(this.getId(), getInstanceOf());
        }
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public getInstanceOf() throws PersistenceException {
        return this.instanceOf;
    }
    public void setInstanceOf(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if(newValue.isTheSameAs(this.instanceOf)) return;
        if(getThis().getInstanceOf() != null)throw new PersistenceException("Final field instanceOf in type ModuleOrModuleGroupOrStudyProgramOrUnit_GL has been set already!",0);
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.instanceOf = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade.instanceOfSet(this.getId(), newValue);
        }
    }
    protected void setThis(PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theModuleOrModuleGroupOrStudyProgramOrUnit_GLFacade.ThisSet(this.getId(), newValue);
        }
    }
    public abstract PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL getThis() throws PersistenceException ;
    
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_GL)This);
		if(this.isTheSameAs(This)){
			this.setInstanceOf((PersistentModuleOrModuleGroupOrStudyProgramOrUnit_PL)final$$Fields.get("instanceOf"));
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{
    }
    public void initializeOnCreation() 
				throws PersistenceException{
        getThis().getInstanceOf().setInEditable();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
    }
    
    
    // Start of section that contains overridden operations only.
    
    public CreditPoints4Public getCalculatedStudentGroupCP() 
				throws PersistenceException{
        return getThis().getInstanceOf().getCalculatedCP();
    }
    public String getName() 
				throws PersistenceException{
        return getThis().getInstanceOf().getName();
    }

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
