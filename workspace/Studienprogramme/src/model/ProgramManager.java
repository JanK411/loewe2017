
package model;

import common.Fraction;
import common.Literals;
import common.NameAlreadyExistsHelper;
import model.meta.StringFACTORY;
import persistence.*;
import model.visitor.*;

import java.text.MessageFormat;
import java.util.Date;


/* Additional import section end */

public class ProgramManager extends PersistentObject implements PersistentProgramManager{
    
    /** Throws persistence exception if the object with the given id does not exist. */
    public static ProgramManager4Public getById(long objectId) throws PersistenceException{
        long classId = ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade.getClass(objectId);
        return (ProgramManager4Public)PersistentProxi.createProxi(objectId, classId);
    }
    
    public static ProgramManager4Public createProgramManager() throws PersistenceException{
        return createProgramManager(false);
    }
    
    public static ProgramManager4Public createProgramManager(boolean delayed$Persistence) throws PersistenceException {
        PersistentProgramManager result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade
                .newDelayedProgramManager();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade
                .newProgramManager(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(result, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    public static ProgramManager4Public createProgramManager(boolean delayed$Persistence,ProgramManager4Public This) throws PersistenceException {
        PersistentProgramManager result = null;
        if(delayed$Persistence){
            result = ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade
                .newDelayedProgramManager();
            result.setDelayed$Persistence(true);
        }else{
            result = ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade
                .newProgramManager(-1);
        }
        java.util.HashMap<String,Object> final$$Fields = new java.util.HashMap<String,Object>();
        result.initialize(This, final$$Fields);
        result.initializeOnCreation();
        return result;
    }
    
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
            result.put("programs", this.getPrograms().getVector(allResults, depth, essentialLevel, forGUI, false, true, inDerived, false, false));
        }
        return result;
    }
    
    public ProgramManager provideCopy() throws PersistenceException{
        ProgramManager result = this;
        result = new ProgramManager(this.This, 
                                    this.getId());
        result.programs = this.programs.copy(result);
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    protected ProgramManager_ProgramsProxi programs;
    protected PersistentProgramManager This;
    
    public ProgramManager(PersistentProgramManager This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super(id);
        this.programs = new ProgramManager_ProgramsProxi(this);
        if (This != null && !(this.isTheSameAs(This))) this.This = This;        
    }
    
    static public long getTypeId() {
        return 194;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        if(!this.isDelayed$Persistence()) return;
        if (this.getClassId() == 194) ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade
            .newProgramManager(this.getId());
        super.store();
        this.getPrograms().store();
        if(!this.isTheSameAs(this.getThis())){
            this.getThis().store();
            ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade.ThisSet(this.getId(), getThis());
        }
        
    }
    
    public ProgramManager_ProgramsProxi getPrograms() throws PersistenceException {
        return this.programs;
    }
    protected void setThis(PersistentProgramManager newValue) throws PersistenceException {
        if (newValue == null) throw new PersistenceException("Null values not allowed!", 0);
        if (newValue.isTheSameAs(this)){
            this.This = null;
            return;
        }
        if(newValue.isTheSameAs(this.This)) return;
        long objectId = newValue.getId();
        long classId = newValue.getClassId();
        this.This = (PersistentProgramManager)PersistentProxi.createProxi(objectId, classId);
        if(!this.isDelayed$Persistence()){
            newValue.store();
            ConnectionHandler.getTheConnectionHandler().theProgramManagerFacade.ThisSet(this.getId(), newValue);
        }
    }
    public PersistentProgramManager getThis() throws PersistenceException {
        if(this.This == null){
            PersistentProgramManager result = (PersistentProgramManager)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentProgramManager)this.This;
    }
    
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleProgramManager(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleProgramManager(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleProgramManager(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleProgramManager(this);
    }
    public int getLeafInfo() throws PersistenceException{
        if (this.getPrograms().getLength() > 0) return 1;
        return 0;
    }
    
    
    public void addModulesOrModuleGroups(final ModuleGroupOrStudyProgram_PL4Public container, final ModuleOrModuleGroup_PLSearchList containees, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		AddModulesOrModuleGroupsCommand4Public command = model.meta.AddModulesOrModuleGroupsCommand.createAddModulesOrModuleGroupsCommand(nw, d1170);
		command.setContainer(container);
		java.util.Iterator<ModuleOrModuleGroup_PL4Public> containeesIterator = containees.iterator();
		while(containeesIterator.hasNext()){
			command.getContainees().add(containeesIterator.next());
		}
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void changeCreditPoints(final CreditPointHavingElement_PL4Public element, final common.Fraction newCP, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		ChangeCreditPointsCommand4Public command = model.meta.ChangeCreditPointsCommand.createChangeCreditPointsCommand(newCP, nw, d1170);
		command.setElement(element);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createAtomicModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name, final CreditPoints4Public cp, final String notenschema, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateAtomicModuleCommand4Public command = model.meta.CreateAtomicModuleCommand.createCreateAtomicModuleCommand(name, notenschema, nw, d1170);
		command.setContainer(container);
		command.setCp(cp);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createModuleGroup(final ModuleGroupOrStudyProgram_PL4Public container, final String name, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateModuleGroupCommand4Public command = model.meta.CreateModuleGroupCommand.createCreateModuleGroupCommand(name, nw, d1170);
		command.setContainer(container);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateModuleCommand4Public command = model.meta.CreateModuleCommand.createCreateModuleCommand(name, nw, d1170);
		command.setContainer(container);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createStudyProgram(final String name, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateStudyProgramCommand4Public command = model.meta.CreateStudyProgramCommand.createCreateStudyProgramCommand(name, nw, d1170);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void createUnit(final NotAtomicModule_PL4Public module, final String name, final CreditPoints4Public cp, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		CreateUnitCommand4Public command = model.meta.CreateUnitCommand.createCreateUnitCommand(name, nw, d1170);
		command.setModule(module);
		command.setCp(cp);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentProgramManager)This);
		if(this.isTheSameAs(This)){
		}
    }
    public void removeUnit(final Unit_PL4Public unit, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		RemoveUnitCommand4Public command = model.meta.RemoveUnitCommand.createRemoveUnitCommand(nw, d1170);
		command.setUnit(unit);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    public void transferCP(final NotAtomicModule_GL4Public toBeChanged, final Unit_GL4Public fromUnit, final Unit_GL4Public toUnit, final common.Fraction amountToTransfer, final Invoker invoker) 
				throws PersistenceException{
        java.sql.Date nw = new java.sql.Date(new java.util.Date().getTime());
		java.sql.Date d1170 = new java.sql.Date(new java.util.Date(0).getTime());
		TransferCPCommand4Public command = model.meta.TransferCPCommand.createTransferCPCommand(amountToTransfer, nw, d1170);
		command.setToBeChanged(toBeChanged);
		command.setFromUnit(fromUnit);
		command.setToUnit(toUnit);
		command.setInvoker(invoker);
		command.setCommandReceiver(getThis());
		model.meta.CommandCoordinator.getTheCommandCoordinator().coordinate(command);
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void addModulesOrModuleGroups(final ModuleGroupOrStudyProgram_PL4Public container, final ModuleOrModuleGroup_PLSearchList containees) 
				throws model.StateException, model.CycleException, model.AlreadyInListException, PersistenceException{
        container.addContainees(containees);
    }
    public void changeCreditPoints(final CreditPointHavingElement_PL4Public element, final common.Fraction newCP) 
				throws model.StateException, PersistenceException{
        element.changeInitialCP(newCP);
    }
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public AtomicModule_PL4Public createAtomicModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name, final CreditPoints4Public cp, final String notenschema) 
				throws model.NameAlreadyExistsException, model.StateException, PersistenceException{
        if (NameAlreadyExistsHelper.nameAlreadyExists(name))
            throw new NameAlreadyExistsException(Literals.NameAlreadyExistsMessage(name));

        // TODO Manager für CP objekte,sodass diese wiederverwendet werden k?nnen hier benutzen
        AtomicModule_PL4Public module = StringFACTORY.createObjectBySubTypeNameForGradeFactory(notenschema).accept(new GradeFactoryReturnVisitor<AtomicModule_PL4Public>() {
            @Override
            public AtomicModule_PL4Public handleRealGrades(RealGrades4Public realGrades) throws PersistenceException {
                return TripleAtomicModule_PL.createTripleAtomicModule_PL(name, cp);
            }

            @Override
            public AtomicModule_PL4Public handlePassedNotPassedGrades(PassedNotPassedGrades4Public passedNotPassedGrades) throws PersistenceException {
                return BinaryAtomicModule_PL.createBinaryAtomicModule_PL(name, cp);
            }
        });

        try {
            container.addContainee(module);
            return module;
        } catch (CycleException | AlreadyInListException e) {
            throw new Error("cannot occur", e);
        }
    }
    public ModuleGroup_PL4Public createModuleGroup(final ModuleGroupOrStudyProgram_PL4Public container, final String name) 
				throws model.NameAlreadyExistsException, model.StateException, PersistenceException{
        if (NameAlreadyExistsHelper.nameAlreadyExists(name))
            throw new NameAlreadyExistsException(Literals.NameAlreadyExistsMessage(name));
        final ModuleGroup_PL4Public moduleGroup = ModuleGroup_PL.createModuleGroup_PL(name);
        try {
            container.addContainee(moduleGroup);
        } catch (CycleException | AlreadyInListException e) {
            throw new Error("cannot occur");
        }
        return moduleGroup;
    }
    public NotAtomicModule_PL4Public createModule(final ModuleGroupOrStudyProgram_PL4Public container, final String name) 
				throws model.NameAlreadyExistsException, model.StateException, PersistenceException{

        if (NameAlreadyExistsHelper.nameAlreadyExists(name))
            throw new NameAlreadyExistsException(Literals.NameAlreadyExistsMessage(name));
        NotAtomicModule_PL4Public module = NotAtomicModule_PL.createNotAtomicModule_PL(name);
        try {
            container.addContainee(module);
        } catch (CycleException | AlreadyInListException e) {
            throw new Error("cannot occur");
        }
        return module;
    }
    public StudyProgram_PL4Public createStudyProgram(final String name) 
				throws model.NameAlreadyExistsException, PersistenceException{
        if (NameAlreadyExistsHelper.nameAlreadyExists(name))
            throw new NameAlreadyExistsException(Literals.NameAlreadyExistsMessage(name));
        final StudyProgram_PL4Public studyProgram_pl = StudyProgram_PL.createStudyProgram_PL(name);
        getThis().getPrograms().add(studyProgram_pl);
        return studyProgram_pl;
    }
    public Unit_PL4Public createUnit(final NotAtomicModule_PL4Public module, final String name, final CreditPoints4Public cp) 
				throws model.NameAlreadyExistsException, model.StateException, PersistenceException{
        if (NameAlreadyExistsHelper.nameAlreadyExists(name)) {
            throw new NameAlreadyExistsException(Literals.NameAlreadyExistsMessage(name));
        }
        Unit_PL4Public unit = Unit_PL.createUnit_PL(cp, name);
        module.addUnit(unit);
        return unit;
    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    public void removeUnit(final Unit_PL4Public unit) 
				throws model.StateException, PersistenceException{
        unit.getContainer().removeUnit(unit);
    }
    public void transferCP(final NotAtomicModule_GL4Public toBeChanged, final Unit_GL4Public fromUnit, final Unit_GL4Public toUnit, final common.Fraction amountToTransfer) 
				throws model.InvalidParameterException, PersistenceException{
        if (fromUnit.getStudentGroupCP().getValue().compareTo(amountToTransfer) < 0) {
            throw new InvalidParameterException("Es können nicht mehr CP transferiert werden, als die <fromUnit> enthält!");
        } else if (amountToTransfer.compareTo(Fraction.Null) < 0) {
            throw new InvalidParameterException("Es dürfen keine negativen CP transferiert werden!");
        }
        // Kann eigentlich bei aktueller Oberfläche nicht auftreten, weiß nicht ob man das braucht
        else if (toBeChanged.getUnits().findFirst(fromUnit::equals) == null) {
            throw new InvalidParameterException("Die <fromUnit> ist keine Unit des Nicht-Atomaren Moduls " + toBeChanged);
        }
        else if (toBeChanged.getUnits().findFirst(toUnit::equals) == null) {
            throw new InvalidParameterException("Die <toUnit> ist keine Unit des Nicht-Atomaren Moduls " + toBeChanged);
        }

        fromUnit.changeStudentGroupCP(CreditPointsManager.getTheCreditPointsManager().getCP(fromUnit.getStudentGroupCP().getValue().sub(amountToTransfer)));
        toUnit.changeStudentGroupCP(CreditPointsManager.getTheCreditPointsManager().getCP(toUnit.getStudentGroupCP().getValue().add(amountToTransfer)));
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
