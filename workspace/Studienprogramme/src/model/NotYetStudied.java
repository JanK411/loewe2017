
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class NotYetStudied extends model.ActiveState implements PersistentNotYetStudied{
    
    private static NotYetStudied4Public theNotYetStudied = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static NotYetStudied4Public getTheNotYetStudied() throws PersistenceException{
        if (theNotYetStudied == null || reset$For$Test){
            if (reset$For$Test) theNotYetStudied = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            NotYetStudied4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theNotYetStudiedFacade.getTheNotYetStudied();
                            theNotYetStudied = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                NotYetStudied4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theNotYetStudied== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theNotYetStudied;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theNotYetStudied;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public NotYetStudied provideCopy() throws PersistenceException{
        NotYetStudied result = this;
        result = new NotYetStudied(this.This, 
                                   this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public NotYetStudied(PersistentActiveState This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentActiveState)This,id);        
    }
    
    static public long getTypeId() {
        return 171;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentNotYetStudied getThis() throws PersistenceException {
        if(this.This == null){
            PersistentNotYetStudied result = (PersistentNotYetStudied)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentNotYetStudied)this.This;
    }
    
    public void accept(ActiveStateVisitor visitor) throws PersistenceException {
        visitor.handleNotYetStudied(this);
    }
    public <R> R accept(ActiveStateReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotYetStudied(this);
    }
    public <E extends model.UserException>  void accept(ActiveStateExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotYetStudied(this);
    }
    public <R, E extends model.UserException> R accept(ActiveStateReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotYetStudied(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleNotYetStudied(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleNotYetStudied(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleNotYetStudied(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleNotYetStudied(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentNotYetStudied)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{
        super.initializeOnCreation();
    }
    public void initializeOnInstantiation() 
				throws PersistenceException{
        super.initializeOnInstantiation();
    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
