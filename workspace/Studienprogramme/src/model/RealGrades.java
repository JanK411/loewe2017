
package model;

import persistence.*;
import model.visitor.*;


/* Additional import section end */

public class RealGrades extends model.GradeFactory implements PersistentRealGrades{
    
    private static RealGrades4Public theRealGrades = null;
    public static boolean reset$For$Test = false;
    private static final Object $$lock = new Object();
    public static RealGrades4Public getTheRealGrades() throws PersistenceException{
        if (theRealGrades == null || reset$For$Test){
            if (reset$For$Test) theRealGrades = null;
            class Initializer implements Runnable {
                PersistenceException exception = null;
                public void /* internal */ run(){
                    this.produceSingleton();
                }
                void produceSingleton() {
                    synchronized ($$lock){
                        try {
                            RealGrades4Public proxi = null;
                            proxi = ConnectionHandler.getTheConnectionHandler().theRealGradesFacade.getTheRealGrades();
                            theRealGrades = proxi;
                            if(proxi.getId() < 0) {
                                ((AbstractPersistentRoot)proxi).setId(proxi.getId() * -1);
                                proxi.initialize(proxi, new java.util.HashMap<String,Object>());
                                proxi.initializeOnCreation();
                            }
                        } catch (PersistenceException e){
                            exception = e;
                        } finally {
                            $$lock.notify();
                        }
                        
                    }
                }
                RealGrades4Public getResult() throws PersistenceException{
                    synchronized ($$lock) {
                        if (exception == null && theRealGrades== null) try {$$lock.wait();} catch (InterruptedException e) {}
                        if(exception != null) throw exception;
                        return theRealGrades;
                    }
                }
                
            }
            reset$For$Test = false;
            Initializer initializer = new Initializer();
            new Thread(initializer).start();
            return initializer.getResult();
        }
        return theRealGrades;
    }
    @SuppressWarnings("unchecked")
    public java.util.HashMap<String,Object> toHashtable(java.util.HashMap<String,Object> allResults, int depth, int essentialLevel, boolean forGUI, boolean leaf, boolean inDerived) throws PersistenceException {
        java.util.HashMap<String,Object> result = null;
        if (depth > 0 && essentialLevel <= common.RPCConstantsAndServices.EssentialDepth){
            String uniqueKey = common.RPCConstantsAndServices.createHashtableKey(this.getClassId(), this.getId());
            if (leaf){
                result = (java.util.HashMap<String,Object>)allResults.get(uniqueKey);
                if (result != null) return result;
            }
            result = super.toHashtable(allResults, depth, essentialLevel, forGUI, false, inDerived);
            if (leaf) allResults.put(uniqueKey, result);
        }
        return result;
    }
    
    public RealGrades provideCopy() throws PersistenceException{
        RealGrades result = this;
        result = new RealGrades(this.This, 
                                this.getId());
        this.copyingPrivateUserAttributes(result);
        return result;
    }
    
    public boolean hasEssentialFields() throws PersistenceException{
        return false;
    }
    
    public RealGrades(PersistentGradeFactory This,long id) throws PersistenceException {
        /* Shall not be used by clients for object construction! Use static create operation instead! */
        super((PersistentGradeFactory)This,id);        
    }
    
    static public long getTypeId() {
        return 157;
    }
    
    public long getClassId() {
        return getTypeId();
    }
    
    public void store() throws PersistenceException {
        // Singletons cannot be delayed!
    }
    
    public PersistentRealGrades getThis() throws PersistenceException {
        if(this.This == null){
            PersistentRealGrades result = (PersistentRealGrades)PersistentProxi.createProxi(this.getId(),this.getClassId());
            result.getTheObject();
            return result;
        }return (PersistentRealGrades)this.This;
    }
    
    public void accept(GradeFactoryVisitor visitor) throws PersistenceException {
        visitor.handleRealGrades(this);
    }
    public <R> R accept(GradeFactoryReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleRealGrades(this);
    }
    public <E extends model.UserException>  void accept(GradeFactoryExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleRealGrades(this);
    }
    public <R, E extends model.UserException> R accept(GradeFactoryReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleRealGrades(this);
    }
    public void accept(AnythingVisitor visitor) throws PersistenceException {
        visitor.handleRealGrades(this);
    }
    public <R> R accept(AnythingReturnVisitor<R>  visitor) throws PersistenceException {
         return visitor.handleRealGrades(this);
    }
    public <E extends model.UserException>  void accept(AnythingExceptionVisitor<E> visitor) throws PersistenceException, E {
         visitor.handleRealGrades(this);
    }
    public <R, E extends model.UserException> R accept(AnythingReturnExceptionVisitor<R, E>  visitor) throws PersistenceException, E {
         return visitor.handleRealGrades(this);
    }
    public int getLeafInfo() throws PersistenceException{
        return 0;
    }
    
    
    public void initialize(final Anything This, final java.util.HashMap<String,Object> final$$Fields) 
				throws PersistenceException{
        this.setThis((PersistentRealGrades)This);
		if(this.isTheSameAs(This)){
		}
    }
    
    
    // Start of section that contains operations that must be implemented.
    
    public void copyingPrivateUserAttributes(final Anything copy) 
				throws PersistenceException{

    }
    public void initializeOnCreation() 
				throws PersistenceException{

    }
    public void initializeOnInstantiation() 
				throws PersistenceException{

    }
    
    
    // Start of section that contains overridden operations only.
    

    /* Start of protected part that is not overridden by persistence generator */
    
    /* End of protected part that is not overridden by persistence generator */
    
}
