package viewClient;

import javafx.scene.image.Image;
import view.*;
import view.objects.ViewRoot;
import view.objects.ViewObjectInTree;

import view.visitor.AnythingStandardVisitor;

import java.util.Optional;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SelectionModel;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;


import com.sun.javafx.geom.Point2D;

import javax.swing.tree.TreeModel;


public class AdminServiceClientView extends BorderPane implements ExceptionAndEventHandler {

    private ConnectionMaster connection;
    private ExceptionAndEventHandler parent;
    private AdminServiceView service;

    /**
     * This is the default constructor
     */
    public AdminServiceClientView(ExceptionAndEventHandler parent, AdminServiceView service) {
        super();
        this.parent = parent;
        this.service = service;
        this.initialize();
    }

    @SuppressWarnings("unused")
    private AdminServiceView getService() {
        return this.service;
    }

    private void initialize() {
        this.setCenter(this.getMainSplitPane());
        if (!WithStaticOperations && this.getMainToolBar().getItems().size() > 0) {
            this.setTop(this.getMainToolBar());
        }
    }

    private ToolBar mainToolBar = null;

    private ToolBar getMainToolBar() {
        if (this.mainToolBar == null) {
            this.mainToolBar = new ToolBar();
            for (Button current : this.getToolButtonsForStaticOperations()) {
                this.mainToolBar.getItems().add(current);
            }
        }
        return this.mainToolBar;
    }

    private SplitPane mainSplitPane = null;

    private SplitPane getMainSplitPane() {
        if (this.mainSplitPane == null) {
            this.mainSplitPane = new SplitPane();
            this.mainSplitPane.setOrientation(Orientation.HORIZONTAL);
            this.mainSplitPane.getItems().addAll(this.getNavigationSplitPane(), this.getTitledDetailsPane());
            this.mainSplitPane.setDividerPosition(0, 0.5);
            this.mainSplitPane.prefHeightProperty().bind(this.heightProperty());
            this.mainSplitPane.prefWidthProperty().bind(this.widthProperty());
        }
        return this.mainSplitPane;
    }

    private SplitPane navigationSplitPane = null;

    private SplitPane getNavigationSplitPane() {
        if (this.navigationSplitPane == null) {
            this.navigationSplitPane = new SplitPane();
            this.navigationSplitPane.setOrientation(Orientation.VERTICAL);
            this.navigationSplitPane.getItems().addAll(this.getNavigationPanel(), this.getErrorPanel());
            this.navigationSplitPane.prefHeightProperty().bind(this.getMainSplitPane().heightProperty());
            this.navigationSplitPane.setDividerPosition(0, 1.0);
            this.navigationSplitPane.heightProperty().addListener(new ChangeListener<Number>() {
                public void changed(
                        ObservableValue<? extends Number> observable,
                        Number oldValue, Number newValue) {
                    if (!getErrorPanel().isVisible()) {
                        navigationSplitPane.setDividerPosition(0, 1.0);
                    } else {
                        navigationSplitPane.setDividerPosition(0, 0.7);
                    }
                }
            });
            this.getErrorPanel().setMinHeight(0);
        }
        return this.navigationSplitPane;
    }

    private TitledPane treePanel = null;

    private TitledPane getNavigationPanel() {
        if (this.treePanel == null) {
            this.treePanel = new TitledPane(GUIConstants.NaviationTitle, this.getNavigationTree());
            this.treePanel.setCollapsible(false);
            this.treePanel.prefHeightProperty().bind(this.getNavigationSplitPane().heightProperty());
        }
        return treePanel;
    }

    private TitledPane errorPanel = null;

    private TitledPane getErrorPanel() {
        if (this.errorPanel == null) {
            this.errorPanel = new TitledPane(GUIConstants.ErrorTitle, this.getErrorTree());
            this.errorPanel.setCollapsible(false);
            this.errorPanel.setVisible(false);
        }
        return this.errorPanel;
    }

    private TreeRefresh errorTree = null;

    private TreeRefresh getErrorTree() {
        if (this.errorTree == null) {
            this.errorTree = new TreeRefresh(parent);
            this.errorTree.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                public void handle(ContextMenuEvent e) {
                    tryShowContextMenu(e, errorTree, false);
                }
            });
        }
        return this.errorTree;
    }

    private void setErrors(TreeModel errors) {
        this.getErrorPanel().setVisible(true);
        this.getErrorTree().setModel(errors);
        this.getNavigationSplitPane().setDividerPosition(0, 0.7);
    }

    private void setNoErrors() {
        this.getErrorPanel().setVisible(false);
        this.getNavigationSplitPane().setDividerPosition(0, 1.0);
    }

    private TitledPane titledDetailsPane = null;

    private TitledPane getTitledDetailsPane() {
        if (this.titledDetailsPane == null) {
            this.titledDetailsPane = new TitledPane();
            this.titledDetailsPane.setText(GUIConstants.DetailsTitle);
            this.titledDetailsPane.setCollapsible(false);
            this.titledDetailsPane.prefHeightProperty().bind(this.getMainSplitPane().heightProperty());
        }
        return this.titledDetailsPane;
    }

    private TreeRefresh navigationTree = null;

    private TreeRefresh getNavigationTree() {
        if (this.navigationTree == null) {
            this.navigationTree = new TreeRefresh(parent);
            this.navigationTree.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            this.navigationTree.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<ViewObjectInTree>>() {
                public void changed(
                        ObservableValue<? extends TreeItem<ViewObjectInTree>> observable,
                        TreeItem<ViewObjectInTree> oldValue,
                        TreeItem<ViewObjectInTree> newValue) {
                    if (menu != null) menu.hide();
                    if (newValue != null) {
                        setDetailsTable((Anything) newValue.getValue().getWrappedObject());
                    }
                }
            });
            this.navigationTree.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
                public void handle(ContextMenuEvent e) {
                    tryShowContextMenu(e, navigationTree, WithStaticOperations);
                }
            });
            navigationTree.setOnKeyReleased(new EventHandler<KeyEvent>() {
                public void handle(KeyEvent event) {
                    if (event.getCode() == KeyCode.F5) {
                        Platform.runLater(new Runnable() {
                            public void run() {
                                try {
                                    getNavigationTree().refreshTree();
                                } catch (ModelException e) {
                                    handleException(e);
                                }
                            }
                        });
                    }
                }
            });
        }
        return this.navigationTree;
    }

    private DetailPanel currentDetails = null;

    protected void setDetailsTable(Anything object) {
        this.getTitledDetailsPane().setVisible(false);
        if (object == null && this.getConnection() != null) object = this.getConnection().getAdminServiceView();
        if (object == null) {
            this.currentDetails = getNoDetailsPanel();
        } else {
            try {
                this.currentDetails = this.getDetailView(object);
            } catch (ModelException e) {
                this.handleException(e);
                this.currentDetails = null;
            }
            if (this.currentDetails == null) this.currentDetails = getStandardDetailsTable(object);
        }
        this.getTitledDetailsPane().setContent(this.currentDetails);
        this.getTitledDetailsPane().setVisible(true);
        this.currentDetails.prefWidthProperty().bind(this.getTitledDetailsPane().widthProperty());
    }

    private DetailPanel getDetailView(final Anything anything) throws ModelException {
        class PanelDecider extends AnythingStandardVisitor {

            private DetailPanel result;

            public DetailPanel getResult() {
                return this.result;
            }

            protected void standardHandling(Anything Anything) throws ModelException {
                this.result = null;
            }
            //TODO Overwrite all handle methods for the types for which you intend to provide a special panel!
        }
        PanelDecider decider = new PanelDecider();
        anything.accept(decider);
        return decider.getResult();
    }

    private NoDetailPanel noDetailPanel = null;

    public DetailPanel getNoDetailsPanel() {
        if (this.noDetailPanel == null) this.noDetailPanel = new NoDetailPanel(this);
        return noDetailPanel;
    }

    private ContextMenu menu = null;

    protected void tryShowContextMenu(ContextMenuEvent e, TreeRefresh tree, boolean withStaticOperations) {
        if (this.menu != null) this.menu.hide();
        SelectionModel<TreeItem<ViewObjectInTree>> selMod = tree.getSelectionModel();
        ViewObjectInTree selVal = selMod.getSelectedItem().getValue();
        ViewRoot selected = selVal.getWrappedObject();
        this.menu = this.getContextMenu(selected, withStaticOperations, new Point2D((float) e.getScreenX(), (float) e.getScreenY()));
        if (this.menu.getItems().size() > 0) {
            this.menu.show(this.getNavigationPanel(), e.getScreenX(), e.getScreenY());
        }
    }

    private DetailPanel getStandardDetailsTable(Anything object) {
        try {
            return DefaultDetailPanel.getStandardDetailPanel(object, this);
        } catch (ModelException e) {
            this.handleException(e);
            return new NoDetailPanel(this);
        }
    }

    public void setConnection(ConnectionMaster connection) {
        this.connection = connection;
    }

    public AdminServiceConnection getConnection() {
        return (AdminServiceConnection) this.connection;
    }

    /**
     * Is called by the refresher thread if the server object has changed
     **/
    public void handleRefresh() {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    getNavigationTree().refreshTree();
                    java.util.Vector<?> errors = getConnection().getAdminServiceView().getErrors();
                    if (errors.size() > 0) setErrors(new ListRoot(errors));
                    else setNoErrors();
                } catch (ModelException e) {
                    handleException(e);
                }
            }
        });
        //TODO adjust implementation: handleRefresh()!
    }

    /**
     * Is called only once after the connection has been established
     **/
    public void initializeConnection() {
        Platform.runLater(new Runnable() {
            public void run() {
                getNavigationTree().setModel((TreeModel) getConnection().getAdminServiceView());
                getNavigationTree().getRoot().setExpanded(true);
                getNavigationTree().getSelectionModel().select(getNavigationTree().getRoot());
            }
        });
        //TODO adjust implementation: initializeConnection
    }

    public void handleException(ModelException exception) {
        this.parent.handleException(exception);
    }

    public void handleOKMessage(String message) {
        this.parent.handleOKMessage(message);
    }

    public void handleUserException(UserException exception) {
        this.parent.handleUserException(exception);
    }

    private javafx.geometry.Point2D getPointForView() {
        javafx.geometry.Point2D result = getNavigationTree().localToScene(getNavigationTree().getWidth() / 18
                        + getNavigationTree().getScene().getWindow().getX() + getNavigationTree().getScene().getX()
                , getNavigationTree().getHeight() / 18
                        + getNavigationTree().getScene().getWindow().getY() + getNavigationTree().getScene().getY());
        return result;
    }

	/* Menu and wizard section start */

	static boolean WithStaticOperations = false;


    interface MenuItemVisitor{
        ImageView handle(ChangeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTRMenuItem menuItem);
        ImageView handle(TransferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTRMenuItem menuItem);
        ImageView handle(ClearErrorPRMTRErrorDisplayPRMTRMenuItem menuItem);
        ImageView handle(AddModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRMenuItem menuItem);
        ImageView handle(CreateModuleGroupPRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(AssignGradeForBinaryAtomicModulePRMTRBinaryAtomicModule_SLPRMTRBinaryGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(AssignGradeForTripleAtomicModulePRMTRTripleAtomicModule_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(AssignTrippleGradeToUnitPRMTRUnit_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(ShowHistoriePRMTRCreditPointHavingElement_SLPRMTRMenuItem menuItem);
        ImageView handle(ResetPasswordPRMTRStudentWrapperPRMTRMenuItem menuItem);
        ImageView handle(CreateStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTRMenuItem menuItem);
        ImageView handle(RemoveStudentPRMTRStudentWrapperPRMTRMenuItem menuItem);
        ImageView handle(AddStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRMenuItem menuItem);
        ImageView handle(CreateStudentGroupPRMTRStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(CreateStudyProgramPRMTRProgramManagerPRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(CreateUnitPRMTRNotAtomicModule_PLPRMTRStringPRMTRFractionPRMTRMenuItem menuItem);
        ImageView handle(MakeGroupActivePRMTRStudentGroupPRMTRMenuItem menuItem);
        ImageView handle(CreateAtomicModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRFractionPRMTRGradeFactorySUBTYPENamePRMTRMenuItem menuItem);
        ImageView handle(FinishesStudyingPRMTRStudentGroupPRMTRMenuItem menuItem);
        ImageView handle(RemoveUnitPRMTRUnit_PLPRMTRMenuItem menuItem);
        ImageView handle(CreateModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem);
        ImageView handle(FindStudentPRMTRStringPRMTRMenuItem menuItem);
    }
    private abstract class AdminServiceMenuItem extends MenuItem{
        private AdminServiceMenuItem(){
            this.setGraphic(getIconForMenuItem(this));
        }
        abstract protected ImageView accept(MenuItemVisitor visitor);
    }
    private class ChangeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class TransferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class ClearErrorPRMTRErrorDisplayPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class AddModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateModuleGroupPRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class AssignGradeForBinaryAtomicModulePRMTRBinaryAtomicModule_SLPRMTRBinaryGradeSUBTYPENamePRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class AssignGradeForTripleAtomicModulePRMTRTripleAtomicModule_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class AssignTrippleGradeToUnitPRMTRUnit_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class ShowHistoriePRMTRCreditPointHavingElement_SLPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class ResetPasswordPRMTRStudentWrapperPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class RemoveStudentPRMTRStudentWrapperPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class AddStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateStudentGroupPRMTRStudyProgram_PLPRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateStudyProgramPRMTRProgramManagerPRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateUnitPRMTRNotAtomicModule_PLPRMTRStringPRMTRFractionPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class MakeGroupActivePRMTRStudentGroupPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateAtomicModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRFractionPRMTRGradeFactorySUBTYPENamePRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class FinishesStudyingPRMTRStudentGroupPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class RemoveUnitPRMTRUnit_PLPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class CreateModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private class FindStudentPRMTRStringPRMTRMenuItem extends AdminServiceMenuItem{
        protected ImageView accept(MenuItemVisitor visitor){
            return visitor.handle(this);
        }
    }
    private java.util.Vector<javafx.scene.control.Button> getToolButtonsForStaticOperations() {
        java.util.Vector<javafx.scene.control.Button> result = new java.util.Vector<javafx.scene.control.Button>();
        javafx.scene.control.Button currentButton = null;
        currentButton = new javafx.scene.control.Button("suche Student ... ");
        currentButton.setGraphic(new FindStudentPRMTRStringPRMTRMenuItem().getGraphic());
        currentButton.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(javafx.event.ActionEvent e) {
                final AdminServiceFindStudentStringMssgWizard wizard = new AdminServiceFindStudentStringMssgWizard("suche Student");
                wizard.setWidth(getNavigationPanel().getWidth());
                wizard.setX( getPointForView().getX());
                wizard.setY( getPointForView().getY());
                wizard.showAndWait();
            }
        });
        result.add(currentButton);
        return result;
    }
    private ContextMenu getContextMenu(final ViewRoot selected, final boolean withStaticOperations, final Point2D menuPos) {
        final ContextMenu result = new ContextMenu();
        MenuItem item = null;
        item = new FindStudentPRMTRStringPRMTRMenuItem();
        item.setText("(S) suche Student ... ");
        item.setOnAction(new EventHandler<ActionEvent>(){
            public void handle(javafx.event.ActionEvent e) {
                final AdminServiceFindStudentStringMssgWizard wizard = new AdminServiceFindStudentStringMssgWizard("suche Student");
                wizard.setWidth(getNavigationPanel().getWidth());
                wizard.setX( getPointForView().getX());
                wizard.setY( getPointForView().getY());
                wizard.showAndWait();
            }
        });
        if (withStaticOperations) result.getItems().add(item);
        if (selected != null){
            try {
                this.setPreCalculatedFilters(this.getConnection().adminService_Menu_Filter((Anything)selected));
            } catch (ModelException me){
                this.handleException(me);
                return result;
            }
            if (selected instanceof NotAtomicModule_GLView){
                if (filter_transferCP((NotAtomicModule_GLView) selected)) {
                    item = new TransferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTRMenuItem();
                    item.setText("Leistungspunkte von Units anpassen ... ");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            final AdminServiceTransferCPNotAtomicModule_GLUnit_GLUnit_GLFractionMssgWizard wizard = new AdminServiceTransferCPNotAtomicModule_GLUnit_GLUnit_GLFractionMssgWizard("Leistungspunkte von Units anpassen");
                            wizard.setFirstArgument((NotAtomicModule_GLView)selected);
                            wizard.setWidth(getNavigationPanel().getWidth());
                            wizard.setX( getPointForView().getX());
                            wizard.setY( getPointForView().getY());
                            wizard.showAndWait();
                        }
                    });
                    result.getItems().add(item);
                }
            }
            if (selected instanceof ModuleGroupOrStudyProgram_PLView){
                item = new AddModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRMenuItem();
                item.setText("Modul hinzufuegen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceAddModulesOrModuleGroupsModuleGroupOrStudyProgram_PLModuleOrModuleGroup_PLLSTMssgWizard wizard = new AdminServiceAddModulesOrModuleGroupsModuleGroupOrStudyProgram_PLModuleOrModuleGroup_PLLSTMssgWizard("Modul hinzufuegen");
                        wizard.setFirstArgument((ModuleGroupOrStudyProgram_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
                item = new CreateModuleGroupPRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem();
                item.setText("Modulgruppe erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateModuleGroupModuleGroupOrStudyProgram_PLStringMssgWizard wizard = new AdminServiceCreateModuleGroupModuleGroupOrStudyProgram_PLStringMssgWizard("Modulgruppe erstellen");
                        wizard.setFirstArgument((ModuleGroupOrStudyProgram_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
                item = new CreateAtomicModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRFractionPRMTRGradeFactorySUBTYPENamePRMTRMenuItem();
                item.setText("atomares Modul erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateAtomicModuleModuleGroupOrStudyProgram_PLStringFractionGradeFactorySUBTYPENameMssgWizard wizard = new AdminServiceCreateAtomicModuleModuleGroupOrStudyProgram_PLStringFractionGradeFactorySUBTYPENameMssgWizard("atomares Modul erstellen");
                        wizard.setFirstArgument((ModuleGroupOrStudyProgram_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
                item = new CreateModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem();
                item.setText("nicht atomares Modul erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateModuleModuleGroupOrStudyProgram_PLStringMssgWizard wizard = new AdminServiceCreateModuleModuleGroupOrStudyProgram_PLStringMssgWizard("nicht atomares Modul erstellen");
                        wizard.setFirstArgument((ModuleGroupOrStudyProgram_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof CreditPointHavingElement_SLView){
                item = new ShowHistoriePRMTRCreditPointHavingElement_SLPRMTRMenuItem();
                item.setText("Notenhistorie anzeigen");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        Alert confirm = new Alert(AlertType.CONFIRMATION);
                        confirm.setTitle(GUIConstants.ConfirmButtonText);
                        confirm.setHeaderText(null);
                        confirm.setContentText("Notenhistorie anzeigen" + GUIConstants.ConfirmQuestionMark);
                        confirm.setX( getPointForView().getX() );
                        confirm.setY( getPointForView().getY() );
                        Optional<ButtonType> buttonResult = confirm.showAndWait();
                        if (buttonResult.get() == ButtonType.OK) {
                            try {
                                ViewRoot result = (ViewRoot)getConnection().showHistorie((CreditPointHavingElement_SLView)selected);
                                getConnection().setEagerRefresh();
                                ReturnValueView view = new ReturnValueView(result, new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9), AdminServiceClientView.this);
                                view.setX( getPointForView().getX() );
                                view.setY( getPointForView().getY() );
                                view.showAndWait();
                            }catch(ModelException me){
                                handleException(me);
                            }
                        }
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof StudyProgram_PLView){
                item = new CreateStudentGroupPRMTRStudyProgram_PLPRMTRStringPRMTRMenuItem();
                item.setText("Studiengruppe zu Studienprogramm erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateStudentGroupStudyProgram_PLStringMssgWizard wizard = new AdminServiceCreateStudentGroupStudyProgram_PLStringMssgWizard("Studiengruppe zu Studienprogramm erstellen");
                        wizard.setFirstArgument((StudyProgram_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof CreditPointHavingElement_PLView){
                if (filter_changeCreditPoints((CreditPointHavingElement_PLView) selected)) {
                    item = new ChangeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTRMenuItem();
                    item.setText("Leistungspunkte aendern ... ");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            final AdminServiceChangeCreditPointsCreditPointHavingElement_PLFractionMssgWizard wizard = new AdminServiceChangeCreditPointsCreditPointHavingElement_PLFractionMssgWizard("Leistungspunkte aendern");
                            wizard.setFirstArgument((CreditPointHavingElement_PLView)selected);
                            wizard.setWidth(getNavigationPanel().getWidth());
                            wizard.setX( getPointForView().getX());
                            wizard.setY( getPointForView().getY());
                            wizard.showAndWait();
                        }
                    });
                    result.getItems().add(item);
                }
            }
            if (selected instanceof ErrorDisplayView){
                item = new ClearErrorPRMTRErrorDisplayPRMTRMenuItem();
                item.setText("Meldung loeschen");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        Alert confirm = new Alert(AlertType.CONFIRMATION);
                        confirm.setTitle(GUIConstants.ConfirmButtonText);
                        confirm.setHeaderText(null);
                        confirm.setContentText("Meldung loeschen" + GUIConstants.ConfirmQuestionMark);
                        confirm.setX( getPointForView().getX() );
                        confirm.setY( getPointForView().getY() );
                        Optional<ButtonType> buttonResult = confirm.showAndWait();
                        if (buttonResult.get() == ButtonType.OK) {
                            try {
                                getConnection().clearError((ErrorDisplayView)selected);
                                getConnection().setEagerRefresh();
                                
                            }catch(ModelException me){
                                handleException(me);
                            }
                        }
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof BinaryAtomicModule_SLView){
                item = new AssignGradeForBinaryAtomicModulePRMTRBinaryAtomicModule_SLPRMTRBinaryGradeSUBTYPENamePRMTRStringPRMTRMenuItem();
                item.setText("Note hinzufuegen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceAssignGradeForBinaryAtomicModuleBinaryAtomicModule_SLBinaryGradeSUBTYPENameStringMssgWizard wizard = new AdminServiceAssignGradeForBinaryAtomicModuleBinaryAtomicModule_SLBinaryGradeSUBTYPENameStringMssgWizard("Note hinzufuegen");
                        wizard.setFirstArgument((BinaryAtomicModule_SLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof NotAtomicModule_PLView){
                item = new CreateUnitPRMTRNotAtomicModule_PLPRMTRStringPRMTRFractionPRMTRMenuItem();
                item.setText("Unit erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateUnitNotAtomicModule_PLStringFractionMssgWizard wizard = new AdminServiceCreateUnitNotAtomicModule_PLStringFractionMssgWizard("Unit erstellen");
                        wizard.setFirstArgument((NotAtomicModule_PLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof Unit_SLView){
                item = new AssignTrippleGradeToUnitPRMTRUnit_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem();
                item.setText("Note hinzufuegen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceAssignTrippleGradeToUnitUnit_SLTripleGradeSUBTYPENameStringMssgWizard wizard = new AdminServiceAssignTrippleGradeToUnitUnit_SLTripleGradeSUBTYPENameStringMssgWizard("Note hinzufuegen");
                        wizard.setFirstArgument((Unit_SLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof StudentWrapperView){
                item = new ResetPasswordPRMTRStudentWrapperPRMTRMenuItem();
                item.setText("Passwort zuruecksetzen");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        Alert confirm = new Alert(AlertType.CONFIRMATION);
                        confirm.setTitle(GUIConstants.ConfirmButtonText);
                        confirm.setHeaderText(null);
                        confirm.setContentText("Passwort zuruecksetzen" + GUIConstants.ConfirmQuestionMark);
                        confirm.setX( getPointForView().getX() );
                        confirm.setY( getPointForView().getY() );
                        Optional<ButtonType> buttonResult = confirm.showAndWait();
                        if (buttonResult.get() == ButtonType.OK) {
                            try {
                                getConnection().resetPassword((StudentWrapperView)selected);
                                getConnection().setEagerRefresh();
                                
                            }catch(ModelException me){
                                handleException(me);
                            }
                        }
                    }
                });
                result.getItems().add(item);
                if (filter_removeStudent((StudentWrapperView) selected)) {
                    item = new RemoveStudentPRMTRStudentWrapperPRMTRMenuItem();
                    item.setText("Student loeschen");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            Alert confirm = new Alert(AlertType.CONFIRMATION);
                            confirm.setTitle(GUIConstants.ConfirmButtonText);
                            confirm.setHeaderText(null);
                            confirm.setContentText("Student loeschen" + GUIConstants.ConfirmQuestionMark);
                            confirm.setX( getPointForView().getX() );
                            confirm.setY( getPointForView().getY() );
                            Optional<ButtonType> buttonResult = confirm.showAndWait();
                            if (buttonResult.get() == ButtonType.OK) {
                                try {
                                    getConnection().removeStudent((StudentWrapperView)selected);
                                    getConnection().setEagerRefresh();
                                    
                                }catch(ModelException me){
                                    handleException(me);
                                }catch (StateException userException){
                                    ReturnValueView view = new ReturnValueView(userException.getMessage(), new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9));
                                    view.setX( getPointForView().getX() );
                                    view.setY( getPointForView().getY() );
                                    view.showAndWait();
                                    getConnection().setEagerRefresh();
                                }
                            }
                        }
                    });
                    result.getItems().add(item);
                }
            }
            if (selected instanceof Unit_PLView){
                item = new RemoveUnitPRMTRUnit_PLPRMTRMenuItem();
                item.setText("entfernen");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        Alert confirm = new Alert(AlertType.CONFIRMATION);
                        confirm.setTitle(GUIConstants.ConfirmButtonText);
                        confirm.setHeaderText(null);
                        confirm.setContentText("entfernen" + GUIConstants.ConfirmQuestionMark);
                        confirm.setX( getPointForView().getX() );
                        confirm.setY( getPointForView().getY() );
                        Optional<ButtonType> buttonResult = confirm.showAndWait();
                        if (buttonResult.get() == ButtonType.OK) {
                            try {
                                getConnection().removeUnit((Unit_PLView)selected);
                                getConnection().setEagerRefresh();
                                
                            }catch(ModelException me){
                                handleException(me);
                            }catch (StateException userException){
                                ReturnValueView view = new ReturnValueView(userException.getMessage(), new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9));
                                view.setX( getPointForView().getX() );
                                view.setY( getPointForView().getY() );
                                view.showAndWait();
                                getConnection().setEagerRefresh();
                            }
                        }
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof TripleAtomicModule_SLView){
                item = new AssignGradeForTripleAtomicModulePRMTRTripleAtomicModule_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem();
                item.setText("Note hinzufuegen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceAssignGradeForTripleAtomicModuleTripleAtomicModule_SLTripleGradeSUBTYPENameStringMssgWizard wizard = new AdminServiceAssignGradeForTripleAtomicModuleTripleAtomicModule_SLTripleGradeSUBTYPENameStringMssgWizard("Note hinzufuegen");
                        wizard.setFirstArgument((TripleAtomicModule_SLView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            if (selected instanceof StudentGroupView){
                if (filter_createStudent((StudentGroupView) selected)) {
                    item = new CreateStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTRMenuItem();
                    item.setText("Student erstellen ... ");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            final AdminServiceCreateStudentStudentGroupStringStringDateMssgWizard wizard = new AdminServiceCreateStudentStudentGroupStringStringDateMssgWizard("Student erstellen");
                            wizard.setFirstArgument((StudentGroupView)selected);
                            wizard.setWidth(getNavigationPanel().getWidth());
                            wizard.setX( getPointForView().getX());
                            wizard.setY( getPointForView().getY());
                            wizard.showAndWait();
                        }
                    });
                    result.getItems().add(item);
                }
                if (filter_addStudentsToGroup((StudentGroupView) selected)) {
                    item = new AddStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRMenuItem();
                    item.setText("Studenten zu Studiengruppe hinzufuegen ... ");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            final AdminServiceAddStudentsToGroupStudentGroupStudentWrapperLSTMssgWizard wizard = new AdminServiceAddStudentsToGroupStudentGroupStudentWrapperLSTMssgWizard("Studenten zu Studiengruppe hinzufuegen");
                            wizard.setFirstArgument((StudentGroupView)selected);
                            wizard.setWidth(getNavigationPanel().getWidth());
                            wizard.setX( getPointForView().getX());
                            wizard.setY( getPointForView().getY());
                            wizard.showAndWait();
                        }
                    });
                    result.getItems().add(item);
                }
                if (filter_makeGroupActive((StudentGroupView) selected)) {
                    item = new MakeGroupActivePRMTRStudentGroupPRMTRMenuItem();
                    item.setText("aktiv schalten");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            Alert confirm = new Alert(AlertType.CONFIRMATION);
                            confirm.setTitle(GUIConstants.ConfirmButtonText);
                            confirm.setHeaderText(null);
                            confirm.setContentText("aktiv schalten" + GUIConstants.ConfirmQuestionMark);
                            confirm.setX( getPointForView().getX() );
                            confirm.setY( getPointForView().getY() );
                            Optional<ButtonType> buttonResult = confirm.showAndWait();
                            if (buttonResult.get() == ButtonType.OK) {
                                try {
                                    getConnection().makeGroupActive((StudentGroupView)selected);
                                    getConnection().setEagerRefresh();
                                    
                                }catch(ModelException me){
                                    handleException(me);
                                }catch (StateException userException){
                                    ReturnValueView view = new ReturnValueView(userException.getMessage(), new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9));
                                    view.setX( getPointForView().getX() );
                                    view.setY( getPointForView().getY() );
                                    view.showAndWait();
                                    getConnection().setEagerRefresh();
                                }
                            }
                        }
                    });
                    result.getItems().add(item);
                }
                if (filter_finishesStudying((StudentGroupView) selected)) {
                    item = new FinishesStudyingPRMTRStudentGroupPRMTRMenuItem();
                    item.setText("auf fertig setzen");
                    item.setOnAction(new EventHandler<ActionEvent>(){
                        public void handle(javafx.event.ActionEvent e) {
                            Alert confirm = new Alert(AlertType.CONFIRMATION);
                            confirm.setTitle(GUIConstants.ConfirmButtonText);
                            confirm.setHeaderText(null);
                            confirm.setContentText("auf fertig setzen" + GUIConstants.ConfirmQuestionMark);
                            confirm.setX( getPointForView().getX() );
                            confirm.setY( getPointForView().getY() );
                            Optional<ButtonType> buttonResult = confirm.showAndWait();
                            if (buttonResult.get() == ButtonType.OK) {
                                try {
                                    getConnection().finishesStudying((StudentGroupView)selected);
                                    getConnection().setEagerRefresh();
                                    
                                }catch(ModelException me){
                                    handleException(me);
                                }catch (StateException userException){
                                    ReturnValueView view = new ReturnValueView(userException.getMessage(), new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9));
                                    view.setX( getPointForView().getX() );
                                    view.setY( getPointForView().getY() );
                                    view.showAndWait();
                                    getConnection().setEagerRefresh();
                                }
                            }
                        }
                    });
                    result.getItems().add(item);
                }
            }
            if (selected instanceof ProgramManagerView){
                item = new CreateStudyProgramPRMTRProgramManagerPRMTRStringPRMTRMenuItem();
                item.setText("Studienprogramm erstellen ... ");
                item.setOnAction(new EventHandler<ActionEvent>(){
                    public void handle(javafx.event.ActionEvent e) {
                        final AdminServiceCreateStudyProgramProgramManagerStringMssgWizard wizard = new AdminServiceCreateStudyProgramProgramManagerStringMssgWizard("Studienprogramm erstellen");
                        wizard.setFirstArgument((ProgramManagerView)selected);
                        wizard.setWidth(getNavigationPanel().getWidth());
                        wizard.setX( getPointForView().getX());
                        wizard.setY( getPointForView().getY());
                        wizard.showAndWait();
                    }
                });
                result.getItems().add(item);
            }
            
        }
        this.addNotGeneratedItems(result,selected);
        return result;
    }
    private String preCalculatedFilters = "";
    private String getPreCalculatedFilters() {
        return this.preCalculatedFilters;
    }
    private void setPreCalculatedFilters(String switchOff) {
        this.preCalculatedFilters = switchOff;
    }
    private boolean filter_changeCreditPoints(CreditPointHavingElement_PLView argument){
        return this.getPreCalculatedFilters().contains("+++changeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTR");
    }
    private boolean filter_transferCP(NotAtomicModule_GLView argument){
        return this.getPreCalculatedFilters().contains("+++transferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTR");
    }
    private boolean filter_createStudent(StudentGroupView argument){
        return this.getPreCalculatedFilters().contains("+++createStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTR");
    }
    private boolean filter_removeStudent(StudentWrapperView argument){
        return this.getPreCalculatedFilters().contains("+++removeStudentPRMTRStudentWrapperPRMTR");
    }
    private boolean filter_addStudentsToGroup(StudentGroupView argument){
        return this.getPreCalculatedFilters().contains("+++addStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTR");
    }
    private boolean filter_makeGroupActive(StudentGroupView argument){
        return this.getPreCalculatedFilters().contains("+++makeGroupActivePRMTRStudentGroupPRMTR");
    }
    private boolean filter_finishesStudying(StudentGroupView argument){
        return this.getPreCalculatedFilters().contains("+++finishesStudyingPRMTRStudentGroupPRMTR");
    }
    
	class AdminServiceAddModulesOrModuleGroupsModuleGroupOrStudyProgram_PLModuleOrModuleGroup_PLLSTMssgWizard extends Wizard {

		protected AdminServiceAddModulesOrModuleGroupsModuleGroupOrStudyProgram_PLModuleOrModuleGroup_PLLSTMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new AddModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceAddModulesOrModuleGroupsModuleGroupOrStudyProgram_PLModuleOrModuleGroup_PLLSTMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().addModulesOrModuleGroups(firstArgument, (java.util.Vector<ModuleOrModuleGroup_PLView>)((ObjectCollectionSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(CycleException e) {
				getStatusBar().setText(e.getMessage());
			}
			catch(AlreadyInListException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			try{
				final ObjectCollectionSelectionPanel panel1 = new ObjectCollectionSelectionPanel("containees", "view.ModuleOrModuleGroup_PLView", null, this, getMultiSelectionFor("addModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRcontainees"));
				getParametersPanel().getChildren().add(panel1);
				panel1.setBrowserRoot((ViewRoot)getConnection().containees_Path_In_AddModulesOrModuleGroups());
			}catch(ModelException me){;
				handleException(me);
				close();
				return;
			 }catch(UserException ue){;
				handleUserException(ue);
				close();
				return;
			 }		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private ModuleGroupOrStudyProgram_PLView firstArgument; 
	
		public void setFirstArgument(ModuleGroupOrStudyProgram_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset((java.util.Vector<?>)firstArgument.getContainees());
				if (!selectionPanel0.check()) selectionPanel0.preset(new java.util.Vector<Object>());
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceAddStudentsToGroupStudentGroupStudentWrapperLSTMssgWizard extends Wizard {

		protected AdminServiceAddStudentsToGroupStudentGroupStudentWrapperLSTMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new AddStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceAddStudentsToGroupStudentGroupStudentWrapperLSTMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().addStudentsToGroup(firstArgument, (java.util.Vector<StudentWrapperView>)((ObjectCollectionSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(AlreadyInListException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			try{
				final ObjectCollectionSelectionPanel panel2 = new ObjectCollectionSelectionPanel("Studenten", "view.StudentWrapperView", null, this, getMultiSelectionFor("addStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRstudents"));
				getParametersPanel().getChildren().add(panel2);
				panel2.setBrowserRoot((ViewRoot)getConnection().students_Path_In_AddStudentsToGroup());
			}catch(ModelException me){;
				handleException(me);
				close();
				return;
			 }catch(UserException ue){;
				handleUserException(ue);
				close();
				return;
			 }		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private StudentGroupView firstArgument; 
	
		public void setFirstArgument(StudentGroupView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset((java.util.Vector<?>)firstArgument.getStudents());
				if (!selectionPanel0.check()) selectionPanel0.preset(new java.util.Vector<Object>());
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceAssignGradeForBinaryAtomicModuleBinaryAtomicModule_SLBinaryGradeSUBTYPENameStringMssgWizard extends Wizard {

		protected AdminServiceAssignGradeForBinaryAtomicModuleBinaryAtomicModule_SLBinaryGradeSUBTYPENameStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new AssignGradeForBinaryAtomicModulePRMTRBinaryAtomicModule_SLPRMTRBinaryGradeSUBTYPENamePRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceAssignGradeForBinaryAtomicModuleBinaryAtomicModule_SLBinaryGradeSUBTYPENameStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().assignGradeForBinaryAtomicModule(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((StringSelectionPanel)getParametersPanel().getChildren().get(1)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new RegExprSelectionPanel("Note", this, common.RegularExpressionManager.binaryGradeSUBTYPEName.getRegExpr()));
			getParametersPanel().getChildren().add(new StringSelectionPanel("Kommentar", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private BinaryAtomicModule_SLView firstArgument; 
	
		public void setFirstArgument(BinaryAtomicModule_SLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	class AdminServiceAssignGradeForTripleAtomicModuleTripleAtomicModule_SLTripleGradeSUBTYPENameStringMssgWizard extends Wizard {

		protected AdminServiceAssignGradeForTripleAtomicModuleTripleAtomicModule_SLTripleGradeSUBTYPENameStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new AssignGradeForTripleAtomicModulePRMTRTripleAtomicModule_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceAssignGradeForTripleAtomicModuleTripleAtomicModule_SLTripleGradeSUBTYPENameStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().assignGradeForTripleAtomicModule(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((StringSelectionPanel)getParametersPanel().getChildren().get(1)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new RegExprSelectionPanel("Note", this, common.RegularExpressionManager.tripleGradeSUBTYPEName.getRegExpr()));
			getParametersPanel().getChildren().add(new StringSelectionPanel("Kommentar", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private TripleAtomicModule_SLView firstArgument; 
	
		public void setFirstArgument(TripleAtomicModule_SLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	class AdminServiceAssignTrippleGradeToUnitUnit_SLTripleGradeSUBTYPENameStringMssgWizard extends Wizard {

		protected AdminServiceAssignTrippleGradeToUnitUnit_SLTripleGradeSUBTYPENameStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new AssignTrippleGradeToUnitPRMTRUnit_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceAssignTrippleGradeToUnitUnit_SLTripleGradeSUBTYPENameStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().assignTrippleGradeToUnit(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((StringSelectionPanel)getParametersPanel().getChildren().get(1)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new RegExprSelectionPanel("Note", this, common.RegularExpressionManager.tripleGradeSUBTYPEName.getRegExpr()));
			getParametersPanel().getChildren().add(new StringSelectionPanel("Kommentar", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private Unit_SLView firstArgument; 
	
		public void setFirstArgument(Unit_SLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	class AdminServiceChangeCreditPointsCreditPointHavingElement_PLFractionMssgWizard extends Wizard {

		protected AdminServiceChangeCreditPointsCreditPointHavingElement_PLFractionMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new ChangeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceChangeCreditPointsCreditPointHavingElement_PLFractionMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().changeCreditPoints(firstArgument, ((FractionSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new FractionSelectionPanel("Neue Leistungspunkte", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private CreditPointHavingElement_PLView firstArgument; 
	
		public void setFirstArgument(CreditPointHavingElement_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	class AdminServiceCreateAtomicModuleModuleGroupOrStudyProgram_PLStringFractionGradeFactorySUBTYPENameMssgWizard extends Wizard {

		protected AdminServiceCreateAtomicModuleModuleGroupOrStudyProgram_PLStringFractionGradeFactorySUBTYPENameMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateAtomicModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRFractionPRMTRGradeFactorySUBTYPENamePRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateAtomicModuleModuleGroupOrStudyProgram_PLStringFractionGradeFactorySUBTYPENameMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createAtomicModule(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((FractionSelectionPanel)getParametersPanel().getChildren().get(1)).getResult(),
									((StringSelectionPanel)getParametersPanel().getChildren().get(2)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(NameAlreadyExistsException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));
			getParametersPanel().getChildren().add(new FractionSelectionPanel("Leistungspunkte", this));
			getParametersPanel().getChildren().add(new RegExprSelectionPanel("Notenschema", this, common.RegularExpressionManager.gradeFactorySUBTYPEName.getRegExpr()));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private ModuleGroupOrStudyProgram_PLView firstArgument; 
	
		public void setFirstArgument(ModuleGroupOrStudyProgram_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset(firstArgument.getName());
				if (!selectionPanel0.check()) selectionPanel0.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceCreateModuleGroupModuleGroupOrStudyProgram_PLStringMssgWizard extends Wizard {

		protected AdminServiceCreateModuleGroupModuleGroupOrStudyProgram_PLStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateModuleGroupPRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateModuleGroupModuleGroupOrStudyProgram_PLStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createModuleGroup(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(NameAlreadyExistsException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private ModuleGroupOrStudyProgram_PLView firstArgument; 
	
		public void setFirstArgument(ModuleGroupOrStudyProgram_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset(firstArgument.getName());
				if (!selectionPanel0.check()) selectionPanel0.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceCreateModuleModuleGroupOrStudyProgram_PLStringMssgWizard extends Wizard {

		protected AdminServiceCreateModuleModuleGroupOrStudyProgram_PLStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateModuleModuleGroupOrStudyProgram_PLStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createModule(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(NameAlreadyExistsException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private ModuleGroupOrStudyProgram_PLView firstArgument; 
	
		public void setFirstArgument(ModuleGroupOrStudyProgram_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset(firstArgument.getName());
				if (!selectionPanel0.check()) selectionPanel0.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceCreateStudentGroupStudyProgram_PLStringMssgWizard extends Wizard {

		protected AdminServiceCreateStudentGroupStudyProgram_PLStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateStudentGroupPRMTRStudyProgram_PLPRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateStudentGroupStudyProgram_PLStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createStudentGroup(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(StateException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private StudyProgram_PLView firstArgument; 
	
		public void setFirstArgument(StudyProgram_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset(firstArgument.getName());
				if (!selectionPanel0.check()) selectionPanel0.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceCreateStudentStudentGroupStringStringDateMssgWizard extends Wizard {

		protected AdminServiceCreateStudentStudentGroupStringStringDateMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateStudentStudentGroupStringStringDateMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createStudent(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((StringSelectionPanel)getParametersPanel().getChildren().get(1)).getResult(),
									((DateSelectionPanel)getParametersPanel().getChildren().get(2)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(StateException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Vorname", this));
			getParametersPanel().getChildren().add(new StringSelectionPanel("Nachname", this));
			getParametersPanel().getChildren().add(new DateSelectionPanel("Geburtsdatum", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private StudentGroupView firstArgument; 
	
		public void setFirstArgument(StudentGroupView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel1 = (SelectionPanel)getParametersPanel().getChildren().get(1);
				selectionPanel1.preset(firstArgument.getName());
				if (!selectionPanel1.check()) selectionPanel1.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceCreateStudyProgramProgramManagerStringMssgWizard extends Wizard {

		protected AdminServiceCreateStudyProgramProgramManagerStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateStudyProgramPRMTRProgramManagerPRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateStudyProgramProgramManagerStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createStudyProgram(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(NameAlreadyExistsException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private ProgramManagerView firstArgument; 
	
		public void setFirstArgument(ProgramManagerView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	class AdminServiceCreateUnitNotAtomicModule_PLStringFractionMssgWizard extends Wizard {

		protected AdminServiceCreateUnitNotAtomicModule_PLStringFractionMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new CreateUnitPRMTRNotAtomicModule_PLPRMTRStringPRMTRFractionPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceCreateUnitNotAtomicModule_PLStringFractionMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().createUnit(firstArgument, ((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									((FractionSelectionPanel)getParametersPanel().getChildren().get(1)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(NameAlreadyExistsException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Name", this));
			getParametersPanel().getChildren().add(new FractionSelectionPanel("Leistungspunkte", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
		private NotAtomicModule_PLView firstArgument; 
	
		public void setFirstArgument(NotAtomicModule_PLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			try{
				final SelectionPanel selectionPanel0 = (SelectionPanel)getParametersPanel().getChildren().get(0);
				selectionPanel0.preset(firstArgument.getName());
				if (!selectionPanel0.check()) selectionPanel0.preset("");
			}catch(ModelException me){
				 handleException(me);
			}
			this.check();
		}
		
		
	}

	class AdminServiceFindStudentStringMssgWizard extends Wizard {

		protected AdminServiceFindStudentStringMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new FindStudentPRMTRStringPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceFindStudentStringMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				java.util.Vector<?> result = getConnection().findStudent(((StringSelectionPanel)getParametersPanel().getChildren().get(0)).getResult());
				ReturnValueView view = new ReturnValueView(result, new javafx.geometry.Dimension2D(getNavigationPanel().getWidth()*8/9,getNavigationPanel().getHeight()*8/9), AdminServiceClientView.this);
				view.setX( this.getX() );
				view.setY( this.getY() );
				getConnection().setEagerRefresh();
				this.close();
				Platform.runLater(new Runnable(){
					public void run(){
						view.showAndWait();
					}
				});	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			getParametersPanel().getChildren().add(new StringSelectionPanel("Kriterium", this));		
		}	
		protected void handleDependencies(int i) {
		}
		
		
	}

	class AdminServiceTransferCPNotAtomicModule_GLUnit_GLUnit_GLFractionMssgWizard extends Wizard {

		protected AdminServiceTransferCPNotAtomicModule_GLUnit_GLUnit_GLFractionMssgWizard(String operationName){
			super(AdminServiceClientView.this);
			getOkButton().setText(operationName);
			getOkButton().setGraphic(new TransferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTRMenuItem ().getGraphic());
		}
		protected void initialize(){
			this.helpFileName = "AdminServiceTransferCPNotAtomicModule_GLUnit_GLUnit_GLFractionMssgWizard.help";
			super.initialize();		
		}
				
		protected void perform() {
			try {
				getConnection().transferCP(firstArgument, (Unit_GLView)((ObjectSelectionPanel)getParametersPanel().getChildren().get(0)).getResult(),
									(Unit_GLView)((ObjectSelectionPanel)getParametersPanel().getChildren().get(1)).getResult(),
									((FractionSelectionPanel)getParametersPanel().getChildren().get(2)).getResult());
				getConnection().setEagerRefresh();
				this.close();	
			} catch(ModelException me){
				handleException(me);
				this.close();
			}
			catch(InvalidParameterException e) {
				getStatusBar().setText(e.getMessage());
			}
			
		}
		protected String checkCompleteParameterSet(){
			return null;
		}
		protected boolean isModifying () {
			return false;
		}
		protected void addParameters(){
			final ObjectSelectionPanel panel3 = new ObjectSelectionPanel("Von Unit", "view.Unit_GLView", null, this)
											{protected ViewRoot getBrowserRoot(){
												{try{
													return new ListRoot(getConnection().fromUnit_Path_In_TransferCP((NotAtomicModule_GLView)this.navigationRoot));
												}catch(ModelException me){
													return (ViewRoot) this.navigationRoot;
												}catch(UserException ue){
													return (ViewRoot) this.navigationRoot;
											}}}};
			getParametersPanel().getChildren().add(panel3);
			final ObjectSelectionPanel panel4 = new ObjectSelectionPanel("Zu Unit", "view.Unit_GLView", null, this)
											{protected ViewRoot getBrowserRoot(){
												{try{
													return new ListRoot(getConnection().toUnit_Path_In_TransferCP((NotAtomicModule_GLView)this.navigationRoot));
												}catch(ModelException me){
													return (ViewRoot) this.navigationRoot;
												}catch(UserException ue){
													return (ViewRoot) this.navigationRoot;
											}}}};
			getParametersPanel().getChildren().add(panel4);
			getParametersPanel().getChildren().add(new FractionSelectionPanel("Anzahl zu uebertragende Leistungspunkte", this));		
		}	
		protected void handleDependencies(int i) {
			if(i == 0){
				((ObjectSelectionPanel)getParametersPanel().getChildren().get(i)).setBrowserRoot((ViewRoot)firstArgument);
			}
			if(i == 1){
				((ObjectSelectionPanel)getParametersPanel().getChildren().get(i)).setBrowserRoot((ViewRoot)firstArgument);
			}
		}
		
		
		private NotAtomicModule_GLView firstArgument; 
	
		public void setFirstArgument(NotAtomicModule_GLView firstArgument){
			this.firstArgument = firstArgument;
			this.setTitle(this.firstArgument.toString());
			this.check();
		}
		
		
	}

	/* Menu and wizard section end */

    private ImageView getIconForMenuItem(AdminServiceMenuItem menuItem) {
        String magnifyingGlassIcon = "/viewResources/MagnifyingGlassIcon.png";
        String plusIcon = "/viewResources/PlusIcon.png";
        String replaceIcon = "/viewResources/ReplaceIcon.png";
        String deleteIcon = "/viewResources/DeleteIcon.png";
        String assignIcon = "/viewResources/AssignIcon.png";
        String resetIcon = "/viewResources/ResetIcon.png";
        String createIcon = "/viewResources/CreateIcon.png";
        String checkmarkIcon = "/viewResources/CheckmarkIcon.png";
        String showPropertyIcon = "/viewResources/ShowPropertyIcon.png";

        return menuItem.accept(new MenuItemVisitor() {
            @Override
            public ImageView handle(ChangeCreditPointsPRMTRCreditPointHavingElement_PLPRMTRFractionPRMTRMenuItem menuItem) {
                return new ImageView(new Image(replaceIcon));
            }

            @Override
            public ImageView handle(TransferCPPRMTRNotAtomicModule_GLPRMTRUnit_GLPRMTRUnit_GLPRMTRFractionPRMTRMenuItem menuItem) {
                return new ImageView(new Image(replaceIcon));
            }

            @Override
            public ImageView handle(ClearErrorPRMTRErrorDisplayPRMTRMenuItem menuItem) {
                return new ImageView(new Image(deleteIcon));
            }

            @Override
            public ImageView handle(AddModulesOrModuleGroupsPRMTRModuleGroupOrStudyProgram_PLPRMTRModuleOrModuleGroup_PLLSTPRMTRMenuItem menuItem) {
                return new ImageView(new Image(plusIcon));
            }

            @Override
            public ImageView handle(CreateModuleGroupPRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(AssignGradeForBinaryAtomicModulePRMTRBinaryAtomicModule_SLPRMTRBinaryGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(assignIcon));
            }

            @Override
            public ImageView handle(AssignGradeForTripleAtomicModulePRMTRTripleAtomicModule_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(assignIcon));
            }

            @Override
            public ImageView handle(AssignTrippleGradeToUnitPRMTRUnit_SLPRMTRTripleGradeSUBTYPENamePRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(assignIcon));
            }

            @Override
            public ImageView handle(ShowHistoriePRMTRCreditPointHavingElement_SLPRMTRMenuItem menuItem) {
                return new ImageView(new Image(showPropertyIcon));
            }

            @Override
            public ImageView handle(ResetPasswordPRMTRStudentWrapperPRMTRMenuItem menuItem) {
                return new ImageView(new Image(resetIcon));
            }

            @Override
            public ImageView handle(CreateStudentPRMTRStudentGroupPRMTRStringPRMTRStringPRMTRDatePRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(RemoveStudentPRMTRStudentWrapperPRMTRMenuItem menuItem) {
                return new ImageView(new Image(deleteIcon));
            }

            @Override
            public ImageView handle(AddStudentsToGroupPRMTRStudentGroupPRMTRStudentWrapperLSTPRMTRMenuItem menuItem) {
                return new ImageView(new Image(plusIcon));
            }

            @Override
            public ImageView handle(CreateStudentGroupPRMTRStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(CreateStudyProgramPRMTRProgramManagerPRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(CreateUnitPRMTRNotAtomicModule_PLPRMTRStringPRMTRFractionPRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(MakeGroupActivePRMTRStudentGroupPRMTRMenuItem menuItem) {
                return new ImageView(new Image(checkmarkIcon));
            }

            @Override
            public ImageView handle(CreateAtomicModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRFractionPRMTRGradeFactorySUBTYPENamePRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(FinishesStudyingPRMTRStudentGroupPRMTRMenuItem menuItem) {
                return new ImageView(new Image(checkmarkIcon));
            }

            @Override
            public ImageView handle(RemoveUnitPRMTRUnit_PLPRMTRMenuItem menuItem) {
                return new ImageView(new Image(deleteIcon));
            }

            @Override
            public ImageView handle(CreateModulePRMTRModuleGroupOrStudyProgram_PLPRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(createIcon));
            }

            @Override
            public ImageView handle(FindStudentPRMTRStringPRMTRMenuItem menuItem) {
                return new ImageView(new Image(magnifyingGlassIcon));
            }
        });
    }

    private void addNotGeneratedItems(ContextMenu result, ViewRoot selected) {
        // TODO Add items to menue if you have not generated service calls!!!
    }

    protected boolean getMultiSelectionFor(String parameterInBrowser) {
        return false;
    }

    public int getPartsPerHour(String parameterInBrowser) {
        // divides 60 minutes into the returned number of parts
        return 59;
    }

}