package viewClient;

import common.Constants;
import javafx.scene.image.Image;

public class IconManager {

	private IconManager(){}

	public static Image getImage(int iconInfo) {
		switch (iconInfo) {
            case Constants.ModuleGroupIconNumber: return new Image("/viewResources/ModuleGroupIcon.png");
            case Constants.StudentIconNumber: return new Image("/viewResources/StudentIcon.png");
            case Constants.StudentGroupIconNumber: return new Image("/viewResources/StudentGroupIcon.png");
            case Constants.StudentGroup_FinishedIconNumber: return new Image("/viewResources/StudentGroupIcon_finished.png");
            case Constants.StudentGroup_NotYetStudiedIconNumber: return new Image("/viewResources/StudentGroupIcon_notYetStudied.png");
            case Constants.StudyProgrammIconNumber: return new Image("/viewResources/StudyProgrammIcon.png");
            case Constants.UnitIconNumber: return new Image("/viewResources/UnitIcon.png");
            case Constants.NotAtomicModuleIconNumber: return new Image("/viewResources/NotAtomicModuleIcon.png");
            case Constants.AtomicModuleIconNumber: return new Image("/viewResources/AtomicModuleIcon.png");
            case Constants.CreditPointIconNumber: return new Image("/viewResources/CreditPointIcon.png");
            case Constants.ManagerIconNumber: return new Image("/viewResources/ManagerIcon.png");
            case Constants.AtomicModuleEditableIconNumber: return new Image("/viewResources/AtomicModuleIcon_editable.png");
            case Constants.ModuleGroupEditableIconNumber: return new Image("/viewResources/ModuleGroupIcon_editable.png");
            case Constants.NotAtomicModuleEditableIconNumber: return new Image("/viewResources/NotAtomicModuleIcon_editable.png");
            case Constants.StudyProgrammEditableIconNumber: return new Image("/viewResources/StudyProgrammIcon_editable.png");
            case Constants.UnitEditableIconNumber: return new Image("/viewResources/UnitIcon_editable.png");
            case Constants.ErrorDisplayIconNumber: return new Image("/viewResources/ExclamationMarkIcon.png");
            case Constants.GradeHistoryIconNumber: return new Image("/viewResources/GradeHistoryIcon.png");
            case Constants.GradeHistoryElementIconNumber: return new Image("/viewResources/GradeHistoryElementIcon.png");
            default:
			return new Image("/viewResources/default.gif");
		}
	}
}
