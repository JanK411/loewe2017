package viewClient;

import view.Anything;
import view.ModelException;
import view.objects.ViewProxi;
import view.visitor.AnythingVisitor;

import common.Fraction;
import common.DateAndTime;

import rGType.CharacterType;
import expressions.RegularExpression;
import expressions.RegularExpressionHandler;
import view.FX.RegExprPanel2;

import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Hashtable;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.ToolBar;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;


abstract public class DetailPanel extends BorderPane {

	public static void setButtonToNeutral( Button button) {
		button.setBackground( new Background( new BackgroundFill( GUIConstants.Yellow, null, Insets.EMPTY)));
		button.setTextFill( GUIConstants.NeutralForeground);
		button.setText( GUIConstants.NeutralText);
		// 	button.setIcon(GUIConstants.NeutralIcon);
		button.setDisable(true);
	}

	public static void setButtonToOK( Button button) {
		button.setBackground( new Background( new BackgroundFill( GUIConstants.Green, null, Insets.EMPTY)));
		button.setTextFill( GUIConstants.OKForeground);
		button.setText( GUIConstants.OKText);
		// button.setIcon(GUIConstants.OKIcon);
		button.setDisable(false);
	}

	public static void setButtonToNotOk( Button button) {
		button.setBackground( new Background( new BackgroundFill( GUIConstants.Red, null, Insets.EMPTY)));
		button.setTextFill( GUIConstants.NotOKForeground);
		button.setText( GUIConstants.NotOKText);
		// button.setIcon(GUIConstants.NotOKIcon);
		button.setDisable(true);
	}

	protected Anything anything;
	private ExceptionAndEventHandler exceptionAndEventHandler;

	protected DetailPanel(ExceptionAndEventHandler exceptionHandler) {
		super();
		this.exceptionAndEventHandler = exceptionHandler;
	}

	abstract public void setAnything(Anything anything);

	protected Anything getAnything() {
		return this.anything;
	}

	protected ExceptionAndEventHandler getExceptionAndEventhandler() {
		return this.exceptionAndEventHandler;
	}
}

abstract class BaseTypePanel extends HBox {

	protected static final int Neutral = 0;
	protected static final int OK 	   = 1;
	protected static final int NotOK   = 2;

	private DefaultDetailPanel master;
	protected Updater updater;

	protected BaseTypePanel( DefaultDetailPanel master, String name, String value) {
        super(10);
		this.master = master;
		this.setAlignment( Pos.BASELINE_LEFT );
		this.setPadding( new Insets( 10,10,0,10 ));
		this.prefWidthProperty().bind( master.widthProperty());        
		this.getChildren().add( this.getUpdateButton() );
		
		HBox nameBox = new HBox();
		nameBox.setAlignment(Pos.BASELINE_LEFT);
		this.getNameLabel().setText( name );
		nameBox.getChildren().add( this.getNameLabel());
		nameBox.getChildren().add( new Label(":"));
		nameBox.setMinWidth(USE_PREF_SIZE);
		nameBox.setMaxWidth(USE_PREF_SIZE);
		this.getChildren().add( nameBox );

		this.getChildren().add( (Node) this.getConcreteValueComponent() );

		this.getChildren().add( this.getUpdateMarker() );
		this.getValueComponent().setComponentValue( value);
	}

	abstract protected ValueComponent getValueComponent();
	
	protected ValueComponent valueComponent = null;
	protected ValueComponent getConcreteValueComponent(){
		if( this.valueComponent == null){
			this.valueComponent = this.getValueComponent();
			HBox.setHgrow( (Node) this.valueComponent, Priority.ALWAYS);
		}
		return this.valueComponent;
	};
	
	private Label updateMarker = null;
	protected Label getUpdateMarker() {
		if( this.updateMarker == null) {
			this.updateMarker = new Label( GUIConstants.UpdateMarker );
			this.updateMarker.setVisible(false);
		}
		return this.updateMarker;
	}

	private Button updateButton;
	private Button getUpdateButton() {
		if( this.updateButton == null) {
			this.updateButton = new Button("    ");
			this.setStatus( Neutral );
			this.updateButton.setMinWidth(USE_PREF_SIZE);
			this.updateButton.setMaxWidth(USE_PREF_SIZE);
			this.updateButton.setOnAction(new EventHandler<ActionEvent>() {
				public void handle( ActionEvent event) {
					if (isUpdatable()) update();
				}
			});
		}
		return this.updateButton;
	}

	public void setOK(boolean ok) {
		if (ok) {
			this.setStatus(OK);
		} else {
			this.setStatus(NotOK);
		}
	}

	private int status = BaseTypePanel.Neutral;
	protected void setStatus(int status) {
		this.status = status;
		if( this.status == Neutral)
			DetailPanel.setButtonToNeutral(this.getUpdateButton());
		if( this.status == OK)
			DetailPanel.setButtonToOK(this.getUpdateButton());
		if( this.status == NotOK)
			DetailPanel.setButtonToNotOk(this.getUpdateButton());
		this.master.check();
	}

	private Label nameLabel;
	protected Label getNameLabel() {
		if( this.nameLabel == null) {
			this.nameLabel = new Label();
		}
		return this.nameLabel;
	}

	protected DefaultDetailPanel getMaster() {
		return this.master;
	}

	protected void registerUpdater( Updater updater) {
		this.updater = updater;
		this.setUpdatable();
		this.getValueComponent().setComponentValue( updater.format(this.getValueComponent().getComponentValue()));
	}

	protected void setUpdatable() {
		this.getUpdateMarker().setVisible(true);
		this.getValueComponent().setComponentEditable();
	}

	public boolean isUpdatable() {
		return this.updater != null;
	}

	protected boolean isReadyForUpdate() {
		return this.isUpdatable() && !this.getUpdateButton().isDisabled();
	}

	public void updateIfReady() {
		if (this.isReadyForUpdate())
			this.update();
	}

	private void update() {
		try {
			this.updater.update(this.getCurrentValue());
			this.setStatus( Neutral);
		} catch (ModelException ex) {
			this.getValueComponent().setComponentValue( ex.getMessage());
			this.setStatus( NotOK);
		}
	}
	protected String getCurrentValue(){
		return this.getValueComponent().getComponentValue();
	}
	public int getStatus() {
		return this.status;
	}
}

interface ValueComponent{
	String getComponentValue();
	void setComponentValue(String value);
	void setComponentEditable();	
}

abstract class BaseTypePanelWithTextField extends BaseTypePanel{

	protected BaseTypePanelWithTextField(DefaultDetailPanel master, String name, String value) {
		super(master, name, value);
	}
	
	protected ValueTextField getValueTextField() {
		ValueTextField valueTextField = new ValueTextField();
		valueTextField.setEditable(false);
		valueTextField.setOnKeyReleased(new EventHandler<KeyEvent>() {
			public void handle(KeyEvent event) {
				if (isUpdatable()) {
					try {
						setOK(updater.check(getValue()));
					} catch (ModelException me) {
						getValueComponent().setText(me.getMessage());
					}
				}
			}
		});
		return valueTextField;
	}

	class ValueTextField extends TextField implements ValueComponent{
		public ValueTextField() {
			super();
		}

		@Override
		public String getComponentValue() {
			return this.getText();
		}

		@Override
		public void setComponentValue(String value) {
			this.setText( value);
			this.home();
		}

		@Override
		public void setComponentEditable() {
			this.setEditable(true);
		}
	}
	
	protected ValueTextField getValueComponent(){
		if( this.valueComponent == null){
			return this.getValueTextField();
		}
		return (ValueTextField) this.valueComponent;
	}
	
	public String getValue() {
		return this.getValueComponent().getComponentValue();
	}

	protected void setValue( String value ) {
		this.getValueComponent().setComponentValue(value);
	}
	
	protected void setEditable(){		
		this.getValueComponent().setComponentEditable();
	}	
}

class IntegerPanel extends BaseTypePanelWithTextField {
	protected IntegerPanel(DefaultDetailPanel master, String name, long value) {
		super(master, name, Long.toString(value));
	}
}

class FractionPanel extends BaseTypePanelWithTextField {
	protected FractionPanel(DefaultDetailPanel master, String name, Fraction value) {
		super(master, name, value.toString());
	}
}

class StringPanel extends BaseTypePanelWithTextField {
	protected StringPanel(DefaultDetailPanel master, String name, String value) {
		super(master, name, value);
	}
}

class RegularExpressionPanel extends BaseTypePanelWithTextField {

	private RegularExpressionHandler handler;

	protected RegularExpressionPanel(DefaultDetailPanel master, String name, String value, RegularExpressionHandler handler) {
		super(master, name, value);
		this.handler = handler;
		this.getValueComponent().setOnMouseClicked(new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				if (getValueComponent().isEditable() ){
					if(e.getClickCount() == 2 || e.isAltDown()) getMaster().switchToRegExprView(RegularExpressionPanel.this);
				}
			}
		});
		this.getValueComponent().setTooltip( new Tooltip( GUIConstants.TextFieldToolTipText));
	}

	protected boolean check() {
		return this.handler.check(new rGType.CharacterValue(this.getValue()));
	}

	public RegularExpression<Character, rGType.CharacterType> getExpression() {
		return handler.getRegExpr();
	}
}

class TextPanel extends BaseTypePanelWithTextField {
	private String value;

	protected TextPanel(DefaultDetailPanel master, String name, String value) {
		super(master, name, value);
		this.value = value;
		this.adjustTextField();
		// open text view to see the whole text, independent if text editable
		this.getValueComponent().setOnMouseClicked( new EventHandler<MouseEvent>() {
			public void handle(MouseEvent e) {
				if (e.getClickCount() == 2 || e.isAltDown()) getMaster().switchToTextView(TextPanel.this);
			}
		});
		this.getValueComponent().setTooltip( new Tooltip( GUIConstants.TextFieldToolTipText));
	}

	public String getValue() {
		return this.value;
	}

	protected void setValue(String value) {
		if (this.value != null && this.value.equals(value)) return;
		this.value = value;
		this.adjustTextField();
		if( this.isUpdatable()) {
			try {
				this.setOK( this.updater.check(getValue()));
			} catch (ModelException me) {
				this.setOK(false);
				this.getValueComponent().setText(me.getMessage());
			}
		}
	}
	private void adjustTextField() {
		boolean tooLong = this.value.length() > GUIConstants.TextPreviewLength;
		this.getValueComponent().setComponentValue(this.value.substring(0, tooLong ? GUIConstants.TextPreviewLength : this.value.length()) + (tooLong ? "....." : ""));
	}
	
	protected void setUpdatable() {
		this.getUpdateMarker().setVisible(true);
	}
	protected String getCurrentValue(){
		return this.getValue();
	}
	
}


class DatePanel extends BaseTypePanel {
	protected DatePanel(DefaultDetailPanel master, String name, java.util.Date value) {
		super( master, name, DateAndTime.toString(value, false));
	}
	
	class ValueDatePicker extends HBox implements ValueComponent{
		ValueDatePicker(){
			super(10);
			this.getChildren().add(this.getDatePicker());
			this.getChildren().add(this.getSpaceHolder());
			this.setAlignment(Pos.BASELINE_LEFT);
			HBox.setHgrow(this.getDatePicker(), Priority.SOMETIMES);
			HBox.setHgrow(this.getSpaceHolder(), Priority.ALWAYS);
		}
		private Label getSpaceHolder() {
			return new Label();
		}
		private DatePicker datePicker;
		private DatePicker getDatePicker(){
			if( this.datePicker == null){
				this.date = LocalDate.now();
				this.result = java.util.Date.from(this.date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());		
				this.datePicker = new DatePicker(); 
				this.datePicker.getEditor().setText(DateAndTime.toString(this.result, false));
				this.setMinWidth(Double.MIN_VALUE);
				this.setComponentNotEditable();
				this.datePicker.getEditor().setOnKeyReleased( new EventHandler<KeyEvent>() {
					public void handle(KeyEvent event) {
						if( isUpdatable() ){
							try {
								setOK( updater.check( getValueComponent().getComponentValue()));
						    } catch (ModelException me) {
						    	getValueComponent().setComponentValue( me.getMessage());
						    }						
						}
					}
				});
				this.datePicker.setOnAction(new EventHandler<ActionEvent>() {				
					public void handle(ActionEvent event) {
						if( isUpdatable() ){
							try {
								setOK( updater.check( getValueComponent().getComponentValue()));
						    } catch (ModelException me) {
						    	getValueComponent().setComponentValue( me.getMessage());
						    }						
						}
					}
				});			
			}
			return this.datePicker;
		}
		protected void setComponentNotEditable() {
			this.datePicker.setDisable(true);
			this.datePicker.setStyle( GUIConstants.STYLE_OPACITY_OFF);
			this.datePicker.getEditor().setStyle( GUIConstants.STYLE_OPACITY_OFF);			
		}
		@Override
		public String getComponentValue() {
			return this.getDatePicker().getEditor().getText();
		}

		private java.util.Date result;
		private LocalDate date;
		
		@Override
		public void setComponentValue(String value) {
			if( value != null){
				java.util.Date tmp;
				try {
					tmp = DateAndTime.parseDate(value);
					this.result = tmp;
					Instant instant = this.result.toInstant();
					ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
					this.date = zdt.toLocalDate();
					this.getDatePicker().getEditor().setText( DateAndTime.toString( this.result, false));
				} catch (ParseException e) {
					this.getDatePicker().getEditor().setText( e.getMessage());		
				}
			} 			
		}

		@Override
		public void setComponentEditable() {
			this.getDatePicker().setDisable(false);
			this.getDatePicker().getEditor().setEditable(true);
		}		
	}
	@Override
	protected ValueComponent getValueComponent() {		
		if( this.valueComponent == null){
			return new ValueDatePicker();
		}
		return this.valueComponent;
	}

}
class TimestampPanel extends BaseTypePanel implements ValueTimestampMaster{
	protected TimestampPanel(DefaultDetailPanel master, String name, java.util.Date value) {
		super(master, name, DateAndTime.toString(value, true));
//System.out.println("TimestampPanel() value formatted " + DateAndTimeConstantsAndServices.toString(value, true));
		}		
	@Override
	protected ValueComponent getValueComponent() {
		if( this.valueComponent == null){
			return new ValueTimestampPanel(this);
		}
		return this.valueComponent;
	}
	@Override
	public boolean checkAndSetOk(String value) throws ModelException {
		boolean checkResult = this.updater.check(value);
		this.setOK(checkResult);
		return checkResult;
	}	
}

interface CompleteUpdater {
	void update() throws ModelException;
}

interface Updater {
	void update(String text) throws ModelException;

	String format(String text);

	boolean check(String text) throws ModelException;
}

abstract class StandardUpdater implements Updater {
	public String format(String text) {
		return text;
	}
}

abstract class UpdaterForInteger extends StandardUpdater {
	public boolean check(String text) throws ModelException {
		try {
			Long.parseLong(text);
			return true;
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
}

abstract class UpdaterForString extends StandardUpdater {
	public boolean check(String text) throws ModelException {
		return true;
	}
}

abstract class UpdaterForText extends StandardUpdater {
	public boolean check(String text) throws ModelException {
		return true;
	}
}

abstract class UpdaterForDate extends StandardUpdater {
	public boolean check(String text) throws ModelException {
		try {
			DateAndTime.parseDate(text);
			return true;
		} catch (java.text.ParseException pe) {
			return false;
		}
	}
}

abstract class UpdaterForTimeStamp extends StandardUpdater {
	public boolean check(String text) throws ModelException {
		try {
			DateAndTime.parseTimestamp(text);
			return true;
		} catch (java.text.ParseException pe) {
			return false;
		}
	}
}

abstract class DefaultDetailPanel extends DetailPanel {

	public static DetailPanel getStandardDetailPanel(Anything object, final ExceptionAndEventHandler handler) throws ModelException {
		DetailPanelFactory factory = new DetailPanelFactory(handler);
		object.accept(factory);
		return factory.result;
	}

	public void check() {
		boolean somethingUpdatable = false;
		this.setStatus(BaseTypePanel.Neutral);
		java.util.Iterator<BaseTypePanel> panelIterator = this.panels.values().iterator();
		while (panelIterator.hasNext()) {
			BaseTypePanel current = panelIterator.next();
			if (current.getStatus() == BaseTypePanel.NotOK) {
				this.setStatus(BaseTypePanel.NotOK);
				return;
			}
			if (current.getStatus() == BaseTypePanel.OK) {
				somethingUpdatable = true;
			}
		}
		if (somethingUpdatable)
			this.setStatus(BaseTypePanel.OK);
	}

	Hashtable<String, BaseTypePanel> panels;
	private CompleteUpdater completeUpdater;

	protected DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
		super(exceptionHandler);
		this.panels = new Hashtable<String, BaseTypePanel>();
		this.setCenter( this.getStandardDetailsPanel() );
		this.setAnything(anything);
	}

	protected void registerUpdater(String fieldName, Updater updater) {
		BaseTypePanel panel = this.panels.get(fieldName);
		if (panel != null)
			panel.registerUpdater(updater);
	}

	protected void registerCompleteUpdater(CompleteUpdater completeUpdater) {
		this.completeUpdater = completeUpdater;
	}

	abstract protected void addFields();

	public BaseTypePanel getPanel(String key) {
		return this.panels.get(key);
	}

	public void switchBackFromTextView( boolean update) {
		this.setCenter(this.getStandardDetailsPanel());
		if( update) {
			this.currentTextPanel.setValue( this.getTextView().getText());
		}
	}

	private TextPanel currentTextPanel = null;
	public void switchToTextView( TextPanel panel) {
		this.currentTextPanel = panel;
		this.getTextView().setText( panel.getValue());
		this.getTextView().positionCaret(0);
		this.getTextView().setEditable( this.currentTextPanel.isUpdatable() );
		this.getTextPaneWithTitle().setText( panel.getNameLabel().getText() );
		this.setCenter( this.getTextPane());
		this.getTextView().requestFocus();
	}

	private RegularExpressionPanel currentRegExpPanel = null;
	public void switchToRegExprView(RegularExpressionPanel panel) {
		this.currentRegExpPanel = panel;
		final BorderPane regExpPanel = this.getRegExprComfortPanel(panel.getExpression(), panel.getNameLabel().getText(), panel.getValue());
		this.setCenter( regExpPanel );
		this.regExprPane.getRegExprInput().focusedProperty().addListener( new ChangeListener<Boolean>() {
			public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
				if(! newValue ){					
					if( regExpOkButton.isFocused() || regExpCancelButton.isFocused()){
					} else {						
						switchBackFromRegExpView(false);
					}
				}
			}
		});
		this.regExprPane.getRegExprInput().requestFocus();
	}

	public void switchBackFromRegExpView( boolean update) {
		this.setCenter( this.getStandardDetailsPanel());
		if( update ) {
			String value = this.regExprPane.getRegExprInput().getText();
			this.currentRegExpPanel.setValue( value);
			this.currentRegExpPanel.setOK( this.currentRegExpPanel.check());
		}
	}

	private BorderPane regExprComfortPanel = null;
	private BorderPane getRegExprComfortPanel(RegularExpression<Character, CharacterType> expression, String name, String value) {
		if( this.regExprComfortPanel == null) {
			this.regExprComfortPanel = new BorderPane();
			this.regExprComfortPanel.setBottom( this.getRegExprComfortToolBar());
			this.regExprComfortPanel.setCenter( this.getRegExprPane( expression, name, value));
		} 
		return regExprComfortPanel;
	}
	private ToolBar regExprComfortToolBar = null;
	private ToolBar getRegExprComfortToolBar() {
		if( this.regExprComfortToolBar == null) {
			this.regExprComfortToolBar = new ToolBar(getRegExpOkButton(), getRegExpCancelButton() );
		}
		return regExprComfortToolBar;
	}
	private Button regExpOkButton = null;
	private Button getRegExpOkButton() {
		if( this.regExpOkButton == null) {
			this.regExpOkButton = new Button( GUIConstants.UpdateButtonText);
			this.regExpOkButton.setMinWidth(Region.USE_PREF_SIZE);
			this.regExpOkButton.setMaxWidth(Region.USE_PREF_SIZE);
			this.regExpOkButton.setOnAction( new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					switchBackFromRegExpView(true);
				}
			});
		}
		return this.regExpOkButton;
	}
	private Button regExpCancelButton = null;
	private Button getRegExpCancelButton() {
		if( this.regExpCancelButton == null) {
			this.regExpCancelButton = new Button( GUIConstants.CancelButtonText );
			this.regExpCancelButton.setOnAction( new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					switchBackFromRegExpView(false);
				}
			});
		}
		return this.regExpCancelButton;
	}
	private RegExprPanel2 regExprPane = null;
	private RegExprPanel2 getRegExprPane(RegularExpression<Character, CharacterType> expression, String name, String value) {
		this.regExprPane = new RegExprPanel2(expression, name, value);
		return regExprPane;
	}
	private BorderPane standardDetailsPane;
	private BorderPane getStandardDetailsPanel() {
		if( this.standardDetailsPane == null) {
			this.standardDetailsPane = new BorderPane();
			this.standardDetailsPane.setBottom( this.getMajorToolBar());
			this.standardDetailsPane.setCenter( this.getDetailScrollPane() );
		}
		return this.standardDetailsPane;
	}
	private ToolBar majorToolBar;
	private ToolBar getMajorToolBar() {
		if( this.majorToolBar == null) {
			this.majorToolBar = new ToolBar(  this.getUpdateAllButton());
		}
		return this.majorToolBar;
	}
	private Button updateAllButton;
	private Button getUpdateAllButton() {
		if( this.updateAllButton == null) {
			this.updateAllButton = new Button( GUIConstants.UpdateText);
			this.updateAllButton.setMinWidth(USE_PREF_SIZE);
			this.updateAllButton.setMaxWidth(USE_PREF_SIZE);
			this.updateAllButton.setOnAction( new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					updateAll();
				}
			});
			this.setStatus( BaseTypePanel.Neutral);
		}
		return this.updateAllButton;
	}

	protected void updateAll() {
		if( this.hasCompleteUpdater()) {
			try {
				this.completeUpdater.update();
				this.resetDetails(true, "");
			} catch (ModelException e) {
				this.resetDetails(false, e.getMessage());
			}
		}
		java.util.Iterator<BaseTypePanel> panelIterator = this.panels.values().iterator();
		while (panelIterator.hasNext()) {
			BaseTypePanel current = panelIterator.next();
			current.updateIfReady();
		}
		this.check();
	}

	private void resetDetails(boolean withoutErrors, String message) {
		java.util.Iterator<BaseTypePanel> panelIterator = this.panels.values().iterator();
		while (panelIterator.hasNext()) {
			BaseTypePanel current = panelIterator.next();
			if (current.isReadyForUpdate()) {
				if (withoutErrors) {
					current.setStatus(BaseTypePanel.Neutral);
				} else {
					current.getValueComponent().setComponentValue(message);
					current.setStatus(BaseTypePanel.NotOK);
				}
			}
		}
		this.check();
	}

	private boolean hasCompleteUpdater() {
		return this.completeUpdater != null;
	}

	private void setStatus(int status) {
		if (status == BaseTypePanel.Neutral)
			DetailPanel.setButtonToNeutral(this.getUpdateAllButton());
		if (status == BaseTypePanel.OK)
			DetailPanel.setButtonToOK(this.getUpdateAllButton());
		if (status == BaseTypePanel.NotOK)
			DetailPanel.setButtonToNotOk(this.getUpdateAllButton());
	}
	private ScrollPane detailScrollPane = null;
	private ScrollPane getDetailScrollPane() {
		if( this.detailScrollPane == null) {
			this.detailScrollPane = new ScrollPane();
			this.detailScrollPane.setContent( this.getScrollablePane());
		    this.detailScrollPane.prefWidthProperty().bind( this.widthProperty().multiply(0.98));			
		    this.detailScrollPane.prefHeightProperty().bind( this.heightProperty());			
		}
		return detailScrollPane;
	}
	private BorderPane textPane = null;
	private BorderPane getTextPane() {
		if( this.textPane == null) {
			this.textPane = new BorderPane();
			this.textPane.setCenter( this.getTextPaneWithTitle());
			this.textPane.setBottom( this.getTextToolBar());
		}
		return this.textPane;
	}
	private TitledPane textPaneTitle;
	private TitledPane getTextPaneWithTitle() {
		if (this.textPaneTitle == null) {
			this.textPaneTitle = new TitledPane("", this.getTextView());
			this.textPaneTitle.setCollapsible(false);
		}
		return this.textPaneTitle;
	}

	private ToolBar textToolBar;
	private ToolBar getTextToolBar() {
		if( this.textToolBar == null) {
			this.textToolBar = new ToolBar();
			this.textToolBar.getItems().add( this.getSwitchBackCancelButton());
			this.textToolBar.getItems().add( this.getSwitchBackUpdateButton());
		}
		return this.textToolBar;
	}

	private Button switchBackUpdateButton;
	private Button getSwitchBackUpdateButton() {
		if( this.switchBackUpdateButton == null) {
			this.switchBackUpdateButton = new Button( GUIConstants.UpdateButtonText);
			this.switchBackUpdateButton.setDisable( true);
			this.switchBackUpdateButton.setOnAction( new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					switchBackFromTextView(true);
				}
			}); 
		}
		return this.switchBackUpdateButton;
	}

	private Button switchBackCancelButton;
	private Button getSwitchBackCancelButton() {
		if( this.switchBackCancelButton == null){
			this.switchBackCancelButton = new Button( GUIConstants.CancelButtonText);
			this.switchBackCancelButton.setOnAction( new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					switchBackFromTextView(false);
				}
			});
		}
		return this.switchBackCancelButton;
	}

	private TextArea textView = null;
	private TextArea getTextView() {
		if( this.textView == null) {
			this.textView = new TextArea();
			this.textView.setWrapText( true );
			this.textView.setPrefHeight(USE_PREF_SIZE);
			this.textView.setMaxHeight(Double.MAX_VALUE);
			this.textView.prefHeightProperty().bind(this.heightProperty());
			this.textView.setOnKeyReleased( new EventHandler<KeyEvent>() {
				public void handle( KeyEvent event ) {
					getSwitchBackUpdateButton().setDisable( !getTextView().isEditable());					
				}
			});
		}
		return this.textView;
	}

	private VBox scrollablePane = null;
	protected VBox getScrollablePane() {
		if( this.scrollablePane == null) {
			this.scrollablePane = new VBox(8);
			this.scrollablePane.setAlignment( Pos.TOP_LEFT);
			this.scrollablePane.prefWidthProperty().bind( this.getDetailScrollPane().prefWidthProperty());
		}
		return this.scrollablePane;
	}

	public void setAnything(Anything anything) {
		this.anything = anything;
		((ViewProxi) anything).expand();
		this.addFields();
	}
}

class ValueTimestampPanel extends HBox implements ValueComponent{
	private java.util.Date result;
	private LocalDate date;
	private LocalTime time;
	private ValueTimestampMaster master;
	private int partsPerHour;
	private int delta;								// delta in seconds of time slider step
	private int hourStart = DateAndTime.HOUR_START; // time slider range start, inclusive
	private int hourEnd = DateAndTime.HOUR_END; 	// time slider range end, exclusive

	ValueTimestampPanel(){		
		this(DateAndTime.PARTS_PER_HOUR, DateAndTime.HOUR_START, DateAndTime.HOUR_END);
	}

	ValueTimestampPanel( ValueTimestampMaster master ){
		this( DateAndTime.PARTS_PER_HOUR, DateAndTime.HOUR_START, DateAndTime.HOUR_END);
		this.master = master;
	}

	ValueTimestampPanel(ValueTimestampMaster master, int partsPerHour, int startHour, int endHour){		
		this( partsPerHour, startHour, endHour);
		this.master = master;
	}

	/**
	 * The Parameter <code> partsPerHour </code> determines the granularity for the time slider in seconds. 
	 * <p> The default is 12.
	 * <p> The range is 1 to 3600.
	 * <p> One hour (60 minutes) is divided into 5 minutes parts by default; e.g. one time slider step is 5 minutes plus or minus.
	 */
	ValueTimestampPanel(int partsPerHour, int startHour, int endHour){		
		super(10);
		this.partsPerHour = partsPerHour;
		this.delta = 3600/this.partsPerHour;
		this.hourStart = startHour;
		this.hourEnd = endHour;

		this.master = new ValueTimestampMaster() {
			public boolean checkAndSetOk(String value) throws ModelException {
				return false;
			}
			public boolean isUpdatable() {
				return false;
			}
		};
		
		this.date = LocalDate.now();
		this.time = LocalTime.now();
		long fractions = this.calcFractions( this.time.getHour(), this.time.getMinute(), this.time.getSecond());
		this.time = LocalTime.ofSecondOfDay(((long) fractions*delta));
//System.out.println("init time " + this.time + " fractions " + fractions + " part " + partsPerHour);		
//		this.time = this.time.minusSeconds(this.time.getSecond());
//		this.time = this.time.minusNanos(this.time.getNano());
        this.result = java.util.Date.from( this.date.atTime(this.time).atZone(ZoneId.systemDefault()).toInstant());
		
		this.setAlignment( Pos.BASELINE_LEFT );
		this.getChildren().add( this.getDatePicker());
		this.getChildren().add( this.getTimeInputField());
		this.getChildren().add( this.getTimeSlider());
		HBox.setHgrow(this.getDatePicker(), Priority.NEVER);
		HBox.setHgrow(this.getTimeInputField(), Priority.SOMETIMES);			
		HBox.setHgrow(this.getTimeSlider(), Priority.ALWAYS);
		
		this.setTimeValue( this.time.getHour(), this.time.getMinute(), this.time.getSecond());
	}
			
	private DatePicker datePicker;
	private DatePicker getDatePicker(){
		if( this.datePicker == null){
			this.datePicker = new DatePicker( this.date );
			this.datePicker.getEditor().setText(DateAndTime.toString(this.result, false));
			this.datePicker.setDisable(true);
			this.datePicker.setStyle(GUIConstants.STYLE_OPACITY_OFF);
			this.datePicker.getEditor().setStyle(GUIConstants.STYLE_OPACITY_OFF);
			this.datePicker.getEditor().setOnKeyReleased( new EventHandler<KeyEvent>() {
				public void handle(KeyEvent event) {
					if( master.isUpdatable() ){
						try {
							master.checkAndSetOk( getComponentValue());
					    } catch (ModelException me) {
					    	datePicker.getEditor().setText( me.getMessage());
					    }						
					}
				}
			});
			this.datePicker.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					if( master.isUpdatable() ){
						try {
							master.checkAndSetOk( getComponentValue());
					    } catch (ModelException me) {
					    	datePicker.getEditor().setText( me.getMessage());
					    }						
					}
				}
			});		
		}
		return this.datePicker;
	}
	
	private Slider timeSlider;
	private Slider getTimeSlider() {
		if( this.timeSlider == null){
			this.timeSlider = new Slider( this.hourStart*this.partsPerHour
										, this.hourEnd*this.partsPerHour - 1
					, this.calcFractions( this.time.getHour(), this.time.getMinute(), this.time.getSecond() ));	
			this.timeSlider.setPrefWidth(20);
			this.timeSlider.setMinWidth(Double.MIN_VALUE);
			this.timeSlider.setTooltip(new Tooltip(GUIConstants.TimeSliderToolTipText));
			this.timeSlider.setVisible(false);
			this.timeSlider.setOnMouseDragged(new EventHandler<MouseEvent>() {
				public void handle(MouseEvent event) {
					time = LocalTime.ofSecondOfDay(((long) (timeSlider.getValue())*delta));
					setTimeValue( time.getHour(), time.getMinute(), time.getSecond());
					if( master.isUpdatable() ){
						try {
							master.checkAndSetOk( getComponentValue());
					    } catch (ModelException me) {
					    	getDatePicker().getEditor().setText( me.getMessage());
					    }						
					}					
				}
			});
			this.timeSlider.setOnKeyPressed(new EventHandler<KeyEvent>() {
				public void handle(KeyEvent event) {
					if( event.getCode() == KeyCode.RIGHT ) {
						time = LocalTime.ofSecondOfDay(((long) (timeSlider.getValue())* delta));							
						if(((long)timeSlider.getValue()) >= hourEnd * partsPerHour - 1) return;						
						time = time.plusSeconds( delta );
						Platform.runLater(new Runnable() {
							public void run() {
								timeSlider.setValue( calcFractions(time.getHour(), time.getMinute(), time.getSecond()));									
								setTimeValue( time.getHour(), time.getMinute(), time.getSecond());						
								if( master.isUpdatable() ){
									try {
										master.checkAndSetOk( getComponentValue());
								    } catch (ModelException me) {
								    	getDatePicker().getEditor().setText( me.getMessage());
								    }						
								}					
							}
						});
					} else if (event.getCode() == KeyCode.LEFT){
						time = LocalTime.ofSecondOfDay(((long) (timeSlider.getValue())* delta));							
						if( ((long)timeSlider.getValue()) <= hourStart*partsPerHour)return;					
						time = time.minusSeconds( delta );
						Platform.runLater(new Runnable() {
							public void run() {
								timeSlider.setValue( calcFractions(time.getHour(), time.getMinute(), time.getSecond()));
								setTimeValue( time.getHour(), time.getMinute(), time.getSecond());						
								if( master.isUpdatable() ){
									try {
										master.checkAndSetOk( getComponentValue());
								    } catch (ModelException me) {
								    	getDatePicker().getEditor().setText( me.getMessage());
								    }						
								}					
							}
						});
					}
				}
			});				
		}
		return this.timeSlider;
	}
	private long calcFractions( int hour, int minute, int second ){
		int totalSeconds = (hour*60+minute)*60 + second;
		return totalSeconds/this.delta + (totalSeconds % this.delta > 0 ? 1 : 0);
	}
	private TextField timeInputField;
	private TextField getTimeInputField() {
		if( this.timeInputField == null){
			this.timeInputField = new TextField();
			this.timeInputField.setPrefWidth(70);
			this.timeInputField.setMinWidth(Double.MIN_VALUE);
			this.timeInputField.setEditable(false);
			this.timeInputField.setOnKeyReleased(new EventHandler<KeyEvent>() {
				public void handle(KeyEvent event) {
					if( master.isUpdatable() ){
						try {
							master.checkAndSetOk( getComponentValue());
							getTimeSlider().setValue( calcFractions(time.getHour(), time.getMinute(), time.getSecond()));
					    } catch (ModelException me) {
					    	getDatePicker().getEditor().setText( me.getMessage());
					    }						
					}
				}
			});			
		}
		return this.timeInputField;
	}
	protected void setTimeValue( int hours, int minutes, int seconds) {
		LocalTime time;
		String format;
		if( this.partsPerHour > 60 ){				
			time =  LocalTime.of( hours, minutes, seconds);
			format = DateAndTime.TIMEFORMAT;
		}
		else {
			time =  LocalTime.of( hours, minutes, 0);				
			format = DateAndTime.TIMEFORMAT_SIMPLE;
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern( format );
		this.getTimeInputField().setText( time.format(formatter));							
	}

	public void setComponentValue( String value) {			
		if( value != null){
			java.util.Date tmp;
			try {
				tmp = DateAndTime.parseTimestamp(value);
				this.setComponentValue(tmp);
			} catch (ParseException e) {
				this.datePicker.getEditor().setText( e.getMessage());
			}
		} 
	}
	
	public void setComponentValue( java.util.Date value) {
		this.result = value;
		Instant instant = this.result.toInstant();
		ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
		this.date = zdt.toLocalDate();
		this.time = zdt.toLocalTime();
		this.datePicker.getEditor().setText( DateAndTime.toString( this.result, false));
		this.setTimeValue( this.time.getHour(), this.time.getMinute(), this.time.getSecond());
	}

	public String getComponentValue() {
		if( check()){
//System.out.println("getComponentValue " + this.getCurrentResult());		
			return this.getCurrentResult();
		}
//System.out.println("getComponentValue gescheitert");				
		return this.getDatePicker().getEditor().getText() + " " + this.getTimeInputField().getText(); 
	}

	String getCurrentResult() {
		return DateAndTime.toString(this.result, true);
	}
	
	public java.util.Date getResult(){
		return this.result;
	}
	
	public boolean check() {
		boolean isResultOk = false;
		java.util.Date tmp = this.result;
		try {
			String dateInputAsText = this.getDatePicker().getEditor().getText();
			if( dateInputAsText.length() > 0){				
				tmp = DateAndTime.parseDate( dateInputAsText );
				this.date = tmp.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
				isResultOk = true;
			} else if( this.getDatePicker().getValue() != null){
				this.date = this.getDatePicker().getValue();
				isResultOk = true;
			}	
		} catch (java.text.ParseException pe) {}
		if( isResultOk){
			try{	
				tmp = DateAndTime.parseTime( this.getTimeInputField().getText() );
				this.time = tmp.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
				this.result = java.util.Date.from( this.date.atTime(this.time).atZone(ZoneId.systemDefault()).toInstant() );
			} catch (ParseException pe) {
				isResultOk = false;
//System.out.println("parse time -> schrott (Format?)" + this.getTimeInputField().getText());				
			}			
		}
		return isResultOk;
	}
		
	public void setComponentEditable() {
		this.getTimeInputField().setEditable(true);
		this.getDatePicker().setDisable(false);
		this.getTimeSlider().setVisible(true);
	}
	
	public void handleFocusGained() {
		this.getDatePicker().requestFocus();
	}

}


interface ValueTimestampMaster{

	boolean isUpdatable();

	boolean checkAndSetOk(String value) throws ModelException;	
}


class DetailPanelFactory implements AnythingVisitor {
	protected DefaultDetailPanel result = null;
	private ExceptionAndEventHandler handler;
	public DetailPanelFactory(ExceptionAndEventHandler handler) {
		this.handler = handler;
	}
    public void handleCONCModuleGroupOrStudyProgram_PL(view.CONCModuleGroupOrStudyProgram_PLView object){
        result = new CONCModuleGroupOrStudyProgram_PLDefaultDetailPanel(handler, object);
    }
    public void handleServer(view.ServerView object){
        result = new ServerDefaultDetailPanel(handler, object);
    }
    public void handledg38(view.dg38View object){
        result = new dg38DefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_GL(view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLView object){
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLDefaultDetailPanel(handler, object);
    }
    public void handledg39(view.dg39View object){
        result = new dg39DefaultDetailPanel(handler, object);
    }
    public void handledg36(view.dg36View object){
        result = new dg36DefaultDetailPanel(handler, object);
    }
    public void handledg34(view.dg34View object){
        result = new dg34DefaultDetailPanel(handler, object);
    }
    public void handledg35(view.dg35View object){
        result = new dg35DefaultDetailPanel(handler, object);
    }
    public void handledg32(view.dg32View object){
        result = new dg32DefaultDetailPanel(handler, object);
    }
    public void handledg31(view.dg31View object){
        result = new dg31DefaultDetailPanel(handler, object);
    }
    public void handleNotAtomicModule_SL(view.NotAtomicModule_SLView object){
        result = new NotAtomicModule_SLDefaultDetailPanel(handler, object);
    }
    public void handlePassedNotPassedGrades(view.PassedNotPassedGradesView object){
        result = new PassedNotPassedGradesDefaultDetailPanel(handler, object);
    }
    public void handleCONCModule_SL(view.CONCModule_SLView object){
        result = new CONCModule_SLDefaultDetailPanel(handler, object);
    }
    public void handletg40(view.tg40View object){
        result = new tg40DefaultDetailPanel(handler, object);
    }
    public void handleCONCAtomicModule_GL(view.CONCAtomicModule_GLView object){
        result = new CONCAtomicModule_GLDefaultDetailPanel(handler, object);
    }
    public void handleModuleGroup_PL(view.ModuleGroup_PLView object){
        result = new ModuleGroup_PLDefaultDetailPanel(handler, object);
    }
    public void handletg37(view.tg37View object){
        result = new tg37DefaultDetailPanel(handler, object);
    }
    public void handleFinished(view.FinishedView object){
        result = new FinishedDefaultDetailPanel(handler, object);
    }
    public void handledg29(view.dg29View object){
        result = new dg29DefaultDetailPanel(handler, object);
    }
    public void handleModuleGroup_GL(view.ModuleGroup_GLView object){
        result = new ModuleGroup_GLDefaultDetailPanel(handler, object);
    }
    public void handletg33(view.tg33View object){
        result = new tg33DefaultDetailPanel(handler, object);
    }
    public void handletg30(view.tg30View object){
        result = new tg30DefaultDetailPanel(handler, object);
    }
    public void handleGradeHistory(view.GradeHistoryView object){
        result = new GradeHistoryDefaultDetailPanel(handler, object);
    }
    public void handledg28(view.dg28View object){
        result = new dg28DefaultDetailPanel(handler, object);
    }
    public void handledg25(view.dg25View object){
        result = new dg25DefaultDetailPanel(handler, object);
    }
    public void handledg26(view.dg26View object){
        result = new dg26DefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_PL(view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLView object){
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLDefaultDetailPanel(handler, object);
    }
    public void handleCONCCreditPointHavingElement_SL(view.CONCCreditPointHavingElement_SLView object){
        result = new CONCCreditPointHavingElement_SLDefaultDetailPanel(handler, object);
    }
    public void handleStudentManager(view.StudentManagerView object){
        result = new StudentManagerDefaultDetailPanel(handler, object);
    }
    public void handledg24(view.dg24View object){
        result = new dg24DefaultDetailPanel(handler, object);
    }
    public void handleStudent(view.StudentView object){
        result = new StudentDefaultDetailPanel(handler, object);
    }
    public void handledg21(view.dg21View object){
        result = new dg21DefaultDetailPanel(handler, object);
    }
    public void handledg22(view.dg22View object){
        result = new dg22DefaultDetailPanel(handler, object);
    }
    public void handleGradeCachedState_SL(view.GradeCachedState_SLView object){
        result = new GradeCachedState_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCAtomicModule_PL(view.CONCAtomicModule_PLView object){
        result = new CONCAtomicModule_PLDefaultDetailPanel(handler, object);
    }
    public void handleNoStudentGroup(view.NoStudentGroupView object){
        result = new NoStudentGroupDefaultDetailPanel(handler, object);
    }
    public void handleCreditPointsManager(view.CreditPointsManagerView object){
        result = new CreditPointsManagerDefaultDetailPanel(handler, object);
    }
    public void handleTripleAtomicModule_SL(view.TripleAtomicModule_SLView object){
        result = new TripleAtomicModule_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleGroupOrStudyProgram_GL(view.CONCModuleGroupOrStudyProgram_GLView object){
        result = new CONCModuleGroupOrStudyProgram_GLDefaultDetailPanel(handler, object);
    }
    public void handleUnit_GL(view.Unit_GLView object){
        result = new Unit_GLDefaultDetailPanel(handler, object);
    }
    public void handleFalsum(view.FalsumView object){
        result = new FalsumDefaultDetailPanel(handler, object);
    }
    public void handleNoProgram_GL(view.NoProgram_GLView object){
        result = new NoProgram_GLDefaultDetailPanel(handler, object);
    }
    public void handleRealGrades(view.RealGradesView object){
        result = new RealGradesDefaultDetailPanel(handler, object);
    }
    public void handleUnit_PL(view.Unit_PLView object){
        result = new Unit_PLDefaultDetailPanel(handler, object);
    }
    public void handleNoProgram_PL(view.NoProgram_PLView object){
        result = new NoProgram_PLDefaultDetailPanel(handler, object);
    }
    public void handleNothingCachedState_SL(view.NothingCachedState_SLView object){
        result = new NothingCachedState_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCState_PL(view.CONCState_PLView object){
        result = new CONCState_PLDefaultDetailPanel(handler, object);
    }
    public void handleNotAtomicModule_PL(view.NotAtomicModule_PLView object){
        result = new NotAtomicModule_PLDefaultDetailPanel(handler, object);
    }
    public void handleBinaryAtomicModule_SL(view.BinaryAtomicModule_SLView object){
        result = new BinaryAtomicModule_SLDefaultDetailPanel(handler, object);
    }
    public void handleStudentWrapper(view.StudentWrapperView object){
        result = new StudentWrapperDefaultDetailPanel(handler, object);
    }
    public void handletg50(view.tg50View object){
        result = new tg50DefaultDetailPanel(handler, object);
    }
    public void handleNotYetStudied(view.NotYetStudiedView object){
        result = new NotYetStudiedDefaultDetailPanel(handler, object);
    }
    public void handleCONCModule_PL(view.CONCModule_PLView object){
        result = new CONCModule_PLDefaultDetailPanel(handler, object);
    }
    public void handleStudentGroup(view.StudentGroupView object){
        result = new StudentGroupDefaultDetailPanel(handler, object);
    }
    public void handlePassed(view.PassedView object){
        result = new PassedDefaultDetailPanel(handler, object);
    }
    public void handleAdminService(view.AdminServiceView object){
        result = new AdminServiceDefaultDetailPanel(handler, object);
    }
    public void handleGradeAggregator(view.GradeAggregatorView object){
        result = new GradeAggregatorDefaultDetailPanel(handler, object);
    }
    public void handleNotAtomicModule_GL(view.NotAtomicModule_GLView object){
        result = new NotAtomicModule_GLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroup_PL(view.CONCModuleOrModuleGroup_PLView object){
        result = new CONCModuleOrModuleGroup_PLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModule_GL(view.CONCModule_GLView object){
        result = new CONCModule_GLDefaultDetailPanel(handler, object);
    }
    public void handleCONCCreditPointHavingElement_PL(view.CONCCreditPointHavingElement_PLView object){
        result = new CONCCreditPointHavingElement_PLDefaultDetailPanel(handler, object);
    }
    public void handleErrorDisplay(view.ErrorDisplayView object){
        result = new ErrorDisplayDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroupOrStudyProgramOrUnit_SL(view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLView object){
        result = new CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCAtomicModule_SL(view.CONCAtomicModule_SLView object){
        result = new CONCAtomicModule_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroup_GL(view.CONCModuleOrModuleGroup_GLView object){
        result = new CONCModuleOrModuleGroup_GLDefaultDetailPanel(handler, object);
    }
    public void handleCONCCreditPointHavingElement_GL(view.CONCCreditPointHavingElement_GLView object){
        result = new CONCCreditPointHavingElement_GLDefaultDetailPanel(handler, object);
    }
    public void handleProgramManager(view.ProgramManagerView object){
        result = new ProgramManagerDefaultDetailPanel(handler, object);
    }
    public void handleTripleAtomicModule_PL(view.TripleAtomicModule_PLView object){
        result = new TripleAtomicModule_PLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleGroupOrStudyProgram_SL(view.CONCModuleGroupOrStudyProgram_SLView object){
        result = new CONCModuleGroupOrStudyProgram_SLDefaultDetailPanel(handler, object);
    }
    public void handleStudentService(view.StudentServiceView object){
        result = new StudentServiceDefaultDetailPanel(handler, object);
    }
    public void handleVerum(view.VerumView object){
        result = new VerumDefaultDetailPanel(handler, object);
    }
    public void handleModuleGroup_SL(view.ModuleGroup_SLView object){
        result = new ModuleGroup_SLDefaultDetailPanel(handler, object);
    }
    public void handleActive(view.ActiveView object){
        result = new ActiveDefaultDetailPanel(handler, object);
    }
    public void handleTripleAtomicModule_GL(view.TripleAtomicModule_GLView object){
        result = new TripleAtomicModule_GLDefaultDetailPanel(handler, object);
    }
    public void handleStudyProgram_SL(view.StudyProgram_SLView object){
        result = new StudyProgram_SLDefaultDetailPanel(handler, object);
    }
    public void handleNoProgram_SL(view.NoProgram_SLView object){
        result = new NoProgram_SLDefaultDetailPanel(handler, object);
    }
    public void handletg23(view.tg23View object){
        result = new tg23DefaultDetailPanel(handler, object);
    }
    public void handledg18(view.dg18View object){
        result = new dg18DefaultDetailPanel(handler, object);
    }
    public void handledg19(view.dg19View object){
        result = new dg19DefaultDetailPanel(handler, object);
    }
    public void handledg16(view.dg16View object){
        result = new dg16DefaultDetailPanel(handler, object);
    }
    public void handletg20(view.tg20View object){
        result = new tg20DefaultDetailPanel(handler, object);
    }
    public void handledg14(view.dg14View object){
        result = new dg14DefaultDetailPanel(handler, object);
    }
    public void handleCONCGrade(view.CONCGradeView object){
        result = new CONCGradeDefaultDetailPanel(handler, object);
    }
    public void handledg15(view.dg15View object){
        result = new dg15DefaultDetailPanel(handler, object);
    }
    public void handledg12(view.dg12View object){
        result = new dg12DefaultDetailPanel(handler, object);
    }
    public void handleBinaryAtomicModule_PL(view.BinaryAtomicModule_PLView object){
        result = new BinaryAtomicModule_PLDefaultDetailPanel(handler, object);
    }
    public void handleStudyProgram_GL(view.StudyProgram_GLView object){
        result = new StudyProgram_GLDefaultDetailPanel(handler, object);
    }
    public void handledg11(view.dg11View object){
        result = new dg11DefaultDetailPanel(handler, object);
    }
    public void handletg27(view.tg27View object){
        result = new tg27DefaultDetailPanel(handler, object);
    }
    public void handleCreditPointsCachedState_PL(view.CreditPointsCachedState_PLView object){
        result = new CreditPointsCachedState_PLDefaultDetailPanel(handler, object);
    }
    public void handleCONCState_SL(view.CONCState_SLView object){
        result = new CONCState_SLDefaultDetailPanel(handler, object);
    }
    public void handleCreditPoints(view.CreditPointsView object){
        result = new CreditPointsDefaultDetailPanel(handler, object);
    }
    public void handleGradeHistoryElement(view.GradeHistoryElementView object){
        result = new GradeHistoryElementDefaultDetailPanel(handler, object);
    }
    public void handleNoGrade(view.NoGradeView object){
        result = new NoGradeDefaultDetailPanel(handler, object);
    }
    public void handleNotPassed(view.NotPassedView object){
        result = new NotPassedDefaultDetailPanel(handler, object);
    }
    public void handleCONCCalculatableGrade(view.CONCCalculatableGradeView object){
        result = new CONCCalculatableGradeDefaultDetailPanel(handler, object);
    }
    public void handleBinaryAtomicModule_GL(view.BinaryAtomicModule_GLView object){
        result = new BinaryAtomicModule_GLDefaultDetailPanel(handler, object);
    }
    public void handletg13(view.tg13View object){
        result = new tg13DefaultDetailPanel(handler, object);
    }
    public void handletg10(view.tg10View object){
        result = new tg10DefaultDetailPanel(handler, object);
    }
    public void handleStudyProgram_PL(view.StudyProgram_PLView object){
        result = new StudyProgram_PLDefaultDetailPanel(handler, object);
    }
    public void handleCONCModuleOrModuleGroup_SL(view.CONCModuleOrModuleGroup_SLView object){
        result = new CONCModuleOrModuleGroup_SLDefaultDetailPanel(handler, object);
    }
    public void handleCONCDecimalGrade(view.CONCDecimalGradeView object){
        result = new CONCDecimalGradeDefaultDetailPanel(handler, object);
    }
    public void handletg17(view.tg17View object){
        result = new tg17DefaultDetailPanel(handler, object);
    }
    public void handleUnit_SL(view.Unit_SLView object){
        result = new Unit_SLDefaultDetailPanel(handler, object);
    }
    public void handleNothingCachedState_PL(view.NothingCachedState_PLView object){
        result = new NothingCachedState_PLDefaultDetailPanel(handler, object);
    }

}



class CONCModuleGroupOrStudyProgram_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String ModuleGroupOrStudyProgram_PL$$containees = "ModuleGroupOrStudyProgram_PL$$containees";
    
    protected CONCModuleGroupOrStudyProgram_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleGroupOrStudyProgram_PLView getAnything(){
        return (view.CONCModuleGroupOrStudyProgram_PLView)this.anything;
    }
}

class ServerDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String Server$$services = "Server$$services";
    protected static final String Server$$user = "Server$$user";
    
    protected ServerDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "user", this.getAnything().getUser());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(Server$$user, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.ServerView getAnything(){
        return (view.ServerView)this.anything;
    }
}

class dg38DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg38DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg38View getAnything(){
        return (view.dg38View)this.anything;
    }
}

class CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLView getAnything(){
        return (view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_GLView)this.anything;
    }
}

class dg39DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg39DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg39View getAnything(){
        return (view.dg39View)this.anything;
    }
}

class dg36DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg36DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg36View getAnything(){
        return (view.dg36View)this.anything;
    }
}

class dg34DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg34DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg34View getAnything(){
        return (view.dg34View)this.anything;
    }
}

class dg35DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg35DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg35View getAnything(){
        return (view.dg35View)this.anything;
    }
}

class dg32DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg32DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg32View getAnything(){
        return (view.dg32View)this.anything;
    }
}

class dg31DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg31DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg31View getAnything(){
        return (view.dg31View)this.anything;
    }
}

class NotAtomicModule_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    protected static final String NotAtomicModule_SL$$units = "NotAtomicModule_SL$$units";
    
    protected NotAtomicModule_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NotAtomicModule_SLView getAnything(){
        return (view.NotAtomicModule_SLView)this.anything;
    }
}

class PassedNotPassedGradesDefaultDetailPanel extends DefaultDetailPanel{
    
    protected PassedNotPassedGradesDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.PassedNotPassedGradesView getAnything(){
        return (view.PassedNotPassedGradesView)this.anything;
    }
}

class CONCModule_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected CONCModule_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModule_SLView getAnything(){
        return (view.CONCModule_SLView)this.anything;
    }
}

class tg40DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg40DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg40View getAnything(){
        return (view.tg40View)this.anything;
    }
}

class CONCAtomicModule_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected CONCAtomicModule_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCAtomicModule_GLView getAnything(){
        return (view.CONCAtomicModule_GLView)this.anything;
    }
}

class ModuleGroup_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String ModuleGroupOrStudyProgram_PL$$containees = "ModuleGroupOrStudyProgram_PL$$containees";
    
    protected ModuleGroup_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.ModuleGroup_PLView getAnything(){
        return (view.ModuleGroup_PLView)this.anything;
    }
}

class tg37DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg37DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg37View getAnything(){
        return (view.tg37View)this.anything;
    }
}

class FinishedDefaultDetailPanel extends DefaultDetailPanel{
    
    protected FinishedDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.FinishedView getAnything(){
        return (view.FinishedView)this.anything;
    }
}

class dg29DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg29DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg29View getAnything(){
        return (view.dg29View)this.anything;
    }
}

class ModuleGroup_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    protected static final String ModuleGroupOrStudyProgram_GL$$containees = "ModuleGroupOrStudyProgram_GL$$containees";
    
    protected ModuleGroup_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.ModuleGroup_GLView getAnything(){
        return (view.ModuleGroup_GLView)this.anything;
    }
}

class tg33DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg33DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg33View getAnything(){
        return (view.tg33View)this.anything;
    }
}

class tg30DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg30DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg30View getAnything(){
        return (view.tg30View)this.anything;
    }
}

class GradeHistoryDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String GradeHistory$$allElements = "GradeHistory$$allElements";
    
    protected GradeHistoryDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.GradeHistoryView getAnything(){
        return (view.GradeHistoryView)this.anything;
    }
}

class dg28DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg28DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg28View getAnything(){
        return (view.dg28View)this.anything;
    }
}

class dg25DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg25DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg25View getAnything(){
        return (view.dg25View)this.anything;
    }
}

class dg26DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg26DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg26View getAnything(){
        return (view.dg26View)this.anything;
    }
}

class CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    
    protected CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLView getAnything(){
        return (view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_PLView)this.anything;
    }
}

class CONCCreditPointHavingElement_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected CONCCreditPointHavingElement_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCCreditPointHavingElement_SLView getAnything(){
        return (view.CONCCreditPointHavingElement_SLView)this.anything;
    }
}

class StudentManagerDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String StudentManager$$studentGroups = "StudentManager$$studentGroups";
    
    protected StudentManagerDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.StudentManagerView getAnything(){
        return (view.StudentManagerView)this.anything;
    }
}

class dg24DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg24DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg24View getAnything(){
        return (view.dg24View)this.anything;
    }
}

class StudentDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String Student$$firstName = "Student$$firstName";
    protected static final String Student$$name = "Student$$name";
    protected static final String Student$$dateOfBirth = "Student$$dateOfBirth";
    protected static final String Student$$matNr = "Student$$matNr";
    protected static final String Student$$studyPrograms = "Student$$studyPrograms";
    
    protected StudentDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Vorname", this.getAnything().getFirstName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(Student$$firstName, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Nachname", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(Student$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new DatePanel(this, "Geburtsdatum", this.getAnything().getDateOfBirth());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(Student$$dateOfBirth, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "MatrikelNummer", this.getAnything().getMatNr());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(Student$$matNr, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudentView getAnything(){
        return (view.StudentView)this.anything;
    }
}

class dg21DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg21DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg21View getAnything(){
        return (view.dg21View)this.anything;
    }
}

class dg22DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg22DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg22View getAnything(){
        return (view.dg22View)this.anything;
    }
}

class GradeCachedState_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_SL$$grade = "State_SL$$grade";
    
    protected GradeCachedState_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.GradeCachedState_SLView getAnything(){
        return (view.GradeCachedState_SLView)this.anything;
    }
}

class CONCAtomicModule_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String CreditPointHavingElement_PL$$initialCP = "CreditPointHavingElement_PL$$initialCP";
    
    protected CONCAtomicModule_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Leistungspunkte", this.getAnything().getInitialCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPointHavingElement_PL$$initialCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCAtomicModule_PLView getAnything(){
        return (view.CONCAtomicModule_PLView)this.anything;
    }
}

class NoStudentGroupDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String StudentGroup$$name = "StudentGroup$$name";
    protected static final String StudentGroup$$studyProgram = "StudentGroup$$studyProgram";
    protected static final String StudentGroup$$students = "StudentGroup$$students";
    protected static final String StudentGroup$$activeState = "StudentGroup$$activeState";
    
    protected NoStudentGroupDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentGroup$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Status", this.getAnything().getActiveState());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentGroup$$activeState, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NoStudentGroupView getAnything(){
        return (view.NoStudentGroupView)this.anything;
    }
}

class CreditPointsManagerDefaultDetailPanel extends DefaultDetailPanel{
    
    protected CreditPointsManagerDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.CreditPointsManagerView getAnything(){
        return (view.CreditPointsManagerView)this.anything;
    }
}

class TripleAtomicModule_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected TripleAtomicModule_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.TripleAtomicModule_SLView getAnything(){
        return (view.TripleAtomicModule_SLView)this.anything;
    }
}

class CONCModuleGroupOrStudyProgram_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    protected static final String ModuleGroupOrStudyProgram_GL$$containees = "ModuleGroupOrStudyProgram_GL$$containees";
    
    protected CONCModuleGroupOrStudyProgram_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleGroupOrStudyProgram_GLView getAnything(){
        return (view.CONCModuleGroupOrStudyProgram_GLView)this.anything;
    }
}

class Unit_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected Unit_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.Unit_GLView getAnything(){
        return (view.Unit_GLView)this.anything;
    }
}

class FalsumDefaultDetailPanel extends DefaultDetailPanel{
    
    protected FalsumDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.FalsumView getAnything(){
        return (view.FalsumView)this.anything;
    }
}

class NoProgram_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    protected static final String ModuleGroupOrStudyProgram_GL$$containees = "ModuleGroupOrStudyProgram_GL$$containees";
    
    protected NoProgram_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NoProgram_GLView getAnything(){
        return (view.NoProgram_GLView)this.anything;
    }
}

class RealGradesDefaultDetailPanel extends DefaultDetailPanel{
    
    protected RealGradesDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.RealGradesView getAnything(){
        return (view.RealGradesView)this.anything;
    }
}

class Unit_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String CreditPointHavingElement_PL$$initialCP = "CreditPointHavingElement_PL$$initialCP";
    
    protected Unit_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Leistungspunkte", this.getAnything().getInitialCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPointHavingElement_PL$$initialCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.Unit_PLView getAnything(){
        return (view.Unit_PLView)this.anything;
    }
}

class NoProgram_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String ModuleGroupOrStudyProgram_PL$$containees = "ModuleGroupOrStudyProgram_PL$$containees";
    
    protected NoProgram_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NoProgram_PLView getAnything(){
        return (view.NoProgram_PLView)this.anything;
    }
}

class NothingCachedState_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_SL$$grade = "State_SL$$grade";
    
    protected NothingCachedState_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.NothingCachedState_SLView getAnything(){
        return (view.NothingCachedState_SLView)this.anything;
    }
}

class CONCState_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_PL$$creditPoints = "State_PL$$creditPoints";
    
    protected CONCState_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.CONCState_PLView getAnything(){
        return (view.CONCState_PLView)this.anything;
    }
}

class NotAtomicModule_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String NotAtomicModule_PL$$units = "NotAtomicModule_PL$$units";
    
    protected NotAtomicModule_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NotAtomicModule_PLView getAnything(){
        return (view.NotAtomicModule_PLView)this.anything;
    }
}

class BinaryAtomicModule_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected BinaryAtomicModule_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.BinaryAtomicModule_SLView getAnything(){
        return (view.BinaryAtomicModule_SLView)this.anything;
    }
}

class StudentWrapperDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String StudentWrapper$$wrappedStudent = "StudentWrapper$$wrappedStudent";
    protected static final String StudentWrapper$$firstName = "StudentWrapper$$firstName";
    protected static final String StudentWrapper$$name = "StudentWrapper$$name";
    protected static final String StudentWrapper$$dateOfBirth = "StudentWrapper$$dateOfBirth";
    protected static final String StudentWrapper$$matNr = "StudentWrapper$$matNr";
    
    protected StudentWrapperDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Vorname", this.getAnything().getFirstName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentWrapper$$firstName, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Nachname", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentWrapper$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new DatePanel(this, "Geburtsdatum", this.getAnything().getDateOfBirth());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentWrapper$$dateOfBirth, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "MatrikelNummer", this.getAnything().getMatNr());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentWrapper$$matNr, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudentWrapperView getAnything(){
        return (view.StudentWrapperView)this.anything;
    }
}

class tg50DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg50DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg50View getAnything(){
        return (view.tg50View)this.anything;
    }
}

class NotYetStudiedDefaultDetailPanel extends DefaultDetailPanel{
    
    protected NotYetStudiedDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.NotYetStudiedView getAnything(){
        return (view.NotYetStudiedView)this.anything;
    }
}

class CONCModule_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    
    protected CONCModule_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModule_PLView getAnything(){
        return (view.CONCModule_PLView)this.anything;
    }
}

class StudentGroupDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String StudentGroup$$name = "StudentGroup$$name";
    protected static final String StudentGroup$$studyProgram = "StudentGroup$$studyProgram";
    protected static final String StudentGroup$$students = "StudentGroup$$students";
    protected static final String StudentGroup$$activeState = "StudentGroup$$activeState";
    
    protected StudentGroupDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentGroup$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Status", this.getAnything().getActiveState());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(StudentGroup$$activeState, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudentGroupView getAnything(){
        return (view.StudentGroupView)this.anything;
    }
}

class PassedDefaultDetailPanel extends DefaultDetailPanel{
    
    protected PassedDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.PassedView getAnything(){
        return (view.PassedView)this.anything;
    }
}

class AdminServiceDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String AdminService$$programManager = "AdminService$$programManager";
    protected static final String AdminService$$studentManager = "AdminService$$studentManager";
    
    protected AdminServiceDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.AdminServiceView getAnything(){
        return (view.AdminServiceView)this.anything;
    }
}

class GradeAggregatorDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String GradeAggregator$$elements = "GradeAggregator$$elements";
    
    protected GradeAggregatorDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.GradeAggregatorView getAnything(){
        return (view.GradeAggregatorView)this.anything;
    }
}

class NotAtomicModule_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    protected static final String NotAtomicModule_GL$$units = "NotAtomicModule_GL$$units";
    
    protected NotAtomicModule_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NotAtomicModule_GLView getAnything(){
        return (view.NotAtomicModule_GLView)this.anything;
    }
}

class CONCModuleOrModuleGroup_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    
    protected CONCModuleOrModuleGroup_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroup_PLView getAnything(){
        return (view.CONCModuleOrModuleGroup_PLView)this.anything;
    }
}

class CONCModule_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected CONCModule_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModule_GLView getAnything(){
        return (view.CONCModule_GLView)this.anything;
    }
}

class CONCCreditPointHavingElement_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String CreditPointHavingElement_PL$$initialCP = "CreditPointHavingElement_PL$$initialCP";
    
    protected CONCCreditPointHavingElement_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Leistungspunkte", this.getAnything().getInitialCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPointHavingElement_PL$$initialCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCCreditPointHavingElement_PLView getAnything(){
        return (view.CONCCreditPointHavingElement_PLView)this.anything;
    }
}

class ErrorDisplayDefaultDetailPanel extends DefaultDetailPanel{
    
    protected ErrorDisplayDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.ErrorDisplayView getAnything(){
        return (view.ErrorDisplayView)this.anything;
    }
}

class CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLView getAnything(){
        return (view.CONCModuleOrModuleGroupOrStudyProgramOrUnit_SLView)this.anything;
    }
}

class CONCAtomicModule_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected CONCAtomicModule_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCAtomicModule_SLView getAnything(){
        return (view.CONCAtomicModule_SLView)this.anything;
    }
}

class CONCModuleOrModuleGroup_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected CONCModuleOrModuleGroup_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroup_GLView getAnything(){
        return (view.CONCModuleOrModuleGroup_GLView)this.anything;
    }
}

class CONCCreditPointHavingElement_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected CONCCreditPointHavingElement_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCCreditPointHavingElement_GLView getAnything(){
        return (view.CONCCreditPointHavingElement_GLView)this.anything;
    }
}

class ProgramManagerDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ProgramManager$$programs = "ProgramManager$$programs";
    
    protected ProgramManagerDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.ProgramManagerView getAnything(){
        return (view.ProgramManagerView)this.anything;
    }
}

class TripleAtomicModule_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String CreditPointHavingElement_PL$$initialCP = "CreditPointHavingElement_PL$$initialCP";
    
    protected TripleAtomicModule_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Leistungspunkte", this.getAnything().getInitialCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPointHavingElement_PL$$initialCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.TripleAtomicModule_PLView getAnything(){
        return (view.TripleAtomicModule_PLView)this.anything;
    }
}

class CONCModuleGroupOrStudyProgram_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    protected static final String ModuleGroupOrStudyProgram_SL$$containees = "ModuleGroupOrStudyProgram_SL$$containees";
    
    protected CONCModuleGroupOrStudyProgram_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleGroupOrStudyProgram_SLView getAnything(){
        return (view.CONCModuleGroupOrStudyProgram_SLView)this.anything;
    }
}

class StudentServiceDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String StudentService$$theStudent = "StudentService$$theStudent";
    
    protected StudentServiceDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.StudentServiceView getAnything(){
        return (view.StudentServiceView)this.anything;
    }
}

class VerumDefaultDetailPanel extends DefaultDetailPanel{
    
    protected VerumDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.VerumView getAnything(){
        return (view.VerumView)this.anything;
    }
}

class ModuleGroup_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    protected static final String ModuleGroupOrStudyProgram_SL$$containees = "ModuleGroupOrStudyProgram_SL$$containees";
    
    protected ModuleGroup_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.ModuleGroup_SLView getAnything(){
        return (view.ModuleGroup_SLView)this.anything;
    }
}

class ActiveDefaultDetailPanel extends DefaultDetailPanel{
    
    protected ActiveDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.ActiveView getAnything(){
        return (view.ActiveView)this.anything;
    }
}

class TripleAtomicModule_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected TripleAtomicModule_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.TripleAtomicModule_GLView getAnything(){
        return (view.TripleAtomicModule_GLView)this.anything;
    }
}

class StudyProgram_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    protected static final String ModuleGroupOrStudyProgram_SL$$containees = "ModuleGroupOrStudyProgram_SL$$containees";
    
    protected StudyProgram_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudyProgram_SLView getAnything(){
        return (view.StudyProgram_SLView)this.anything;
    }
}

class NoProgram_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    protected static final String ModuleGroupOrStudyProgram_SL$$containees = "ModuleGroupOrStudyProgram_SL$$containees";
    
    protected NoProgram_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.NoProgram_SLView getAnything(){
        return (view.NoProgram_SLView)this.anything;
    }
}

class tg23DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg23DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg23View getAnything(){
        return (view.tg23View)this.anything;
    }
}

class dg18DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg18DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg18View getAnything(){
        return (view.dg18View)this.anything;
    }
}

class dg19DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg19DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg19View getAnything(){
        return (view.dg19View)this.anything;
    }
}

class dg16DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg16DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg16View getAnything(){
        return (view.dg16View)this.anything;
    }
}

class tg20DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg20DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg20View getAnything(){
        return (view.tg20View)this.anything;
    }
}

class dg14DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg14DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg14View getAnything(){
        return (view.dg14View)this.anything;
    }
}

class CONCGradeDefaultDetailPanel extends DefaultDetailPanel{
    
    protected CONCGradeDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.CONCGradeView getAnything(){
        return (view.CONCGradeView)this.anything;
    }
}

class dg15DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg15DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg15View getAnything(){
        return (view.dg15View)this.anything;
    }
}

class dg12DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg12DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg12View getAnything(){
        return (view.dg12View)this.anything;
    }
}

class BinaryAtomicModule_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String CreditPointHavingElement_PL$$initialCP = "CreditPointHavingElement_PL$$initialCP";
    
    protected BinaryAtomicModule_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Leistungspunkte", this.getAnything().getInitialCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPointHavingElement_PL$$initialCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.BinaryAtomicModule_PLView getAnything(){
        return (view.BinaryAtomicModule_PLView)this.anything;
    }
}

class StudyProgram_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    protected static final String ModuleGroupOrStudyProgram_GL$$containees = "ModuleGroupOrStudyProgram_GL$$containees";
    
    protected StudyProgram_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudyProgram_GLView getAnything(){
        return (view.StudyProgram_GLView)this.anything;
    }
}

class dg11DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected dg11DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.dg11View getAnything(){
        return (view.dg11View)this.anything;
    }
}

class tg27DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg27DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg27View getAnything(){
        return (view.tg27View)this.anything;
    }
}

class CreditPointsCachedState_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_PL$$creditPoints = "State_PL$$creditPoints";
    
    protected CreditPointsCachedState_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.CreditPointsCachedState_PLView getAnything(){
        return (view.CreditPointsCachedState_PLView)this.anything;
    }
}

class CONCState_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_SL$$grade = "State_SL$$grade";
    
    protected CONCState_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.CONCState_SLView getAnything(){
        return (view.CONCState_SLView)this.anything;
    }
}

class CreditPointsDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CreditPoints$$value = "CreditPoints$$value";
    
    protected CreditPointsDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CreditPoints$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CreditPointsView getAnything(){
        return (view.CreditPointsView)this.anything;
    }
}

class GradeHistoryElementDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String GradeHistoryElement$$comment = "GradeHistoryElement$$comment";
    protected static final String GradeHistoryElement$$timestamp = "GradeHistoryElement$$timestamp";
    
    protected GradeHistoryElementDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "comment", this.getAnything().getComment());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(GradeHistoryElement$$comment, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new TimestampPanel(this, "timestamp", this.getAnything().getTimestamp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(GradeHistoryElement$$timestamp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.GradeHistoryElementView getAnything(){
        return (view.GradeHistoryElementView)this.anything;
    }
}

class NoGradeDefaultDetailPanel extends DefaultDetailPanel{
    
    protected NoGradeDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.NoGradeView getAnything(){
        return (view.NoGradeView)this.anything;
    }
}

class NotPassedDefaultDetailPanel extends DefaultDetailPanel{
    
    protected NotPassedDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.NotPassedView getAnything(){
        return (view.NotPassedView)this.anything;
    }
}

class CONCCalculatableGradeDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected CONCCalculatableGradeDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCCalculatableGradeView getAnything(){
        return (view.CONCCalculatableGradeView)this.anything;
    }
}

class BinaryAtomicModule_GLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name";
    
    protected BinaryAtomicModule_GLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_GL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.BinaryAtomicModule_GLView getAnything(){
        return (view.BinaryAtomicModule_GLView)this.anything;
    }
}

class tg13DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg13DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg13View getAnything(){
        return (view.tg13View)this.anything;
    }
}

class tg10DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg10DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg10View getAnything(){
        return (view.tg10View)this.anything;
    }
}

class StudyProgram_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name";
    protected static final String ModuleGroupOrStudyProgram_PL$$containees = "ModuleGroupOrStudyProgram_PL$$containees";
    
    protected StudyProgram_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_PL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.StudyProgram_PLView getAnything(){
        return (view.StudyProgram_PLView)this.anything;
    }
}

class CONCModuleOrModuleGroup_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected CONCModuleOrModuleGroup_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCModuleOrModuleGroup_SLView getAnything(){
        return (view.CONCModuleOrModuleGroup_SLView)this.anything;
    }
}

class CONCDecimalGradeDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected CONCDecimalGradeDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.CONCDecimalGradeView getAnything(){
        return (view.CONCDecimalGradeView)this.anything;
    }
}

class tg17DefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String CalculatableGrade$$value = "CalculatableGrade$$value";
    
    protected tg17DefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new FractionPanel(this, "value", this.getAnything().getValue());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(CalculatableGrade$$value, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.tg17View getAnything(){
        return (view.tg17View)this.anything;
    }
}

class Unit_SLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name";
    protected static final String ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp = "ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp";
    
    protected Unit_SLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichte Leistungspunkte", this.getAnything().getAlreadyEarnedCP());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$alreadyEarnedCP, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Name", this.getAnything().getName());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$name, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        try{
            BaseTypePanel panel = new StringPanel(this, "Erreichbare Leistungspunkte", this.getAnything().getCp());
            this.getScrollablePane().getChildren().add(panel);
            this.panels.put(ModuleOrModuleGroupOrStudyProgramOrUnit_SL$$cp, panel);
        }catch(ModelException e){
            this.getExceptionAndEventhandler().handleException(e);
        }
        
    }
    protected view.Unit_SLView getAnything(){
        return (view.Unit_SLView)this.anything;
    }
}

class NothingCachedState_PLDefaultDetailPanel extends DefaultDetailPanel{
    
    protected static final String State_PL$$creditPoints = "State_PL$$creditPoints";
    
    protected NothingCachedState_PLDefaultDetailPanel(ExceptionAndEventHandler exceptionHandler, Anything anything) {
        super(exceptionHandler, anything);
    }
    protected void addFields(){
        
    }
    protected view.NothingCachedState_PLView getAnything(){
        return (view.NothingCachedState_PLView)this.anything;
    }
}
