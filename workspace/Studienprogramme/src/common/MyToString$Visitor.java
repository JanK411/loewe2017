package common;

import persistence.*;

public abstract class MyToString$Visitor extends model.visitor.ToString$Visitor {

    protected String result;

    public synchronized String toString(Anything anything) throws PersistenceException {
        try {
            result = null;
            anything.accept(this);
            if (result == null) {
                this.standardHandling(anything);
            } else {
                if (common.RPCConstantsAndServices.test)
                    result = " " + anything.getClassId() + " , " + anything.getId() + "::  " + result;
            }
        } catch (Exception ex) {
            System.out.println("During toString of " + anything.getClass() + " happened " + ex);
            this.standardHandling(anything);
        }
        return result;
    }

    protected abstract void handleGrade(CalculatableGrade4Public grade) throws PersistenceException;

    protected abstract void handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(ModuleOrModuleGroupOrStudyProgramOrUnit_PL4Public element) throws PersistenceException;

    @Override
    public void handleNotAtomicModule_PL(NotAtomicModule_PL4Public notAtomicModule_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(notAtomicModule_PL);
    }

    @Override
    public void handleModuleGroup_PL(ModuleGroup_PL4Public moduleGroup_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(moduleGroup_PL);
    }

    @Override
    public void handleUnit_PL(Unit_PL4Public unit_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(unit_PL);
    }

    @Override
    public void handleNoProgram_PL(NoProgram_PL4Public noProgram_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(noProgram_PL);
    }

    @Override
    public void handleBinaryAtomicModule_PL(BinaryAtomicModule_PL4Public binaryAtomicModule_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(binaryAtomicModule_PL);
    }

    @Override
    public void handleTripleAtomicModule_PL(TripleAtomicModule_PL4Public tripleAtomicModule_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(tripleAtomicModule_PL);
    }

    @Override
    public void handleStudyProgram_PL(StudyProgram_PL4Public studyProgram_PL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_PL(studyProgram_PL);
    }

    protected abstract void handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(ModuleOrModuleGroupOrStudyProgramOrUnit_GL4Public element) throws PersistenceException;

    @Override
    public void handleNotAtomicModule_GL(NotAtomicModule_GL4Public notAtomicModule_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(notAtomicModule_GL);
    }

    @Override
    public void handleModuleGroup_GL(ModuleGroup_GL4Public moduleGroup_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(moduleGroup_GL);
    }

    @Override
    public void handleUnit_GL(Unit_GL4Public unit_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(unit_GL);
    }

    @Override
    public void handleNoProgram_GL(NoProgram_GL4Public noProgram_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(noProgram_GL);
    }

    @Override
    public void handleBinaryAtomicModule_GL(BinaryAtomicModule_GL4Public binaryAtomicModule_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(binaryAtomicModule_GL);
    }

    @Override
    public void handleTripleAtomicModule_GL(TripleAtomicModule_GL4Public tripleAtomicModule_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(tripleAtomicModule_GL);
    }

    @Override
    public void handleStudyProgram_GL(StudyProgram_GL4Public studyProgram_GL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_GL(studyProgram_GL);
    }

    protected abstract void handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(ModuleOrModuleGroupOrStudyProgramOrUnit_SL4Public element) throws PersistenceException;

    @Override
    public void handleNotAtomicModule_SL(NotAtomicModule_SL4Public notAtomicModule_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(notAtomicModule_SL);
    }

    @Override
    public void handleModuleGroup_SL(ModuleGroup_SL4Public moduleGroup_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(moduleGroup_SL);
    }

    @Override
    public void handleUnit_SL(Unit_SL4Public unit_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(unit_SL);
    }

    @Override
    public void handleNoProgram_SL(NoProgram_SL4Public noProgram_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(noProgram_SL);
    }

    @Override
    public void handleBinaryAtomicModule_SL(BinaryAtomicModule_SL4Public binaryAtomicModule_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(binaryAtomicModule_SL);
    }

    @Override
    public void handleTripleAtomicModule_SL(TripleAtomicModule_SL4Public tripleAtomicModule_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(tripleAtomicModule_SL);
    }

    @Override
    public void handleStudyProgram_SL(StudyProgram_SL4Public studyProgram_SL) throws PersistenceException {
        handleModuleOrModuleGroupOrStudyProgramOrUnit_SL(studyProgram_SL);
    }

    @Override
    public void handledg38(dg384Public dg38) throws PersistenceException {
        handleGrade(dg38);
    }

    @Override
    public void handledg39(dg394Public dg39) throws PersistenceException {
        handleGrade(dg39);
    }

    @Override
    public void handledg36(dg364Public dg36) throws PersistenceException {
        handleGrade(dg36);
    }


    @Override
    public void handledg34(dg344Public dg34) throws PersistenceException {
        handleGrade(dg34);
    }

    @Override
    public void handledg35(dg354Public dg35) throws PersistenceException {
        handleGrade(dg35);
    }

    @Override
    public void handledg32(dg324Public dg32) throws PersistenceException {
        handleGrade(dg32);
    }

    @Override
    public void handledg31(dg314Public dg31) throws PersistenceException {
        handleGrade(dg31);
    }

    @Override
    public void handledg28(dg284Public dg28) throws PersistenceException {
        handleGrade(dg28);
    }

    @Override
    public void handledg25(dg254Public dg25) throws PersistenceException {
        handleGrade(dg25);
    }

    @Override
    public void handledg26(dg264Public dg26) throws PersistenceException {
        handleGrade(dg26);
    }

    @Override
    public void handletg23(tg234Public tg23) throws PersistenceException {
        handleGrade(tg23);
    }

    @Override
    public void handledg18(dg184Public dg18) throws PersistenceException {
        handleGrade(dg18);
    }

    @Override
    public void handledg19(dg194Public dg19) throws PersistenceException {
        handleGrade(dg19);
    }

    @Override
    public void handledg16(dg164Public dg16) throws PersistenceException {
        handleGrade(dg16);
    }

    @Override
    public void handletg20(tg204Public tg20) throws PersistenceException {
        handleGrade(tg20);
    }


    @Override
    public void handledg14(dg144Public dg14) throws PersistenceException {
        handleGrade(dg14);
    }

    @Override
    public void handledg15(dg154Public dg15) throws PersistenceException {
        handleGrade(dg15);
    }

    @Override
    public void handledg12(dg124Public dg12) throws PersistenceException {
        handleGrade(dg12);
    }


    @Override
    public void handletg17(tg174Public tg17) throws PersistenceException {
        handleGrade(tg17);
    }


    @Override
    public void handledg11(dg114Public dg11) throws PersistenceException {
        handleGrade(dg11);
    }


    @Override
    public void handledg21(dg214Public dg21) throws PersistenceException {
        handleGrade(dg21);
    }

    @Override
    public void handledg22(dg224Public dg22) throws PersistenceException {
        handleGrade(dg22);
    }

    @Override
    public void handledg24(dg244Public dg24) throws PersistenceException {
        handleGrade(dg24);
    }


    @Override
    public void handledg29(dg294Public dg29) throws PersistenceException {
        handleGrade(dg29);
    }

    @Override
    public void handletg10(tg104Public tg10) throws PersistenceException {
        handleGrade(tg10);
    }

    @Override
    public void handletg13(tg134Public tg13) throws PersistenceException {
        handleGrade(tg13);
    }

    @Override
    public void handletg27(tg274Public tg27) throws PersistenceException {
        handleGrade(tg27);
    }

    @Override
    public void handletg30(tg304Public tg30) throws PersistenceException {
        handleGrade(tg30
        );
    }

    @Override
    public void handletg33(tg334Public tg33) throws PersistenceException {
        handleGrade(tg33);
    }

    @Override
    public void handletg37(tg374Public tg37) throws PersistenceException {
        handleGrade(tg37);
    }

    @Override
    public void handletg40(tg404Public tg40) throws PersistenceException {
        handleGrade(tg40);
    }

    @Override
    public void handletg50(tg504Public tg50) throws PersistenceException {
        handleGrade(tg50);
    }
}
