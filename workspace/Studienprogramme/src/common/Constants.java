package common;

public abstract class Constants {

    //ICON NUMBERS
    public static final int ModuleGroupIconNumber = 1;
    public static final int StudentIconNumber = 2;
    public static final int StudentGroupIconNumber = 3;
    public static final int ManagerIconNumber = 5;
    public static final int StudyProgrammIconNumber = 8;
    public static final int UnitIconNumber = 10;
    public static final int NotAtomicModuleIconNumber = 11;
    public static final int AtomicModuleIconNumber = 12;
    public static final int CreditPointIconNumber = 13;
    public static final int StudentGroup_NotYetStudiedIconNumber = 14;
    public static final int StudentGroup_FinishedIconNumber = 15;
    public static final int ModuleGroupEditableIconNumber = 16;
    public static final int StudyProgrammEditableIconNumber = 17;
    public static final int NotAtomicModuleEditableIconNumber = 18;
    public static final int AtomicModuleEditableIconNumber = 19;
    public static final int UnitEditableIconNumber = 20;
    public static final int ErrorDisplayIconNumber = 21;
    public static final int GradeHistoryIconNumber = 22;
    public static final int GradeHistoryElementIconNumber = 23;


    private Constants() {}
}
