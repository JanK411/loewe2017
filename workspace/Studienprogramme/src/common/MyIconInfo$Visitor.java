package common;

import persistence.*;

public abstract class MyIconInfo$Visitor extends model.visitor.AnythingStandardVisitor {
    protected int result = 0;

    public int getIconInfo(Anything anything) throws PersistenceException {
        anything.accept(this);
        return result;
    }

    @Override
    protected void standardHandling(Anything anything) throws PersistenceException {
        result = 0;
    }

    protected abstract void handleManager(Anything manager) throws PersistenceException;

    @Override
    public void handleProgramManager(ProgramManager4Public programManager) throws PersistenceException {
        handleManager(programManager);
    }

    @Override
    public void handleStudentManager(StudentManager4Public studentManager) throws PersistenceException {
        handleManager(studentManager);
    }
}
