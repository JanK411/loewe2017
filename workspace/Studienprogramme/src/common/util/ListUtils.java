package common.util;

import persistence.*;

import java.util.Arrays;

public class ListUtils {

    public static StudentWrapperSearchList toList(StudentWrapper4Public... things) {
        return new StudentWrapperSearchList() {{
            Arrays.stream(things).forEach(this::add);
        }};
    }

    public static ModuleOrModuleGroup_PLSearchList toList(ModuleOrModuleGroup_PL4Public... things) {
        return new ModuleOrModuleGroup_PLSearchList() {{
            Arrays.stream(things).forEach(this::add);
        }};
    }
}
