package common.util;

import java.util.SortedSet;
import java.util.TreeSet;

import common.Fraction;

public class NotenFractions {
	// Difference
	public static final Fraction f03 = Fraction.parse("0,3");
	public static final Fraction f01 = Fraction.parse("0,1");

	// Drittelnoten
	public static final Fraction f50 = Fraction.parse("5,0");
	public static final Fraction f40 = Fraction.parse("4,0");
	public static final Fraction f37 = Fraction.parse("3,7");
	public static final Fraction f33 = Fraction.parse("3,3");
	public static final Fraction f30 = Fraction.parse("3,0");
	public static final Fraction f27 = Fraction.parse("2,7");
	public static final Fraction f23 = Fraction.parse("2,3");
	public static final Fraction f20 = Fraction.parse("2,0");
	public static final Fraction f17 = Fraction.parse("1,7");
	public static final Fraction f13 = Fraction.parse("1,3");
	public static final Fraction f10 = Fraction.One;
	// Restliche Zehntelnoten
	public static final Fraction f39 = Fraction.parse("3,9");
	public static final Fraction f38 = Fraction.parse("3,8");
	public static final Fraction f36 = Fraction.parse("3,6");
	public static final Fraction f35 = Fraction.parse("3,5");
	public static final Fraction f34 = Fraction.parse("3,4");
	public static final Fraction f32 = Fraction.parse("3,2");
	public static final Fraction f31 = Fraction.parse("3,1");
	public static final Fraction f29 = Fraction.parse("2,9");
	public static final Fraction f28 = Fraction.parse("2,8");
	public static final Fraction f26 = Fraction.parse("2,6");
	public static final Fraction f25 = Fraction.parse("2,5");
	public static final Fraction f24 = Fraction.parse("2,4");
	public static final Fraction f22 = Fraction.parse("2,2");
	public static final Fraction f21 = Fraction.parse("2,1");
	public static final Fraction f19 = Fraction.parse("1,9");
	public static final Fraction f18 = Fraction.parse("1,8");
	public static final Fraction f16 = Fraction.parse("1,6");
	public static final Fraction f15 = Fraction.parse("1,5");
	public static final Fraction f14 = Fraction.parse("1,4");
	public static final Fraction f12 = Fraction.parse("1,2");
	public static final Fraction f11 = Fraction.parse("1,1");

	public static SortedSet<Fraction> getAllTripleGrades() {
		SortedSet<Fraction> drittelNoten = new TreeSet<>();
		drittelNoten.add(f50);
		drittelNoten.add(f40);
		drittelNoten.add(f37);
		drittelNoten.add(f33);
		drittelNoten.add(f30);
		drittelNoten.add(f27);
		drittelNoten.add(f23);
		drittelNoten.add(f20);
		drittelNoten.add(f17);
		drittelNoten.add(f13);
		drittelNoten.add(f10);
		return drittelNoten;
	}

	public static SortedSet<Fraction> getAllDecimalGrades() {
		SortedSet<Fraction> zehntelNoten = new TreeSet<>();

		zehntelNoten.addAll(getAllTripleGrades());

		zehntelNoten.add(f39);
		zehntelNoten.add(f38);
		zehntelNoten.add(f36);
		zehntelNoten.add(f35);
		zehntelNoten.add(f34);
		zehntelNoten.add(f32);
		zehntelNoten.add(f31);
		zehntelNoten.add(f29);
		zehntelNoten.add(f28);
		zehntelNoten.add(f26);
		zehntelNoten.add(f25);
		zehntelNoten.add(f24);
		zehntelNoten.add(f22);
		zehntelNoten.add(f21);
		zehntelNoten.add(f19);
		zehntelNoten.add(f18);
		zehntelNoten.add(f16);
		zehntelNoten.add(f15);
		zehntelNoten.add(f14);
		zehntelNoten.add(f12);
		zehntelNoten.add(f11);

		return zehntelNoten;
	}

}
