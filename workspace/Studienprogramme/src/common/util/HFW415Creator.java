package common.util;

import common.Fraction;
import model.*;
import model.meta.StringFACTORY;
import persistence.*;

import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;

import static common.util.ListUtils.toList;

public class HFW415Creator {
    // Literale für allgemein
    public static final String DrittelNoten = StringFACTORY.GradeFactory$FACTORY$RealGrades;

    // Literale für Grundlagen und ueberfachliche Qualifikationen
    public static final String ModuleGroupNameGrundlagen = "Grundlagen und ueberfachliche Qualifikationen";
    public static final String ModuleNameSoko = "Grundlagen sozialer Kompetenz";
    public static final String UnitNameSoko1 = "Praesentationen und Gespraechsfuehrung";
    public static final String UnitNameSoko2 = "Arbeiten im Team";
    public static final String ModuleNameEnglisch1 = "Wirtschaftsenglisch 1";
    public static final String ModuleNameEnglisch2 = "Wirtschaftsenglisch 2";
    public static final String UnitNameEnglisch1_1 = "Introduction to the language of business";
    public static final String UnitNameEnglisch1_2 = "Correspondence";
    public static final String UnitNameEnglisch2_1 = "Meetings";
    public static final String UnitNameEnglisch2_2 = "Presentations";
    public static final String ModuleNameWirtschaftsrecht = "Wirtschaftsrecht";
    public static final String UnitNameWirtschaftsrecht1 = "Grundlagen des Rechts und Zivilrecht";
    public static final String UnitNameWirtschaftsrecht2 = "Handels-, Gesellschafts- und Arbeitsrecht";
    public static final String ModuleNameMathematik1 = "Mathematik 1";
    public static final String ModuleNameMathematik2 = "Mathematik 2";
    public static final String ModuleNameStatistik = "Statistik";
    // Literale für praktische Informatik
    public static final String ModuleGroupNameInformatik = "Praktische Informatik";
    public static final String ModuleNameDB1 = "Datenbanken 1";
    public static final String ModuleNameDB2 = "Datenbanken 2";
    public static final String UnitNameDB1_1 = "Information Modeling and Retrieval";
    public static final String UnitNameDB1_2 = "Database Programming";
    public static final String UnitNameDB2_1 = "Transaction Processing";
    public static final String UnitNameDB2_2 = "Business Intelligence";
    public static final String ModuleNameProgrammieren = "Methodisches Programmieren";
    public static final String ModuleNameSWT1 = "Objektorientierte Softwaretechnik 1";
    public static final String ModuleNameSWT2 = "Objektorientierte Softwaretechnik 2";
    // Literale für BWL
    public static final String ModuleGroupNameBWL = "Betriebswirtschaft";
    public static final String ModuleNameBWL1 = "Allgemeine Betrieswirtschaftslehre 1";
    public static final String ModuleNameBWL2 = "Allgemeine Betrieswirtschaftslehre 2";
    public static final String ModuleNameRW1 = "Rechnungswesen 1";
    public static final String ModuleNameRW2 = "Rechnungswesen 2";
    public static final String ModuleNameVWL = "Volkswirtschaftslehre";
    public static final String ModuleNameControlling = "Controlling";
    public static final String UnitNameBWL1_1 = "Entscheidungslehre und betriebliche Finanzierung";
    public static final String UnitNameBWL1_2 = "Absatzwirtschaft";
    public static final String UnitNameBWL2_1 = "Organisation und Personal";
    public static final String UnitNameBWL2_2 = "Produktion und Materialwirtschaft";
    public static final String UnitNameRW1_1 = "Buchfuehrung";
    public static final String UnitNameRW1_2 = "Kostenrechnung";
    public static final String UnitNameRW2_1 = "Jahresabschluss nach HGB";
    public static final String UnitNameRW2_2 = "Jahresabschluss nach IFRS";
    public static final String UnitNameVWL1 = "Grundlagen der Volkswirtschaftslehre";
    public static final String UnitNameVWL2 = "Makrooekonomie";
    public static final String UnitNameControlling1 = "Operatives Controlling";
    public static final String UnitNameControlling2 = "Strategisches Controlling";
    // Literale für Wirtschaftsinformatik
    public static final String ModuleGroupNameWINF = "Wirtschaftsinformatik";
    public static final String ModuleNameIIS = "Informationsinfrastrukturen";
    public static final String UnitNameIIS1 = "System-Architekturen und -Dienste";
    public static final String UnitNameIIS2 = "Standardkomponenten betrieblicher Anwendungssysteme";
    public static final String ModuleNameBE = "Grundlagen des Business Engineering";
    public static final String UnitNameBE1 = "Service Oriented Architectures";
    public static final String UnitNameBE2 = "Semantic Business Process Management";
    // Konstanten für die CP Fraction values
    private static final Fraction Fraction2_5 = new Fraction(BigInteger.valueOf(5), BigInteger.valueOf(2));
    private static final Fraction Fraction3 = new Fraction(BigInteger.valueOf(3), BigInteger.valueOf(1));
    private static final Fraction Fraction3_5 = new Fraction(BigInteger.valueOf(7), BigInteger.valueOf(2));
    private static final Fraction Fraction4 = new Fraction(BigInteger.valueOf(4), BigInteger.valueOf(1));
    private static final Fraction Fraction4_5 = new Fraction(BigInteger.valueOf(9), BigInteger.valueOf(2));
    private static final Fraction Fraction5 = new Fraction(BigInteger.valueOf(5), BigInteger.ONE);
    private static final Fraction Fraction7 = new Fraction(BigInteger.valueOf(7), BigInteger.ONE);
    private static final Fraction Fraction8 = new Fraction(BigInteger.valueOf(8), BigInteger.ONE);
    private static final Fraction Fraction9 = new Fraction(BigInteger.valueOf(9), BigInteger.ONE);
    private static final Fraction Fraction11 = new Fraction(BigInteger.valueOf(11), BigInteger.ONE);
    // Student Names
    private static final String Kristina = "Kristina";
    private static final String Gaar = "Gaar";
    private static final String Jan = "Jan";
    private static final String Koeper = "Koeper";
    private static final String Tim = "Tim";
    private static final String Kraeuter = "Kraeuter";
    private static final String Jeremy = "Jeremy";
    private static final String Ziegler = "Ziegler";

    public static ModuleGroup_PL4Public createGrundlagen(AdminService4Public adminService, StudyProgram_PL4Public program) throws PersistenceException, UserException {
        ProgramManager4Public programManager = adminService.getProgramManager();

        ModuleGroup_PL4Public grundlagen_und_ueberfachliche_qualifikationen = programManager.createModuleGroup(program, ModuleGroupNameGrundlagen);
        NotAtomicModule_PL4Public grundlagen_sozialer_kompetenz = programManager.createModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameSoko);
        NotAtomicModule_PL4Public englisch1 = programManager.createModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameEnglisch1);
        NotAtomicModule_PL4Public englisch2 = programManager.createModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameEnglisch2);
        NotAtomicModule_PL4Public wirtschaftsRecht = programManager.createModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameWirtschaftsrecht);
        AtomicModule_PL4Public math1 = programManager.createAtomicModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameMathematik1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction8), DrittelNoten);
        AtomicModule_PL4Public math2 = programManager.createAtomicModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameMathematik2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction7), DrittelNoten);
        AtomicModule_PL4Public statistik = programManager.createAtomicModule(grundlagen_und_ueberfachliche_qualifikationen, ModuleNameStatistik, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction5), DrittelNoten);
        programManager.createUnit(grundlagen_sozialer_kompetenz, UnitNameSoko1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(grundlagen_sozialer_kompetenz, UnitNameSoko2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(englisch1, UnitNameEnglisch1_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(englisch1, UnitNameEnglisch1_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(englisch2, UnitNameEnglisch2_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(englisch2, UnitNameEnglisch2_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction2_5));
        programManager.createUnit(wirtschaftsRecht, UnitNameWirtschaftsrecht1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        programManager.createUnit(wirtschaftsRecht, UnitNameWirtschaftsrecht2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        return grundlagen_und_ueberfachliche_qualifikationen;
    }

    public static ModuleGroup_PL4Public createPraktischeInformatik(AdminService4Public adminService, StudyProgram_PL4Public program) throws UserException, PersistenceException {
        ProgramManager4Public programManager = adminService.getProgramManager();

        ModuleGroup_PL4Public praktische_informatik = programManager.createModuleGroup(program, ModuleGroupNameInformatik);
        NotAtomicModule_PL4Public db1 = programManager.createModule(praktische_informatik, ModuleNameDB1);
        NotAtomicModule_PL4Public db2 = programManager.createModule(praktische_informatik, ModuleNameDB2);
        AtomicModule_PL4Public programming = programManager.createAtomicModule(praktische_informatik, ModuleNameProgrammieren, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction11), DrittelNoten);
        AtomicModule_PL4Public swt1 = programManager.createAtomicModule(praktische_informatik, ModuleNameSWT1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction7), DrittelNoten);
        AtomicModule_PL4Public swt2 = programManager.createAtomicModule(praktische_informatik, ModuleNameSWT2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction9), DrittelNoten);
        programManager.createUnit(db1, UnitNameDB1_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4));
        programManager.createUnit(db1, UnitNameDB1_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4));
        programManager.createUnit(db2, UnitNameDB2_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        programManager.createUnit(db2, UnitNameDB2_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        return praktische_informatik;
    }

    public static ModuleGroup_PL4Public createBetriebswirtschaft(AdminService4Public adminService, StudyProgram_PL4Public program) throws PersistenceException, UserException {
        ProgramManager4Public programManager = adminService.getProgramManager();

        ModuleGroup_PL4Public betriebswirtschaft = programManager.createModuleGroup(program, ModuleGroupNameBWL);
        NotAtomicModule_PL4Public bwl1 = programManager.createModule(betriebswirtschaft, ModuleNameBWL1);
        NotAtomicModule_PL4Public bwl2 = programManager.createModule(betriebswirtschaft, ModuleNameBWL2);
        NotAtomicModule_PL4Public rw1 = programManager.createModule(betriebswirtschaft, ModuleNameRW1);
        NotAtomicModule_PL4Public rw2 = programManager.createModule(betriebswirtschaft, ModuleNameRW2);
        NotAtomicModule_PL4Public vwl = programManager.createModule(betriebswirtschaft, ModuleNameVWL);
        NotAtomicModule_PL4Public controlling = programManager.createModule(betriebswirtschaft, ModuleNameControlling);
        programManager.createUnit(bwl1, UnitNameBWL1_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(bwl1, UnitNameBWL1_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(bwl2, UnitNameBWL2_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        programManager.createUnit(bwl2, UnitNameBWL2_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3));
        programManager.createUnit(rw1, UnitNameRW1_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(rw1, UnitNameRW1_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(rw2, UnitNameRW2_1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(rw2, UnitNameRW2_2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(vwl, UnitNameVWL1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(vwl, UnitNameVWL2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction3_5));
        programManager.createUnit(controlling, UnitNameControlling1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4));
        programManager.createUnit(controlling, UnitNameControlling2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4));
        return betriebswirtschaft;
    }

    // Keine Integrationsprojekte und Praxisarbeiten
    public static ModuleGroup_PL4Public createWirtschaftsInformatik(AdminService4Public adminService, StudyProgram_PL4Public program) throws PersistenceException, UserException {
        ProgramManager4Public programManager = adminService.getProgramManager();

        ModuleGroup_PL4Public wirtschaftsinformatik = programManager.createModuleGroup(program, ModuleGroupNameWINF);
        NotAtomicModule_PL4Public iis = programManager.createModule(wirtschaftsinformatik, ModuleNameIIS);
        NotAtomicModule_PL4Public be = programManager.createModule(wirtschaftsinformatik, ModuleNameBE);
        programManager.createUnit(iis, UnitNameIIS1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4_5));
        programManager.createUnit(iis, UnitNameIIS2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4_5));
        programManager.createUnit(be, UnitNameBE1, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4_5));
        programManager.createUnit(be, UnitNameBE2, CreditPointsManager.getTheCreditPointsManager().getCP(Fraction4_5));
        return wirtschaftsinformatik;
    }

    public static void addDevStudentsToGroup(AdminService4Public adminService, StudentGroup4Public group) throws PersistenceException, AlreadyInListException, StateException {
        StudentWrapperSearchList list = new StudentWrapperSearchList() {{
            add(Student.createStudent(Kristina, Gaar, Date.valueOf(LocalDate.now())).wrap());
            add(Student.createStudent(Jan, Koeper, Date.valueOf(LocalDate.now())).wrap());
            add(Student.createStudent(Tim, Kraeuter, Date.valueOf(LocalDate.now())).wrap());
            add(Student.createStudent(Jeremy, Ziegler, Date.valueOf(LocalDate.now())).wrap());
        }};
        adminService.getStudentManager().addStudentsToGroup(group, list);
    }
}
