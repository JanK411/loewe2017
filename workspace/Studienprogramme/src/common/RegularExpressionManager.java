package common;

import expressions.RegularExpressionHandler;

public class RegularExpressionManager {

    public static RegularExpressionHandler binaryGradeSUBTYPEName = new RegularExpressionHandler( "[(bestanden)" + 
		"(nicht\\ bestanden)]" );
    public static RegularExpressionHandler decimalGradeSUBTYPEName = new RegularExpressionHandler( "[(2.3)" + 
		"(1.8)" + 
		"(1.9)" + 
		"(3.8)" + 
		"(1.6)" + 
		"(3.9)" + 
		"(2.0)" + 
		"(3.6)" + 
		"(1.4)" + 
		"(1.5)" + 
		"(3.4)" + 
		"(1.2)" + 
		"(3.5)" + 
		"(3.2)" + 
		"(1.1)" + 
		"(2.7)" + 
		"(3.1)" + 
		"(4.0)" + 
		"(3.7)" + 
		"(1.3)" + 
		"(1.0)" + 
		"(2.9)" + 
		"(3.3)" + 
		"(3.0)" + 
		"(2.8)" + 
		"(2.5)" + 
		"(2.6)" + 
		"(2.4)" + 
		"(2.1)" + 
		"(2.2)" + 
		"(1.7)" + 
		"(5.0)]" );
    public static RegularExpressionHandler gradeFactorySUBTYPEName = new RegularExpressionHandler( "[(BestandenNichtBestanden)" + 
		"(Drittelnoten)]" );
    public static RegularExpressionHandler tripleGradeSUBTYPEName = new RegularExpressionHandler( "[(3.7)" + 
		"(2.3)" + 
		"(1.3)" + 
		"(1.0)" + 
		"(3.3)" + 
		"(3.0)" + 
		"(2.0)" + 
		"(5.0)" + 
		"(4.0)" + 
		"(2.7)" + 
		"(1.7)]" );
    

}
