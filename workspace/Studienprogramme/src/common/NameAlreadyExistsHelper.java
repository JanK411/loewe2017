package common;

import model.ModuleOrModuleGroupOrStudyProgramOrUnit_PL;
import persistence.ModuleOrModuleGroupOrStudyProgramOrUnit_PLSearchList;
import persistence.PersistenceException;

public class NameAlreadyExistsHelper {

    public static boolean nameAlreadyExists(String name) throws PersistenceException {
        ModuleOrModuleGroupOrStudyProgramOrUnit_PLSearchList result = ModuleOrModuleGroupOrStudyProgramOrUnit_PL.getModuleOrModuleGroupOrStudyProgramOrUnit_PLByName(name);
        if (result.getLength() > 0)
            return true;
        return false;
    }

}
