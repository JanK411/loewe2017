package common;

import model.StudentGroup;
import persistence.ActiveState4Public;
import persistence.Anything;
import persistence.Student4Public;
import persistence.StudentGroup4Public;
import view.CONCState_PLView;

import java.text.MessageFormat;

public class Literals {
    public static final String Note = "  Note: ";
    public static String notAllStudentsHaveEarnedAllCps = "Nicht alle Studenten haben alle Units oder atomaren Module bestanden.";

    public static String IncorrectPW() {
        return "Password incorrect.";
    }

    public static String AlreadyInListMessage(Anything enthaltenes, Anything container) {
        return MessageFormat.format("{0} ist bereits in {1} enthalten.", enthaltenes, container);
    }

    public static String IneditableMessage(Anything ineditableThing) {
        return MessageFormat.format("{0} is ineditable! Es gibt bereits eine Studiengruppe, die {0} studiert.", ineditableThing);
    }

    public static String NameAlreadyExistsMessage(String thingWithDoubledName) {
        return MessageFormat.format("Ein Modul/Unit/Modulgruppe/Studienprogramm mit dem Namen {0} existiert bereits", thingWithDoubledName);
    }

    public static String ActiveStatusMessage(StudentGroup4Public studentGroup, ActiveState4Public activeState) {
        return MessageFormat.format("Status der Studiengruppe {0} erlaubt kein Hinzufuegen: {1}", studentGroup, activeState);
    }

    public static String NotActiveMessage(StudentGroup4Public studentGroup, ActiveState4Public activeState) {
        return MessageFormat.format("Status der Studiengruppe {0} ist nicht aktiv, sondern: {1}. Eine Studiengruppe muss aktiv sein, damit man sie auf fertig studiert setzten kann.", studentGroup, activeState);
    }

    public static String ActiveStudentMessage(Student4Public student) {
        return MessageFormat.format("Student {0} studiert bereits aktiv in einer anderen Studiengruppe.", student);
    }

    public static String PasswordNoMatch() {
        return "Passw�rter stimmen nicht �berein";
    }

    public static String cONCVisitorMessage() {
        return "Es wird im Visitor niemals an das gerufene konkrete Element deligiert, falls doch, sollte hier einiges schief gegangen sein...";
    }
}
